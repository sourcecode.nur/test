package com.prudential.lms.resource.quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruLmsResourceQuizApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruLmsResourceQuizApplication.class, args);
	}
}
