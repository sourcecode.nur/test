package com.prudential.lms.resource.quiz.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.prudential.lms.resource.quiz.mapper.QuestionMapper;
import com.prudential.lms.resource.quiz.mapper.QuizMapper;
import com.prudential.lms.resource.quiz.model.Answer;
import com.prudential.lms.resource.quiz.model.QQuestion;
import com.prudential.lms.resource.quiz.model.Question;
import com.prudential.lms.resource.quiz.model.Quiz;
import com.prudential.lms.resource.quiz.repository.QuestionRepository;
import com.prudential.lms.resource.quiz.repository.QuizRepository;
import com.prudential.lms.resource.quiz.service.QuizService;
import com.prudential.lms.shared.constant.CorrectAnswer;
import com.prudential.lms.shared.constant.QuestionDifficulty;
import com.prudential.lms.shared.dto.quiz.AnswerDTO;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.dto.quiz.QuizDTO;
import com.prudential.lms.shared.dto.quiz.RandomQuizDTO;
import com.prudential.lms.shared.dto.training.TrainingTestQuestionDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;

@Service
@Transactional
public class QuizServiceImpl implements QuizService {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuizServiceImpl.class);

	@Autowired
	private QuizRepository quizRepo;

	@Autowired
	private QuizMapper quizMapper;

	@Autowired
	private QuestionRepository questionRepo;

	@Autowired
	private QuestionMapper questionMapper;

	private SecureRandom random = new SecureRandom();

	@Value("${app.upload.temp}")
	private String tempUploadPath;

	@Override
	public TrainingTestResultDTO verifyQuiz(TrainingTestResultDTO dto) {

		TrainingTestResultDTO result = new TrainingTestResultDTO();
		result.setRegistrationNo(dto.getRegistrationNo());
		result.setTestType(dto.getTestType());
		result.setAttempt(dto.getAttempt());
		result.setFinishDuration(dto.getFinishDuration());

		float qCount = dto.getQuestions().size();
		int correct = 0;
		for (TrainingTestQuestionDTO qdto : dto.getQuestions()) {

			BooleanExpression predicate = QQuestion.question.quiz.id.eq(qdto.getQuizId());
			predicate = predicate.and(QQuestion.question.id.eq(qdto.getQuestionId()));
			Question q = questionRepo.findOne(predicate);
			if (q != null) {

				qdto.setQuestionText(q.getLabelID());
				qdto.setDifficulty(q.getDifficulty());
				qdto.setWeight(q.getWeight());

				List<Answer> answers = q.getAnswers();
				int i = 1;
				for (Answer a : answers) {

					switch (i) {
					case 1:
						if (a.isCorrect()) {
							qdto.setCorrectAnswer(CorrectAnswer.A);
						}

						if (qdto.getAnswerId().equalsIgnoreCase(a.getAid())) {
							qdto.setUserAnswer(CorrectAnswer.A);
						}

						qdto.setAnswerA(a.getLabelID());
						break;
					case 2:
						if (a.isCorrect()) {
							qdto.setCorrectAnswer(CorrectAnswer.B);
						}

						if (qdto.getAnswerId().equalsIgnoreCase(a.getAid())) {
							qdto.setUserAnswer(CorrectAnswer.B);
						}

						qdto.setAnswerB(a.getLabelID());
						break;
					case 3:
						if (a.isCorrect()) {
							qdto.setCorrectAnswer(CorrectAnswer.C);
						}

						if (qdto.getAnswerId().equalsIgnoreCase(a.getAid())) {
							qdto.setUserAnswer(CorrectAnswer.C);
						}

						qdto.setAnswerC(a.getLabelID());
						break;
					case 4:
						if (a.isCorrect()) {
							qdto.setCorrectAnswer(CorrectAnswer.D);
						}

						if (qdto.getAnswerId().equalsIgnoreCase(a.getAid())) {
							qdto.setUserAnswer(CorrectAnswer.D);
						}

						qdto.setAnswerD(a.getLabelID());
						break;
					default:
						break;
					} // switch (i) {

					i++;

				} // for (Answer a : answers) {

				if (qdto.getCorrectAnswer() == qdto.getUserAnswer()) {
					qdto.setCorrect(true);
					correct++;
				} else {
					qdto.setCorrect(false);
				}

			} // if (q != null) {

			result.addQuestions(qdto);

		} // for (QuizSubmissionDTO qdto : dtos) {
		
		result.setPoint(new BigDecimal(Math.round((100 / qCount) * correct), MathContext.DECIMAL32));

		return result;

	}

	@Override
	public void importQuiz(InputStream inputStream, Long quizId) throws IOException, LmsException {

		if (null == quizId)
			throw new LmsException("Cannot import quiz with null quizId");

		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet sheet = workbook.getSheetAt(0);

		int i = 0;
		for (Row row : sheet) {
			if (i > 0) {

				QuestionDTO q = new QuestionDTO();
				q.setQuizId(quizId);
				q.setLabelID(row.getCell(0).getStringCellValue());

				switch (row.getCell(5).getStringCellValue()) {
				case "e":
					q.setDifficulty(QuestionDifficulty.EASY);
					break;
				case "m":
					q.setDifficulty(QuestionDifficulty.MEDIUM);
					break;
				case "h":
					q.setDifficulty(QuestionDifficulty.HARD);
					break;
				default:
					q.setDifficulty(QuestionDifficulty.EASY);
				}

				AnswerDTO a = new AnswerDTO();
				a.setLabelID(row.getCell(1).getStringCellValue());
				a.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("a"));
				q.addAnswers(a);

				AnswerDTO b = new AnswerDTO();
				b.setLabelID(row.getCell(2).getStringCellValue());
				b.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("b"));
				q.addAnswers(b);

				AnswerDTO c = new AnswerDTO();
				c.setLabelID(row.getCell(3).getStringCellValue());
				c.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("c"));
				q.addAnswers(c);

				AnswerDTO d = new AnswerDTO();
				d.setLabelID(row.getCell(4).getStringCellValue());
				d.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("d"));
				q.addAnswers(d);

				questionRepo.saveAndFlush(questionMapper.createEntity(q));
			}
			i++;
		}

		// Quiz currentQuiz = quizRepo.findOne(quizId);
		// currentQuiz.setQuestionsCount(i - 1);
		// quizRepo.saveAndFlush(currentQuiz);

		workbook.close();

	}

	@Override
	public List<QuestionDTO> randomQuestions(RandomQuizDTO dto) throws LmsException {

		List<QuestionDTO> result = new ArrayList<>();

		Long quizId = dto.getQuizId();
		if (null == quizId)
			throw new LmsException("Cannot random questions with unknown quizId");

		int easyCount = dto.getEasyCount();
		BooleanExpression easyQPredicate = QQuestion.question.quiz.id.eq(quizId);
		easyQPredicate = easyQPredicate.and(QQuestion.question.difficulty.eq(QuestionDifficulty.EASY));
		Iterable<Question> easyQuest = questionRepo.findAll(easyQPredicate);
		if (easyCount > Iterables.size(easyQuest))
			throw new LmsException("Easy Questions available were less than requested");
		List<Question> easyQuestions = Lists.newArrayList(easyQuest);

		int mediumCount = dto.getMediumCount();
		BooleanExpression mediumQPredicate = QQuestion.question.quiz.id.eq(quizId);
		mediumQPredicate = mediumQPredicate.and(QQuestion.question.difficulty.eq(QuestionDifficulty.MEDIUM));
		Iterable<Question> mediumQuest = questionRepo.findAll(mediumQPredicate);
		if (mediumCount > Iterables.size(mediumQuest))
			throw new LmsException("Medium Questions available were less than requested");
		List<Question> mediumQuestions = Lists.newArrayList(mediumQuest);

		int hardCount = dto.getHardCount();
		BooleanExpression hardQPredicate = QQuestion.question.quiz.id.eq(quizId);
		hardQPredicate = hardQPredicate.and(QQuestion.question.difficulty.eq(QuestionDifficulty.HARD));
		Iterable<Question> hardQuest = questionRepo.findAll(hardQPredicate);
		if (hardCount > Iterables.size(hardQuest))
			throw new LmsException("Hard Questions available were less than requested");
		List<Question> hardQuestions = Lists.newArrayList(hardQuest);

		result.addAll(randomQuestions(easyQuestions, easyCount));
		result.addAll(randomQuestions(mediumQuestions, mediumCount));
		result.addAll(randomQuestions(hardQuestions, hardCount));

		return result;
	}

	private List<QuestionDTO> randomQuestions(List<Question> questions, int qCount) {

		List<QuestionDTO> result = new ArrayList<>();

		int qSize = questions.size();
		if (qSize == qCount) {
			result.addAll(questionMapper.convertToDto(questions));
		} else {
			int tqMax = qSize - 1;
			int[] rsv = new int[qCount];
			for (int i = 0; i < qCount; i++) {
				int temp = random.nextInt(tqMax + 1);
				if (ArrayUtils.contains(rsv, temp)) {
					i--;
				} else {
					rsv[i] = temp;
				}
			}

			for (int i = 0; i < qCount; i++) {
				result.add(i, questionMapper.convertToDto(questions.get(rsv[i])));
			}
		}

		return result;

	}

	@Override
	public File exportQuiz(Quiz quiz) throws IOException, LmsException {

		FileOutputStream excelOutputStream = null;

		if (null == quiz)
			throw new LmsException("Unable export null quiz");

		String fileName = quiz.getCode().concat("-").concat(quiz.getName());
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(fileName);

		String[] headers = { "question", "answer_a", "answer_b", "answer_c", "answer_d", "difficulty",
				"correct_answer" };
		int cellCount = headers.length;
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < cellCount; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(headers[i]);
		}

		BooleanExpression qp = QQuestion.question.quiz.id.eq(quiz.getId());
		OrderSpecifier<Long> sortByIdASC = QQuestion.question.id.asc();
		Iterable<Question> qit = questionRepo.findAll(qp, sortByIdASC);

		int rowIdx = 1;
		for (Question q : qit) {
			Row row = sheet.createRow(rowIdx);
			for (int i = 0; i < cellCount; i++) {

				// question
				Cell cell = row.createCell(i++);
				cell.setCellValue(q.getLabelID());

				// answers
				List<Answer> answers = q.getAnswers();
				for (Answer a : answers) {
					Cell cellAns = row.createCell(i++);
					cellAns.setCellValue(a.getLabelID());
				}

				// difficulty
				Cell cellDfc = row.createCell(i++);
				String dfc = "null";
				QuestionDifficulty difficulty = q.getDifficulty();
				switch (difficulty.getWeight()) {
				case 1:
					dfc = "e";
					break;
				case 2:
					dfc = "m";
					break;
				case 3:
					dfc = "h";
					break;
				default:
					break;
				}
				cellDfc.setCellValue(dfc);

				// correct answer
				Cell correctAnsCell = row.createCell(i++);
				correctAnsCell.setCellValue(getCorrectAnswer(answers));

			}

			rowIdx++;
		}

		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		String uniqDir = tempUploadPath.concat(File.separator).concat(uuid);
		new File(uniqDir).mkdir();
		File result = new File(uniqDir.concat(File.separator).concat(fileName).concat(".xlsx"));
		excelOutputStream = new FileOutputStream(result);

		if (null != excelOutputStream) {
			workbook.write(excelOutputStream);
			workbook.close();
			excelOutputStream.close();
		}

		return result;
	}

	private String getCorrectAnswer(List<Answer> answers) {
		String result = "null";
		int ai = 0;
		for (Answer answer : answers) {
			ai++;
			if (answer.isCorrect())
				break;
		}

		switch (ai) {
		case 1:
			result = "a";
			break;
		case 2:
			result = "b";
			break;
		case 3:
			result = "c";
			break;
		case 4:
			result = "d";
			break;
		default:
			break;
		}

		return result;
	}

	@Override
	public QuizDTO parseQuiz(InputStream inputStream) throws IOException, LmsException {

		QuizDTO result = new QuizDTO();
		result.setId(0l);
		result.setName("PARSED" + System.currentTimeMillis());
		result.setDescription("Parsed Quiz not saved to database");
		Workbook workbook = null;
		try {

			workbook = new XSSFWorkbook(inputStream);
			Sheet sheet = workbook.getSheetAt(0);

			int i = 0;
			for (Row row : sheet) {
				if (i > 0) {

					QuestionDTO q = new QuestionDTO();
					q.setId(Long.valueOf(String.valueOf(i)));
					q.setQuizId(0l);
					q.setLabelID(row.getCell(0).getStringCellValue());

					switch (row.getCell(5).getStringCellValue()) {
					case "e":
						q.setDifficulty(QuestionDifficulty.EASY);
						break;
					case "m":
						q.setDifficulty(QuestionDifficulty.MEDIUM);
						break;
					case "h":
						q.setDifficulty(QuestionDifficulty.HARD);
						break;
					default:
						q.setDifficulty(QuestionDifficulty.EASY);
					}

					AnswerDTO a = new AnswerDTO();
					a.setLabelID(row.getCell(1).getStringCellValue());
					a.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("a"));
					q.addAnswers(a);

					AnswerDTO b = new AnswerDTO();
					b.setLabelID(row.getCell(2).getStringCellValue());
					b.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("b"));
					q.addAnswers(b);

					AnswerDTO c = new AnswerDTO();
					c.setLabelID(row.getCell(3).getStringCellValue());
					c.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("c"));
					q.addAnswers(c);

					AnswerDTO d = new AnswerDTO();
					d.setLabelID(row.getCell(4).getStringCellValue());
					d.setCorrect(row.getCell(6).getStringCellValue().equalsIgnoreCase("d"));
					q.addAnswers(d);

					result.getQuestions().add(q);
				}
				i++;
			}

			workbook.close();

		} catch (Exception e) {
			if (null != workbook)
				workbook.close();
			throw new LmsException(e.getMessage());
		}

		return result;
	}

}
