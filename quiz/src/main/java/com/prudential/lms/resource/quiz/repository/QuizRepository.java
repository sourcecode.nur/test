package com.prudential.lms.resource.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.quiz.model.Quiz;

public interface QuizRepository extends JpaRepository<Quiz, Long>, QueryDslPredicateExecutor<Quiz> {

//	List<Quiz> findByTrainingCode(String trainingCode);
	
	List<Quiz> findByCode(String code);

	List<Quiz> findByName(String name);

	List<Quiz> findByDescription(String description);

}
