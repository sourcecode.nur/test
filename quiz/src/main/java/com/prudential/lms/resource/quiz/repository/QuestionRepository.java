package com.prudential.lms.resource.quiz.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.quiz.model.Question;
import com.prudential.lms.resource.quiz.model.Quiz;
import com.prudential.lms.shared.constant.QuestionDifficulty;

public interface QuestionRepository extends JpaRepository<Question, Long>, QueryDslPredicateExecutor<Question> {

	List<Question> findByLabelEN(String labelEN);

	List<Question> findByLabelID(String labelID);

	List<Question> findByWeight(int weight);

	List<Question> findByDifficulty(QuestionDifficulty difficulty);

	List<Question> findByQuiz(Quiz quiz);

}
