package com.prudential.lms.resource.quiz.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.prudential.lms.resource.quiz.mapper.QuestionMapper;
import com.prudential.lms.resource.quiz.mapper.QuizMapper;
import com.prudential.lms.resource.quiz.model.QQuiz;
import com.prudential.lms.resource.quiz.model.Quiz;
import com.prudential.lms.resource.quiz.repository.QuestionRepository;
import com.prudential.lms.resource.quiz.repository.QuizRepository;
import com.prudential.lms.resource.quiz.service.QuizService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.QuizStatus;
import com.prudential.lms.shared.dto.quiz.AnswerDTO;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.dto.quiz.QuizDTO;
import com.prudential.lms.shared.dto.quiz.RandomQuizDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/quiz")
public class QuizController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuizController.class);

    @Autowired
    private QuizRepository repo;

    @Autowired
    private QuestionRepository questionRepo;

    @Autowired
    private QuizService service;

    @Autowired
    private QuizMapper mapper;

    @Autowired
    private QuestionMapper questionMapper;

    @Value("${app.download.quiz-template}")
    private String quizTemplatePath;

    // List Paging
    @RequestMapping(value = "/list/all", method = RequestMethod.POST)
    public Object listAll(@RequestBody(required = false) QuizDTO dto) {

        Response<Object> response = null;
        Iterable<Quiz> quizPage = null;
        // PageRequest pr = new PageRequest(page, size, Direction.ASC,
        // AppConstants.PAGE_SORT_PROPERTIES);

        try {

            BooleanExpression predicate = QQuiz.quiz.id.gt(-1);

            if (null != dto) {

                String name = dto.getName();
                if (StringUtils.isNotEmpty(name)) {
                    predicate = predicate.and(QQuiz.quiz.name.likeIgnoreCase("%" + name + "%"));
                }

            }

            quizPage = repo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));

        } catch (Exception e) {
            LOGGER.error("#####ERROR Listing Quiz", e);
            return Response.internalServerError(e.getMessage());
        }

        response = Iterables.size(quizPage) > 0 ? Response.found(mapper.convertToDto(quizPage)) : Response.notFound();

        return response;
    }

    // verify quiz
    @RequestMapping(value = "/verify", method = RequestMethod.POST)
    public Object verify(@RequestBody TrainingTestResultDTO dto) {

        TrainingTestResultDTO result = null;

        try {
            result = service.verifyQuiz(dto);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);

    }

    // upload update quiz
    @RequestMapping(value = "/parse", method = RequestMethod.POST)
    public Object parseQuiz(@RequestParam("content") MultipartFile content) {

        QuizDTO result = null;

        try {
            result = service.parseQuiz(content.getInputStream());
        } catch (LmsException | IOException e) {
            return Response.badRequest("Error when parsing");
        }

        return Response.ok(result);

    }

    // upload update quiz
    @RequestMapping(value = "/upload/update", method = RequestMethod.POST)
    @Transactional
    public Object uploadUpdateQuiz(@RequestParam(name = "content", required = false) MultipartFile content,
            QuizDTO dto) {

        LOGGER.debug("quizDto>> {}", dto.toString());

        Long quizId = dto.getId();
        if (null == quizId) {
            return Response.badRequest("Missing quizId Parameter");
        }

        Quiz quiz = repo.findOne(quizId);
        if (null == quiz) {
            return Response.notFound("Quiz with id: " + quizId + " not found");
        }

        // Quiz importedQuiz = null;
        try {

            questionRepo.delete(quiz.getQuestions());
            questionRepo.flush();
            // quiz = repo.saveAndFlush(quiz);

            // quiz.setQuestions(new HashSet<Question>());
            // Quiz savedQuiz = repo.save(quiz);
            // if (null == quiz)
            // return Response.internalServerError("Failed updating Quiz");
            // Long currentQuizId = savedQuiz.getId();
            // questionRepo.delete(savedQuiz.getQuestions());
            // savedQuiz.getQuestions().clear();
            // repo.save(savedQuiz);
            // if(null != content)
            service.importQuiz(content.getInputStream(), quizId);

            // repo.save(mapper.updateEntity(dto));
            // importedQuiz = repo.findOne(quizId);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok();

    }

    // update status quiz
    @RequestMapping(value = "/update/status/{quizId}/{statusEnum}", method = RequestMethod.POST)
    public Object updateStatusQuiz(@PathVariable("quizId") Long quizId, @PathVariable("statusEnum") QuizStatus status) {

        QuizDTO result = null;

        Quiz quiz = repo.findOne(quizId);
        if (null == quiz) {
            return Response.notFound("Quiz with id: " + quizId + " not found");
        }

        if (null == status) {
            return Response.badRequest("Invalid QuizStatus");
        }

        try {
            quiz.setStatus(status);
            Quiz savedQuiz = repo.save(quiz);

            result = mapper.convertToDto(savedQuiz);

        } catch (Exception e) {
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);

    }

    // download quiz template
    @RequestMapping(value = "/download/quiz-template", method = RequestMethod.GET)
    public Object downloadQuizTemplate() {

        HttpHeaders headers = new HttpHeaders();
        ByteArrayResource resource = null;
        File excelFile = null;

        try {

            excelFile = new File(quizTemplatePath);
            Path path = Paths.get(quizTemplatePath);
            resource = new ByteArrayResource(Files.readAllBytes(path));

            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            headers.add("Content-Disposition", "attachment; filename=" + excelFile.getName());
            headers.add("Access-Control-Expose-Headers", "Content-Disposition");

        } catch (LmsException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return ResponseEntity.ok().headers(headers).contentLength(excelFile.length())
                .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

    }

    // download quiz by Id
    @RequestMapping(value = "/download/{quizId}", method = RequestMethod.GET)
    public Object downloadQuiz(@PathVariable("quizId") Long quizId) {

        Quiz quiz = repo.findOne(quizId);
        if (null == quiz) {
            return Response.notFound("Quiz with id: " + quizId + "not found");
        }

        HttpHeaders headers = new HttpHeaders();
        ByteArrayResource resource = null;
        File excelFile = null;

        try {

            excelFile = service.exportQuiz(quiz);
            Path path = Paths.get(excelFile.getAbsolutePath());
            resource = new ByteArrayResource(Files.readAllBytes(path));

            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            headers.add("Content-Disposition", "attachment; filename=" + excelFile.getName());
            headers.add("Access-Control-Expose-Headers", "Content-Disposition");

        } catch (LmsException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return ResponseEntity.ok().headers(headers).contentLength(excelFile.length())
                .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

    }

    // upload quiz
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @Transactional
    public Object uploadQuiz(@RequestParam("content") MultipartFile content, QuizDTO dto) {

        LOGGER.debug("quizDto>> {}", dto.toString());

        Quiz savedQuiz = null;
        Quiz importedQuiz = null;

        try {

            savedQuiz = repo.save(mapper.createEntity(dto));
            if (null == savedQuiz) {
                return Response.internalServerError("Failed creating Quiz");
            }

            Long newQuizId = savedQuiz.getId();
            service.importQuiz(content.getInputStream(), newQuizId);
            // repo.flush();
            // repo.save(savedQuiz);

            importedQuiz = repo.findOne(newQuizId);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(mapper.convertToDto(importedQuiz));

    }

    // Get Random Questions by Quiz Id
    @RequestMapping(value = "/random", method = RequestMethod.POST)
    public Object getRandomQuestionsByQuiz(@RequestBody RandomQuizDTO dto) {

        List<QuestionDTO> result = new ArrayList<>();

        try {

            result.addAll(service.randomQuestions(dto));

            for (QuestionDTO qdto : result) {
                Set<AnswerDTO> answers = qdto.getAnswers();
                for (AnswerDTO adto : answers) {
                    adto.setCorrect(null);
                }
            }

        } catch (LmsException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);
    }

    // List Paging
    @RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
    public Object listPaging(@PathVariable("page") int page, @PathVariable("size") int size,
            @RequestBody(required = false) QuizDTO dto) {

        Response<Object> response = null;
        Page<Quiz> quizPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.ASC, AppConstants.PAGE_SORT_PROPERTIES);

        try {

            BooleanExpression predicate = QQuiz.quiz.id.gt(-1);

            if (null != dto) {

                String name = dto.getName();
                if (StringUtils.isNotEmpty(name)) {
                    predicate = predicate.and(QQuiz.quiz.name.likeIgnoreCase("%" + name + "%"));
                }

            }

            quizPage = repo.findAll(predicate, pr);

        } catch (Exception e) {
            LOGGER.error("#####ERROR Listing Quiz", e);
            return Response.internalServerError(e.getMessage());
        }

        Page<QuizDTO> pageResult = new PageImpl<QuizDTO>(mapper.convertToDto(quizPage.getContent()), pr,
                quizPage.getTotalElements());
        response = quizPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

        return response;
    }

    // Create
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Object create(@RequestBody QuizDTO dto) {
        Quiz saved = null;

        try {
            saved = repo.save(mapper.createEntity(dto));
        } catch (Exception e) {
            LOGGER.error("#####ERROR Saving Answer", e);
            return Response.internalServerError();
        }

        return Response.created(mapper.convertToDto(saved));
    }

    // Find By Id
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Object findOne(@PathVariable("id") Long id) {
        Quiz quiz = repo.findOne(id);
        if (null == quiz) {
            return Response.notFound("Quiz with id: " + id + " not found");
        }

        return Response.found(mapper.convertToDto(quiz));
    }

    // List All
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Object findAll(@RequestBody(required = false) QuizDTO dto) {

        List<Quiz> quizes = new ArrayList<>();

        if (null == dto) {
            quizes.addAll(repo.findAll());
        } else {

            BooleanExpression qPredicate = QQuiz.quiz.id.gt(-1);

            String name = dto.getName();
            if (StringUtils.isNotEmpty(name)) {
                qPredicate = qPredicate.and(QQuiz.quiz.name.likeIgnoreCase("%" + name + "%"));
            }

            Iterable<Quiz> quizz = repo.findAll(qPredicate);
            quizes.addAll(Lists.newArrayList(quizz));

        }

        return Response.found(mapper.convertToDto(quizes));
    }

    // Update
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Object update(@PathVariable("id") Long id, @RequestBody QuizDTO dto) {

        Quiz updatedEntity = null;
        Quiz quiz = repo.findOne(id);

        if (null == quiz) {
            return Response.notFound();
        } else {

            try {
                updatedEntity = repo.save(mapper.updateEntity(dto));
            } catch (Exception e) {
                LOGGER.error("#####ERROR Updating Quiz", e);
                return Response.internalServerError(e.getMessage());
            }

        }

        return Response.ok(mapper.convertToDto(updatedEntity));
    }

    // Delete
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Object delete(@PathVariable("id") Long id) {
        Quiz quiz = repo.findOne(id);

        if (null == quiz) {
            return Response.notFound();
        }

        try {
            questionRepo.delete(quiz.getQuestions());
            repo.delete(quiz);
        } catch (Exception e) {
            LOGGER.error("#####ERROR Deleting Quiz", e);
            return Response.internalServerError();
        }

        return Response.ok();
    }

}
