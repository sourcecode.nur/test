package com.prudential.lms.resource.quiz.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.prudential.lms.resource.quiz.model.Quiz;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.dto.quiz.QuizDTO;
import com.prudential.lms.shared.dto.quiz.RandomQuizDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;

public interface QuizService {

	TrainingTestResultDTO verifyQuiz(TrainingTestResultDTO dto);

	void importQuiz(InputStream inputStream, Long quizId) throws IOException, LmsException;

	List<QuestionDTO> randomQuestions(RandomQuizDTO dto) throws LmsException;

	File exportQuiz(Quiz quiz) throws IOException, LmsException;

	QuizDTO parseQuiz(InputStream inputStream) throws IOException, LmsException;
}
