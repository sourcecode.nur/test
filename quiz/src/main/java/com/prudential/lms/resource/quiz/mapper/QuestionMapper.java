package com.prudential.lms.resource.quiz.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.quiz.model.Answer;
import com.prudential.lms.resource.quiz.model.Question;
import com.prudential.lms.resource.quiz.repository.QuestionRepository;
import com.prudential.lms.resource.quiz.repository.QuizRepository;
import com.prudential.lms.shared.dto.quiz.AnswerDTO;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class QuestionMapper implements EntityToDTOMapper<Question, QuestionDTO> {

	@Autowired
	private QuestionRepository repo;

	@Autowired
	private QuizRepository quizRepo;
	
	@Autowired
	private AnswerMapper answerMapper;

	@Override
	public Question createEntity(QuestionDTO dto) {
		Question entity = new Question();
		entity.setLabelEN(dto.getLabelEN());
		entity.setLabelID(dto.getLabelID());
		entity.setDifficulty(dto.getDifficulty());

		Long quizId = dto.getQuizId();
		if (null != quizId) {
			entity.setQuiz(quizRepo.findOne(quizId));
		}
		
		Set<AnswerDTO> answersDTOs = dto.getAnswers();
		for (AnswerDTO answerDTO : answersDTOs) {
			entity.addAnswers(answerMapper.createEntity(answerDTO));
		}

		return entity;
	}

	@Override
	public Question updateEntity(QuestionDTO dto) {
		Question entity = repo.findOne(dto.getId());
		entity.setLabelEN(dto.getLabelEN());
		entity.setLabelID(dto.getLabelID());
		entity.setWeight(dto.getWeight());
		entity.setDifficulty(dto.getDifficulty());

		return entity;
	}

	@Override
	public QuestionDTO convertToDto(Question entity) {
		QuestionDTO dto = new QuestionDTO();
		dto.setId(entity.getId());
		dto.setLabelEN(entity.getLabelEN());
		dto.setLabelID(entity.getLabelID());
		dto.setWeight(entity.getWeight());
		dto.setDifficulty(entity.getDifficulty());

		List<Answer> answers = entity.getAnswers();
		if(null != answers)
			dto.getAnswers().addAll(answerMapper.convertToDto(answers));
		
		// Auditable
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setUpdatedBy(entity.getUpdatedBy());
		dto.setUpdatedDate(entity.getUpdatedDate());
		dto.setVersion(entity.getVersion());

		return dto;
	}

	@Override
	public List<QuestionDTO> convertToDto(Iterable<Question> entities) {
		List<QuestionDTO> dtoList = new ArrayList<>();
		for (Question entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public Question convertToEntity(QuestionDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Question> convertToEntity(Iterable<QuestionDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
