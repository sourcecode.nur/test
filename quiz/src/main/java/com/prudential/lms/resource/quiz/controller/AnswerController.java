package com.prudential.lms.resource.quiz.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.quiz.mapper.AnswerMapper;
import com.prudential.lms.resource.quiz.model.Answer;
//import com.prudential.lms.resource.quiz.repository.AnswerRepository;
import com.prudential.lms.resource.quiz.repository.QuestionRepository;
import com.prudential.lms.shared.dto.quiz.AnswerDTO;
import com.prudential.lms.shared.message.Response;

//@RestController
//@RequestMapping("/answer")
public class AnswerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AnswerController.class);
	
//	@Autowired
//	private AnswerRepository repo;

//	@Autowired
//	private QuestionRepository questionRepo;
//
//	@Autowired
//	private AnswerMapper mapper;
//
//	@RequestMapping(value = "/list", method = RequestMethod.POST)
//	public Object list(@RequestBody(required = false) AnswerDTO dto) {
//
//		Response<Object> response = null;
//		List<Answer> listAnswer = null;
//
//		try {
//
//			listAnswer = repo.findAll();
//		} catch (Exception e) {
//			LOGGER.error("#####ERROR Listing Answer", e);
//			return Response.internalServerError();
//		}
//
//		response = listAnswer.size() > 0 ? Response.found(mapper.convertToDto(listAnswer)) : Response.notFound();
//
//		return response;
//	}
//
//	// Create
//	@RequestMapping(value = "", method = RequestMethod.POST)
//	public Object create(@RequestBody AnswerDTO dto) {
//		Answer saved = null;
//
//		try {
//			saved = repo.save(mapper.createEntity(dto));
//		} catch (Exception e) {
//			LOGGER.error("#####ERROR Saving Answer", e);
//			return Response.internalServerError();
//		}
//
//		return Response.created(mapper.convertToDto(saved));
//	}
//
//	// Find By Id
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
//	public Object findOne(@PathVariable("id") Long id) {
//		Answer tc = repo.findOne(id);
//		if (null == tc)
//			return Response.notFound();
//
//		return Response.found(mapper.convertToDto(tc));
//	}
//
//	// Find by sample
//	@RequestMapping(value = "/find", method = RequestMethod.GET)
//	public Object findByExample(@RequestBody AnswerDTO example) {
//		Response<Object> response = null;
//		List<Answer> listAnswer = null;
//		if (example != null) {
//			if (example.getLabelEN() != null) {
//				listAnswer = repo.findBylabelEN(example.getLabelEN());
//			} else if (example.getLabelID() != null) {
//				listAnswer = repo.findByLabelID(example.getLabelID());
//			} else if (example.isCorrect()) {
//				listAnswer = repo.findByCorrect(example.isCorrect());
//			} else {
//				listAnswer = repo.findByQuestion(questionRepo.findOne(example.getQuestionId()));
//			}
//			response = listAnswer.size() > 0 ? Response.found(mapper.convertToDto(listAnswer)) : Response.notFound();
//
//			return response;
//		} else
//
//			return Response.notFound();
//	}
//
//	// Update
//	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
//	public Object update(@PathVariable("id") Long id, @RequestBody AnswerDTO dto) {
//
//		Answer updatedEntity = null;
//		Answer tc = repo.findOne(id);
//
//		if (null == tc) {
//			return Response.notFound();
//		} else {
//
//			try {
//				updatedEntity = repo.save(mapper.updateEntity(dto));
//			} catch (Exception e) {
//				LOGGER.error("#####ERROR Updating Answer", e);
//				return Response.internalServerError();
//			}
//
//		}
//
//		return Response.ok(mapper.convertToDto(updatedEntity));
//	}
//
//	// Delete
//	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//	public Object delete(@PathVariable("id") Long id) {
//		Answer tc = repo.findOne(id);
//
//		if (null == tc) {
//			return Response.notFound();
//		}
//
//		try {
//			repo.delete(tc);
//		} catch (Exception e) {
//			LOGGER.error("#####ERROR Deleting Answer", e);
//			return Response.internalServerError();
//		}
//
//		return Response.ok();
//	}

}
