package com.prudential.lms.resource.quiz.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.quiz.model.Answer;
import com.prudential.lms.shared.dto.quiz.AnswerDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class AnswerMapper implements EntityToDTOMapper<Answer, AnswerDTO> {

	@Autowired
	private ModelMapper mapper;

	@Override
	public Answer createEntity(AnswerDTO dto) {
		return mapper.map(dto, new Answer().getClass());
	}

	@Override
	public Answer updateEntity(AnswerDTO dto) {
		return null;
	}

	@Override
	public AnswerDTO convertToDto(Answer entity) {
		return mapper.map(entity, AnswerDTO.class);
	}

	@Override
	public List<AnswerDTO> convertToDto(Iterable<Answer> entities) {
		List<AnswerDTO> dtoList = new ArrayList<>();
		for (Answer entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public Answer convertToEntity(AnswerDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Answer> convertToEntity(Iterable<AnswerDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
