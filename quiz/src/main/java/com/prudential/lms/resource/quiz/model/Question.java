package com.prudential.lms.resource.quiz.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.prudential.lms.shared.constant.QuestionDifficulty;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_quiz_question")
public class Question extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8228995030144164735L;
	private static final String SEQ_GEN = "seq_gen_quiz_question";
	private static final String SEQ = "seq_quiz_question";

	private Long id;
	private String labelID;
	private String labelEN;
	private int weight;
	private QuestionDifficulty difficulty;
	private Quiz quiz;
	private List<Answer> answers = new ArrayList<>();

	@PrePersist
	private void prePersist() {
		this.weight = getDifficulty().getWeight();
		for (Answer answer : this.answers) {
			answer.setAid(UUID.randomUUID().toString().replaceAll("-", ""));
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@ElementCollection
	@CollectionTable(name = "lms_quiz_answer", joinColumns = { @JoinColumn(name = "question_id") })
	@OrderColumn(name = "answer_id")
	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	@Transient
	public void addAnswers(Answer... answers) {
		for (Answer answer : answers) {
			this.answers.add(answer);
		}
	}

	@ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "quiz_id")
	public Quiz getQuiz() {
		return quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}

	@Column(name = "label_id")
	public String getLabelID() {
		return labelID;
	}

	public void setLabelID(String labelID) {
		this.labelID = labelID;
	}

	@Column(name = "label_en")
	public String getLabelEN() {
		return labelEN;
	}

	public void setLabelEN(String labelEN) {
		this.labelEN = labelEN;
	}

	@Enumerated(EnumType.STRING)
	public QuestionDifficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(QuestionDifficulty difficulty) {
		this.difficulty = difficulty;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
