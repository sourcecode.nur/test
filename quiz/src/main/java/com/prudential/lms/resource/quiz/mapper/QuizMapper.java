package com.prudential.lms.resource.quiz.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.quiz.model.Answer;
import com.prudential.lms.resource.quiz.model.Question;
import com.prudential.lms.resource.quiz.model.Quiz;
import com.prudential.lms.resource.quiz.repository.QuizRepository;
import com.prudential.lms.shared.constant.QuizStatus;
import com.prudential.lms.shared.dto.quiz.AnswerDTO;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.dto.quiz.QuizDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class QuizMapper implements EntityToDTOMapper<Quiz, QuizDTO> {

	@Autowired
	private QuizRepository repo;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private AnswerMapper answerMapper;

	@Override
	public Quiz createEntity(QuizDTO dto) {
		Quiz entity = new Quiz();
		entity.setCode(String.valueOf(System.currentTimeMillis()));
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());

		QuizStatus status = dto.getStatus();
		entity.setStatus(null != status ? status : QuizStatus.REQUEST);

		return entity;
	}

	@Override
	public Quiz updateEntity(QuizDTO dto) {
		Quiz entity = repo.findOne(dto.getId());
		// entity.setCode(dto.getCode());

		String name = dto.getName();
		if (StringUtils.isNotEmpty(name))
			entity.setName(name);

		String description = dto.getDescription();
		if (StringUtils.isNotEmpty(description))
			entity.setDescription(description);

		QuizStatus status = dto.getStatus();
		entity.setStatus(null != status ? status : QuizStatus.REVISE);

		return entity;
	}

	@Override
	public QuizDTO convertToDto(Quiz entity) {
		QuizDTO dto = new QuizDTO();
		dto.setId(entity.getId());
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setStatus(entity.getStatus());
		dto.setQuestionsCount(entity.getQuestionsCount());

		int easy, medium, hard;
		easy = medium = hard = 0;

		Set<Question> questions = entity.getQuestions();
		for (Question question : questions) {
			QuestionDTO questionDTO = new QuestionDTO();
			questionDTO.setId(question.getId());
			questionDTO.setLabelEN(question.getLabelEN());
			questionDTO.setLabelID(question.getLabelID());
			questionDTO.setWeight(question.getWeight());
			questionDTO.setDifficulty(question.getDifficulty());
			questionDTO.setQuizId(question.getQuiz().getId());
			questionDTO.setQuizCode(question.getQuiz().getCode());
			questionDTO.setQuizName(question.getQuiz().getName());
			questionDTO.setQuizDescription(question.getQuiz().getDescription());
			questionDTO.getAnswers().addAll(answerMapper.convertToDto(question.getAnswers()));

			switch (question.getWeight()) {
			case 1:
				easy++;
				break;
			case 2:
				medium++;
				break;
			case 3:
				hard++;
				break;

			default:
				break;
			}

			dto.getQuestions().add(questionDTO);
		}

		dto.setEasyCount(easy);
		dto.setMediumCount(medium);
		dto.setHardCount(hard);

		// Auditable
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setUpdatedBy(entity.getUpdatedBy());
		dto.setUpdatedDate(entity.getUpdatedDate());
		dto.setVersion(entity.getVersion());

		return dto;
	}

	@Override
	public List<QuizDTO> convertToDto(Iterable<Quiz> entities) {
		List<QuizDTO> dtoList = new ArrayList<>();
		for (Quiz entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public Quiz convertToEntity(QuizDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Quiz> convertToEntity(Iterable<QuizDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
