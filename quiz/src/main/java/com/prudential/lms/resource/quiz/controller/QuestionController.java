package com.prudential.lms.resource.quiz.controller;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.quiz.mapper.QuestionMapper;
import com.prudential.lms.resource.quiz.model.Answer;
import com.prudential.lms.resource.quiz.model.QQuestion;
import com.prudential.lms.resource.quiz.model.Question;
import com.prudential.lms.resource.quiz.repository.QuestionRepository;
import com.prudential.lms.resource.quiz.repository.QuizRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/question")
public class QuestionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuestionController.class);

	@Autowired
	private QuestionRepository repo;

	@Autowired
	private QuizRepository quizRepo;

	@Autowired
	private QuestionMapper mapper;

	@GetMapping("/magification")
	public Object magification() {

		try {

			Iterable<Question> questions = repo.findAll();
			for (Question q : questions) {
				List<Answer> answers = q.getAnswers();
				for (Answer a : answers) {
					if (StringUtils.isEmpty(a.getAid()) || null == a.getAid()) {
						a.setAid(UUID.randomUUID().toString().replaceAll("-", ""));
					}
				}
				repo.saveAndFlush(q);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

	// List Paging
	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
	public Object listPaging(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) QuestionDTO dto) {

		Response<Object> response = null;
		Page<Question> qPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES);

		try {

			BooleanExpression predicate = QQuestion.question.id.gt(-1);

			if (null != dto) {

				Long quizId = dto.getQuizId();
				if (null != quizId)
					predicate = predicate.and(QQuestion.question.quiz.id.eq(quizId));

			}

			qPage = repo.findAll(predicate, pr);

		} catch (Exception e) {
			LOGGER.error("#####ERROR Paging Questions", e);
			return Response.internalServerError(e.getMessage());
		}

		Page<QuestionDTO> pageResult = new PageImpl<QuestionDTO>(mapper.convertToDto(qPage.getContent()), pr,
				qPage.getTotalElements());
		response = qPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

		return response;
	}

	// Create
	// @RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody QuestionDTO dto) {
		Question saved = null;

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving Question", e);
			return Response.internalServerError();
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	// @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		Question question = repo.findOne(id);
		if (null == question)
			return Response.notFound();

		return Response.found(mapper.convertToDto(question));
	}

	// Find by sample
	// @RequestMapping(value = "/find", method = RequestMethod.GET)
	// public Object findByExample(@RequestBody QuestionDTO example) {
	// Response<Object> response = null;
	// List<Question> listQuestion = null;
	// if (example != null) {
	// if (example.getLabelEN() != null) {
	// listQuestion = repo.findByLabelEN(example.getLabelEN());
	// } else if (example.getLabelID() != null) {
	// listQuestion = repo.findByLabelID(example.getLabelID());
	// } else if (example.getDifficulty() != null) {
	// listQuestion = repo.findByDifficulty(example.getDifficulty());
	// } else if (example.getWeight() >= 0) {
	// listQuestion = repo.findByWeight(example.getWeight());
	// } else if (example.getQuizId() != null) {
	// listQuestion = repo.findByQuiz(quizRepo.findOne(example.getQuizId()));
	// }
	// response = listQuestion.size() > 0 ?
	// Response.found(mapper.convertToDto(listQuestion))
	// : Response.notFound();
	//
	// return response;
	// } else
	//
	// return Response.notFound();
	// }

	// Update
	// @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody QuestionDTO dto) {

		Question updatedEntity = null;
		Question question = repo.findOne(id);

		if (null == question) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating Question", e);
				return Response.internalServerError();
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		Question question = repo.findOne(id);

		if (null == question) {
			return Response.notFound();
		}

		try {
			repo.delete(question);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting Question", e);
			return Response.internalServerError();
		}

		return Response.ok();
	}
}
