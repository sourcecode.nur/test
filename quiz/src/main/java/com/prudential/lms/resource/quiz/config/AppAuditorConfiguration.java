package com.prudential.lms.resource.quiz.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "appAuditorAware", modifyOnCreate = true)
public class AppAuditorConfiguration {

}
