package com.prudential.lms.resource.quiz.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.constant.QuizStatus;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_quiz")
public class Quiz extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5959345542522377853L;
	private static final String SEQ_GEN = "seq_gen_quiz";
	private static final String SEQ = "seq_quiz";

	private Long id;
	private String code;
	private String name;
	private String description;
	private QuizStatus status;
	private int questionsCount;

	private Set<Question> questions = new HashSet<>();
	
	@PrePersist
	private void prePersist(){
		this.questionsCount = this.questions.size();
	}

	@PreUpdate
	private void preUpdate(){
		this.questionsCount = this.questions.size();
	}
	
	@Transient
	public void calculateQuestions(){
		this.questionsCount = getQuestions().size();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = 32, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "quiz")
	public Set<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	@Enumerated(EnumType.STRING)
	public QuizStatus getStatus() {
		return status;
	}

	public void setStatus(QuizStatus status) {
		this.status = status;
	}

	public int getQuestionsCount() {
		return questionsCount;
	}

	public void setQuestionsCount(int questionsCount) {
		this.questionsCount = questionsCount;
	}
}
