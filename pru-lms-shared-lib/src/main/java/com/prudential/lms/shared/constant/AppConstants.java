package com.prudential.lms.shared.constant;

public class AppConstants {

	public static final String[] PAGE_SORT_PROPERTIES = { "updatedDate", "createdDate" };

	public static final String SCORM_MANIFEST_FILE = "imsmanifest.xml";

	public static final String CONTENT_TYPE_VIDEO = "video/mp4";
	public static final String CONTENT_TYPE_PDF = "application/pdf";
	public static final String CONTENT_TYPE_SCORM = "application/zip";
	public static final String CONTENT_TYPE_QUIZ = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	
	public static final String LOGIN_CHANNEL_AGENT = "agen";
	
	public static final String HTTP_HEADER_AUTHORIZATION = "Authorization";

}
