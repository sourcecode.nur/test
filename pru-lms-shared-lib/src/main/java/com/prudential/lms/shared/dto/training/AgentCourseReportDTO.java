package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AgentCourseReportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4106361120875888671L;

	private String agentCode;
	private String trainingCode;
	private String assesmentDate;
	private String assesmentScore;
	private String minimumScore;
	private String agentName;
	private String salesUnit;
	private String createdBy = "PRUFORCE";
	private String originId;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getTrainingCode() {
		return trainingCode;
	}

	public void setTrainingCode(String trainingCode) {
		this.trainingCode = trainingCode;
	}

	public String getAssesmentDate() {
		return assesmentDate;
	}

	public void setAssesmentDate(String assesmentDate) {
		this.assesmentDate = assesmentDate;
	}

	public String getAssesmentScore() {
		return assesmentScore;
	}

	public void setAssesmentScore(String assesmentScore) {
		this.assesmentScore = assesmentScore;
	}

	public String getMinimumScore() {
		return minimumScore;
	}

	public void setMinimumScore(String minimumScore) {
		this.minimumScore = minimumScore;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getSalesUnit() {
		return salesUnit;
	}

	public void setSalesUnit(String salesUnit) {
		this.salesUnit = salesUnit;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getOriginId() {
		return originId;
	}

	public void setOriginId(String originId) {
		this.originId = originId;
	}

	@Override
	public String toString() {
		return "AgentCourseReportDTO [agentCode=" + agentCode + ", trainingCode=" + trainingCode + ", assesmentDate="
				+ assesmentDate + ", assesmentScore=" + assesmentScore + ", minimumScore=" + minimumScore
				+ ", agentName=" + agentName + ", salesUnit=" + salesUnit + ", createdBy=" + createdBy + ", originId="
				+ originId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentCode == null) ? 0 : agentCode.hashCode());
		result = prime * result + ((agentName == null) ? 0 : agentName.hashCode());
		result = prime * result + ((assesmentDate == null) ? 0 : assesmentDate.hashCode());
		result = prime * result + ((assesmentScore == null) ? 0 : assesmentScore.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((minimumScore == null) ? 0 : minimumScore.hashCode());
		result = prime * result + ((originId == null) ? 0 : originId.hashCode());
		result = prime * result + ((salesUnit == null) ? 0 : salesUnit.hashCode());
		result = prime * result + ((trainingCode == null) ? 0 : trainingCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgentCourseReportDTO other = (AgentCourseReportDTO) obj;
		if (agentCode == null) {
			if (other.agentCode != null)
				return false;
		} else if (!agentCode.equals(other.agentCode))
			return false;
		if (agentName == null) {
			if (other.agentName != null)
				return false;
		} else if (!agentName.equals(other.agentName))
			return false;
		if (assesmentDate == null) {
			if (other.assesmentDate != null)
				return false;
		} else if (!assesmentDate.equals(other.assesmentDate))
			return false;
		if (assesmentScore == null) {
			if (other.assesmentScore != null)
				return false;
		} else if (!assesmentScore.equals(other.assesmentScore))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (minimumScore == null) {
			if (other.minimumScore != null)
				return false;
		} else if (!minimumScore.equals(other.minimumScore))
			return false;
		if (originId == null) {
			if (other.originId != null)
				return false;
		} else if (!originId.equals(other.originId))
			return false;
		if (salesUnit == null) {
			if (other.salesUnit != null)
				return false;
		} else if (!salesUnit.equals(other.salesUnit))
			return false;
		if (trainingCode == null) {
			if (other.trainingCode != null)
				return false;
		} else if (!trainingCode.equals(other.trainingCode))
			return false;
		return true;
	}

}
