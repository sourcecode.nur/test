package com.prudential.lms.shared.constant;

public enum TrainingCategoryStatus {

	APPROVE, REQUEST, REVISE, REJECT

}
