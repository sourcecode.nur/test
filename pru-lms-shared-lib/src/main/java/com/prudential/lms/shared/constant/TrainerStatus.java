package com.prudential.lms.shared.constant;

public enum TrainerStatus {

	AVAILABLE, UNAVAILABLE

}
