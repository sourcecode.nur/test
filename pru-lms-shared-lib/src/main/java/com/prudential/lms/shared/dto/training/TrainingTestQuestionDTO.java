package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.CorrectAnswer;
import com.prudential.lms.shared.constant.QuestionDifficulty;

@JsonInclude(Include.NON_NULL)
public class TrainingTestQuestionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8454241563378841384L;
	private Long quizId;
	private Long questionId;
	private String answerId;
	private int weight;
	private QuestionDifficulty difficulty;
	private String questionText;
	private String answerA;
	private String answerB;
	private String answerC;
	private String answerD;
	private CorrectAnswer correctAnswer;
	private CorrectAnswer userAnswer;
	private Boolean correct;

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public String getAnswerId() {
		return answerId;
	}

	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public QuestionDifficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(QuestionDifficulty difficulty) {
		this.difficulty = difficulty;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getAnswerA() {
		return answerA;
	}

	public void setAnswerA(String answerA) {
		this.answerA = answerA;
	}

	public String getAnswerB() {
		return answerB;
	}

	public void setAnswerB(String answerB) {
		this.answerB = answerB;
	}

	public String getAnswerC() {
		return answerC;
	}

	public void setAnswerC(String answerC) {
		this.answerC = answerC;
	}

	public String getAnswerD() {
		return answerD;
	}

	public void setAnswerD(String answerD) {
		this.answerD = answerD;
	}

	public CorrectAnswer getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(CorrectAnswer correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public CorrectAnswer getUserAnswer() {
		return userAnswer;
	}

	public void setUserAnswer(CorrectAnswer userAnswer) {
		this.userAnswer = userAnswer;
	}

	public Boolean getCorrect() {
		return correct;
	}

	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}

}
