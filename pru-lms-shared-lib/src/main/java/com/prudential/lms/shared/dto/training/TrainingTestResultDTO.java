package com.prudential.lms.shared.dto.training;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingTestResultDTO extends AuditableDTO<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2611697961564944296L;

	private BigDecimal point;
	private BigDecimal minPoint;
	private int attempt;
	private int finishDuration;
	private Date finishDate;
	private TrainingTestType testType;
	private Boolean pass;
	private Date submitedToLAS;
	private Long trainingRegistrationScheduleId;
	private String registrationNo;

	private Long trainingId;
	private String trainingCode;
	private String trainingName;

	private Long agentId;
	private String agentCode;
	private String agentName;
	private String agentOfficeCode;
	private Date trainingDate;
	private int cpdPoint;

	private String originId; // same with id

	private List<TrainingTestQuestionDTO> questions = new ArrayList<>();

	public BigDecimal getPoint() {
		return point;
	}

	public void setPoint(BigDecimal point) {
		this.point = point;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public int getFinishDuration() {
		return finishDuration;
	}

	public void setFinishDuration(int finishDuration) {
		this.finishDuration = finishDuration;
	}

	public Boolean getPass() {
		return pass;
	}

	public void setPass(Boolean pass) {
		this.pass = pass;
	}

	public Date getSubmitedToLAS() {
		return submitedToLAS;
	}

	public void setSubmitedToLAS(Date submitedToLAS) {
		this.submitedToLAS = submitedToLAS;
	}

	public Long getTrainingRegistrationScheduleId() {
		return trainingRegistrationScheduleId;
	}

	public void setTrainingRegistrationScheduleId(Long trainingRegistrationScheduleId) {
		this.trainingRegistrationScheduleId = trainingRegistrationScheduleId;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public TrainingTestType getTestType() {
		return testType;
	}

	public void setTestType(TrainingTestType testType) {
		this.testType = testType;
	}

	public List<TrainingTestQuestionDTO> getQuestions() {
		return questions;
	}

	public void setQuestions(List<TrainingTestQuestionDTO> questions) {
		this.questions = questions;
	}

	public void addQuestions(TrainingTestQuestionDTO... questions) {
		for (TrainingTestQuestionDTO trnTestQues : questions) {
			this.questions.add(trnTestQues);
		}
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	public int getCpdPoint() {
		return cpdPoint;
	}

	public void setCpdPoint(int cpdPoint) {
		this.cpdPoint = cpdPoint;
	}

	public String getTrainingCode() {
		return trainingCode;
	}

	public void setTrainingCode(String trainingCode) {
		this.trainingCode = trainingCode;
	}

	public BigDecimal getMinPoint() {
		return minPoint;
	}

	public void setMinPoint(BigDecimal minPoint) {
		this.minPoint = minPoint;
	}

	public String getOriginId() {
		return originId;
	}

	public void setOriginId(String originId) {
		this.originId = originId;
	}

	public String getAgentOfficeCode() {
		return agentOfficeCode;
	}

	public void setAgentOfficeCode(String agentOfficeCode) {
		this.agentOfficeCode = agentOfficeCode;
	}

}
