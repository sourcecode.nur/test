package com.prudential.lms.shared.dto.training;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingRegistrationScheduleDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -965602817392656716L;

	private TrainingScheduleDTO schedule;
	private TrainingRegistrationDTO registration;
	private String registrationNo;
	private Date startDate;
	private Set<TrainingProgressDTO> progresses = new HashSet<>();
	private TrainingProgressStatus status;
	private Integer point;
	private Date submitedToLAS;
	private Boolean pass;

	public TrainingRegistrationDTO getRegistration() {
		return registration;
	}

	public void setRegistration(TrainingRegistrationDTO registration) {
		this.registration = registration;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public TrainingScheduleDTO getSchedule() {
		return schedule;
	}

	public void setSchedule(TrainingScheduleDTO schedule) {
		this.schedule = schedule;
	}

	public Set<TrainingProgressDTO> getProgresses() {
		return progresses;
	}

	public void setProgresses(Set<TrainingProgressDTO> progresses) {
		this.progresses = progresses;
	}

	public TrainingProgressStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingProgressStatus status) {
		this.status = status;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Date getSubmitedToLAS() {
		return submitedToLAS;
	}
	

	public void setSubmitedToLAS(Date submitedToLAS) {
		this.submitedToLAS = submitedToLAS;
	}

	public Boolean getPass() {
		return pass;
	}
	

	public void setPass(Boolean pass) {
		this.pass = pass;
	}
	
	

}
