package com.prudential.lms.shared.dto;

public interface SoftDeleteDTO {
	
	boolean isDeleted();

	void setDeleted(boolean deleted);
	
}
