/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prudential.lms.shared.dto.training;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingContentType;
import com.prudential.lms.shared.dto.AuditableDTO;

/**
 *
 * @author CodeId
 */
@JsonInclude(Include.NON_NULL)
public class ContentLibraryMapDTO extends AuditableDTO<Long> {
    private static final long serialVersionUID = -6277477943685098686L;
        private Long libraryId;
	private String libraryTitle;
	private String libraryDescription;
	private String libraryMimeType;
	private String libraryPath;
	private String libraryBaseUri;
	private String libraryUri;
	private String libraryThumbnail;
	private String libraryContentPDF;
	private String libraryFileName;
        private TrainingContentType trainingContentType;
        private String trainingCode;
        private String trainingName;
        private boolean trainingDeleted = true;

    public Long getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Long libraryId) {
        this.libraryId = libraryId;
    }
    
    public String getLibraryTitle() {
        return libraryTitle;
    }

    public void setLibraryTitle(String libraryTitle) {
        this.libraryTitle = libraryTitle;
    }

    public String getLibraryDescription() {
        return libraryDescription;
    }

    public void setLibraryDescription(String libraryDescription) {
        this.libraryDescription = libraryDescription;
    }

    public String getLibraryMimeType() {
        return libraryMimeType;
    }

    public void setLibraryMimeType(String libraryMimeType) {
        this.libraryMimeType = libraryMimeType;
    }

    public String getLibraryPath() {
        return libraryPath;
    }

    public void setLibraryPath(String libraryPath) {
        this.libraryPath = libraryPath;
    }

    public String getLibraryBaseUri() {
        return libraryBaseUri;
    }

    public void setLibraryBaseUri(String libraryBaseUri) {
        this.libraryBaseUri = libraryBaseUri;
    }

    public String getLibraryUri() {
        return libraryUri;
    }

    public void setLibraryUri(String libraryUri) {
        this.libraryUri = libraryUri;
    }

    public String getLibraryThumbnail() {
        return libraryThumbnail;
    }

    public void setLibraryThumbnail(String libraryThumbnail) {
        this.libraryThumbnail = libraryThumbnail;
    }

    public String getLibraryContentPDF() {
        return libraryContentPDF;
    }

    public void setLibraryContentPDF(String libraryContentPDF) {
        this.libraryContentPDF = libraryContentPDF;
    }

    public String getLibraryFileName() {
        return libraryFileName;
    }

    public void setLibraryFileName(String libraryFileName) {
        this.libraryFileName = libraryFileName;
    }

    public TrainingContentType getTrainingContentType() {
        return trainingContentType;
    }

    public void setTrainingContentType(TrainingContentType trainingContentType) {
        this.trainingContentType = trainingContentType;
    }

    public String getTrainingCode() {
        return trainingCode;
    }

    public void setTrainingCode(String trainingCode) {
        this.trainingCode = trainingCode;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }

    public boolean isTrainingDeleted() {
        return trainingDeleted;
    }

    public void setTrainingDeleted(boolean trainingDeleted) {
        this.trainingDeleted = trainingDeleted;
    }
    
}
