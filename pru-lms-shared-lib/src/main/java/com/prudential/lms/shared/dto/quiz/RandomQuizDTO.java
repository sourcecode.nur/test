package com.prudential.lms.shared.dto.quiz;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RandomQuizDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2419254367551689142L;
    private Long quizId;
    private int easyCount;
    private int mediumCount;
    private int hardCount;
    private String registrationNo;

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public int getEasyCount() {
        return easyCount;
    }

    public void setEasyCount(int easyCount) {
        this.easyCount = easyCount;
    }

    public int getMediumCount() {
        return mediumCount;
    }

    public void setMediumCount(int mediumCount) {
        this.mediumCount = mediumCount;
    }

    public int getHardCount() {
        return hardCount;
    }

    public void setHardCount(int hardCount) {
        this.hardCount = hardCount;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }        

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + easyCount;
        result = prime * result + hardCount;
        result = prime * result + mediumCount;
        result = prime * result + ((quizId == null) ? 0 : quizId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RandomQuizDTO other = (RandomQuizDTO) obj;
        if (easyCount != other.easyCount) {
            return false;
        }
        if (hardCount != other.hardCount) {
            return false;
        }
        if (mediumCount != other.mediumCount) {
            return false;
        }
        if (quizId == null) {
            if (other.quizId != null) {
                return false;
            }
        } else if (!quizId.equals(other.quizId)) {
            return false;
        }
        return true;
    }

}
