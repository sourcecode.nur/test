package com.prudential.lms.shared.dto.training;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingCategoryStatus;
import com.prudential.lms.shared.dto.AuditableDTO;
import com.prudential.lms.shared.dto.SoftDeleteDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingCategoryDTO extends AuditableDTO<Long> implements SoftDeleteDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4095090352748358458L;

	private String name;
	private String code;
	private String description;
	private TrainingCategoryStatus status;

	private Long parentId;
	private String parentName;
	private String parentCode;
	private String parentDescription;

	private Set<TrainingCategoryDTO> children = new HashSet<>();
	private String fileName;
	private String thumbnail;
	private boolean deleted;
	private int trainingCount;
	private boolean draft;

	private int videoCount;

	private boolean active;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<TrainingCategoryDTO> getChildren() {
		return children;
	}

	public void setChildren(Set<TrainingCategoryDTO> children) {
		this.children = children;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getParentDescription() {
		return parentDescription;
	}

	public void setParentDescription(String parentDescription) {
		this.parentDescription = parentDescription;
	}

	public TrainingCategoryStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingCategoryStatus status) {
		this.status = status;
	}

	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getTrainingCount() {
		return trainingCount;
	}

	public void setTrainingCount(int trainingCount) {
		this.trainingCount = trainingCount;
	}

	public int getVideoCount() {
		return videoCount;
	}

	public void setVideoCount(int videoCount) {
		this.videoCount = videoCount;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	 
}
