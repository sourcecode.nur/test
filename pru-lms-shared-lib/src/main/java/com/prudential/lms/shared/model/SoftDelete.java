package com.prudential.lms.shared.model;

public interface SoftDelete {

	boolean isDeleted();

	void setDeleted(boolean deleted);
	
}
