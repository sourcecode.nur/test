package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingContentType;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingContentDTO extends AuditableDTO<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8541504102003380803L;

	private TrainingContentType type;
	private Long trainingId;
	private Long libraryId;
	private String libraryTitle;
	private String libraryDescription;
	private String libraryMimeType;
	private String libraryPath;
	private String libraryUri;
	private String libraryThumbnail;
	private String libraryFileName;

	public TrainingContentType getType() {
		return type;
	}

	public void setType(TrainingContentType type) {
		this.type = type;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public Long getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(Long libraryId) {
		this.libraryId = libraryId;
	}

	public String getLibraryTitle() {
		return libraryTitle;
	}

	public void setLibraryTitle(String libraryTitle) {
		this.libraryTitle = libraryTitle;
	}

	public String getLibraryDescription() {
		return libraryDescription;
	}

	public void setLibraryDescription(String libraryDescription) {
		this.libraryDescription = libraryDescription;
	}

	public String getLibraryMimeType() {
		return libraryMimeType;
	}

	public void setLibraryMimeType(String libraryMimeType) {
		this.libraryMimeType = libraryMimeType;
	}

	public String getLibraryPath() {
		return libraryPath;
	}

	public void setLibraryPath(String libraryPath) {
		this.libraryPath = libraryPath;
	}

	public String getLibraryUri() {
		return libraryUri;
	}

	public void setLibraryUri(String libraryUri) {
		this.libraryUri = libraryUri;
	}

	public String getLibraryThumbnail() {
		return libraryThumbnail;
	}

	public void setLibraryThumbnail(String libraryThumbnail) {
		this.libraryThumbnail = libraryThumbnail;
	}

	public String getLibraryFileName() {
		return libraryFileName;
	}
	

	public void setLibraryFileName(String libraryFileName) {
		this.libraryFileName = libraryFileName;
	}
	

}
