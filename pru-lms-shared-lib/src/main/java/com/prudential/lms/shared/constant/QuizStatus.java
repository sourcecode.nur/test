package com.prudential.lms.shared.constant;

public enum QuizStatus {

	APPROVE, REQUEST, REVISE, REJECT

}
