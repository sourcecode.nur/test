package com.prudential.lms.shared.dto.sysref;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class SystemRefGroupDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1572904256130424855L;

	private Long id;
	private String code;
	private String description;

	private Set<SystemRefDetailDTO> details = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<SystemRefDetailDTO> getDetails() {
		return details;
	}

	public void setDetails(Set<SystemRefDetailDTO> details) {
		this.details = details;
	}

	public void addDetails(SystemRefDetailDTO... details) {
		for (SystemRefDetailDTO dtl : details) {
			getDetails().add(dtl);
		}
	}

}
