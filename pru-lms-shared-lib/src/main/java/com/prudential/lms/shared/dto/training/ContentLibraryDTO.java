package com.prudential.lms.shared.dto.training;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class ContentLibraryDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6277477943685098686L;
	private String title;
	private String description;
	private String mimeType;
	private String path;
	private String baseUri;
	private String uri;
	private String thumbnail;
	private String contentPDF;
	private String fileName;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getBaseUri() {
		return baseUri;
	}

	public void setBaseUri(String baseUri) {
		this.baseUri = baseUri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getContentPDF() {
		return contentPDF;
	}

	public void setContentPDF(String contentPDF) {
		this.contentPDF = contentPDF;
	}

	public String getFileName() {
		return fileName;
	}
	

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	

}
