package com.prudential.lms.shared.dto.training;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class CurriculumDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -877210060112859568L;

	private String name;
	private String code;
	private String description;
	private String fileName;
	private String thumbnail;
	private boolean draft;
	private Set<TrainingCurriculumDTO> trainings = new HashSet<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<TrainingCurriculumDTO> getTrainings() {
		return trainings;
	}

	public void setTrainings(Set<TrainingCurriculumDTO> trainings) {
		this.trainings = trainings;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}

	
}
