package com.prudential.lms.shared.constant;

public enum ContentType {
	PDF, VIDEO, ELEARNING
}
