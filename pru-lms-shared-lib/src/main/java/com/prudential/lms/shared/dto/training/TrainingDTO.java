package com.prudential.lms.shared.dto.training;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.ContentType;
import com.prudential.lms.shared.constant.TrainingStatus;
import com.prudential.lms.shared.constant.TrainingType;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6312916248399967100L;

	private String code;
	private String name;
	private String description;

	// TrainingCategory
	private Long trainingCategoryId;
	private String trainingCategoryCode;
	private String trainingCategoryName;
	private String traingCategoryDescription;

	private List<TrainingCurriculumDTO> curriculums = new ArrayList<>();
	private boolean deleted;
	private boolean refreshment;
	private TrainingType trainingType;
	private TrainingStatus status;
	private String fileName;
	private String thumbnail;
	private int cpdPoint;
	private ContentType trainingContentType;
	private boolean draft;
	private boolean preEqualPost;

	// Training PRE & POST Test
	private List<TrainingTestDTO> trainingTests = new ArrayList<>();

	// Training Requirements
	private int minAgentAge;
	private int minCasePoint;
	private BigDecimal minAPIPoint;
	private boolean agentUnderReview;
	private List<AgentTypeRequirementDTO> agentTypeReqs = new ArrayList<>();
	private List<PreRequiredTrainingDTO> preReqTrainings = new ArrayList<>();

	// Training Contents
	private List<TrainingContentDTO> contents = new ArrayList<>();

	private boolean active;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getTrainingCategoryId() {
		return trainingCategoryId;
	}

	public void setTrainingCategoryId(Long trainingCategoryId) {
		this.trainingCategoryId = trainingCategoryId;
	}

	public String getTrainingCategoryCode() {
		return trainingCategoryCode;
	}

	public void setTrainingCategoryCode(String trainingCategoryCode) {
		this.trainingCategoryCode = trainingCategoryCode;
	}

	public String getTrainingCategoryName() {
		return trainingCategoryName;
	}

	public void setTrainingCategoryName(String trainingCategoryName) {
		this.trainingCategoryName = trainingCategoryName;
	}

	public String getTraingCategoryDescription() {
		return traingCategoryDescription;
	}

	public void setTraingCategoryDescription(String traingCategoryDescription) {
		this.traingCategoryDescription = traingCategoryDescription;
	}

	public TrainingType getTrainingType() {
		return trainingType;
	}

	public void setTrainingType(TrainingType trainingType) {
		this.trainingType = trainingType;
	}

	public boolean isRefreshment() {
		return refreshment;
	}

	public void setRefreshment(boolean refreshment) {
		this.refreshment = refreshment;
	}

	public TrainingStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingStatus status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getCpdPoint() {
		return cpdPoint;
	}

	public void setCpdPoint(int cpdPoint) {
		this.cpdPoint = cpdPoint;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public List<TrainingCurriculumDTO> getCurriculums() {
		return curriculums;
	}

	public void setCurriculums(List<TrainingCurriculumDTO> curriculums) {
		this.curriculums = curriculums;
	}

	public int getMinAgentAge() {
		return minAgentAge;
	}

	public void setMinAgentAge(int minAgentAge) {
		this.minAgentAge = minAgentAge;
	}

	public int getMinCasePoint() {
		return minCasePoint;
	}

	public void setMinCasePoint(int minCasePoint) {
		this.minCasePoint = minCasePoint;
	}

	public BigDecimal getMinAPIPoint() {
		return minAPIPoint;
	}

	public void setMinAPIPoint(BigDecimal minAPIPoint) {
		this.minAPIPoint = minAPIPoint;
	}

	public boolean isAgentUnderReview() {
		return agentUnderReview;
	}

	public void setAgentUnderReview(boolean agentUnderReview) {
		this.agentUnderReview = agentUnderReview;
	}

	public List<AgentTypeRequirementDTO> getAgentTypeReqs() {
		return agentTypeReqs;
	}

	public void setAgentTypeReqs(List<AgentTypeRequirementDTO> agentTypeReqs) {
		this.agentTypeReqs = agentTypeReqs;
	}

	public List<PreRequiredTrainingDTO> getPreReqTrainings() {
		return preReqTrainings;
	}

	public void setPreReqTrainings(List<PreRequiredTrainingDTO> preReqTrainings) {
		this.preReqTrainings = preReqTrainings;
	}

	public List<TrainingContentDTO> getContents() {
		return contents;
	}

	public void setContents(List<TrainingContentDTO> contents) {
		this.contents = contents;
	}

	public ContentType getTrainingContentType() {
		return trainingContentType;
	}

	public void setTrainingContentType(ContentType trainingContentType) {
		this.trainingContentType = trainingContentType;
	}

	public List<TrainingTestDTO> getTrainingTests() {
		return trainingTests;
	}

	public void setTrainingTests(List<TrainingTestDTO> trainingTests) {
		this.trainingTests = trainingTests;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}

	public boolean isPreEqualPost() {
		return preEqualPost;
	}

	public void setPreEqualPost(boolean preEqualPost) {
		this.preEqualPost = preEqualPost;
	}

}
