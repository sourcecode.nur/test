package com.prudential.lms.shared.dto.training;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class AgentDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6993978757468943710L;

	private String code;
	private String name;
	private Date startLicenseDate;
	private Date endLicenseDate;
	private Long agentGroupId;
	private boolean groupLeader;
	private List<TrainingCPDDTO> cpds;
	private TrainingRegistrationDTO registration;

	private Date birthDate;
	private int age;
	private int casePoint;
	private BigDecimal apiPoint;
	private boolean underReview;
	private String levelType;

	private String officeCode;
	private String officeName;
	private String radd;
	private String ads;
	
	private int cpdRegister;
	private int cpdAAJI;
	
	private String leaderCode;
	private String leaderName;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartLicenseDate() {
		return startLicenseDate;
	}

	public void setStartLicenseDate(Date startLicenseDate) {
		this.startLicenseDate = startLicenseDate;
	}

	public Date getEndLicenseDate() {
		return endLicenseDate;
	}

	public void setEndLicenseDate(Date endLicenseDate) {
		this.endLicenseDate = endLicenseDate;
	}

	public Long getAgentGroupId() {
		return agentGroupId;
	}

	public void setAgentGroupId(Long agentGroupId) {
		this.agentGroupId = agentGroupId;
	}

	public boolean isGroupLeader() {
		return groupLeader;
	}

	public void setGroupLeader(boolean groupLeader) {
		this.groupLeader = groupLeader;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getCasePoint() {
		return casePoint;
	}

	public void setCasePoint(int casePoint) {
		this.casePoint = casePoint;
	}

	public BigDecimal getApiPoint() {
		return apiPoint;
	}

	public void setApiPoint(BigDecimal apiPoint) {
		this.apiPoint = apiPoint;
	}

	public boolean isUnderReview() {
		return underReview;
	}

	public void setUnderReview(boolean underReview) {
		this.underReview = underReview;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getRadd() {
		return radd;
	}

	public void setRadd(String radd) {
		this.radd = radd;
	}

	public String getAds() {
		return ads;
	}

	public void setAds(String ads) {
		this.ads = ads;
	}

	public List<TrainingCPDDTO> getCpds() {
		return cpds;
	}

	public void setCpds(List<TrainingCPDDTO> cpds) {
		this.cpds = cpds;
	}

	public TrainingRegistrationDTO getRegistration() {
		return registration;
	}

	public void setRegistration(TrainingRegistrationDTO registration) {
		this.registration = registration;
	}

	public int getCpdRegister() {
		return cpdRegister;
	}
	

	public void setCpdRegister(int cpdRegister) {
		this.cpdRegister = cpdRegister;
	}
	

	public int getCpdAAJI() {
		return cpdAAJI;
	}
	

	public void setCpdAAJI(int cpdAAJI) {
		this.cpdAAJI = cpdAAJI;
	}

	public String getLeaderCode() {
		return leaderCode;
	}
	

	public void setLeaderCode(String leaderCode) {
		this.leaderCode = leaderCode;
	}
	

	public String getLeaderName() {
		return leaderName;
	}
	

	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	
	

}
