package com.prudential.lms.shared.dto.training;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingTestDTO extends AuditableDTO<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5377035479771014114L;
	private TrainingTestType type;
	private int questionCount;
	private int easyCount;
	private int mediumCount;
	private int hardCount;
	private int minimumPoint;
	private int duration;
	private int maxRemedialCount;
	private Long trainingId;
	private Long quizId;

	public TrainingTestType getType() {
		return type;
	}

	public void setType(TrainingTestType type) {
		this.type = type;
	}

	public int getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(int questionCount) {
		this.questionCount = questionCount;
	}

	public int getEasyCount() {
		return easyCount;
	}

	public void setEasyCount(int easyCount) {
		this.easyCount = easyCount;
	}

	public int getMediumCount() {
		return mediumCount;
	}

	public void setMediumCount(int mediumCount) {
		this.mediumCount = mediumCount;
	}

	public int getHardCount() {
		return hardCount;
	}

	public void setHardCount(int hardCount) {
		this.hardCount = hardCount;
	}

	public int getMinimumPoint() {
		return minimumPoint;
	}

	public void setMinimumPoint(int minimumPoint) {
		this.minimumPoint = minimumPoint;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getMaxRemedialCount() {
		return maxRemedialCount;
	}

	public void setMaxRemedialCount(int maxRemedialCount) {
		this.maxRemedialCount = maxRemedialCount;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

}
