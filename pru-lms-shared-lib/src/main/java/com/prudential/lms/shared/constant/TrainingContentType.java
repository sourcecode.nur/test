package com.prudential.lms.shared.constant;

public enum TrainingContentType {
	MAIN, SUPPORT
}
