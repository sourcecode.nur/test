package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

public class TrainingTypeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7608132989276613585L;
	private Long id;
	private String name;
	private String code;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "TrainingTypeDTO [id=" + id + ", name=" + name + ", code=" + code + "]";
	}

}
