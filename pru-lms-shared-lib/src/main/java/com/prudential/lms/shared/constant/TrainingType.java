package com.prudential.lms.shared.constant;

public enum TrainingType {
	ONLINE, CLASSROOM
}
