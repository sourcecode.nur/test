package com.prudential.lms.shared.dto.training;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.ContentType;
import com.prudential.lms.shared.constant.TrainingScheduleStatus;
import com.prudential.lms.shared.constant.TrainingType;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingScheduleDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6596096515080432438L;
	private String batchCode;
	private Date publishStartDate;
	private Date publishEndDate;
	private Date startDate;
	private Date endDate;
	private int quota;
	private int registered;
	private Boolean published;
	private TrainingScheduleStatus status;
	private Long trainingId;
	private String trainingName;
	private TrainingType trainingType;
	private String trainingCategoryName;
	private ContentType trainingContentType;
	private String trainingDescription;
	private int cpdPoint;
	private boolean draft;
	
	private List<TrainingTestDTO> trainingTests = new ArrayList<>();
	private List<TrainingContentDTO> contents = new ArrayList<>();

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getQuota() {
		return quota;
	}

	public void setQuota(int quota) {
		this.quota = quota;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public Date getPublishStartDate() {
		return publishStartDate;
	}

	public void setPublishStartDate(Date publishStartDate) {
		this.publishStartDate = publishStartDate;
	}

	public Date getPublishEndDate() {
		return publishEndDate;
	}

	public void setPublishEndDate(Date publishEndDate) {
		this.publishEndDate = publishEndDate;
	}

	public int getRegistered() {
		return registered;
	}

	public void setRegistered(int registered) {
		this.registered = registered;
	}

	public Boolean getPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	public TrainingScheduleStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingScheduleStatus status) {
		this.status = status;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public TrainingType getTrainingType() {
		return trainingType;
	}
	

	public void setTrainingType(TrainingType trainingType) {
		this.trainingType = trainingType;
	}
	

	public String getTrainingCategoryName() {
		return trainingCategoryName;
	}
	

	public void setTrainingCategoryName(String trainingCategoryName) {
		this.trainingCategoryName = trainingCategoryName;
	}

	public ContentType getTrainingContentType() {
		return trainingContentType;
	}
	

	public void setTrainingContentType(ContentType trainingContentType) {
		this.trainingContentType = trainingContentType;
	}

	public String getTrainingDescription() {
		return trainingDescription;
	}
	

	public void setTrainingDescription(String trainingDescription) {
		this.trainingDescription = trainingDescription;
	}

	public int getCpdPoint() {
		return cpdPoint;
	}
	

	public void setCpdPoint(int cpdPoint) {
		this.cpdPoint = cpdPoint;
	}

	public List<TrainingTestDTO> getTrainingTests() {
		return trainingTests;
	}
	

	public void setTrainingTests(List<TrainingTestDTO> trainingTests) {
		this.trainingTests = trainingTests;
	}
	

	public List<TrainingContentDTO> getContents() {
		return contents;
	}
	

	public void setContents(List<TrainingContentDTO> contents) {
		this.contents = contents;
	}

	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	
	 

}
