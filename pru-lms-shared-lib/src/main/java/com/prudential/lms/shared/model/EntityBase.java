package com.prudential.lms.shared.model;

public interface EntityBase<T> {

	T getId();

	void setId(T id);

}
