package com.prudential.lms.shared.constant;

public enum TrainingCPDType {

	LMS, AAJI

}
