package com.prudential.lms.shared.constant;

public enum TrainingStatus {

	APPROVE, REQUEST, REVISE, REJECT

}
