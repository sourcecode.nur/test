package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.CPDMutationOperation;

@JsonInclude(Include.NON_NULL)
public class TrainingCPDMutationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -972388643582199485L;
	private Long id;
	private Long trainingCPDId;
	private int prevPoint;
	private int deltaPoint;
	private int currentPoint;
	private CPDMutationOperation mutationOperation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTrainingCPDId() {
		return trainingCPDId;
	}

	public void setTrainingCPDId(Long trainingCPDId) {
		this.trainingCPDId = trainingCPDId;
	}

	public int getPrevPoint() {
		return prevPoint;
	}

	public void setPrevPoint(int prevPoint) {
		this.prevPoint = prevPoint;
	}

	public int getDeltaPoint() {
		return deltaPoint;
	}

	public void setDeltaPoint(int deltaPoint) {
		this.deltaPoint = deltaPoint;
	}

	public int getCurrentPoint() {
		return currentPoint;
	}

	public void setCurrentPoint(int currentPoint) {
		this.currentPoint = currentPoint;
	}

	public CPDMutationOperation getMutationOperation() {
		return mutationOperation;
	}

	public void setMutationOperation(CPDMutationOperation mutationOperation) {
		this.mutationOperation = mutationOperation;
	}

}
