package com.prudential.lms.shared.dto.training;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingCPDType;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingCPDDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2282389038098019023L;
	private int point;

	private Long agentId;
	private String agentName;
	private String agentCode;
	private Date expiredDate;
	private TrainingCPDType type;

	// Training Information
	private Long trainingId;
	private String trainingCode;
	private String trainingName;
	private Date trainingDate;

	private String passStatus;

	private List<TrainingCPDMutationDTO> cpdAAJI = new ArrayList<TrainingCPDMutationDTO>();

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public TrainingCPDType getType() {
		return type;
	}

	public void setType(TrainingCPDType type) {
		this.type = type;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public String getTrainingCode() {
		return trainingCode;
	}

	public void setTrainingCode(String trainingCode) {
		this.trainingCode = trainingCode;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	public String getPassStatus() {
		return passStatus;
	}

	public void setPassStatus(String passStatus) {
		this.passStatus = passStatus;
	}

	public List<TrainingCPDMutationDTO> getCpdAAJI() {
		return cpdAAJI;
	}

	public void setCpdAAJI(List<TrainingCPDMutationDTO> cpdAAJI) {
		this.cpdAAJI = cpdAAJI;
	}

}
