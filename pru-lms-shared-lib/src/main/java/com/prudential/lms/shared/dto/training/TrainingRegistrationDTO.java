package com.prudential.lms.shared.dto.training;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingRegistrationDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5873829204654717662L;

	private Long agentId;
	private String agentName;
	private String agentCode;
	private List<TrainingRegistrationScheduleDTO> schedules = new ArrayList<>();

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public List<TrainingRegistrationScheduleDTO> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<TrainingRegistrationScheduleDTO> schedules) {
		this.schedules = schedules;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}
	

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	

}
