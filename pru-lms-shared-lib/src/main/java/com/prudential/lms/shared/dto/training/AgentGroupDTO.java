package com.prudential.lms.shared.dto.training;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class AgentGroupDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6119635440960084338L;
    private Long id;
    private String group;
    private Set<AgentDTO> agent = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Set<AgentDTO> getAgent() {
        return agent;
    }

    public void setAgent(Set<AgentDTO> agent) {
        this.agent = agent;
    }

}
