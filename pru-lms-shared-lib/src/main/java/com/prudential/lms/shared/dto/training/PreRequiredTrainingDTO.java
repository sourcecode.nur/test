package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PreRequiredTrainingDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1798949525671154361L;
	private Long trnId;
	private String trnName;
	private String trnDesc;

	public Long getTrnId() {
		return trnId;
	}

	public void setTrnId(Long trnId) {
		this.trnId = trnId;
	}

	public String getTrnName() {
		return trnName;
	}

	public void setTrnName(String trnName) {
		this.trnName = trnName;
	}

	public String getTrnDesc() {
		return trnDesc;
	}

	public void setTrnDesc(String trnDesc) {
		this.trnDesc = trnDesc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trnDesc == null) ? 0 : trnDesc.hashCode());
		result = prime * result + ((trnId == null) ? 0 : trnId.hashCode());
		result = prime * result + ((trnName == null) ? 0 : trnName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreRequiredTrainingDTO other = (PreRequiredTrainingDTO) obj;
		if (trnDesc == null) {
			if (other.trnDesc != null)
				return false;
		} else if (!trnDesc.equals(other.trnDesc))
			return false;
		if (trnId == null) {
			if (other.trnId != null)
				return false;
		} else if (!trnId.equals(other.trnId))
			return false;
		if (trnName == null) {
			if (other.trnName != null)
				return false;
		} else if (!trnName.equals(other.trnName))
			return false;
		return true;
	}

}
