package com.prudential.lms.shared.constant;

public enum TrainingScheduleStatus {

	APPROVE, REQUEST, REVISE, REJECT,
	INPROGRESS,
	FINISH,
	CANCEL
	
}
