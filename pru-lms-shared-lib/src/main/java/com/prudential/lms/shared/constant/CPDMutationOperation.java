package com.prudential.lms.shared.constant;

public enum CPDMutationOperation {
	PLUS, MINUS
}
