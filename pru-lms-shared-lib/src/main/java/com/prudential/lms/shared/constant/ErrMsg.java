package com.prudential.lms.shared.constant;


public class ErrMsg {

	// Agent
	public static final String AGENT_TOTAL_CASE_POINT_NOT_CAPABLE_CODE = "1000";
	public static final String AGENT_TOTAL_CASE_POINT_NOT_CAPABLE_MSG = "Agent Total Case Point not capable";
	public static final String AGENT_API_POINT_NOT_CAPABLE_CODE = "1001";
	public static final String AGENT_API_POINT_NOT_CAPABLE_MSG = "Agent API Production Point not capable";
	public static final String AGENT_NOT_FOUND_CODE = "1404";
	public static final String AGENT_NOT_FOUND_MSG = "Agent not found";
	
	// Training
	public static final String TRAINING_INVALID_CODE = "2000";
	public static final String TRAINING_INVALID_MSG = "Invalid Training";
	
	// Training Schedule
	public static final String TRAINING_SCHEDULE_INVALID_CODE = "3000";
	public static final String TRAINING_SCHEDULE_INVALID_MSG = "Invalid Training Schedule";
	
	// Training Curriculum
	public static final String CURRICULUM_DELETE_HAS_TRAINING_CODE = "4000";
	public static final String CURRICULUM_DELETE_HAS_TRAINING_MSG = "Cannot Delete Curriculum which has sub Training Curriculum";
	
	
	
}
