package com.prudential.lms.shared.dto.training;

import java.io.Serializable;

public class TrainingRegistrationProgressDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -762112318192587539L;
	private Long id;
	private Long trainingRegistrationId;
	private int progressOrder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTrainingRegistrationId() {
		return trainingRegistrationId;
	}

	public void setTrainingRegistrationId(Long trainingRegistrationId) {
		this.trainingRegistrationId = trainingRegistrationId;
	}

	public int getProgressOrder() {
		return progressOrder;
	}

	public void setProgressOrder(int progressOrder) {
		this.progressOrder = progressOrder;
	}

}
