package com.prudential.lms.shared.exception;

public class LmsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6615920606449903117L;

	public LmsException(String message) {
		super(message);
	}

	public LmsException(String message, Throwable cause) {
		super(message, cause);
	}

}
