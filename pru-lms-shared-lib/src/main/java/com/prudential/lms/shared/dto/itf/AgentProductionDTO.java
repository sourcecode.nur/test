package com.prudential.lms.shared.dto.itf;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AgentProductionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6056627466293284476L;

	private String agentCode;
	private String type;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
