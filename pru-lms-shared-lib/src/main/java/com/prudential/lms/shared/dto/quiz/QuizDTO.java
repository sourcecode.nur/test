package com.prudential.lms.shared.dto.quiz;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.QuizStatus;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class QuizDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5523289935909506427L;

	private String code;
	private String name;
	private String description;
	private QuizStatus status;
	private int questionsCount;
	private int easyCount;
	private int mediumCount;
	private int hardCount;

	private Set<QuestionDTO> questions = new HashSet<>();

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<QuestionDTO> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<QuestionDTO> questions) {
		this.questions = questions;
	}

	public QuizStatus getStatus() {
		return status;
	}

	public void setStatus(QuizStatus status) {
		this.status = status;
	}

	public int getQuestionsCount() {
		return questionsCount;
	}

	public void setQuestionsCount(int questionsCount) {
		this.questionsCount = questionsCount;
	}

	@Override
	public String toString() {
		return "QuizDTO [code=" + code + ", name=" + name + ", description=" + description + ", status=" + status
				+ ", questionsCount=" + questionsCount + ", questions=" + questions + "]";
	}

	public int getEasyCount() {
		return easyCount;
	}
	

	public void setEasyCount(int easyCount) {
		this.easyCount = easyCount;
	}
	

	public int getMediumCount() {
		return mediumCount;
	}
	

	public void setMediumCount(int mediumCount) {
		this.mediumCount = mediumCount;
	}
	

	public int getHardCount() {
		return hardCount;
	}
	

	public void setHardCount(int hardCount) {
		this.hardCount = hardCount;
	}
	

}
