package com.prudential.lms.shared.constant;

public enum PassStatus {
	PASS, FAIL
}
