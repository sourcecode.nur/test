package com.prudential.lms.shared.constant;

public enum TrainingProgressStatus {

	REGISTERED(-1),
	BEGIN(0),
	PRETEST(1),
	PRETEST_FINISH(2),
	TRAINING(3),
	TRAINING_FINISH(4),
	POSTTEST(5),
	POSTTEST_FINISH(6),
	END(7);
	
	
	TrainingProgressStatus(int order){
		this.order = order;
	}
	
	private final int order;

	public int getOrder() {
		return order;
	}
	
}
