package com.prudential.lms.shared.constant;

public enum QuestionDifficulty {

	EASY(1), MEDIUM(2), HARD(3);

	QuestionDifficulty(int weight) {
		this.weight = weight;
	}

	private final int weight;

	public int getWeight() {
		return weight;
	}

}
