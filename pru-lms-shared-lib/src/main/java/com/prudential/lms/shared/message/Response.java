package com.prudential.lms.shared.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Response<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(Response.class);

	private final ObjectMapper mapper = new ObjectMapper();

	private String code;
	private String message;
	private T data;
	private ResponseEntity<T> responseEntity;
	private String jsonString;

	public Response() {
	}

	public Response(ResponseEntity<T> responseEntity) {
		super();
		this.responseEntity = responseEntity;
		this.code = responseEntity.getStatusCode().toString();
		this.message = responseEntity.getStatusCode().name();
		this.data = responseEntity.getBody();
	}

	public static Response<Object> customError(String code, String message) {
		Response<Object> response = new Response<Object>();
		response.setCode(code);
		response.setMessage(message);
		response.setData(null);
		response.setResponseEntity(new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR));
		return response;
	}

	public static Response<Object> ok() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.OK));
	}

	public static Response<Object> ok(String customMessage) {
		return ok().setCustomMessage(customMessage);
	}

	public static Response<Object> ok(Object customData) {
		return ok().setCustomData(customData);
	}

	public static Response<Object> created() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.CREATED));
	}

	public static Response<Object> created(String customMessage) {
		return created().setCustomMessage(customMessage);
	}

	public static Response<Object> created(Object customData) {
		return created().setCustomData(customData);
	}

	public static Response<Object> found() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.FOUND));
	}

	public static Response<Object> found(String customMessage) {
		return found().setCustomMessage(customMessage);
	}

	public static Response<Object> found(Object customData) {
		return found().setCustomData(customData);
	}

	public static Response<Object> notFound() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));
	}

	public static Response<Object> notFound(String customMessage) {
		return notFound().setCustomMessage(customMessage);
	}

	public static Response<Object> badRequest() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.BAD_REQUEST));
	}

	public static Response<Object> badRequest(String customMessage) {
		return badRequest().setCustomMessage(customMessage);
	}

	public static Response<Object> internalServerError() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR));
	}

	public static Response<Object> internalServerError(String customMessage) {
		return internalServerError().setCustomMessage(customMessage);
	}

	public static Response<Object> unauthorized() {
		return new Response<Object>(new ResponseEntity<Object>(HttpStatus.UNAUTHORIZED));
	}

	public static Response<Object> unauthorized(String customMessage) {
		return unauthorized().setCustomMessage(customMessage);
	}

	public String toJSONString() {
		String json = null;
		try {
			json = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			LOGGER.error("Failed parsing Response", e);
		}
		return json;
	}

	public Response<T> setCustomMessage(String customMessage) {
		this.message = customMessage;
		return this;
	}

	public Response<T> setCustomData(T customData) {
		this.data = customData;
		return this;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@JsonIgnore
	public ResponseEntity<T> getResponseEntity() {
		return responseEntity;
	}

	public void setResponseEntity(ResponseEntity<T> responseEntity) {
		this.responseEntity = responseEntity;
	}

	@JsonIgnore
	public String getJsonString() {
		String json = "{}";

		try {
			json = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		setJsonString(json);

		return jsonString;
	}

	private void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

}