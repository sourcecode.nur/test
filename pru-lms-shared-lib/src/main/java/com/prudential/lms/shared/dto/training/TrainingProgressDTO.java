package com.prudential.lms.shared.dto.training;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.dto.AuditableDTO;

@JsonInclude(Include.NON_NULL)
public class TrainingProgressDTO extends AuditableDTO<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -762112318192587539L;
	
	private Long trainingRegistrationId;
	private String trainingRegistrationNo;
	private TrainingProgressStatus status;
	private int progressOrder;
	private int subProgressOrder;

	public Long getTrainingRegistrationId() {
		return trainingRegistrationId;
	}

	public void setTrainingRegistrationId(Long trainingRegistrationId) {
		this.trainingRegistrationId = trainingRegistrationId;
	}

	public int getProgressOrder() {
		return progressOrder;
	}

	public void setProgressOrder(int progressOrder) {
		this.progressOrder = progressOrder;
	}

	public TrainingProgressStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingProgressStatus status) {
		this.status = status;
	}

	public String getTrainingRegistrationNo() {
		return trainingRegistrationNo;
	}

	public void setTrainingRegistrationNo(String trainingRegistrationNo) {
		this.trainingRegistrationNo = trainingRegistrationNo;
	}

	public int getSubProgressOrder() {
		return subProgressOrder;
	}

	public void setSubProgressOrder(int subProgressOrder) {
		this.subProgressOrder = subProgressOrder;
	}

}
