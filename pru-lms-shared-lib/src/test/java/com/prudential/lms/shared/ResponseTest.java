package com.prudential.lms.shared;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prudential.lms.shared.message.Response;

public class ResponseTest {
	
	public static void main(String[] args) {
		System.out.println(Response.ok("ASIK").toJSONString());
	}

//	@Test
	public void to_json_string() throws JsonParseException, JsonMappingException, IOException{
		String customMessage = "Unauthorized Request";
		String jsonResponse = Response.unauthorized(customMessage).toJSONString();
		ObjectMapper mapper = new ObjectMapper();
		Response response = mapper.readValue(jsonResponse, Response.class);
		Assert.assertNotNull(response);
		Assert.assertEquals(customMessage, response.getMessage());
	}
	
}
