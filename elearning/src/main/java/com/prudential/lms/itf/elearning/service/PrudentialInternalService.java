
package com.prudential.lms.itf.elearning.service;

import java.math.BigDecimal;

import com.prudential.lms.shared.dto.training.AgentCourseReportDTO;

public interface PrudentialInternalService {

	String sendAgentCourseReport(String authBearer, AgentCourseReportDTO agentCourseRpt);
	
	BigDecimal getAgentProductionApeYtd(String authBearer, String agentCode);

	Integer getAgentTotalCasePoint(String authBearer, String agentCode);

}
