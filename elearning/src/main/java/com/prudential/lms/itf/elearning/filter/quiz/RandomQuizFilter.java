package com.prudential.lms.itf.elearning.filter.quiz;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import com.prudential.lms.itf.elearning.filter.LMSBaseFilter;
import com.prudential.lms.itf.elearning.service.QuizService;
import com.prudential.lms.itf.elearning.service.TrainingService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.quiz.QuestionDTO;
import com.prudential.lms.shared.dto.quiz.RandomQuizDTO;
import com.prudential.lms.shared.dto.training.TrainingDTO;
import com.prudential.lms.shared.dto.training.TrainingRegistrationScheduleDTO;
import java.io.BufferedReader;
import org.apache.commons.io.IOUtils;

@Component
public class RandomQuizFilter extends LMSBaseFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RandomQuizFilter.class);

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private QuizService quizService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private RestTemplate restTemplate;

    private Long trnRegSchId;

    private String registrationNo;

    @Override
    public Object run() {
        RequestContext ctx = getRequestContext();
        ctx.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");
        ctx.setSendZuulResponse(false);
        List<QuestionDTO> result = new ArrayList<>();

        try {
            String authBearer = getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION);
            TrainingRegistrationScheduleDTO trnRegSchDto = trainingService.getTrainingRegistrationScheduleByRegistrationNo(authBearer, registrationNo);
            
            
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            ctx.setResponseStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            ctx.setResponseBody("{\"error\" : \"" + e.getMessage() + "\"}");
            return null;
        }

        ctx.setSendZuulResponse(true);
        return null;
    }

    @Override
    public boolean shouldFilter() {
        HttpServletRequest request = getRequest();
        String uriToFilter = "/quiz/random";
        LOGGER.debug("RandomQuizFilter.uriToFilter: {}", uriToFilter);
        try {
            String uri = getURI();
            String url = (null != getRequestHeader("X-Requested-URL")) ? getRequestHeader("X-Requested-URL") : "";
            LOGGER.debug("uri: {} - url: {}", uri, url);
            if (uri.contains(uriToFilter) || url.contains(uriToFilter)) {
                BufferedReader reader = request.getReader();
                
                if(reader == null){
                    return false;
                }
                
                String body = IOUtils.toString(reader);
                LOGGER.debug("body: {}", body);
                RandomQuizDTO dto = mapper.readValue(body, RandomQuizDTO.class);
                if(dto != null){
                    setRegistrationNo(dto.getRegistrationNo());
                }else{
                    return false;
                }
                
                String authBearer = getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION);
                TrainingDTO trnDto = trainingService.getTrainingByRegistrationNo(authBearer, registrationNo);
                if (trnDto != null) {
                    return trnDto.isPreEqualPost();
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return false;
    }

    @Override
    public int filterOrder() {
        return 90;
    }

    @Override
    public String filterType() {
        return PRE;
    }

    public Long getTrnRegSchId() {
        return trnRegSchId;
    }

    public void setTrnRegSchId(Long trnRegSchId) {
        this.trnRegSchId = trnRegSchId;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

}
