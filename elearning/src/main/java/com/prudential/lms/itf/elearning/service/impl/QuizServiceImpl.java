package com.prudential.lms.itf.elearning.service.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prudential.lms.itf.elearning.service.QuizService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.quiz.QuizDTO;
import com.prudential.lms.shared.dto.training.TrainingDTO;

@Service
public class QuizServiceImpl implements QuizService {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuizServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	@Value("${app.service.base-proxy-url}")
	private String baseProxyURL;

	@Value("${zuul.routes.lms-quiz.url}")
	private String quizUrl;

	@Override
	public QuizDTO updateQuiz(String authBearer, QuizDTO dto) {

		Long quizId = dto.getId();
		QuizDTO result = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
		headers.add("Content-Type", "application/json");

		try {
			HttpEntity<QuizDTO> entity = new HttpEntity<QuizDTO>(dto, headers);
			String url = quizUrl.concat("/quiz/").concat(String.valueOf(quizId));
			LOGGER.debug("PATCH " + url);
			ResponseEntity<String> res = restTemplate.exchange(url, HttpMethod.PATCH, entity, String.class);

			if (null != res) {
				HttpStatus httpStatus = res.getStatusCode();
				if (httpStatus.equals(HttpStatus.OK)) {
					JSONObject json = new JSONObject(res.getBody());
					result = mapper.readValue(String.valueOf(json.get("data")), QuizDTO.class);
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}

		return result;
	}

}
