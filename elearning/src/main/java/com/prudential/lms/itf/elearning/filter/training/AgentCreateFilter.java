package com.prudential.lms.itf.elearning.filter.training;

import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.http.ServletInputStreamWrapper;
import com.prudential.lms.itf.elearning.filter.LMSBaseFilter;
import com.prudential.lms.itf.elearning.service.AgentService;
import com.prudential.lms.itf.elearning.service.PrudentialInternalService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.training.AgentDTO;
import com.prudential.lms.shared.message.Response;

@Component
public class AgentCreateFilter extends LMSBaseFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgentCreateFilter.class);

	@Autowired
	private AgentService agentService;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private PrudentialInternalService pruInternalService;

	@Override
	public Object run() {

		RequestContext ctx = getRequestContext();
		HttpServletRequest request = getRequest();

		try {
			String body = IOUtils.toString(request.getReader());

			AgentDTO agent = mapper.readValue(body, AgentDTO.class);

			String agentCode = agent.getCode();
			if (StringUtils.isEmpty(agentCode)) {
				ctx.setSendZuulResponse(false);
				ctx.setResponseStatusCode(HttpStatus.OK.value());
				ctx.setResponseBody(Response.badRequest("Invalid Agent Code").toJSONString());
				return null;
			}

			String headerAgentCode = getRequestHeader("Agent-Number");
			LOGGER.debug("headerAgentCode: {}", headerAgentCode);
			if (null == headerAgentCode) {
				ctx.setSendZuulResponse(false);
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
				return null;
			} else {

				if (!headerAgentCode.equalsIgnoreCase(agentCode)) {
					ctx.setSendZuulResponse(false);
					ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
					ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
					return null;
				}

			}

			BigDecimal agentProductionApeYtd = pruInternalService
					.getAgentProductionApeYtd(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), agentCode);
			agent.setApiPoint(null != agentProductionApeYtd ? agentProductionApeYtd : new BigDecimal(0));

			Integer agentTotalCasePoint = pruInternalService
					.getAgentTotalCasePoint(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), agentCode);
			agent.setCasePoint(null != agentTotalCasePoint ? agentTotalCasePoint : 0);

			String newBody = mapper.writeValueAsString(agent);
			byte[] bytes = newBody.getBytes("UTF-8");

			ctx.setRequest(new HttpServletRequestWrapper(getRequest()) {

				@Override
				public ServletInputStream getInputStream() throws IOException {
					return new ServletInputStreamWrapper(bytes);
				}

				@Override
				public int getContentLength() {
					return bytes.length;
				}

				@Override
				public long getContentLengthLong() {
					return bytes.length;
				}

			});

		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}

		return null;
	}

	@Override
	public boolean shouldFilter() {

		String uri = getURI();
		if (uri.endsWith("/agent"))
			return true;

		String url = getRequest().getHeader("X-Requested-URL");
		if (StringUtils.isNotEmpty(url)) {
			if (url.endsWith("/agent"))
				return true;
		}

		return false;
	}

	@Override
	public int filterOrder() {
		return 99;
	}

	@Override
	public String filterType() {
		return PRE;
	}

}
