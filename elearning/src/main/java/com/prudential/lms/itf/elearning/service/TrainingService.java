package com.prudential.lms.itf.elearning.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.prudential.lms.shared.dto.training.TrainingDTO;
import com.prudential.lms.shared.dto.training.TrainingRegistrationScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingTestDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;

public interface TrainingService {

	TrainingTestResultDTO addResult(String authBearer, TrainingTestResultDTO dto) throws LmsException, JsonParseException, JsonMappingException, IOException;

	TrainingDTO getTrainingById(String authBearer, Long id);

	TrainingScheduleDTO getTrainingScheduleById(String authBearer, Long id);

	List<TrainingTestDTO> getTrainingTestByQuizId(String authBearer, Long id);
	
	TrainingDTO getTrainingByTrainingRegistrationScheduleId(String authBearer, Long id);
	
	TrainingDTO getTrainingByRegistrationNo(String authBearer, String registrationNo);
        
        TrainingRegistrationScheduleDTO getTrainingRegistrationScheduleByRegistrationNo(String authBearer, String registrationNo);
        
        TrainingTestResultDTO getTrainingTestResultByRegisterScheduleId(String authBearer, Long id);

}
