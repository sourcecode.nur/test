package com.prudential.lms.itf.elearning.service;

public interface AuthenticationService {

	boolean validToken(String paramString);

}
