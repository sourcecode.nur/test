package com.prudential.lms.itf.elearning.filter.training;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import com.prudential.lms.itf.elearning.filter.LMSBaseFilter;
import com.prudential.lms.itf.elearning.service.AgentService;
import com.prudential.lms.itf.elearning.service.PrudentialInternalService;
import com.prudential.lms.itf.elearning.service.TrainingService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.ErrMsg;
import com.prudential.lms.shared.dto.training.AgentDTO;
import com.prudential.lms.shared.dto.training.TrainingDTO;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.message.Response;

@Component
public class TrainingRegistrationFilter extends LMSBaseFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingRegistrationFilter.class);

	@Autowired
	private AgentService agentService;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private PrudentialInternalService pruInternalService;

	@Autowired
	private TrainingService trainingService;

	private String agentCode;

	private Long scheduleId;

	@Override
	public Object run() {

		LOGGER.debug("TrainingRegistrationFilter.run");
		RequestContext ctx = getRequestContext();
		ctx.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");
		ctx.setSendZuulResponse(false);

		try {

			String headerAgentCode = getRequestHeader("Agent-Number");
			LOGGER.debug("headerAgentCode: {}", headerAgentCode);
			if (null == headerAgentCode) {
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
				return null;
			} else {

				if (!headerAgentCode.equalsIgnoreCase(getAgentCode())) {
					ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
					ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
					return null;
				}

			}

			AgentDTO agent = agentService.findByCode(getAgentCode());
			if (null == agent) {
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(mapper.writeValueAsString(
						Response.customError(ErrMsg.AGENT_NOT_FOUND_CODE, ErrMsg.AGENT_NOT_FOUND_MSG)));
				return null;
			}

			TrainingScheduleDTO trnSchedule = trainingService
					.getTrainingScheduleById(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), getScheduleId());
			if (null == trnSchedule) {
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(mapper.writeValueAsString(Response
						.customError(ErrMsg.TRAINING_SCHEDULE_INVALID_CODE, ErrMsg.TRAINING_SCHEDULE_INVALID_MSG)));
				return null;
			}

			TrainingDTO training = trainingService.getTrainingById(
					getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), trnSchedule.getTrainingId());
			if (null == training) {
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(mapper.writeValueAsString(
						Response.customError(ErrMsg.TRAINING_INVALID_CODE, ErrMsg.TRAINING_INVALID_MSG)));
				return null;
			}

			BigDecimal agentProductionApeYtd = pruInternalService
					.getAgentProductionApeYtd(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), agentCode);
			if (null == agentProductionApeYtd)
				agentProductionApeYtd = new BigDecimal(0);
			if (agentProductionApeYtd.compareTo(training.getMinAPIPoint()) < 0) {
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(mapper.writeValueAsString(Response
						.customError(ErrMsg.AGENT_API_POINT_NOT_CAPABLE_CODE, ErrMsg.AGENT_API_POINT_NOT_CAPABLE_MSG)));
				return null;
			}

			Integer agentTotalCasePoint = pruInternalService
					.getAgentTotalCasePoint(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), agentCode);
			if (null == agentTotalCasePoint)
				agentTotalCasePoint = 0;
			if (agentTotalCasePoint < training.getMinCasePoint()) {
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(
						mapper.writeValueAsString(Response.customError(ErrMsg.AGENT_TOTAL_CASE_POINT_NOT_CAPABLE_CODE,
								ErrMsg.AGENT_TOTAL_CASE_POINT_NOT_CAPABLE_MSG)));
				return null;
			}

		} catch (JsonProcessingException e) {
			LOGGER.error(e.getMessage(), e);
			ctx.setResponseStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			ctx.setResponseBody(Response.internalServerError(e.getMessage()).toJSONString());
			return null;
		}

		ctx.setSendZuulResponse(true);
		return null;
	}

	@Override
	public boolean shouldFilter() {

		String uriToFilter = "/training-registration/register/by-agent-code";

		String uri = getURI();
		if (uri.contains(uriToFilter)) {
			String[] params = uri.split("/");
			setAgentCode(params[params.length - 2]);
			setScheduleId(Long.valueOf(params[params.length - 1]));
			return true;
		}

		String url = getRequest().getHeader("X-Requested-URL");
		if (StringUtils.isNotEmpty(url)) {
			if (url.contains(uriToFilter)) {
				String[] params = uri.split("/");
				setAgentCode(params[params.length - 2]);
				setScheduleId(Long.valueOf(params[params.length - 1]));
				return true;
			}
		}

		return false;
	}

	@Override
	public int filterOrder() {
		return 99;
	}

	@Override
	public String filterType() {
		return PRE;
	}

	public AgentService getAgentService() {
		return agentService;
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

}
