package com.prudential.lms.itf.elearning.service;

import com.prudential.lms.shared.dto.training.AgentDTO;

public interface AgentService {

	AgentDTO findByCode(String agentCode);

}
