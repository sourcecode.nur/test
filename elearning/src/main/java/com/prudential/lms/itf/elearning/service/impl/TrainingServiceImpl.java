package com.prudential.lms.itf.elearning.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.prudential.lms.itf.elearning.service.TrainingService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.training.TrainingDTO;
import com.prudential.lms.shared.dto.training.TrainingRegistrationScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingTestDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;

@Service
public class TrainingServiceImpl implements TrainingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    @Value("${app.service.base-proxy-url}")
    private String baseProxyURL;

    @Value("${zuul.routes.lms-training.url}")
    private String trainingUrl;

    @Override
    public TrainingTestResultDTO addResult(String authBearer, TrainingTestResultDTO dto) throws LmsException, JsonParseException, JsonMappingException, IOException {

        TrainingTestResultDTO result = null;
        String strDto = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training-test/add-result");
        headers.add("Content-Type", "application/json");

//		try {
        HttpEntity<TrainingTestResultDTO> entity = new HttpEntity<TrainingTestResultDTO>(dto, headers);
        ResponseEntity<String> res = restTemplate.exchange(baseProxyURL, HttpMethod.POST, entity, String.class);

        if (null != res) {
            HttpStatus httpStatus = res.getStatusCode();
            if (httpStatus.equals(HttpStatus.OK)) {
                strDto = res.getBody();
                LOGGER.debug("result: {}", strDto);
                JSONObject jres = new JSONObject(strDto);

                String code = jres.getString("code");
                String message = jres.getString("message");
                if (!"201".equals(code)) {
                    throw new LmsException(message);
                }

                JSONObject data = jres.getJSONObject("data");
                LOGGER.debug("data: {}", data.toString(1));
                result = mapper.readValue(data.toString(), TrainingTestResultDTO.class);
            }
        }

//		} catch (Exception e) {
//			LOGGER.error(e.getMessage());
//		}
        return result;
    }

    @Override
    public List<TrainingTestDTO> getTrainingTestByQuizId(String authBearer, Long id) {

        List<TrainingTestDTO> result = null;

        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training-test/find-by-quiz/".concat(String.valueOf(id)));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String url = trainingUrl.concat("/training-test/find-by-quiz/").concat(String.valueOf(id));
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    int code = json.getInt("code");
                    if (code == 302) {
                        result = new ArrayList<TrainingTestDTO>();
                        JSONArray jarr = json.getJSONArray("data");
                        for (int i = 0; i < jarr.length(); i++) {
                            JSONObject o = jarr.getJSONObject(i);
                            result.add(mapper.readValue(o.toString(), TrainingTestDTO.class));
                        }
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }

    @Override
    public TrainingDTO getTrainingById(String authBearer, Long id) {

        TrainingDTO result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training/".concat(String.valueOf(id)));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // ResponseEntity<String> res = restTemplate.exchange(baseProxyURL,
            // HttpMethod.GET, entity, String.class);
            String url = trainingUrl.concat("/training/").concat(String.valueOf(id));
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    result = mapper.readValue(String.valueOf(json.get("data")), TrainingDTO.class);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;

    }

    @Override
    public TrainingScheduleDTO getTrainingScheduleById(String authBearer, Long id) {

        TrainingScheduleDTO result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training-schedule/".concat(String.valueOf(id)));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // ResponseEntity<String> res = restTemplate.exchange(baseProxyURL,
            // HttpMethod.GET, entity, String.class);
            String url = trainingUrl.concat("/training-schedule/").concat(String.valueOf(id));
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    result = mapper.readValue(String.valueOf(json.get("data")), TrainingScheduleDTO.class);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }

    @Override
    public TrainingDTO getTrainingByTrainingRegistrationScheduleId(String authBearer, Long id) {
        TrainingDTO result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training/by-trn-reg-sch-id".concat(String.valueOf(id)));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // ResponseEntity<String> res = restTemplate.exchange(baseProxyURL,
            // HttpMethod.GET, entity, String.class);
            String url = trainingUrl.concat("/training/by-trn-reg-sch-id/").concat(String.valueOf(id));
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    result = mapper.readValue(String.valueOf(json.get("data")), TrainingDTO.class);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;

    }
//

    @Override
    public TrainingDTO getTrainingByRegistrationNo(String authBearer, String registrationNo) {
        TrainingDTO result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training/by-registration-no/".concat(registrationNo));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // ResponseEntity<String> res = restTemplate.exchange(baseProxyURL,
            // HttpMethod.GET, entity, String.class);
            String url = trainingUrl.concat("/training/by-registration-no/").concat(registrationNo);
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    result = mapper.readValue(String.valueOf(json.get("data")), TrainingDTO.class);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }

    @Override
    public TrainingRegistrationScheduleDTO getTrainingRegistrationScheduleByRegistrationNo(String authBearer, String registrationNo) {
        TrainingRegistrationScheduleDTO result = null;
        
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training-registration/registration-schedule/by-registration-no/".concat(registrationNo));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // ResponseEntity<String> res = restTemplate.exchange(baseProxyURL,
            // HttpMethod.GET, entity, String.class);
            String url = trainingUrl.concat("/lms/training/training-registration/registration-schedule/by-registration-no/").concat(registrationNo);
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    result = mapper.readValue(String.valueOf(json.get("data")), TrainingRegistrationScheduleDTO.class);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return result;
    }

    @Override
    public TrainingTestResultDTO getTrainingTestResultByRegisterScheduleId(String authBearer, Long id) {
        TrainingTestResultDTO result = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
        headers.add("X-Requested-URL", "/lms/training/training-test/get-training-test-result/by-register-schedule-id/".concat(id.toString()));
        headers.add("Content-Type", "application/json");

        try {
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            // ResponseEntity<String> res = restTemplate.exchange(baseProxyURL,
            // HttpMethod.GET, entity, String.class);
            String url = trainingUrl.concat("/lms/training/training-test/get-training-test-result/by-register-schedule-id/").concat(id.toString());
            ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);

            if (null != res) {
                HttpStatus httpStatus = res.getStatusCode();
                if (httpStatus.equals(HttpStatus.OK)) {
                    JSONObject json = new JSONObject(res.getBody());
                    result = mapper.readValue(String.valueOf(json.get("data")), TrainingTestResultDTO.class);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }

}
