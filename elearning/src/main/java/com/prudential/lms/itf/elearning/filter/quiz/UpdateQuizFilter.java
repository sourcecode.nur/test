package com.prudential.lms.itf.elearning.filter.quiz;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import com.prudential.lms.itf.elearning.filter.LMSBaseFilter;
import com.prudential.lms.itf.elearning.service.QuizService;
import com.prudential.lms.itf.elearning.service.TrainingService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.quiz.QuizDTO;
import com.prudential.lms.shared.dto.training.TrainingTestDTO;
import com.prudential.lms.shared.message.Response;

@Component
public class UpdateQuizFilter extends LMSBaseFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateQuizFilter.class);

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private QuizService quizService;

    @Autowired
    private ObjectMapper mapper;

    private Long quizId;

    private String quizName;

    @Override
    public Object run() {

        LOGGER.debug("UpdateQuizFilter.run");
        RequestContext ctx = getRequestContext();
        ctx.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");
        ctx.setSendZuulResponse(false);

        try {

            List<TrainingTestDTO> trainingTests = trainingService
                    .getTrainingTestByQuizId(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), quizId);
            if (null != trainingTests) {
                ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
                ctx.setResponseBody(mapper.writeValueAsString(Response.badRequest("Quiz Already Used")));
                return null;
            }

            QuizDTO qdto = new QuizDTO();
            qdto.setId(getQuizId());
            qdto.setName(getQuizName());
            quizService.updateQuiz(getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION), qdto);

        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage());
            ctx.setResponseStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            ctx.setResponseBody("{\"error\" : \"" + e.getMessage() + "\"}");
            return null;
        }

        ctx.setSendZuulResponse(true);
        return null;
    }

    @Override
    public boolean shouldFilter() {

        HttpServletRequest request = getRequest();
        String uriToFilter = "/quiz/upload/update";
        LOGGER.debug("UpdateQuizFilter.uriToFilter: {}", uriToFilter);

        try {

            String uri = getURI();
            LOGGER.debug("UpdateQuizFilter.uri: {}", uri);
            if (uri.contains(uriToFilter)) {

//				String id = IOUtils.toString(request.getPart("id").getInputStream(), StandardCharsets.UTF_8);
//				LOGGER.debug("UpdateQuizFilter.id: {}", id);
//				if (StringUtils.isEmpty(id))
//					return false;
//				setQuizId(Long.valueOf(id));
//				
//				String name = IOUtils.toString(request.getPart("name").getInputStream(), StandardCharsets.UTF_8);
//				LOGGER.debug("UpdateQuizFilter.name: {}", name);
//				setQuizName(name);
                FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);
                Iterator items = upload.parseRequest(request).iterator();

                while (items.hasNext()) {
                    FileItem thisItem = (FileItem) items.next();
                    if (thisItem.isFormField()) {
                        if (thisItem.getFieldName().equals("id")) {
                            setQuizId(Long.valueOf(thisItem.getString()));
                        }
                        if (thisItem.getFieldName().equals("name")) {
                            setQuizName(thisItem.getString());
                        }
                    }

                }

                return true;
            }

            String url = request.getHeader("X-Requested-URL");
            if (StringUtils.isNotEmpty(url)) {
                if (url.contains(uriToFilter)) {

                    String id = IOUtils.toString(request.getPart("id").getInputStream(), StandardCharsets.UTF_8);
                    if (StringUtils.isEmpty(id)) {
                        return false;
                    }

                    setQuizId(Long.valueOf(id));

                    String name = IOUtils.toString(request.getPart("name").getInputStream(), StandardCharsets.UTF_8);
                    setQuizName(name);

                    return true;
                }
            }

//			String payload = IOUtils.toString(request.getReader());
//			LOGGER.debug("payload: {}", payload);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return false;

    }

    @Override
    public int filterOrder() {
        return 10;
    }

    @Override
    public String filterType() {
        return PRE;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

}
