package com.prudential.lms.itf.elearning.filter.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;
import com.prudential.lms.itf.elearning.filter.LMSBaseFilter;
import com.prudential.lms.itf.elearning.service.PrudentialInternalService;
import com.prudential.lms.itf.elearning.service.TrainingService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.dto.training.AgentCourseReportDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.prudential.lms.shared.message.Response;

@Component
public class TrainingTestResultFilter extends LMSBaseFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingTestResultFilter.class);

    @Autowired
    private ObjectMapper mapper;

    private String registrationNo;

    @Autowired
    private PrudentialInternalService pruInternalService;

    @Autowired
    private TrainingService trainingService;

    @Override
    public Object run() {

        RequestContext ctx = getRequestContext();
        ctx.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");
        ctx.setSendZuulResponse(false);

        String headerAgentCode = getRequestHeader("Agent-Number");
        LOGGER.debug("headerAgentCode: {}", headerAgentCode);
        if (null == headerAgentCode) {
            ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
            ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
            return null;
        } else {

            if (!getRegistrationNo().startsWith(headerAgentCode)) {
                ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
                ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
                return null;
            }

        }

        JSONObject jres = null;
        String authBearer = getRequestHeader(AppConstants.HTTP_HEADER_AUTHORIZATION);
        TrainingTestResultDTO dto = null;

        try {
            String response = StreamUtils.copyToString(ctx.getResponseDataStream(), StandardCharsets.UTF_8);
            LOGGER.debug("response: {}", response);
            jres = new JSONObject(response);
            JSONObject data = jres.getJSONObject("data");
            LOGGER.debug("data: {}", data.toString(1));
            TrainingTestResultDTO resDto = mapper.readValue(data.toString(), TrainingTestResultDTO.class);

            dto = trainingService.addResult(authBearer, resDto);

            if (null != dto) {

                boolean pass = null != dto.getPass() ? dto.getPass() : false;
                boolean isPostTest = TrainingTestType.POST == dto.getTestType();

                if (pass && isPostTest) {
                    AgentCourseReportDTO agentCourseRpt = new AgentCourseReportDTO();
                    agentCourseRpt.setAgentCode(dto.getAgentCode());
                    agentCourseRpt.setAgentName(dto.getAgentName());
                    agentCourseRpt.setTrainingCode(dto.getTrainingCode());
                    DateFormat df = new SimpleDateFormat("yyyyMMdd");
                    Date assesmentDate = dto.getFinishDate();
                    agentCourseRpt.setAssesmentDate(df.format(assesmentDate));

                    String[] point = String.valueOf(dto.getPoint()).split("\\.");
                    String assesmentScore = point.length > 0 ? point[0] : String.valueOf(dto.getPoint());
                    assesmentScore = assesmentScore.length() > 3 ? assesmentScore.substring(0, 3) : assesmentScore;
                    agentCourseRpt.setAssesmentScore(assesmentScore);

                    String[] minPoint = String.valueOf(dto.getMinPoint()).split("\\.");
                    String minimumScore = minPoint.length > 0 ? minPoint[0] : String.valueOf(dto.getMinPoint());
                    minimumScore = minimumScore.length() > 3 ? minimumScore.substring(0, 3) : minimumScore;
                    agentCourseRpt.setMinimumScore(minimumScore);

                    agentCourseRpt.setSalesUnit(dto.getAgentOfficeCode());
                    agentCourseRpt.setOriginId(dto.getId());
                    LOGGER.debug("agentCourseRpt: {}", agentCourseRpt.toString());

                    String agentCourseRes = pruInternalService.sendAgentCourseReport(authBearer, agentCourseRpt);
                    LOGGER.debug("agentCourseRes: {}", agentCourseRes);
                }

            }

        } catch (LmsException | JSONException | IOException e) {
            LOGGER.error(e.getMessage(), e);
            ctx.setResponseStatusCode(HttpStatus.OK.value());
            ctx.setResponseBody(Response.badRequest(e.getMessage()).toJSONString());
            return null;
        }

        ctx.setResponseStatusCode(HttpStatus.OK.value());
        ctx.setResponseBody(Response.ok(dto).toJSONString());

        return null;
    }

    @Override
    public boolean shouldFilter() {

        String uriToFilter = "/quiz/verify";
        String uri = getURI();
        String url = null != getRequestHeader("X-Requested-URL") ? getRequestHeader("X-Requested-URL") : "";

        try {

            if (uri.contains(uriToFilter) || url.contains(uriToFilter)) {

                LOGGER.debug("uri: {} - url: {}", uri, url);

                HttpServletRequest request = getRequest();
                BufferedReader reader = request.getReader();
                if (null == reader) {
                    return false;
                }

                String body = IOUtils.toString(reader);
                LOGGER.debug("body: {}", body);
                TrainingTestResultDTO dto = mapper.readValue(body, TrainingTestResultDTO.class);
                if (null != dto) {
                    setRegistrationNo(dto.getRegistrationNo());
                }

                return true;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return false;
    }

    @Override
    public int filterOrder() {
        return 99;
    }

    @Override
    public String filterType() {
        return POST;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

}
