package com.prudential.lms.itf.elearning.filter;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public abstract class LMSBaseFilter extends ZuulFilter {

	public static String PRE = "pre";
	public static String ROUTING = "routing";
	public static String POST = "post";

	public HttpServletRequest getRequest() {
		return RequestContext.getCurrentContext().getRequest();
	}

	public HttpServletResponse getResponse() {
		return RequestContext.getCurrentContext().getResponse();
	}

	public RequestContext getRequestContext() {
		return RequestContext.getCurrentContext();
	}

	public String getURI() {
		return getRequest().getRequestURI();
	}

	public String getRequestMethod() {
		return getRequest().getMethod();
	}

	public HttpHeaders getRequestHeaders() {

		HttpHeaders headers = new HttpHeaders();
		Enumeration<String> headerNames = getRequest().getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			headers.add(headerName, getRequest().getHeader(headerName));
		}

		return headers;
	}
	
	public String getRequestHeader(String headerName){
		return getRequest().getHeader(headerName);
	}
}
