package com.prudential.lms.itf.elearning.service.impl;

import java.math.BigDecimal;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prudential.lms.itf.elearning.service.PrudentialInternalService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.itf.AgentProductionDTO;
import com.prudential.lms.shared.dto.training.AgentCourseReportDTO;

@Service
public class PrudentialInternalServiceImpl implements PrudentialInternalService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrudentialInternalServiceImpl.class);

	@Value("${app.service.base-proxy-url}")
	private String baseProxyURL;

	@Value("${app.service.production-newods-path}")
	private String findProductionPath;

	@Value("${app.service.case-point-path}")
	private String casePointPath;

	@Value("${app.service.agent-save-report-path}")
	private String agentCourseReportPath;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	@Override
	public String sendAgentCourseReport(String authBearer, AgentCourseReportDTO agentCourseRpt) {

		LOGGER.debug("baseProxyURL: {}", baseProxyURL);
		LOGGER.debug("agentCourseReportPath: {}", agentCourseReportPath);

		String result = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
		headers.add("X-Requested-URL", agentCourseReportPath);
		headers.add("Content-Type", "application/json");

		try {

			HttpEntity<AgentCourseReportDTO> entity = new HttpEntity<AgentCourseReportDTO>(agentCourseRpt, headers);
			ResponseEntity<String> res = restTemplate.exchange(baseProxyURL, HttpMethod.POST, entity, String.class);

			if (null != res) {
				HttpStatus httpStatus = res.getStatusCode();
				if (httpStatus.equals(HttpStatus.OK)) {
					result = res.getBody();
					LOGGER.debug("result: {}", result);
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return result;
	}

	@Override
	public Integer getAgentTotalCasePoint(String authBearer, String agentCode) {

		Integer result = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
		headers.add("X-Requested-URL", casePointPath);
		headers.add("Content-Type", "application/json");

		try {

			JSONObject param = new JSONObject();
			param.put("agentCode", agentCode);
			String[] status = { "IF" };
			param.put("status", new JSONArray(status));
			HttpEntity<String> entity = new HttpEntity<String>(param.toString(), headers);

			ResponseEntity<String> res = restTemplate.exchange(baseProxyURL, HttpMethod.POST, entity, String.class);

			if (null != res) {
				HttpStatus httpStatus = res.getStatusCode();
				if (httpStatus.equals(HttpStatus.OK)) {

					JSONObject json = new JSONObject(res.getBody());
					String apeStr = String.valueOf(json.get("total"));
					result = new Integer(apeStr);

				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return result;

	}

	@Override
	public BigDecimal getAgentProductionApeYtd(String authBearer, String agentCode) {

		BigDecimal result = null;
		HttpHeaders headers = new HttpHeaders();
		headers.add(AppConstants.HTTP_HEADER_AUTHORIZATION, authBearer);
		headers.add("X-Requested-URL", findProductionPath);
		headers.add("Content-Type", "application/json");

		AgentProductionDTO agentProductionDto = new AgentProductionDTO();
		agentProductionDto.setAgentCode(agentCode);
		agentProductionDto.setType("I");
		HttpEntity<AgentProductionDTO> entity = new HttpEntity<AgentProductionDTO>(agentProductionDto, headers);

		try {

			ResponseEntity<String> res = restTemplate.exchange(baseProxyURL, HttpMethod.POST, entity, String.class);

			if (null != res) {
				HttpStatus status = res.getStatusCode();
				if (status.equals(HttpStatus.OK)) {

					JSONObject json = new JSONObject(res.getBody());
					String apeStr = String.valueOf(json.get("ape_ytd"));
					result = new BigDecimal(apeStr);
					// String apePlain = apeDec.toPlainString();
					// System.out.println(casePointPlain.substring(0,casePointPlain.indexOf(".")));
					// BigDecimal newDes = new
					// BigDecimal(apePlain.substring(0,apePlain.indexOf(".")));
					// System.out.println(newDes);

				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return result;

	}

	// private String restViaBaseProxy(HttpMethod httpMethod, HttpEntity entity)
	// {
	//
	// String result = null;
	//
	// try {
	//
	// ResponseEntity<String> response = restTemplate.exchange(baseProxyURL,
	// httpMethod, entity, String.class);
	// if (null != response) {
	// result = response.getBody();
	// }
	//
	// } catch (Exception e) {
	// LOGGER.error(e.getMessage());
	// }
	//
	// return result;
	// }

}
