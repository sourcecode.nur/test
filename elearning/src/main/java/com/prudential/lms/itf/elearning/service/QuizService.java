package com.prudential.lms.itf.elearning.service;

import com.prudential.lms.shared.dto.quiz.QuizDTO;

public interface QuizService {

	QuizDTO updateQuiz(String authBearer, QuizDTO dto);

}
