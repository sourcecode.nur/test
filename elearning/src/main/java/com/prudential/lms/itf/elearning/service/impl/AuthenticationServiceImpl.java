package com.prudential.lms.itf.elearning.service.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prudential.lms.itf.elearning.service.AuthenticationService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper objectMapper;

	@Value("${security.auth-url}")
	private String authUrl;

	public boolean validToken(String token) {
		boolean isValid = false;
		String url = this.authUrl.concat("/agent/verify");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", token);
		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		try {
			ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.GET, entity, String.class,
					new Object[0]);
			if ((response.getStatusCode().equals(HttpStatus.OK)) && (null != response.getBody())) {
				JSONObject res = new JSONObject((String) response.getBody());
				if (res.has("agentCode")) {
					LOGGER.info("agentCode: " + res.get("agentCode"));
					isValid = true;
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Failed verifying authorization token", e);
			isValid = false;
		}
		return isValid;
	}
}
