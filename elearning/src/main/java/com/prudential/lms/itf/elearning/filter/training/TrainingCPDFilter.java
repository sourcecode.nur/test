package com.prudential.lms.itf.elearning.filter.training;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.netflix.zuul.context.RequestContext;
import com.prudential.lms.itf.elearning.filter.LMSBaseFilter;
import com.prudential.lms.shared.message.Response;

@Component
public class TrainingCPDFilter extends LMSBaseFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCPDFilter.class);

	private String agentCode;

	@Override
	public Object run() {

		RequestContext ctx = getRequestContext();
		ctx.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");

		String headerAgentCode = getRequestHeader("Agent-Number");
		LOGGER.debug("headerAgentCode: {}", headerAgentCode);
		if (null == headerAgentCode) {
			ctx.setSendZuulResponse(false);
			ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
			ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
		} else {

			if (!headerAgentCode.equalsIgnoreCase(getAgentCode())) {
				ctx.setSendZuulResponse(false);
				ctx.setResponseStatusCode(HttpStatus.BAD_REQUEST.value());
				ctx.setResponseBody(Response.badRequest("GTFOH").toJSONString());
			}

		}

		return null;
	}

	@Override
	public boolean shouldFilter() {

		String uriToFilter = "/training-cpd/agent-code/";

		try {

			String uri = getURI();
			if (uri.contains(uriToFilter)) {
				String[] params = uri.split("/");
				setAgentCode(params[params.length - 1]);
				return true;
			}

			String url = getRequestHeader("X-Requested-URL");
			if (StringUtils.isNotEmpty(url)) {
				if (url.contains(uriToFilter)) {
					String[] params = uri.split("/");
					setAgentCode(params[params.length - 1]);
					return true;
				}
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return false;
	}

	@Override
	public String filterType() {
		return PRE;
	}

	@Override
	public int filterOrder() {
		return 90;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

}
