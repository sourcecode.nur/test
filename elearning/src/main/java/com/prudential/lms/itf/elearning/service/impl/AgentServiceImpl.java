package com.prudential.lms.itf.elearning.service.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prudential.lms.itf.elearning.service.AgentService;
import com.prudential.lms.shared.dto.training.AgentDTO;
import com.prudential.lms.shared.message.Response;

@Service
public class AgentServiceImpl implements AgentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgentServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;

	@Value("${zuul.routes.lms-training.url}")
	private String trainingUrl;

	@Override
	public AgentDTO findByCode(String agentCode) {
		AgentDTO result = null;
		String url = trainingUrl.concat("/agent/code/").concat(agentCode);
		LOGGER.debug("GET " + url);
		try {
			ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
			if (null != response) {
				if (response.getStatusCode().equals(HttpStatus.OK)) {
					JSONObject obj = new JSONObject(response.getBody());
					result = mapper.readValue(String.valueOf(obj.get("data")), AgentDTO.class);
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}

		return result;
	}

}
