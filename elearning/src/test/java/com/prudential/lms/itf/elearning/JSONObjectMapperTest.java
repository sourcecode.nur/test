package com.prudential.lms.itf.elearning;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;

@RunWith(SpringRunner.class)
public class JSONObjectMapperTest {

	@TestConfiguration
	static class JSONObjectMapperTestCfg {

		@Bean
		public ObjectMapper objectMapper() {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			return mapper;
		}

	}

	@Autowired
	private ObjectMapper mapper;

	@Test
	public void test_null_mapping() throws JsonParseException, JsonMappingException, IOException, JSONException {

		JSONObject jres = new JSONObject("{\"code\":\"404\",\"message\":\"NOT_FOUND\",\"data\":null}");
		JSONObject data = jres.getJSONObject("data");
		TrainingTestResultDTO dto = mapper.readValue(data.toString(), TrainingTestResultDTO.class);

	}

	@Test
	public void test_map_unixtimestamp() throws IOException {

		long milis = 1511158986000l;
		TrainingTestResultDTO dto = new TrainingTestResultDTO();
		dto.setFinishDate(new Date(milis));

		String dtoStr = mapper.writeValueAsString(dto);
		System.out.println("########################################################");
		System.out.println("dtoStr: " + dtoStr);
		System.out.println("########################################################");

		TrainingTestResultDTO newDto = mapper.readValue(dtoStr, TrainingTestResultDTO.class);

		Assert.assertEquals(newDto.getFinishDate(), dto.getFinishDate());

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date assesmentDate = newDto.getFinishDate();
		String formattedAssesmentDate = df.format(assesmentDate);
		System.out.println("assesmentDate: " + formattedAssesmentDate);
		Assert.assertEquals("20171120", formattedAssesmentDate);
		System.out.println("########################################################");

	}

	@Test
	public void test_json_to_string() throws JSONException, JsonProcessingException {

		JSONObject json = new JSONObject();
		json.put("key", "value");

		String jsonStr = json.toString();

		String result = mapper.writeValueAsString(json);
		System.out.println("########################################################");
		System.out.println(jsonStr);
		System.out.println("########################################################");
		System.out.println(result);
		System.out.println("########################################################");
		Assert.assertNotEquals(jsonStr, result);

	}

}
