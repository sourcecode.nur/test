package com.prudential.lms.common.sysref.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.common.sysref.model.SystemRefGroup;

public interface SystemRefGroupRepository
		extends JpaRepository<SystemRefGroup, Long>, QueryDslPredicateExecutor<SystemRefGroup> {

	SystemRefGroup findByCode(String code);

}
