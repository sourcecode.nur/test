package com.prudential.lms.common.sysref.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.common.sysref.model.SystemRefDetail;

public interface SystemRefDetailRepository
		extends JpaRepository<SystemRefDetail, Long>, QueryDslPredicateExecutor<SystemRefDetail> {

	SystemRefDetail findByCode(String code);

	List<SystemRefDetail> findByGroupId(Long id);

}
