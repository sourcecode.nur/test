package com.prudential.lms.common.sysref.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_sys_ref_grp")
public class SystemRefGroup extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8840966320547169132L;
	private static final String SEQ_GEN = "seq_gen_sys_ref_grp";
	private static final String SEQ = "seq_sys_ref_grp";

	private Long id;
	private String code;
	private String description;
	private Set<SystemRefDetail> details = new HashSet<>();

	@PrePersist
	private void prePersist() {
		this.code = getCode().toUpperCase();
	}

	@PreUpdate
	private void preUdate() {
		this.code = getCode().toUpperCase();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(length = 32, unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
	public Set<SystemRefDetail> getDetails() {
		return details;
	}

	public void setDetails(Set<SystemRefDetail> details) {
		this.details = details;
	}

	@Transient
	public boolean hasDetails() {
		return !getDetails().isEmpty();
	}

}
