package com.prudential.lms.common.sysref.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.common.sysref.model.SystemRefGroup;
import com.prudential.lms.common.sysref.repository.SystemRefGroupRepository;
import com.prudential.lms.shared.dto.sysref.SystemRefGroupDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class SystemRefGroupMapper implements EntityToDTOMapper<SystemRefGroup, SystemRefGroupDTO> {

	@Autowired
	private SystemRefGroupRepository repo;

	@Autowired
	private SystemRefDetailMapper dtlMapper;

	@Autowired
	private ModelMapper mapper;

	@Override
	public SystemRefGroup createEntity(SystemRefGroupDTO dto) {
		SystemRefGroup entity = new SystemRefGroup();
		entity.setCode(dto.getCode());
		entity.setDescription(dto.getDescription());
		return entity;
	}

	@Override
	public SystemRefGroup updateEntity(SystemRefGroupDTO dto) {
		SystemRefGroup entity = repo.findByCode(dto.getCode());
		entity.setDescription(dto.getDescription());
		return entity;
	}

	@Override
	public SystemRefGroupDTO convertToDto(SystemRefGroup entity) {
		SystemRefGroupDTO dto = mapper.map(entity, SystemRefGroupDTO.class);
		return dto;
	}

	@Override
	public List<SystemRefGroupDTO> convertToDto(Iterable<SystemRefGroup> entities) {
		List<SystemRefGroupDTO> dtos = new ArrayList<>();
		for (SystemRefGroup entity : entities) {
			dtos.add(convertToDto(entity));
		}
		return dtos;
	}

	@Override
	public SystemRefGroup convertToEntity(SystemRefGroupDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemRefGroup> convertToEntity(Iterable<SystemRefGroupDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
