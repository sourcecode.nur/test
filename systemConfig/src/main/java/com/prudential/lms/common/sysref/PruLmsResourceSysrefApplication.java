package com.prudential.lms.common.sysref;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruLmsResourceSysrefApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruLmsResourceSysrefApplication.class, args);
	}
}
