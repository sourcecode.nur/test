package com.prudential.lms.common.sysref.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.common.sysref.mapper.SystemRefDetailMapper;
import com.prudential.lms.common.sysref.model.SystemRefDetail;
import com.prudential.lms.common.sysref.repository.SystemRefDetailRepository;
import com.prudential.lms.common.sysref.repository.SystemRefGroupRepository;
import com.prudential.lms.shared.dto.sysref.SystemRefDetailDTO;
import com.prudential.lms.shared.message.Response;

@RestController
@RequestMapping("/dtl")
public class SystemRefDetailController {

	@Autowired
	private SystemRefDetailRepository repo;

	@Autowired
	private SystemRefGroupRepository repoGroup;

	@Autowired
	private SystemRefDetailMapper mapper;

	// update by code
	@RequestMapping(value = "/update/{dtlCode}", method = RequestMethod.POST)
	public Object updateByCode(@PathVariable("dtlCode") String dtlCode, @RequestBody SystemRefDetailDTO dto) {

		SystemRefDetail saved = repo.findByCode(dtlCode);
		if (null == saved)
			return Response.notFound("SystemRefDetail with code: " + dtlCode + " not found");

		try {
			dto.setCode(dtlCode);
			saved = repo.save(mapper.updateEntity(dto));
		} catch (Exception e) {
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(mapper.convertToDto(saved));
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody SystemRefDetailDTO dto) {
		SystemRefDetail saved = null;

		if (null == dto.getGroupId()) {
			return Response.badRequest("groupId cannot be empty");
		} else {
			if (null == repoGroup.findOne(dto.getGroupId()))
				return Response.notFound("Group not found");
		}

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

}
