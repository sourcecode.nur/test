package com.prudential.lms.common.sysref.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.common.sysref.model.SystemRefDetail;
import com.prudential.lms.common.sysref.model.SystemRefGroup;
import com.prudential.lms.common.sysref.repository.SystemRefDetailRepository;
import com.prudential.lms.common.sysref.repository.SystemRefGroupRepository;
import com.prudential.lms.shared.dto.sysref.SystemRefDetailDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class SystemRefDetailMapper implements EntityToDTOMapper<SystemRefDetail, SystemRefDetailDTO> {

	@Autowired
	private SystemRefDetailRepository repo;

	@Autowired
	private SystemRefGroupRepository repoGroup;

	@Autowired
	private ModelMapper mapper;

	@Override
	public SystemRefDetail createEntity(SystemRefDetailDTO dto) {
		SystemRefDetail entity = new SystemRefDetail();
		entity.setCode(dto.getCode());
		entity.setLabelID(dto.getLabelID());
		entity.setLabelEN(dto.getLabelEN());
		entity.setDescription(dto.getDescription());

		Long groupId = dto.getGroupId();
		if (null != groupId) {
			SystemRefGroup group = repoGroup.findOne(dto.getGroupId());
			if (null != group)
				entity.setGroup(group);
		}

		return entity;
	}

	@Override
	public SystemRefDetail updateEntity(SystemRefDetailDTO dto) {
		SystemRefDetail entity = repo.findByCode(dto.getCode());
		entity.setLabelID(dto.getLabelID());
		entity.setLabelEN(dto.getLabelEN());
		entity.setDescription(dto.getDescription());

		Long groupId = dto.getGroupId();
		if (null != groupId) {
			SystemRefGroup group = repoGroup.findOne(dto.getGroupId());
			if (null != group)
				entity.setGroup(group);
		}

		return entity;
	}

	@Override
	public SystemRefDetailDTO convertToDto(SystemRefDetail entity) {
		SystemRefDetailDTO dto = mapper.map(entity, SystemRefDetailDTO.class);
		return dto;
	}

	@Override
	public List<SystemRefDetailDTO> convertToDto(Iterable<SystemRefDetail> entities) {
		List<SystemRefDetailDTO> dtos = new ArrayList<>();
		for (SystemRefDetail entity : entities) {
			dtos.add(convertToDto(entity));
		}
		return dtos;
	}

	@Override
	public SystemRefDetail convertToEntity(SystemRefDetailDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SystemRefDetail> convertToEntity(Iterable<SystemRefDetailDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
