package com.prudential.lms.common.sysref.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.common.sysref.mapper.SystemRefGroupMapper;
import com.prudential.lms.common.sysref.model.SystemRefGroup;
import com.prudential.lms.common.sysref.repository.SystemRefDetailRepository;
import com.prudential.lms.common.sysref.repository.SystemRefGroupRepository;
import com.prudential.lms.shared.dto.sysref.SystemRefGroupDTO;
import com.prudential.lms.shared.message.Response;

@RestController
@RequestMapping("/grp")
public class SystemRefGroupController {

	@Autowired
	private SystemRefGroupRepository repo;

	@Autowired
	private SystemRefDetailRepository dtlRepo;

	@Autowired
	private SystemRefGroupMapper mapper;

	// Find By Code
	@RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
	public Object findByCode(@PathVariable("code") String code) {
		SystemRefGroup grp = repo.findByCode(code);
		if (null == grp)
			return Response.notFound();

		return Response.found(mapper.convertToDto(grp));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		SystemRefGroup grp = repo.findOne(id);
		if (null == grp)
			return Response.notFound();

		return Response.found(mapper.convertToDto(grp));
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody SystemRefGroupDTO dto) {
		SystemRefGroup saved = null;

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			return Response.internalServerError();
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {

		SystemRefGroup tc = repo.findOne(id);
		if (null == tc) {
			return Response.notFound("Group not found");
		}

		try {
			dtlRepo.delete(tc.getDetails());
			repo.delete(id);
		} catch (Exception e) {
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

}
