package com.prudential.lms.common.sysref.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_sys_ref_dtl")
public class SystemRefDetail extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7581632295650265716L;
	private static final String SEQ_GEN = "seq_gen_sys_ref_dtl";
	private static final String SEQ = "seq_sys_ref_dtl";

	private Long id;
	private String code;
	private String labelID;
	private String labelEN;
	private String description;
	private int sortOrder;
	private SystemRefGroup group;

	@PrePersist
	private void prePersist(){
		this.code = getCode().toUpperCase();
	}
	
	@PreUpdate
	private void preUdate() {
		this.code = getCode().toUpperCase();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}
	
	@Column(length = 64, unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "label_id")
	public String getLabelID() {
		return labelID;
	}

	public void setLabelID(String labelID) {
		this.labelID = labelID;
	}

	@Column(name = "label_en")
	public String getLabelEN() {
		return labelEN;
	}

	public void setLabelEN(String labelEN) {
		this.labelEN = labelEN;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "group_id")
	public SystemRefGroup getGroup() {
		return group;
	}

	public void setGroup(SystemRefGroup group) {
		this.group = group;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

}
