package com.prudential.lms.resource.training.service.impl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.file.remote.InputStreamCallback;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.jcraft.jsch.ChannelSftp.LsEntry;

@RunWith(SpringRunner.class)
public class SFTPGetTest {

	@TestConfiguration
	static class SFTPGetTestCtxCfg {

		private String ftpRootDir = "/app/ews/httpd/www/html/microservice/contentfile/lms";

		private ExpressionParser expressionParser = new SpelExpressionParser();

		@Bean
		public SftpRemoteFileTemplate sftpTemplate() {
			SftpRemoteFileTemplate sftpTemplate = new SftpRemoteFileTemplate(ftpsSessionFactory());
			sftpTemplate.setRemoteDirectoryExpression(expressionParser.parseExpression("'" + ftpRootDir + "'"));
			sftpTemplate.setAutoCreateDirectory(true);
			return sftpTemplate;
		}

		@Bean
		public SessionFactory<LsEntry> ftpsSessionFactory() {
			DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
			factory.setHost("10.170.49.247");
			factory.setPort(22);
			factory.setUser("jbossupload");
			factory.setPassword("Password09");
			factory.setAllowUnknownKeys(true);
			return new CachingSessionFactory<LsEntry>(factory);
		}

	}

	@Autowired
	private SftpRemoteFileTemplate sftpTemplate;

	private String result;

	private InputStreamCallback base64callback = new InputStreamCallback() {

		@Override
		public void doWithInputStream(InputStream inputStream) throws IOException {

//			int available = inputStream.available();
//			BufferedInputStream reader = new BufferedInputStream(inputStream);
			byte[] bytes = IOUtils.toByteArray(inputStream);
//			reader.read(bytes);
//			reader.close();

//			byte[] decoded = Base64.decodeBase64(bytes);
			result = new String(Base64.encodeBase64(bytes), "UTF-8");
		}
	};

	@Test
	public void test_get_ftp() {

		String remotePath = "/app/ews/httpd/www/html/microservice/contentfile/lms/contents/pdf/73b44ef63e454d0986daa094c87b02f8/samplepdf.pdf";

		boolean ok = sftpTemplate.get(remotePath, base64callback);
		Assert.assertTrue(ok);

		String base64str = getResult();
		Assert.assertNotNull(base64str);
		
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
