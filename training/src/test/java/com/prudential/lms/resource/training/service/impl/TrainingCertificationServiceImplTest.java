package com.prudential.lms.resource.training.service.impl;

import java.util.Date;

import org.junit.Test;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;

public class TrainingCertificationServiceImplTest {

	private TrainingCertificationServiceImpl service = new TrainingCertificationServiceImpl();
	
	private static final String templateImage = "http://vps.hexa-ds.co.id/certificate_templates/certificate_003.png";
	@Test
	public void testGenerateTrainingCertification() {
		Agent agent = new Agent();
		agent.setName("Sampel Agent Name");
		agent.setCode("CODE 00001");
		
		TrainingRegistration trainingRegistration = new TrainingRegistration();
		trainingRegistration.setAgent(agent);
		
		Training training = new Training();
		training.setName("Sampel Training Name");
		
		TrainingSchedule trainingSchedule = new TrainingSchedule();
		trainingSchedule.setTraining(training);
		trainingSchedule.setEndDate(new Date());
		
		TrainingRegistrationSchedule trainingRegistrationSchedule = new TrainingRegistrationSchedule();
		trainingRegistrationSchedule.setRegistration(trainingRegistration);
		trainingRegistrationSchedule.setSchedule(trainingSchedule);
		
		service.generateTrainingCertification(trainingRegistrationSchedule, templateImage);
	}

}
