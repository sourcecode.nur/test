package com.prudential.lms.resource.training.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.prudential.lms.resource.training.mapper.TrainingCPDMapper;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.repository.TrainingCPDMutationRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.service.TrainingCPDService;
import com.prudential.lms.shared.dto.training.TrainingCPDDTO;
import com.prudential.lms.shared.exception.LmsException;

@RunWith(SpringRunner.class)
public class TrainingCPDServiceImplTest {

	@TestConfiguration
	static class TrainingCPDServiceImplTestContextConfiguration {

		@Bean
		public TrainingCPDService trainingCPDService() {
			return new TrainingCPDServiceImpl();
		}
	}

	@Autowired
	private TrainingCPDService trainingCPDService;

	@MockBean
	private TrainingCPDRepository trnCpdRepo;

	@MockBean
	private TrainingCPDMutationRepository trnCpdMtnRepo;

	@MockBean
	private TrainingRepository trnRepo;

	@MockBean
	private TrainingCPDMapper trnCpdMapper;

	@Before
	public void setUp() {
		Training training = new Training();
		training.setCode("TR1000");

		Mockito.when(trnRepo.findByCode(training.getCode())).thenReturn(training);

	}

	@Test
	public void servicenotnull() {
		Assert.assertNotNull(trainingCPDService);
	}

//	@Test
//	public void importparsecpd() throws LmsException, FileNotFoundException, IOException {
//
//		ClassLoader classLoader = getClass().getClassLoader();
//		File excel = new File(classLoader.getResource("upload_cpd_sample_data.xlsx").getFile());
//		List<TrainingCPDDTO> result = trainingCPDService.importParseCPD(new FileInputStream(excel));
//		Assert.assertNotNull(result);
//		Assert.assertEquals(1, result.size());
//	}

}
