package com.prudential.lms.resource.training.mapper;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.AgentGroup;
import com.prudential.lms.resource.training.model.TrainingCPD;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.shared.dto.training.AgentDTO;

public class AgentMapperTest {
	
	@Autowired
	AgentMapper mapper;
	
	Agent entity = new Agent();
	
	@Before
    public void setup() {
		AgentGroup agentGroup = new AgentGroup();
		agentGroup.setId(1L);
		agentGroup.setGroup("Agent Group 1");
		
		TrainingCPD cpd = new TrainingCPD();
		cpd.setId(1L);
		cpd.setPoint(2);

		Set<TrainingRegistration> registrations = new HashSet<TrainingRegistration>();
		for(int i = 1; i <= 3; i++) {
			TrainingRegistration registration = new TrainingRegistration();
			registration.setAgent(entity);
			registration.setId((long)i);
//			registration.setRegistrationNo("00"+i);
//			registration.setStartDate(new Date());
			
			registrations.add(registration);
		}
		
		entity.setAgentGroup(agentGroup);
		entity.setCode("001");
//		entity.setCpd(cpd);
		entity.setEndLicenseDate(new Date());
		entity.setGroupLeader(false);
		entity.setId(1L);
		entity.setName("Agent 007");
		
//		entity.setRegistrations(registrations);
		
		entity.setStartLicenseDate(new Date());
	}
	
	@Test
	public void whenConvertAgentEntityToAgentDto_thenCorrect() {
		AgentDTO dto = mapper.convertToDto(entity);

		assertEquals(entity.getId(), dto.getId());
		assertEquals(entity.getAgentGroup().getId(), dto.getAgentGroupId());
		assertEquals(null, entity.getCode(), dto.getCode());
//		assertEquals(null, entity.getCpd().getId(), dto.getTrainingCpdId());
		assertEquals(null, entity.getEndLicenseDate(), dto.getEndLicenseDate());
		assertEquals(null, entity.isGroupLeader(), dto.isGroupLeader());
		assertEquals(null, entity.getName(), dto.getName());
//		assertEquals(null, entity.getRegistrations(), dto.getRegistrations());
		assertEquals(null, entity.getStartLicenseDate(), dto.getStartLicenseDate());
	}

	//@Test
	public void whenConvertAgentDtoToAgentEntity_thenCorrect() {
		
	}
}
