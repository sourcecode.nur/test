package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.ContentLibrary;
import com.prudential.lms.resource.training.repository.ContentLibraryRepository;
import com.prudential.lms.shared.dto.training.ContentLibraryDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class ContentLibraryMapper implements EntityToDTOMapper<ContentLibrary, ContentLibraryDTO> {

	@Autowired
	private ContentLibraryRepository repo;

	@Autowired
	private ModelMapper mapper;

	@Override
	public ContentLibrary createEntity(ContentLibraryDTO dto) {
		ContentLibrary entity = mapper.map(dto, new ContentLibrary().getClass());
		return entity;
	}

	@Override
	public ContentLibrary updateEntity(ContentLibraryDTO dto) {
		ContentLibrary entity = repo.findOne(dto.getId());
		entity = mapper.map(dto, entity.getClass());
		return entity;
	}

	@Override
	public ContentLibraryDTO convertToDto(ContentLibrary entity) {
		ContentLibraryDTO dto = mapper.map(entity, ContentLibraryDTO.class);
		return dto;
	}

	@Override
	public List<ContentLibraryDTO> convertToDto(Iterable<ContentLibrary> entities) {
		List<ContentLibraryDTO> dtoList = new ArrayList<>();
		for (ContentLibrary entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public ContentLibrary convertToEntity(ContentLibraryDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ContentLibrary> convertToEntity(Iterable<ContentLibraryDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
