package com.prudential.lms.resource.training.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.prudential.lms.shared.constant.TrainingContentType;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_x_content")
public class TrainingContent extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8500056073340933013L;
	private static final String SEQ_GEN = "seq_gen_trn_ctn";
	private static final String SEQ = "seq_trn_ctn";

	private Long id;
	private TrainingContentType type;
	private Training training;
	private ContentLibrary library;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "training_id", referencedColumnName = "id")
	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	@Enumerated(EnumType.STRING)
	public TrainingContentType getType() {
		return type;
	}

	public void setType(TrainingContentType type) {
		this.type = type;
	}

	@ManyToOne
	@JoinColumn(name = "library_id", referencedColumnName = "id")
	public ContentLibrary getLibrary() {
		return library;
	}

	public void setLibrary(ContentLibrary library) {
		this.library = library;
	}

}
