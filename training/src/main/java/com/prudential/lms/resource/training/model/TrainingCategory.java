package com.prudential.lms.resource.training.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.constant.TrainingCategoryStatus;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;
import com.prudential.lms.shared.model.SoftDelete;

@Entity
@Table(name = "lms_training_ctgr")
public class TrainingCategory extends AuditableEntityBase implements EntityBase<Long>, SoftDelete {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6519781259667174154L;
	private static final String SEQ_GEN = "seq_gen_training_ctgr";
	private static final String SEQ = "seq_training_ctgr";

	private Long id;
	private String name;
	private String code;
	private String description;
	private TrainingCategory parent;
	private Set<TrainingCategory> children = new HashSet<>();
	private TrainingCategoryStatus status;
	private int sortOrder;
	private int trainingCount;
	private int depth = 1;
	private Set<Training> trainings = new HashSet<>();
	private String fileName;
	private String thumbnail;
	private boolean deleted;
	private boolean draft;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@PrePersist
	private void prePersist() {
		this.trainingCount = getTrainings().size();
		this.depth = countDepth(this);
	}
	
	@PreUpdate
	private void preUpdate() {
		this.trainingCount = getTrainings().size();
		this.depth = countDepth(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(length = 32, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	public TrainingCategory getParent() {
		return parent;
	}

	public void setParent(TrainingCategory parent) {
		this.parent = parent;
	}

	@OneToMany(mappedBy = "parent")
	public Set<TrainingCategory> getChildren() {
		return children;
	}

	public void setChildren(Set<TrainingCategory> children) {
		this.children = children;
	}

	public void addChildren(TrainingCategory children) {
		this.children.add(children);
	}

	@Transient
	public boolean hasTraining() {
		return getTrainingCount() > 0;
	}
	
	@Transient
	public boolean hasChildren() {
		return getChildren().size() > 0;
	}

	@Transient
	public boolean isRoot() {
		return null == getParent();
	}

	@Transient
	public boolean isNode() {
		return null != getParent() && hasChildren();
	}

	@Transient
	public boolean isLeaf() {
		return !isRoot() && !hasChildren();
	}

	@Transient
	public int countDepth(TrainingCategory category) {
		int result = this.depth;
		if (!category.isRoot()) {
			result = result + countDepth(category.getParent());
		}
		return result;
	}

	@Type(type = "yes_no")
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name = "sort_order")
	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Column(name = "training_count")
	public int getTrainingCount() {
		return trainingCount;
	}

	public void setTrainingCount(int trainingCount) {
		this.trainingCount = trainingCount;
	}
	
	public void calcTrainingCount(){
		setTrainingCount(getTrainings().size());
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	@Enumerated(EnumType.STRING)
	public TrainingCategoryStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingCategoryStatus status) {
		this.status = status;
	}

	@OneToMany(mappedBy = "category")
	public Set<Training> getTrainings() {
		return trainings;
	}

	public void setTrainings(Set<Training> trainings) {
		this.trainings = trainings;
	}
	 
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	@Type(type = "yes_no")
	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	
	

}
