package com.prudential.lms.resource.training.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prudential.lms.resource.training.mapper.TrainingTestResultMapper;
import com.prudential.lms.resource.training.model.QTrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingTestResult;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingTestRepository;
import com.prudential.lms.resource.training.repository.TrainingTestResultRepository;
import com.prudential.lms.resource.training.service.TrainingTestService;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.querydsl.core.types.dsl.BooleanExpression;

@Service
public class TrainingTestServiceImpl implements TrainingTestService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingTestServiceImpl.class);

	@Autowired
	private TrainingTestRepository repo;

	@Autowired
	private TrainingTestResultRepository trnTestResultRepo;

	@Autowired
	private TrainingTestResultMapper trnTestResMapper;

	@Autowired
	private TrainingRegistrationScheduleRepository trnRegSchRepo;

	@Transactional
	@Override
	public List<TrainingTestResultDTO> importTrainingResult(InputStream inputStream) throws IOException, LmsException {

		List<TrainingTestResultDTO> result = new ArrayList<>();
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet sheet = workbook.getSheetAt(0);

		int i = 0;
		for (Row row : sheet) {
			if (i > 0) {

				// training code
				Cell cellTrainingCode = row.getCell(0);
				String trainingCode = null != cellTrainingCode ? cellTrainingCode.getStringCellValue() : "";

				// training name
				Cell cellTrainingName = row.getCell(1);
				String trainingName = null != cellTrainingName ? cellTrainingName.getStringCellValue() : "";

				// agent number / code
				Cell cellAgentNumber = row.getCell(2);
				CellType agentCelltype = cellAgentNumber.getCellTypeEnum();
				String agentNumber = "";
				if (agentCelltype == CellType.NUMERIC) {
					Double numAgentNumber = cellAgentNumber.getNumericCellValue();
					agentNumber = String.valueOf(numAgentNumber).replaceAll(".0", "");
				} else {
					agentNumber = cellAgentNumber.getStringCellValue();
				}

				// agent name
				Cell cellAgentName = row.getCell(3);
				String agentName = null != cellAgentName ? cellAgentName.getStringCellValue() : "";

				// training date
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String trainingDate = row.getCell(4).getStringCellValue();
				Date trnDateParsed = null;
				try {
					trnDateParsed = df.parse(trainingDate);
				} catch (ParseException e) {
					LOGGER.error("Failed parsing training date");
				}

				CellType cpdCellType = row.getCell(5).getCellTypeEnum();
				String cpdPoint = "";
				if (cpdCellType == CellType.NUMERIC) {
					Double cpdPointNum = row.getCell(5).getNumericCellValue();
					cpdPoint = String.valueOf(cpdPointNum).replaceAll(".0", "");
				} else {
					cpdPoint = row.getCell(5).getStringCellValue();
				}

				String passStatus = row.getCell(6).getStringCellValue();
				
				Double pointDbl = row.getCell(7).getNumericCellValue();
				BigDecimal point = new BigDecimal(pointDbl, MathContext.DECIMAL32);

				BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.training.code
						.eq(trainingCode);
				predicate = predicate
						.and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
								.eq(agentNumber));

				TrainingRegistrationSchedule trnRegSch = trnRegSchRepo.findOne(predicate);
				if (null == trnRegSch)
					continue;

				TrainingTestResult trnTestRes = new TrainingTestResult();
				trnTestRes.setFinishDate(trnDateParsed);
				trnTestRes.setPass("PASS".equals(passStatus));
				trnTestRes.setTrainingRegistrationSchedule(trnRegSch);
				trnTestRes.setPoint(point);

				result.add(trnTestResMapper.convertToDto(trnTestResultRepo.save(trnTestRes)));

			}
			i++;
		}

		workbook.close();

		return result;

	}
	
	@Override
	public List<TrainingTestResultDTO> importParseTrainingResult(InputStream inputStream) throws IOException, LmsException {

		List<TrainingTestResultDTO> result = new ArrayList<>();
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet sheet = workbook.getSheetAt(0);

		int i = 0;
		for (Row row : sheet) {
			if (i > 0) {

				// training code
				Cell cellTrainingCode = row.getCell(0);
				String trainingCode = null != cellTrainingCode ? cellTrainingCode.getStringCellValue() : "";

				// training name
				Cell cellTrainingName = row.getCell(1);
				String trainingName = null != cellTrainingName ? cellTrainingName.getStringCellValue() : "";

				// agent number / code
				Cell cellAgentNumber = row.getCell(2);
				CellType agentCelltype = cellAgentNumber.getCellTypeEnum();
				String agentNumber = "";
				if (agentCelltype == CellType.NUMERIC) {
					Double numAgentNumber = cellAgentNumber.getNumericCellValue();
					agentNumber = String.valueOf(numAgentNumber).replaceAll(".0", "");
				} else {
					agentNumber = cellAgentNumber.getStringCellValue();
				}

				// agent name
				Cell cellAgentName = row.getCell(3);
				String agentName = null != cellAgentName ? cellAgentName.getStringCellValue() : "";

				// training date
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String trainingDate = row.getCell(4).getStringCellValue();
				Date trnDateParsed = null;
				try {
					trnDateParsed = df.parse(trainingDate);
				} catch (ParseException e) {
					LOGGER.error("Failed parsing training date");
				}

				CellType cpdCellType = row.getCell(5).getCellTypeEnum();
				String cpdPoint = "";
				if (cpdCellType == CellType.NUMERIC) {
					Double cpdPointNum = row.getCell(5).getNumericCellValue();
					cpdPoint = String.valueOf(cpdPointNum).replaceAll(".0", "");
				} else {
					cpdPoint = row.getCell(5).getStringCellValue();
				}

				String passStatus = row.getCell(6).getStringCellValue();
				
				Double pointDbl = row.getCell(7).getNumericCellValue();
				BigDecimal point = new BigDecimal(pointDbl, MathContext.DECIMAL32);

				TrainingTestResultDTO tDto = new TrainingTestResultDTO();
				tDto.setTrainingCode(trainingCode);
				tDto.setTrainingName(trainingName);
				tDto.setAgentCode(agentNumber);
				tDto.setAgentName(agentName);
				tDto.setTrainingDate(trnDateParsed);
				tDto.setCpdPoint(Integer.valueOf(cpdPoint));
				tDto.setPass("PASS".equals(passStatus));
				tDto.setPoint(point);

				result.add(tDto);

			}
			i++;
		}

		workbook.close();

		return result;

	}

}
