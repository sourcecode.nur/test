package com.prudential.lms.resource.training.service;

import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;

public interface TrainingCertificationService {
	String generateTrainingCertification(TrainingRegistrationSchedule trainingRegistrationSchedule, String templateImage);
}
