package com.prudential.lms.resource.training.config;

import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.ftp.session.FtpRemoteFileTemplate;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;

import com.jcraft.jsch.ChannelSftp.LsEntry;

@Configuration
public class ContentFTPConfiguration {

	@Value("${app.content.ftp.host}")
	private String ftpHost;
	@Value("${app.content.ftp.port}")
	private int ftpPort;
	@Value("${app.content.ftp.username}")
	private String ftpUsername;
	@Value("${app.content.ftp.password}")
	private String ftpPassword;
	@Value("${app.content.ftp.root-dir}")
	private String ftpRootDir;

	private ExpressionParser expressionParser = new SpelExpressionParser();
	
	
	@Bean
	public SessionFactory<LsEntry> ftpsSessionFactory() {
		DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
	    factory.setHost(ftpHost);
	    factory.setPort(ftpPort);
	    factory.setUser(ftpUsername);
	    factory.setPassword(ftpPassword);
	    factory.setAllowUnknownKeys(true);
		return new CachingSessionFactory<LsEntry>(factory);
	}
	
	@Bean
	public SftpRemoteFileTemplate sftpTemplate() {
		SftpRemoteFileTemplate sftpTemplate = new SftpRemoteFileTemplate(ftpsSessionFactory());
		sftpTemplate.setRemoteDirectoryExpression(expressionParser.parseExpression("'" + ftpRootDir + "'"));
		sftpTemplate.setAutoCreateDirectory(true);
		return sftpTemplate;
	}
	
	@Bean
	public SessionFactory<FTPFile> ftpSessionFactory() {
		DefaultFtpSessionFactory sf = new DefaultFtpSessionFactory();
		sf.setHost(ftpHost);
		sf.setPort(ftpPort);
		sf.setUsername(ftpUsername);
		sf.setPassword(ftpPassword);
		return new CachingSessionFactory<FTPFile>(sf);
	}

	@Bean
	public FtpRemoteFileTemplate ftpTemplate() {
		FtpRemoteFileTemplate ftpTemplate = new FtpRemoteFileTemplate(ftpSessionFactory());
		ftpTemplate.setRemoteDirectoryExpression(expressionParser.parseExpression("'" + ftpRootDir + "'"));
		ftpTemplate.setAutoCreateDirectory(true);
		return ftpTemplate;
	}

}
