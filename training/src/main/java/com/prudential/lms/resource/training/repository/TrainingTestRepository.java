package com.prudential.lms.resource.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingTest;

public interface TrainingTestRepository
		extends JpaRepository<TrainingTest, String>, QueryDslPredicateExecutor<TrainingTest> {

}
