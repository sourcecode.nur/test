package com.prudential.lms.resource.training.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prudential.lms.resource.training.mapper.TrainingCPDMapper;
import com.prudential.lms.resource.training.model.QTrainingCPD;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCPD;
import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.TrainingCPDMutationRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.service.TrainingCPDService;
import com.prudential.lms.shared.constant.CPDMutationOperation;
import com.prudential.lms.shared.dto.training.TrainingCPDDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.querydsl.core.types.dsl.BooleanExpression;

@Service
public class TrainingCPDServiceImpl implements TrainingCPDService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCPDServiceImpl.class);

    @Autowired
    private TrainingCPDRepository trnCpdRepo;

    @Autowired
    private TrainingCPDMutationRepository trnCpdMtnRepo;

    @Autowired
    private TrainingRepository trnRepo;

    @Autowired
    private TrainingCPDMapper trnCpdMapper;

    @Transactional
    @Override
    public List<TrainingCPDDTO> importUpdateCPD(InputStream inputStream) throws IOException, LmsException {

        List<TrainingCPDDTO> result = new ArrayList<>();
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);

        int i = 0;
        for (Row row : sheet) {
            if (i > 0) {

                //training code
                Cell cellTrainingCode = row.getCell(0);
                String trainingCode = null != cellTrainingCode ? cellTrainingCode.getStringCellValue() : "";

                //training name
                Cell cellTrainingName = row.getCell(1);
                String trainingName = null != cellTrainingName ? cellTrainingName.getStringCellValue() : "";

                CellType agentCelltype = row.getCell(2).getCellTypeEnum();
                String agentNumber = "";
                if (agentCelltype == CellType.NUMERIC) {
                    Double numAgentNumber = row.getCell(2).getNumericCellValue();
                    agentNumber = String.valueOf(numAgentNumber).replaceAll(".0", "");
                } else {
                    agentNumber = row.getCell(2).getStringCellValue();
                }

                //agent name
                Cell cellAgentName = row.getCell(3);
                String agentName = null != cellAgentName ? cellAgentName.getStringCellValue() : "";

                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                String trainingDate = row.getCell(4).getStringCellValue();
                Date trnDateParsed = null;
                try {
                    trnDateParsed = df.parse(trainingDate);
                } catch (ParseException e) {
                    throw new LmsException("Failed parsing training date");
                }

                CellType cpdCellType = row.getCell(5).getCellTypeEnum();
                String cpdPoint = "";
                if (cpdCellType == CellType.NUMERIC) {
                    Double cpdPointNum = row.getCell(5).getNumericCellValue();
                    cpdPoint = String.valueOf(cpdPointNum).replaceAll(".0", "");
                } else {
                    cpdPoint = row.getCell(5).getStringCellValue();
                }

                // String passStatus = row.getCell(6).getStringCellValue();
                //
                // TrainingCPDDTO dto = new TrainingCPDDTO();
                // dto.setAgentCode(agentNumber);
                // dto.setAgentName(agentName);
                // dto.setPoint(Integer.valueOf(cpdPoint));
                //
                // Training training = trnRepo.findByCode(trainingCode);
                // if (null == training)
                // throw new LmsException("Training with code: " + trainingCode
                // + " not found");
                //
                // dto.setTrainingId(training.getId());
                // dto.setTrainingCode(trainingCode);
                // dto.setTrainingName(trainingName);
                // dto.setTrainingDate(trnDateParsed);
                BooleanExpression predicate = QTrainingCPD.trainingCPD.trainingCode.equalsIgnoreCase(trainingCode);
                predicate = predicate.and(QTrainingCPD.trainingCPD.agent.code.eq(agentNumber));

                TrainingCPD trnCPD = trnCpdRepo.findOne(predicate);
                if (null == trnCPD) {
                    continue;
                }

                int currentCPD = trnCPD.getPoint(); // 1
                int currentCPDAAJI = trnCPD.getMutations().size(); // 0
                int maxLoop = currentCPD - currentCPDAAJI;
//				currentCPD = currentCPD - currentCPDAAJI; // 1
                int cpd = Integer.valueOf(cpdPoint); //1
                maxLoop = maxLoop - cpd;

                if (currentCPDAAJI + cpd <= currentCPD) {
                    List<TrainingCPDMutation> mtnList = new ArrayList<TrainingCPDMutation>();
//					1, 2
                    for (int j = 0; j < cpd; j++) {
//					for (int j = currentCPDAAJI+cpd ; j <=currentCPD  ; j++) {
                        TrainingCPDMutation mtn = new TrainingCPDMutation();
                        mtn.setCpd(trnCPD);
                        mtn.setCurrentPoint(1);
                        mtn.setMutationOperation(CPDMutationOperation.PLUS);
                        mtnList.add(mtn);
                    }
                    trnCpdMtnRepo.save(mtnList);
                }

            }
            i++;
        }

        workbook.close();

        return result;

    }

    @Override
    public List<TrainingCPDDTO> importParseCPD(InputStream inputStream) throws IOException, LmsException {

        List<TrainingCPDDTO> result = new ArrayList<>();
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);

        int i = 0;
        for (Row row : sheet) {
            if (i > 0) {

                //training code
                Cell cellTrainingCode = row.getCell(0);
                if (cellTrainingCode == null) {
                    continue;
                }
                String trainingCode = null != cellTrainingCode ? cellTrainingCode.getStringCellValue() : "";

                //training name
                Cell cellTrainingName = row.getCell(1);
                if (cellTrainingName == null) {
                    continue;
                }
                String trainingName = null != cellTrainingName ? cellTrainingName.getStringCellValue() : "";

                //agent number / code
                Cell cellAgentNumber = row.getCell(2);
                if (cellAgentNumber == null) {
                    continue;
                }
                CellType agentCelltype = cellAgentNumber.getCellTypeEnum();
                String agentNumber = "";
                if (agentCelltype == CellType.NUMERIC) {
                    Double numAgentNumber = cellAgentNumber.getNumericCellValue();
                    agentNumber = String.valueOf(numAgentNumber).replaceAll(".0", "");
                } else {
                    agentNumber = cellAgentNumber.getStringCellValue();
                }

                //agent name
                Cell cellAgentName = row.getCell(3);
                if (cellTrainingCode == null) {
                    continue;
                }
                String agentName = null != cellAgentName ? cellAgentName.getStringCellValue() : "";

                //training date
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                Cell cellTrainingDate = row.getCell(4);
                if (cellTrainingDate == null) {
                    continue;
                }
                String trainingDate = cellTrainingDate != null ? cellTrainingDate.getStringCellValue() : "";
                Date trnDateParsed = null;
                try {
                    trnDateParsed = df.parse(trainingDate);
                } catch (ParseException e) {
                    LOGGER.error("Failed parsing training date");
                }

                //cpd point
                Cell cellCpdCellType = row.getCell(5);
                if (cellCpdCellType == null) {
                    continue;
                }
                CellType cpdCellType = cellCpdCellType != null ? cellCpdCellType.getCellTypeEnum() : null;
                String cpdPoint = "";
                if (cpdCellType == CellType.NUMERIC) {
                    Double cpdPointNum = row.getCell(5).getNumericCellValue();
                    cpdPoint = String.valueOf(cpdPointNum).replaceAll(".0", "");
                } else {
                    cpdPoint = cellCpdCellType != null ? cellCpdCellType.getStringCellValue() : "0";
                }

                TrainingCPDDTO dto = new TrainingCPDDTO();
                dto.setAgentCode(agentNumber);
                dto.setAgentName(agentName);
                dto.setPoint(Integer.valueOf(cpdPoint));

                Training training = trnRepo.findByCode(trainingCode);
                if (null == training) {
                    dto.setTrainingId(0l);
                } else {
                    dto.setTrainingId(training.getId());
                }

                dto.setTrainingCode(trainingCode);
                dto.setTrainingName(trainingName);
                dto.setTrainingDate(trnDateParsed);

                result.add(dto);
            }
            i++;
        }

        workbook.close();

        return result;
    }

}
