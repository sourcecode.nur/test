package com.prudential.lms.resource.training.service.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.service.TrainingCertificationService;

@Service
public class TrainingCertificationServiceImpl implements TrainingCertificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCertificationServiceImpl.class);

	@Override
	public String generateTrainingCertification(TrainingRegistrationSchedule trainingRegistrationSchedule,
			String templateImageLocation) {
		// File templatePath = new File("");// TODO: change templatePath
		File targetPath = new File(System.getProperty("user.home") + File.separator + "tmp");// TODO:
																								// change
																								// targetPath

		try {
			// load source images
			LOGGER.debug("Load sourceImage from " + templateImageLocation);
			BufferedImage templateImage = ImageIO.read(new URL(templateImageLocation));

			LOGGER.debug("processText");
			BufferedImage combined = this.processText(trainingRegistrationSchedule, templateImage);

			LOGGER.debug("save combined image");
			// Save as new image
			ImageIO.write(combined, "PNG", new File(targetPath, "combined.png"));

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private BufferedImage processText(TrainingRegistrationSchedule trainingRegistrationSchedule,
			BufferedImage templateImage) {
		int w = templateImage.getWidth();
		int h = templateImage.getHeight();

		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = img.createGraphics();

		g2d.drawImage(templateImage, 0, 0, w, h, null);
		g2d.setPaint(Color.BLACK);

		String agentName = trainingRegistrationSchedule.getRegistration().getAgent().getName();
		String agentCode = trainingRegistrationSchedule.getRegistration().getAgent().getCode();
		String trainingName = trainingRegistrationSchedule.getSchedule().getTraining().getName();
		String trainingFinishDate = DateFormat.getDateInstance()
				.format(trainingRegistrationSchedule.getSchedule().getEndDate());
		String certificateCode = "CER-0001"; // TODO : find how to get
												// certification code

		g2d.setFont(new Font("Serif", Font.BOLD, 82));
		g2d.drawString(agentName, (img.getWidth() - g2d.getFontMetrics().stringWidth(agentName)) / 2, 417);
		g2d.setFont(new Font("Serif", Font.BOLD, 62));
		g2d.drawString(agentCode, (img.getWidth() - g2d.getFontMetrics().stringWidth(agentCode)) / 2, 509);
		g2d.setFont(new Font("Serif", Font.BOLD, 54));
		g2d.drawString(trainingName, (img.getWidth() - g2d.getFontMetrics().stringWidth(trainingName)) / 2, 681);
		g2d.setFont(new Font("Serif", Font.BOLD, 32));
		g2d.drawString(trainingFinishDate, (img.getWidth() - g2d.getFontMetrics().stringWidth(trainingFinishDate)) / 2,
				780);
		g2d.setFont(new Font("Serif", Font.PLAIN, 20));
		g2d.drawString(certificateCode, 87, 1090);

		g2d.dispose();

		return img;
	}

}
