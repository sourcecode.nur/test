package com.prudential.lms.resource.training.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_question")
public class TrainingQuestion extends AuditableEntityBase implements EntityBase<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8709152453680800637L;
	private static final String SEQ_GEN = "seq_gen_trn_quest_uuid";
	private static final String SEQ_STRATEGY = "uuid";

	private String id;
	
	@Id
	@GeneratedValue(generator = SEQ_GEN)
	@GenericGenerator(name = SEQ_GEN, strategy = SEQ_STRATEGY)
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

}
