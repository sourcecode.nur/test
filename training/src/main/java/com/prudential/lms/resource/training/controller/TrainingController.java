package com.prudential.lms.resource.training.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.TrainingContentMapper;
import com.prudential.lms.resource.training.mapper.TrainingMapper;
import com.prudential.lms.resource.training.mapper.TrainingTestMapper;
import com.prudential.lms.resource.training.model.ContentLibrary;
import com.prudential.lms.resource.training.model.QTraining;
import com.prudential.lms.resource.training.model.QTrainingContent;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCategory;
import com.prudential.lms.resource.training.model.TrainingContent;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingTest;
import com.prudential.lms.resource.training.repository.ContentLibraryRepository;
import com.prudential.lms.resource.training.repository.TrainingCategoryRepository;
import com.prudential.lms.resource.training.repository.TrainingContentRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.repository.TrainingTestRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.TrainingContentType;
import com.prudential.lms.shared.constant.TrainingStatus;
import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.constant.TrainingType;
import com.prudential.lms.shared.dto.training.TrainingContentDTO;
import com.prudential.lms.shared.dto.training.TrainingDTO;
import com.prudential.lms.shared.dto.training.TrainingTestDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training")
public class TrainingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingController.class);

	@Autowired
	private TrainingRepository repo;

	@Autowired
	private TrainingCategoryRepository trainingCategoryRepo;

	@Autowired
	private TrainingMapper mapper;

	@Autowired
	private TrainingTestMapper trnTestMapper;

	@Autowired
	private TrainingTestRepository trnTestRepo;

	@Autowired
	private ContentLibraryRepository ctnLibRepo;

	@Autowired
	private TrainingContentRepository trnCtnRepo;

	@Autowired
	private TrainingRegistrationScheduleRepository trnRegSchRepo;

	@Autowired
	private TrainingContentMapper trnCtnMapper;

	// List All
	@RequestMapping(value = "/list/all", method = RequestMethod.POST)
	public Object listAll(@RequestBody(required = false) TrainingDTO dto) {

		Response<Object> response = null;
		Iterable<Training> trnPage = null;

		try {

			BooleanExpression predicate = QTraining.training.id.gt(-1);
			predicate = predicate.and(QTraining.training.deleted.isFalse());

			if (null != dto) {

				Long trnCtgrId = dto.getTrainingCategoryId();
				if (null != trnCtgrId)
					predicate = predicate.and(QTraining.training.category.id.eq(trnCtgrId));

				TrainingType trainingType = dto.getTrainingType();
				if (null != trainingType)
					predicate = predicate.and(QTraining.training.trainingType.eq(trainingType));

				String name = dto.getName();
				if (StringUtils.isNotEmpty(name))
					predicate = predicate.and(QTraining.training.name.likeIgnoreCase("%" + name + "%"));

				String description = dto.getDescription();
				if (StringUtils.isNotEmpty(description))
					predicate = predicate.and(QTraining.training.description.likeIgnoreCase("%" + description + "%"));

				TrainingStatus status = dto.getStatus();
				if (null != status)
					predicate = predicate.and(QTraining.training.status.eq(status));

				int cpdPoint = dto.getCpdPoint();
				if (cpdPoint > -1)
					predicate = predicate.and(QTraining.training.cpdPoint.eq(cpdPoint));

				boolean draft = dto.isDraft();
				predicate = predicate.and(QTraining.training.draft.eq(draft));

				boolean preEqualPost = dto.isPreEqualPost();
				predicate = predicate.and(QTraining.training.preEqualPost.eq(preEqualPost));

			}

			trnPage = repo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));
		} catch (Exception e) {
			LOGGER.error("#####ERROR List Training", e);
			return Response.internalServerError(e.getMessage());
		}

		List<TrainingDTO> listResult = mapper.convertToDto(trnPage);
		for (TrainingDTO trainingDTO : listResult) {
			trainingDTO.setThumbnail("");
		}

		response = listResult.size() > 0 ? Response.found(listResult) : Response.notFound();

		return response;
	}

	// Remove Content
	@RequestMapping(value = "/content/remove/{trainingId}/{trainingContentId}", method = RequestMethod.POST)
	public Object removeContent(@PathVariable("trainingId") Long trainingId,
			@PathVariable("trainingContentId") Long trainingContentId) {

		BooleanExpression p = QTrainingContent.trainingContent.id.eq(trainingContentId);
		p = p.and(QTrainingContent.trainingContent.training.id.eq(trainingId));

		TrainingContent content = trnCtnRepo.findOne(p);
		if (null == content)
			return Response.notFound("No TrainingContent found with provided id's");

		trnCtnRepo.delete(content);

		return Response.ok();
	}

	// Add Main Content
	@RequestMapping(value = "/content/main/{trainingId}/{contentLibraryId}", method = RequestMethod.POST)
	public Object addMainContent(@PathVariable("trainingId") Long trainingId,
			@PathVariable("contentLibraryId") Long contentLibraryId) {

		Response<Object> result = null;

		Training training = repo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id " + trainingId + " not found");

		ContentLibrary library = ctnLibRepo.findOne(contentLibraryId);
		if (null == library)
			return Response.notFound("ContentLibrary with id " + contentLibraryId + " not found");

		try {

			TrainingContentDTO dto = new TrainingContentDTO();
			dto.setType(TrainingContentType.MAIN);
			dto.setTrainingId(trainingId);
			dto.setLibraryId(contentLibraryId);
			TrainingContent trnCtnSaved = trnCtnRepo.save(trnCtnMapper.createEntity(dto));
			if (null != trnCtnSaved)
				result = Response.ok(trnCtnMapper.convertToDto(trnCtnSaved));

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return result;
	}

	// Add Support Content
	@RequestMapping(value = "/content/support/{trainingId}/{contentLibraryId}", method = RequestMethod.POST)
	public Object addSupportContent(@PathVariable("trainingId") Long trainingId,
			@PathVariable("contentLibraryId") Long contentLibraryId) {

		Response<Object> result = null;

		Training training = repo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id " + trainingId + " not found");

		ContentLibrary library = ctnLibRepo.findOne(contentLibraryId);
		if (null == library)
			return Response.notFound("ContentLibrary with id " + contentLibraryId + " not found");

		try {

			TrainingContentDTO dto = new TrainingContentDTO();
			dto.setType(TrainingContentType.SUPPORT);
			dto.setTrainingId(trainingId);
			dto.setLibraryId(contentLibraryId);
			TrainingContent trnCtnSaved = trnCtnRepo.save(trnCtnMapper.createEntity(dto));
			if (null != trnCtnSaved)
				result = Response.ok(trnCtnMapper.convertToDto(trnCtnSaved));

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return result;
	}

	// Remove PostTest
	@RequestMapping(value = "/posttest/remove/{trainingId}", method = RequestMethod.POST)
	public Object removePostTest(@PathVariable("trainingId") Long trainingId) {

		Training training = repo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id " + trainingId + " not found");

		try {

			List<TrainingTest> trainingTests = training.getTrainingTests();
			TrainingTest postTest = getTest(trainingTests, TrainingTestType.POST);
			if (null == postTest)
				return Response.notFound("Training with id " + trainingId + " has no PostTest");

			trnTestRepo.delete(postTest);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok("PostTest removed from Training with id" + trainingId);

	}

	// Remove PreTest
	@RequestMapping(value = "/pretest/remove/{trainingId}", method = RequestMethod.POST)
	public Object removePreTest(@PathVariable("trainingId") Long trainingId) {

		Training training = repo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id " + trainingId + " not found");

		try {

			List<TrainingTest> trainingTests = training.getTrainingTests();
			TrainingTest preTest = getTest(trainingTests, TrainingTestType.PRE);
			if (null == preTest)
				return Response.notFound("Training with id " + trainingId + " has no PreTest");

			trnTestRepo.delete(preTest);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok("PreTest removed from Training with id " + trainingId);

	}

	// Add or Update PreTest
	@RequestMapping(value = "/pretest/{trainingId}", method = RequestMethod.POST)
	public Object addOrUpdatePreTest(@PathVariable("trainingId") Long trainingId, @RequestBody TrainingTestDTO dto) {

		TrainingTest result = null;

		Training training = repo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id " + trainingId + " not found");

		try {

			List<TrainingTest> trainingTests = training.getTrainingTests();
			TrainingTest preTest = getTest(trainingTests, TrainingTestType.PRE);
			if (null == preTest) {
				dto.setType(TrainingTestType.PRE);
				dto.setTrainingId(trainingId);
				preTest = trnTestMapper.createEntity(dto);
			} else {
				dto.setId(preTest.getId());
				dto.setTrainingId(trainingId);
				dto.setType(TrainingTestType.PRE);
				preTest = trnTestMapper.updateEntity(dto);
			}

			result = trnTestRepo.save(preTest);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(trnTestMapper.convertToDto(result));
	}

	// Add or Update PostTest
	@RequestMapping(value = "/posttest/{trainingId}", method = RequestMethod.POST)
	public Object addOrUpdatePostTest(@PathVariable("trainingId") Long trainingId, @RequestBody TrainingTestDTO dto) {

		TrainingTest result = null;

		Training training = repo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id " + trainingId + " not found");

		try {

			List<TrainingTest> trainingTests = training.getTrainingTests();
			TrainingTest postTest = getTest(trainingTests, TrainingTestType.POST);
			if (null == postTest) {
				dto.setType(TrainingTestType.POST);
				dto.setTrainingId(trainingId);
				postTest = trnTestMapper.createEntity(dto);
			} else {
				dto.setId(postTest.getId());
				dto.setTrainingId(trainingId);
				dto.setType(TrainingTestType.POST);
				postTest = trnTestMapper.updateEntity(dto);
			}

			result = trnTestRepo.save(postTest);

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(trnTestMapper.convertToDto(result));
	}

	// List Paging
	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
	public Object listPaging(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) TrainingDTO dto) {

		Response<Object> response = null;
		Page<Training> trnPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES);

		try {

			BooleanExpression predicate = QTraining.training.id.gt(-1);
			predicate = predicate.and(QTraining.training.deleted.isFalse());

			if (null != dto) {

				Long trnCtgrId = dto.getTrainingCategoryId();
				if (null != trnCtgrId)
					predicate = predicate.and(QTraining.training.category.id.eq(trnCtgrId));

				TrainingType trainingType = dto.getTrainingType();
				if (null != trainingType)
					predicate = predicate.and(QTraining.training.trainingType.eq(trainingType));

				String name = dto.getName();
				if (StringUtils.isNotEmpty(name))
					predicate = predicate.and(QTraining.training.name.likeIgnoreCase("%" + name + "%"));

				String description = dto.getDescription();
				if (StringUtils.isNotEmpty(description))
					predicate = predicate.and(QTraining.training.description.likeIgnoreCase("%" + description + "%"));

				TrainingStatus status = dto.getStatus();
				if (null != status)
					predicate = predicate.and(QTraining.training.status.eq(status));

				int cpdPoint = dto.getCpdPoint();
				if (cpdPoint > -1)
					predicate = predicate.and(QTraining.training.cpdPoint.eq(cpdPoint));

				boolean draft = dto.isDraft();
				predicate = predicate.and(QTraining.training.draft.eq(draft));

				boolean preEqualPost = dto.isPreEqualPost();
				predicate = predicate.and(QTraining.training.preEqualPost.eq(preEqualPost));

			}

			trnPage = repo.findAll(predicate, pr);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Paging Training", e);
			return Response.internalServerError(e.getMessage());
		}

		List<TrainingDTO> listResult = mapper.convertToDto(trnPage.getContent());
		for (TrainingDTO trainingDTO : listResult) {
			trainingDTO.setThumbnail("");
		}

		Page<TrainingDTO> pageResult = new PageImpl<>(listResult, pr, trnPage.getTotalElements());
		response = trnPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

		return response;
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody TrainingDTO dto) {
		Training saved = null;

		String code = dto.getCode();
		if (code.isEmpty() || code.equals(null)) {
			return Response.badRequest("Training code is required!");
		}

		Training byCode = repo.findByCode(dto.getCode());
		if (null != byCode)
			return Response.badRequest("Training with code " + dto.getCode() + " already exist");

		try {
			saved = repo.save(mapper.createEntity(dto));

			if (null != saved && !saved.isDraft()) {
				TrainingCategory tc = saved.getCategory();
				if (null != tc) {
					tc.calcTrainingCount();
					trainingCategoryRepo.save(tc);
				}
			}

		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving Training", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		Training training = repo.findOne(id);
		if (null == training)
			return Response.notFound("Training with id " + id + " not found");

		return Response.found(mapper.convertToDto(training));
	}

	// Find by Training Register Schedule ID
	@RequestMapping(value = "/by-trn-reg-sch-id/{id}", method = RequestMethod.GET)
	public Object getTrainingByTrainingRegistrationScheduleId(@PathVariable("id") Long id) {
		TrainingRegistrationSchedule trs = trnRegSchRepo.findOne(id);
		if (trs == null) {
			return Response.notFound();
		}

		return Response.found(mapper.convertToDto(trs.getSchedule().getTraining()));
	}

	// Find by Training Register Schedule ID
	@RequestMapping(value = "/by-registration-no/{registrationNo}", method = RequestMethod.GET)
	public Object getTrainingByRegistrationNo(@PathVariable("registrationNo") String registrationNo) {
		TrainingRegistrationSchedule trs = trnRegSchRepo.findByRegistrationNo(registrationNo);
		if (trs == null) {
			return Response.notFound();
		}

		return Response.found(mapper.convertToDto(trs.getSchedule().getTraining()));
	}

	// Find by sample
	// @RequestMapping(value = "/find", method = RequestMethod.POST)
	// public Object findByExample(@RequestBody TrainingDTO example) {
	// Response<Object> response = null;
	// List<Training> listTraining = null;
	// if (example != null) {
	// if (example.getCode() != null) {
	// // listTraining = repo.findByCode(example.getCode());
	// } else if (example.getName() != null) {
	// listTraining = repo.findByName(example.getName());
	// } else if (example.getDescription() != null) {
	// listTraining = repo.findByDescription(example.getDescription());
	// } else if (example.getTrainingCategoryId() != null) {
	// listTraining =
	// repo.findByCategory(trainingCategoryRepo.findOne(example.getTrainingCategoryId()));
	// }
	// response = listTraining.size() > 0 ?
	// Response.found(mapper.convertToDto(listTraining))
	// : Response.notFound();
	//
	// return response;
	// } else
	//
	// return Response.notFound();
	// }

	// Update
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody TrainingDTO dto) {

		Training updatedEntity = null;

		try {
			Training training = repo.findOne(id);

			if (null == training) {
				return Response.notFound("Training with id " + id + " not found");
			} else {

				// try {
				TrainingCategory oldTc = training.getCategory();
				dto.setId(id);
				updatedEntity = repo.save(mapper.updateEntity(dto));

				if (null != updatedEntity) {

					TrainingCategory tc = updatedEntity.getCategory();
					if (null != tc) {
						tc.calcTrainingCount();
						trainingCategoryRepo.save(tc);

						if (oldTc != null && tc.getId() != oldTc.getId()) {
							oldTc.calcTrainingCount();
							trainingCategoryRepo.save(oldTc);
						}

					}
				}

				// } catch (Exception e) {
				// LOGGER.error("#####ERROR Updating Training", e);
				// return Response.internalServerError(e.getMessage());
				// }

			}
		} catch (Exception e) {
			LOGGER.error("#####ERROR Updating Training", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		Training training = repo.findOne(id);

		if (null == training) {
			return Response.notFound();
		}

		try {
			repo.delete(training);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting Training", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

	@RequestMapping(value = "/listCPD", method = RequestMethod.GET)
	public Object listCpd() {

		Response<Object> response = null;
		List<Training> listTraining = null;

		try {

			listTraining = repo.findWhereCpdPointWasNotNull();
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing Training", e);
			return Response.internalServerError(e.getMessage());
		}

		List<TrainingDTO> listDto = mapper.convertToDto(listTraining);
		for (TrainingDTO trainingDTO : listDto) {
			trainingDTO.setThumbnail("");
		}

		response = listTraining.size() > 0 ? Response.found(listDto) : Response.notFound();

		return response;
	}

	private TrainingTest getTest(List<TrainingTest> tests, TrainingTestType type) {

		TrainingTest result = null;
		for (TrainingTest trainingTest : tests) {
			if (trainingTest.getType().equals(type)) {
				result = trainingTest;
				break;
			}
		}

		return result;
	}

	// Soft Delete
	@RequestMapping(value = "/softdelete/{id}", method = RequestMethod.POST)
	public Object softDelete(@PathVariable("id") Long id) {

		Training tc = repo.findOne(id);
		if (null == tc)
			return Response.notFound("Training with id " + id + " not found");

		try {
			processSoftDelete(tc, true);
		} catch (Exception e) {
			return Response.internalServerError();
		}

		return Response.ok();
	}

	private void processSoftDelete(Training training, boolean deleted) {
		try {

			training.setDeleted(deleted);
			repo.save(training);

		} catch (Exception e) {
			LOGGER.error("#####ERROR Soft-Deleting Training", e);
		}

	}

}
