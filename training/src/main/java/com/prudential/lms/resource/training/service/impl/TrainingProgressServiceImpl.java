package com.prudential.lms.resource.training.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prudential.lms.resource.training.model.TrainingProgress;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.repository.TrainingProgressRepository;
import com.prudential.lms.resource.training.service.TrainingProgressService;
import com.prudential.lms.shared.constant.TrainingProgressStatus;
import static com.prudential.lms.shared.constant.TrainingProgressStatus.*;

@Service
@Transactional
public class TrainingProgressServiceImpl implements TrainingProgressService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingProgressServiceImpl.class);

	@Autowired
	private TrainingProgressRepository progressRepo;

	private TrainingProgress addProgress(TrainingRegistrationSchedule regSch, TrainingProgressStatus status) {
		TrainingProgress progress = new TrainingProgress();
		progress.setStatus(status);
		progress.setRegistrationSchedule(regSch);
		return progressRepo.save(progress);
	}

	@Override
	public void register(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, REGISTERED);
		LOGGER.debug("Training registrationNo "+regSch.getRegistrationNo()+" REGISTERED");
	}

	@Override
	public void begin(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, BEGIN);
	}

	@Override
	public void pretestStart(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, PRETEST);
	}

	@Override
	public void pretestFinish(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, PRETEST_FINISH);
	}

	@Override
	public void trainingStart(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, TRAINING);
	}

	@Override
	public void trainingFinish(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, TRAINING_FINISH);
	}

	@Override
	public void posttestStart(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, POSTTEST);
	}

	@Override
	public void posttestFinish(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, POSTTEST_FINISH);
	}

	@Override
	public void end(TrainingRegistrationSchedule regSch) {
		addProgress(regSch, END);
	}

}
