package com.prudential.lms.resource.training.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.AgentGroup;
import com.prudential.lms.resource.training.model.TrainingCPD;

public interface AgentRepository extends JpaRepository<Agent, Long>, QueryDslPredicateExecutor<Agent> {

	Agent findByCode(String code);

	Agent findByName(String name);

	List<Agent> findByStartLicenseDate(Date startLicenseDate);

	List<Agent> findByEndLicenseDate(Date endLicenseDate);

	List<Agent> findByAgentGroup(AgentGroup agentGroup);

	//List<Agent> findByCpd(TrainingCPD trainingCPD);

}
