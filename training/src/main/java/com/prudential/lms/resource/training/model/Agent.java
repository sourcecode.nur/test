package com.prudential.lms.resource.training.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;
import com.prudential.lms.shared.model.SoftDelete;

@Entity
@Table(name = "lms_agent")
public class Agent extends AuditableEntityBase implements EntityBase<Long>, SoftDelete {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2250645196349430881L;
	private static final String SEQ_GEN = "seq_gen_agent";
	private static final String SEQ = "seq_agent";

	private Long id;
	private String code;
	private String name;
	private Date startLicenseDate;
	private Date endLicenseDate;
	private AgentGroup agentGroup;
	private boolean groupLeader;
	private boolean deleted;
	private List<TrainingCPD> cpds;
	private TrainingRegistration registration;

	// for training requirement
	private Date birthDate = new Date(-769208400000l); // default is 17-08-1945
	private int age;
	private int casePoint;
	private BigDecimal apiPoint;
	private boolean underReview;
	private String levelType;

	private String officeCode;
	private String officeName;
	private String radd;
	private String ads;
	
	private String leaderCode;
	private String leaderName;

	@PrePersist
	private void prePersist() {
		this.age = new Date(System.currentTimeMillis()).getYear() - getBirthDate().getYear();
	}

	@PreUpdate
	private void preUpdate() {
		this.age = new Date(System.currentTimeMillis()).getYear() - getBirthDate().getYear();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Type(type = "yes_no")
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;

	}

	@Column(length = 64, unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartLicenseDate() {
		return startLicenseDate;
	}

	public void setStartLicenseDate(Date startLicenseDate) {
		this.startLicenseDate = startLicenseDate;
	}

	public Date getEndLicenseDate() {
		return endLicenseDate;
	}

	public void setEndLicenseDate(Date endLicenseDate) {
		this.endLicenseDate = endLicenseDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agent_group_id")
	public AgentGroup getAgentGroup() {
		return agentGroup;
	}

	public void setAgentGroup(AgentGroup agentGroup) {
		this.agentGroup = agentGroup;
	}

	@Type(type = "yes_no")
	public boolean isGroupLeader() {
		return groupLeader;
	}

	public void setGroupLeader(boolean groupLeader) {
		this.groupLeader = groupLeader;
	}

	@OneToMany(mappedBy = "agent")
	public List<TrainingCPD> getCpds() {
		return cpds;
	}

	public void setCpds(List<TrainingCPD> cpds) {
		this.cpds = cpds;
	}

	@Transient
	public void addCpds(TrainingCPD... cpds) {
		for (TrainingCPD trainingCPD : cpds) {
			this.cpds.add(trainingCPD);
		}
	}

	@OneToOne(mappedBy = "agent", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	public TrainingRegistration getRegistration() {
		return registration;
	}

	public void setRegistration(TrainingRegistration registration) {
		this.registration = registration;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getCasePoint() {
		return casePoint;
	}

	public void setCasePoint(int casePoint) {
		this.casePoint = casePoint;
	}

	public BigDecimal getApiPoint() {
		return apiPoint;
	}

	public void setApiPoint(BigDecimal apiPoint) {
		this.apiPoint = apiPoint;
	}

	public boolean isUnderReview() {
		return underReview;
	}

	public void setUnderReview(boolean underReview) {
		this.underReview = underReview;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getRadd() {
		return radd;
	}

	public void setRadd(String radd) {
		this.radd = radd;
	}

	public String getAds() {
		return ads;
	}

	public void setAds(String ads) {
		this.ads = ads;
	}

	public String getLeaderName() {
		return leaderName;
	}
	

	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}

	public String getLeaderCode() {
		return leaderCode;
	}
	

	public void setLeaderCode(String leaderCode) {
		this.leaderCode = leaderCode;
	}
	
	

}
