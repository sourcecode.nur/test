package com.prudential.lms.resource.training.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.prudential.lms.resource.training.mapper.ContentLibraryMapper;
import com.prudential.lms.resource.training.model.ContentLibrary;
import com.prudential.lms.resource.training.model.QContentLibrary;
import com.prudential.lms.resource.training.model.QTrainingContent;
import com.prudential.lms.resource.training.model.TrainingContent;
import com.prudential.lms.resource.training.repository.ContentLibraryRepository;
import com.prudential.lms.resource.training.repository.TrainingContentRepository;
import com.prudential.lms.resource.training.service.TrainingContentService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.training.ContentLibraryDTO;
import com.prudential.lms.shared.dto.training.ContentLibraryMapDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;
import java.util.ArrayList;
import org.modelmapper.ModelMapper;

@RestController
//@Controller
@RequestMapping("/content-library")
public class ContentLibraryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentLibraryController.class);

    @Autowired
    private TrainingContentService trnCtnService;

    @Value("${app.upload.base}")
    private String uploadBase;

    @Value("${app.upload.temp}")
    private String uploadTemp;

    @Value("${app.content.base-uri}")
    private String lmsContentBaseUri;

    @Value("${app.content.ftp.root-dir}")
    private String rootFtpPath;

    @Autowired
    private ContentLibraryMapper mapper;

    @Autowired
    private ModelMapper tcMapper;

    @Autowired
    private ContentLibraryRepository repo;

    @Autowired
    private TrainingContentRepository tcRepo;

    private Tika tika = new Tika();

    @RequestMapping(value = "/get-base64-pdf/{libraryId}", method = RequestMethod.POST)
    public Object getBase64(@PathVariable("libraryId") Long libraryId) {

        ContentLibrary library = repo.findOne(libraryId);
        if (null == library) {
            return Response.notFound("Content Library with id: " + libraryId + " not found");
        }

        String mimeType = library.getMimeType();
        if (!mimeType.equalsIgnoreCase("application/pdf")) {
            return Response.badRequest("Content is not pdf file");
        }

        try {

            String pdfPath = library.getPath();
            String result = trnCtnService.getBase64Pdf(pdfPath);
            if (null != result) {
                ContentLibraryDTO dto = new ContentLibraryDTO();
                dto.setContentPDF(result);
                return dto;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError("Resource unavailable");
        }

        return null;

    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public Object list(@RequestBody(required = false) ContentLibraryDTO dto)
            throws IllegalStateException, IOException, LmsException {

        List<ContentLibraryDTO> result = null;

        try {

            BooleanExpression predicate = QContentLibrary.contentLibrary.id.gt(-1);

            if (null != dto) {

                String title = dto.getTitle();
                if (StringUtils.isNotEmpty(title)) {
                    predicate = predicate.and(QContentLibrary.contentLibrary.title.likeIgnoreCase("%" + title + "%"));
                }

                String mimeType = dto.getMimeType();
                if (StringUtils.isNotEmpty(mimeType)) {
                    predicate = predicate
                            .and(QContentLibrary.contentLibrary.mimeType.likeIgnoreCase("%" + mimeType + "%"));
                }

            }

            Iterable<ContentLibrary> contents = repo.findAll(predicate);
            result = mapper.convertToDto(contents);

        } catch (Exception e) {
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);
    }

    @RequestMapping(value = "/list-library", method = RequestMethod.POST)
    public Object listLibrary(@RequestBody(required = false) ContentLibraryMapDTO dto)
            throws IllegalStateException, IOException, LmsException {

        List<ContentLibraryMapDTO> result = null;
        try {
            BooleanExpression predicate = QTrainingContent.trainingContent.library.id.gt(-1);
            if (null != dto) {

                String title = dto.getLibraryTitle();
                if (StringUtils.isNotEmpty(title)) {
                    predicate = predicate.and(QTrainingContent.trainingContent.library.title.likeIgnoreCase("%" + title + "%"));
                }

                String mimeType = dto.getLibraryMimeType();
                if (StringUtils.isNotEmpty(mimeType)) {
                    predicate = predicate
                            .and(QTrainingContent.trainingContent.library.mimeType.likeIgnoreCase("%" + mimeType + "%"));
                }
            }

            Iterable<TrainingContent> items = tcRepo.findAll(predicate);
            result = new ArrayList<>();
            if (items != null) {

                for (TrainingContent tc : items) {
                    ContentLibraryMapDTO clmd = tcMapper.map(tc, ContentLibraryMapDTO.class);
                    result.add(clmd);
                }
            }

            predicate = QContentLibrary.contentLibrary.id.gt(-1);
            predicate = predicate.and(QContentLibrary.contentLibrary.trainingContents.size().eq(0));
            Iterable<ContentLibrary> libItems = repo.findAll(predicate);
            if (libItems != null) {
                for (ContentLibrary item : libItems) {
                    ContentLibraryMapDTO clmd = tcMapper.map(item, ContentLibraryMapDTO.class);
                    clmd.setId(null);
                    result.add(clmd);
                }
            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return Response.internalServerError(e.getMessage());
        }
        return Response.ok(result);
    }

    // Delete
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Object delete(Long id) {
        
        ContentLibrary contentLibrary = repo.findOne(id);
        if (null == contentLibrary) {
            return Response.notFound();
        }
        boolean isUsed = false;
        List<TrainingContent> contents = contentLibrary.getTrainingContents();
        for(TrainingContent tc : contents){
            isUsed = !tc.getTraining().isDeleted();
            if(isUsed){
                break;
            }
        }
        if(isUsed){
            return Response.badRequest("Content library is used!");
        }

        try {
            boolean deleteStatus = false;
            if(contentLibrary.getMimeType().equalsIgnoreCase(AppConstants.CONTENT_TYPE_SCORM)){
                deleteStatus = trnCtnService.deleteScorm(contentLibrary.getPath());
            }else{
                deleteStatus = trnCtnService.delete(contentLibrary.getPath());
            }
            if(deleteStatus){
               repo.delete(contentLibrary); 
            } 
        } catch(LmsException e){
            LOGGER.error("#####ERROR Deleting ContentLibrary", e);
            return Response.internalServerError(e.getMessage());
        }  
        catch (Exception e) {
            LOGGER.error("#####ERROR Deleting ContentLibrary", e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok();
    }

    // Upload Video Content
    @RequestMapping(value = "/upload/video", method = RequestMethod.POST)
    public Object uploadVideo(@RequestParam("content") MultipartFile content, ContentLibraryDTO dto)
            throws IllegalStateException, IOException, LmsException {

        String uniqTempDir = File.separator.concat(UUID.randomUUID().toString().replaceAll("-", ""));
        File tempDir = new File(uploadTemp.concat(uniqTempDir));
        tempDir.mkdirs();

        String fileName = UUID.randomUUID().toString().replaceAll("-", "").concat(".mp4");
        File file = new File(tempDir.getAbsolutePath().concat(File.separator).concat(fileName));
        content.transferTo(file);

        String contentType = tika.detect(file);
        if (!contentType.equalsIgnoreCase(AppConstants.CONTENT_TYPE_VIDEO)) {
            file.delete();
            return Response.badRequest("invalid video format");
        }

        String videoSent = trnCtnService.sendFtpVideo(file);
        LOGGER.debug("stored: {}", videoSent);

        dto.setMimeType(contentType);
        dto.setPath(videoSent);
        dto.setBaseUri(lmsContentBaseUri);
        String uri = lmsContentBaseUri.concat(videoSent).replaceAll(rootFtpPath, "");
        dto.setUri(uri);
        ContentLibrary clib = repo.save(mapper.createEntity(dto));

        return Response.ok(mapper.convertToDto(clib));
    }

    // Upload PDF Content
    @RequestMapping(value = "/upload/pdf", method = RequestMethod.POST)
    public Object uploadPDF(@RequestPart("content") MultipartFile content, ContentLibraryDTO dto)
            throws IllegalStateException, IOException, LmsException {

        String uniqTempDir = File.separator.concat(UUID.randomUUID().toString().replaceAll("-", ""));
        File tempDir = new File(uploadTemp.concat(uniqTempDir));
        tempDir.mkdirs();

        String fileName = UUID.randomUUID().toString().replaceAll("-", "").concat(".pdf");
        File file = new File(tempDir.getAbsolutePath().concat(File.separator).concat(fileName));
        content.transferTo(file);

        String contentType = tika.detect(file);
        if (!contentType.equalsIgnoreCase(AppConstants.CONTENT_TYPE_PDF)) {
            file.delete();
            return Response.badRequest("invalid pdf format");
        }

        String pdfSent = trnCtnService.sendFtpPDF(file);
        LOGGER.debug("stored: {}", pdfSent);

        dto.setMimeType(contentType);
        dto.setPath(pdfSent);
        dto.setBaseUri(lmsContentBaseUri);
        String uri = lmsContentBaseUri.concat(pdfSent).replaceAll(rootFtpPath, "");
        dto.setUri(uri);
        ContentLibrary clib = repo.save(mapper.createEntity(dto));

        return Response.ok(mapper.convertToDto(clib));
    }

    // Upload SCORM Content
    @RequestMapping(value = "/upload/scorm", method = RequestMethod.POST)
    public Object uploadScorm(@RequestParam("content") MultipartFile content, ContentLibraryDTO dto)
            throws IllegalStateException, IOException, LmsException {

        String uniqTempDir = File.separator.concat(UUID.randomUUID().toString().replaceAll("-", ""));
        File tempDir = new File(uploadTemp.concat(uniqTempDir));
        tempDir.mkdirs();

        String fileName = content.getOriginalFilename();
        File file = new File(tempDir.getAbsolutePath().concat(File.separator).concat(fileName));
        content.transferTo(file);

        String contentType = tika.detect(file);
        if (!contentType.equalsIgnoreCase(AppConstants.CONTENT_TYPE_SCORM)) {
            return Response.badRequest("invalid scorm format");
        }

        String scormManifestSent = trnCtnService.sendFtpScorm(file);
        scormManifestSent = rootFtpPath + scormManifestSent.replaceAll(uploadBase, "");
        LOGGER.debug("stored: " + scormManifestSent);

        dto.setMimeType(contentType);
        dto.setPath(scormManifestSent);
        dto.setBaseUri(lmsContentBaseUri);
        String uri = lmsContentBaseUri.concat(scormManifestSent).replaceAll(rootFtpPath, "");
        dto.setUri(uri);
        ContentLibrary clib = repo.save(mapper.createEntity(dto));

        return Response.ok(mapper.convertToDto(clib));
    }

}
