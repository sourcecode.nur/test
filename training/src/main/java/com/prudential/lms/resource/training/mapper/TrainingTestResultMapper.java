package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.model.TrainingTestQuestion;
import com.prudential.lms.resource.training.model.TrainingTestResult;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingTestResultRepository;
import com.prudential.lms.shared.dto.training.TrainingTestQuestionDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingTestResultMapper implements EntityToDTOMapper<TrainingTestResult, TrainingTestResultDTO> {

    @Autowired
    private TrainingTestResultRepository repo;

    @Autowired
    private TrainingRegistrationScheduleRepository trnRegSchRepo;

    @Autowired
    private ModelMapper mapper;

    @Override
    public TrainingTestResult createEntity(TrainingTestResultDTO dto) {
        TrainingTestResult entity = new TrainingTestResult();
        entity.setPoint(dto.getPoint());
        entity.setMinPoint(dto.getMinPoint());
        entity.setAttempt(dto.getAttempt());
        entity.setFinishDuration(dto.getFinishDuration());
        entity.setPass(dto.getPass());
        entity.setSubmitedToLAS(dto.getSubmitedToLAS());
        entity.setFinishDate(new Date());
        entity.setTestType(dto.getTestType());

        Long trnRegSchId = dto.getTrainingRegistrationScheduleId();
        if (null != trnRegSchId) {

            TrainingRegistrationSchedule trnRegSch = trnRegSchRepo.findOne(trnRegSchId);
            if (null != trnRegSch) {
                entity.setTrainingRegistrationSchedule(trnRegSch);
            }

        }

        List<TrainingTestQuestionDTO> questions = dto.getQuestions();
        for (TrainingTestQuestionDTO qDto : questions) {
            TrainingTestQuestion nq = mapper.map(qDto, new TrainingTestQuestion().getClass());
            nq.setAnswerA(qDto.getAnswerA());
            entity.addQuestions(nq);
        }

        return entity;
    }

    @Override
    public TrainingTestResult updateEntity(TrainingTestResultDTO dto) {
        TrainingTestResult entity = repo.findOne(dto.getId());
        entity.setPoint(dto.getPoint());
        entity.setMinPoint(dto.getMinPoint());
        entity.setAttempt(dto.getAttempt());
        entity.setFinishDuration(dto.getFinishDuration());
        entity.setPass(dto.getPass());
        entity.setSubmitedToLAS(dto.getSubmitedToLAS());
        entity.setFinishDate(dto.getFinishDate());
        entity.setTestType(dto.getTestType());

        Long trnRegSchId = dto.getTrainingRegistrationScheduleId();
        if (null != trnRegSchId) {
            TrainingRegistrationSchedule trnRegSch = trnRegSchRepo.findOne(trnRegSchId);
            entity.setTrainingRegistrationSchedule(trnRegSch);
        }

        List<TrainingTestQuestionDTO> questions = dto.getQuestions();
        for (TrainingTestQuestionDTO qDto : questions) {
            entity.addQuestions(mapper.map(qDto, new TrainingTestQuestion().getClass()));
        }

        return entity;
    }

    @Override
    public TrainingTestResultDTO convertToDto(TrainingTestResult entity) {
        TrainingTestResultDTO dto = new TrainingTestResultDTO();
        dto.setId(entity.getId());
        dto.setPoint(entity.getPoint());
        dto.setMinPoint(entity.getMinPoint());
        dto.setAttempt(entity.getAttempt());
        dto.setFinishDuration(entity.getFinishDuration());
        dto.setPass(entity.isPass());
        dto.setSubmitedToLAS(entity.getSubmitedToLAS());
        dto.setFinishDate(entity.getFinishDate());
        dto.setTestType(entity.getTestType());

        TrainingRegistrationSchedule trnRegSch = entity.getTrainingRegistrationSchedule();
        if (null != trnRegSch) {
            dto.setTrainingRegistrationScheduleId(trnRegSch.getId());
            dto.setRegistrationNo(trnRegSch.getRegistrationNo());

            TrainingSchedule schedule = trnRegSch.getSchedule();
            if (null != schedule) {
                Training training = schedule.getTraining();
                if (null != training) {
                    dto.setTrainingId(training.getId());
                    dto.setTrainingCode(training.getCode());
                    dto.setTrainingName(training.getName());
                    dto.setCpdPoint(training.getCpdPoint());
                }
            }

            TrainingRegistration registration = trnRegSch.getRegistration();
            if (null != registration) {
                Agent agent = registration.getAgent();
                if (null != agent) {
                    dto.setAgentId(agent.getId());
                    dto.setAgentCode(agent.getCode());
                    dto.setAgentName(agent.getName());
                    dto.setAgentOfficeCode(agent.getOfficeCode());
                }
            }

        }
        
        List<TrainingTestQuestionDTO> trnTestQuestionDTOList = new ArrayList<>();
        
        List<TrainingTestQuestion> questions = entity.getQuestions();
        if(questions != null){ 
            for(TrainingTestQuestion item : questions){
                TrainingTestQuestionDTO obj = mapper.map(item, TrainingTestQuestionDTO.class);
                trnTestQuestionDTOList.add(obj);
            } 
        }
        
        dto.setQuestions(trnTestQuestionDTOList);

        return dto;
    }

    @Override
    public List<TrainingTestResultDTO> convertToDto(Iterable<TrainingTestResult> entities) {
        List<TrainingTestResultDTO> dtoList = new ArrayList<>();
        for (TrainingTestResult entity : entities) {
            dtoList.add(convertToDto(entity));
        }
        return dtoList;
    }

    @Override
    public TrainingTestResult convertToEntity(TrainingTestResultDTO dto) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<TrainingTestResult> convertToEntity(Iterable<TrainingTestResultDTO> dtos) {
        // TODO Auto-generated method stub
        return null;
    }

}
