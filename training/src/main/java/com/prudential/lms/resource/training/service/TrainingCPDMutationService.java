package com.prudential.lms.resource.training.service;

import com.prudential.lms.resource.training.model.TrainingCPDMutation;

public interface TrainingCPDMutationService {

	TrainingCPDMutation plusCPD(Long trainingCPDId, int point) throws Exception;

	TrainingCPDMutation minusCPD(Long trainingCPDId, int point) throws Exception;

}
