package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.Curriculum;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCurriculum;

public interface TrainingCurriculumRepository
		extends JpaRepository<TrainingCurriculum, Long>, QueryDslPredicateExecutor<TrainingCurriculum> {

	List<TrainingCurriculum> findByCurriculum(Curriculum curriculum);

	List<TrainingCurriculum> findByTraining(Training training);

}
