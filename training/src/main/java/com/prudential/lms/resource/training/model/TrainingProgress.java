package com.prudential.lms.resource.training.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_progress")
public class TrainingProgress extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1852793542018911040L;
	private static final String SEQ_GEN = "seq_gen_training_progress";
	private static final String SEQ = "seq_training_progress";

	private Long id;
	private TrainingRegistrationSchedule registrationSchedule;
	private TrainingProgressStatus status;
	private int progressOrder;
	private int subProgressOrder;

	@PrePersist
	private void prePersist() {
		this.progressOrder = this.status.getOrder();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	public int getProgressOrder() {
		return progressOrder;
	}

	void setProgressOrder(int progressOrder) {
		this.progressOrder = progressOrder;
	}

	@Enumerated(EnumType.STRING)
	public TrainingProgressStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingProgressStatus status) {
		this.status = status;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "trn_reg_sch_id")
	public TrainingRegistrationSchedule getRegistrationSchedule() {
		return registrationSchedule;
	}

	public void setRegistrationSchedule(TrainingRegistrationSchedule registrationSchedule) {
		this.registrationSchedule = registrationSchedule;
	}

	public int getSubProgressOrder() {
		return subProgressOrder;
	}

	public void setSubProgressOrder(int subProgressOrder) {
		this.subProgressOrder = subProgressOrder;
	}

	

}
