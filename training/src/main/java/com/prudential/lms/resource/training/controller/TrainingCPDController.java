package com.prudential.lms.resource.training.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Iterables;
import com.prudential.lms.resource.training.mapper.TrainingCPDMapper;
import com.prudential.lms.resource.training.mapper.TrainingCPDMutationMapper;
import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.QTrainingCPD;
import com.prudential.lms.resource.training.model.TrainingCPD;
import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.resource.training.service.TrainingCPDMutationService;
import com.prudential.lms.resource.training.service.TrainingCPDService;
import com.prudential.lms.shared.dto.training.TrainingCPDDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-cpd")
public class TrainingCPDController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCPDController.class);

	@Autowired
	private TrainingCPDRepository repo;

	@Autowired
	private AgentRepository agentRepo;

	@Autowired
	private TrainingCPDMapper mapper;

	@Autowired
	private TrainingCPDMutationMapper mutationMapper;

	@Autowired
	private TrainingCPDMutationService cpdMutationService;

	@Autowired
	private TrainingCPDService trnCpdService;

	@Value("${app.download.cpd-template}")
	private String cpdTemplatePath;

	// download cpd template
	@RequestMapping(value = "/download/cpd-template", method = RequestMethod.GET)
	public Object downloadCPDTemplate() {

		HttpHeaders headers = new HttpHeaders();
		ByteArrayResource resource = null;
		File excelFile = null;

		try {

			excelFile = new File(cpdTemplatePath);
			Path path = Paths.get(cpdTemplatePath);
			resource = new ByteArrayResource(Files.readAllBytes(path));

			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			headers.add("Content-Disposition", "attachment; filename=" + excelFile.getName());
			headers.add("Access-Control-Expose-Headers", "Content-Disposition");

		} catch (LmsException e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return ResponseEntity.ok().headers(headers).contentLength(excelFile.length())
				.contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

	}

	// parse cpd
	@RequestMapping(value = "/parse", method = RequestMethod.POST)
	public Object parseOnly(@RequestParam("content") MultipartFile content) {

		List<TrainingCPDDTO> result = null;

		try {
			result = trnCpdService.importParseCPD(content.getInputStream());
		} catch (LmsException | IOException e) {
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(result);

	}

	// upload update quiz
	@RequestMapping(value = "/upload/update-cpd", method = RequestMethod.POST)
	public Object parseQuiz(@RequestParam("content") MultipartFile content) {

		List<TrainingCPDDTO> result = null;

		try {
			result = trnCpdService.importUpdateCPD(content.getInputStream());
		} catch (LmsException | IOException e) {
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(result);

	}

	// @RequestMapping(value = "/plus/{agentId}/{point}", method =
	// RequestMethod.PATCH)
	public Object plusCPD(@PathVariable("agentId") Long agentId, @PathVariable("point") int point) {

		Agent agent = agentRepo.findOne(agentId);
		if (null == agent)
			return Response.badRequest("Agent id " + agentId + " not found");

		TrainingCPD cpd = repo.findByAgentId(agentId);
		if (null == cpd)
			return Response.badRequest("TrainingCPD for Agent id " + agentId + " not found");

		Long cpdId = cpd.getId();
		TrainingCPDMutation result = null;

		try {
			result = cpdMutationService.plusCPD(cpdId, point);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(mutationMapper.convertToDto(result));
	}

	// @RequestMapping(value = "/minus/{agentId}/{point}", method =
	// RequestMethod.PATCH)
	public Object minusCPD(@PathVariable("agentId") Long agentId, @PathVariable("point") int point) {

		Agent agent = agentRepo.findOne(agentId);
		if (null == agent)
			return Response.badRequest("Agent id " + agentId + " not found");

		TrainingCPD cpd = repo.findByAgentId(agentId);
		if (null == cpd)
			return Response.badRequest("TrainingCPD for Agent id " + agentId + " not found");

		Long cpdId = cpd.getId();
		TrainingCPDMutation result = null;

		try {
			result = cpdMutationService.minusCPD(cpdId, point);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok(mutationMapper.convertToDto(result));
	}

	// List Paging
	// @RequestMapping(value = "/list/{page}/{size}", method =
	// RequestMethod.POST)
	public Object list(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) TrainingCPDDTO dto) {

		Response<Object> response = null;
		Page<TrainingCPD> trainingCPDPage = null;

		try {
			PageRequest pr = new PageRequest(page, size, Direction.ASC, "sortOrder");

			BooleanExpression point = QTrainingCPD.trainingCPD.point.eq(dto.getPoint());

			trainingCPDPage = repo.findAll(point, pr);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingCPD", e);
			return Response.internalServerError();
		}

		response = trainingCPDPage.getSize() > 0 ? Response.found(mapper.convertToDto(trainingCPDPage.getContent()))
				: Response.notFound();

		return response;
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody TrainingCPDDTO dto) {
		TrainingCPD saved = null;

		Long agentId = dto.getAgentId();
		if (null == agentId)
			return Response.notFound("Agent not found");
		if(null == dto.getTrainingCode())
			return Response.notFound("Training code not found");
		
		Agent agent = agentRepo.findOne(agentId);
		if (null == agent)
			return Response.notFound("Agent not found");

		BooleanExpression predicate = QTrainingCPD.trainingCPD.agent.id.eq(agentId);

		predicate = predicate.and(QTrainingCPD.trainingCPD.trainingCode.equalsIgnoreCase(dto.getTrainingCode()));

		Iterable<TrainingCPD> findAll = repo.findAll(predicate);
		int size = Iterables.size(findAll);

		try {
			
			if (size > 0) return Response.created(mapper.convertToDto(repo.findOne(predicate)));

			saved = repo.save(mapper.createEntity(dto));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving TrainingCPD", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		TrainingCPD trainingCPD = repo.findOne(id);
		if (null == trainingCPD)
			return Response.notFound();

		return Response.found(mapper.convertToDto(trainingCPD));
	}

	// Find By agentId
	@GetMapping("/agent-code/{agentCode}")
	public Object findByAgentCode(@PathVariable("agentCode") String agentCode) {

		List<TrainingCPD> trainingCPD = repo.findByAgentCode(agentCode);

		if (null == trainingCPD)
			return Response.notFound();

		return Response.found(mapper.convertToDto(trainingCPD));
	}

	// Find By agentId
	// @RequestMapping(value = "/agent/{agentId}", method = RequestMethod.GET)
	public Object findByAgentId(@PathVariable("agentId") Long agentId) {
		TrainingCPD trainingCPD = repo.findByAgentId(agentId);
		if (null == trainingCPD)
			return Response.notFound();

		return Response.found(mapper.convertToDto(trainingCPD));
	}

	// Update
	// @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody TrainingCPDDTO dto) {

		TrainingCPD updatedEntity = null;
		TrainingCPD trainingCPD = repo.findOne(id);

		if (null == trainingCPD) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating TrainingCPD", e);
				return Response.internalServerError(e.getMessage());
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Find by sample
	// @RequestMapping(value = "/find", method = RequestMethod.GET)
	// public Object findByExample(@RequestBody TrainingCPDDTO example) {
	// Response<Object> response = null;
	// List<TrainingCPD> listTrainingCPD = null;
	//
	// if (example != null) {
	// if (example.getPoint() > 0) {
	// listTrainingCPD = repo.findByPoint(example.getPoint());
	//
	// }
	// response = listTrainingCPD.size() > 0 ?
	// Response.found(mapper.convertToDto(listTrainingCPD))
	// : Response.notFound();
	//
	// return response;
	// } else
	//
	// return Response.notFound();
	// }

	// Delete
	// @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		TrainingCPD trainingCPD = repo.findOne(id);

		if (null == trainingCPD) {
			return Response.notFound();
		}

		try {
			repo.delete(trainingCPD);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting TrainingCPD", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}
}
