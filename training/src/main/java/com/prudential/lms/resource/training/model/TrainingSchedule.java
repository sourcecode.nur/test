package com.prudential.lms.resource.training.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.constant.TrainingScheduleStatus;
import com.prudential.lms.shared.constant.TrainingType;
import static com.prudential.lms.shared.constant.TrainingType.*;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;
import com.prudential.lms.shared.model.SoftDelete;

@Entity
@Table(name = "lms_training_schedule")
public class TrainingSchedule extends AuditableEntityBase implements EntityBase<Long>, SoftDelete {

	/**
	 * 
	 */
	private static final long serialVersionUID = -40649120299810964L;
	private static final String SEQ_GEN = "seq_gen_training_schedule";
	private static final String SEQ = "seq_training_schedule";

	private Long id;
	private Training training;
	private String batchCode;
	private Date publishStartDate;
	private Date publishEndDate;
	private Date startDate;
	private Date endDate;
	private int quota;
	private int registered;
	private boolean deleted;
	private boolean published;
	private TrainingScheduleStatus status;
	private List<TrainingRegistrationSchedule> registrations = new ArrayList<>();
	private boolean draft;

	@PrePersist
	private void prePersist() {
		TrainingType trainingType = this.training.getTrainingType();
		if (trainingType.equals(ONLINE)) {
			this.batchCode = "99999";
		} else if (trainingType.equals(CLASSROOM)) {
			this.batchCode = getTraining().getCode().concat(String.valueOf(System.currentTimeMillis()));
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "training_id")
	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	@Column(name = "start_dt")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_dt")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getQuota() {
		return quota;
	}

	public void setQuota(int quota) {
		this.quota = quota;
	}

	@Type(type = "yes_no")
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name = "publish_start_dt")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getPublishStartDate() {
		return publishStartDate;
	}

	public void setPublishStartDate(Date publishStartDate) {
		this.publishStartDate = publishStartDate;
	}

	@Column(name = "publish_end_dt")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getPublishEndDate() {
		return publishEndDate;
	}

	public void setPublishEndDate(Date publishEndDate) {
		this.publishEndDate = publishEndDate;
	}

	@OneToMany(mappedBy = "schedule")
	public List<TrainingRegistrationSchedule> getRegistrations() {
		return registrations;
	}

	public void setRegistrations(List<TrainingRegistrationSchedule> registrations) {
		this.registrations = registrations;
	}

	public int getRegistered() {
		return registered;
	}

	public void setRegistered(int registered) {
		this.registered = registered;
	}

	public void register(int peopleCount) {
		this.registered = getRegistered() + peopleCount;
	}

	@Type(type = "yes_no")
	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	@Enumerated(EnumType.STRING)
	public TrainingScheduleStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingScheduleStatus status) {
		this.status = status;
	}

	@Type(type = "yes_no")
	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	

}
