package com.prudential.lms.resource.training.controller;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.TrainingProgressMapper;
import com.prudential.lms.resource.training.model.QTrainingProgress;
import com.prudential.lms.resource.training.model.TrainingProgress;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.repository.TrainingProgressRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.dto.training.TrainingProgressDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-progress")
public class TrainingProgressController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingProgressController.class);

	@Autowired
	private TrainingProgressRepository trnPrgRepo;

	@Autowired
	private TrainingRegistrationRepository trnRegRepo;
	
	@Autowired
	private TrainingRegistrationScheduleRepository trnRegSchRepo;

	@Autowired
	private TrainingProgressMapper trnPrgMapper;

	// List Training Progress By Agent Id
	@RequestMapping(value = "/list/by-agent-id/{agentId}/{registrationNo}", method = RequestMethod.GET)
	public Object listTrainingProgressByAgentId(@PathVariable("agentId") Long agentId, @PathVariable("registrationNo") String registrationNo) {

		TrainingRegistration trnReg = trnRegRepo.findByAgentId(agentId);
		return listTrainingProgress(trnReg, registrationNo);
	}
	
	// List Training Progress By Agent Code
	@RequestMapping(value = "/list/by-agent-code/{agentCode}/{registrationNo}", method = RequestMethod.GET)
	public Object listTrainingProgressByAgentCode(@PathVariable("agentCode") String agentCode, @PathVariable("registrationNo") String registrationNo) {

		TrainingRegistration trnReg = trnRegRepo.findByAgentCode(agentCode);
		return listTrainingProgress(trnReg, registrationNo);
	}
	
	private Object listTrainingProgress(TrainingRegistration trnReg, String registrationNo){
		
		if (null == trnReg)
			return Response.badRequest("TrainingRegistration of Agent not found");

		Iterable<TrainingProgress> result = null;
		BooleanExpression predicate = null;

		try {

			predicate = QTrainingProgress.trainingProgress.id.gt(-1);
			predicate = predicate.and(QTrainingProgress.trainingProgress.registrationSchedule.registrationNo
					.equalsIgnoreCase(registrationNo));
			result = trnPrgRepo.findAll(predicate, new Sort(Direction.ASC, "createdDate", "progressOrder", "subProgressOrder"));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingProgress", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.found(trnPrgMapper.convertToDto(result));
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody TrainingProgressDTO dto) {
		TrainingProgress saved = null;

		// if has END then reject
		boolean hasEnded = false;
		Long trainingRegistrationId = dto.getTrainingRegistrationId();
		TrainingRegistrationSchedule trnRegSch = trnRegSchRepo.findOne(trainingRegistrationId);
		if(null == trnRegSch)
			return Response.badRequest("TrainingRegistrationSchedule id: "+ trainingRegistrationId.toString() +" not found");
		
		Set<TrainingProgress> progresses = trnRegSch.getProgresses();
		for (TrainingProgress progress : progresses) {
			if(progress.getStatus() == TrainingProgressStatus.END){
				hasEnded = true;
				break;
			}
		}
		
		if(hasEnded)
			return Response.badRequest("TrainingRegistrationSchedule already ENDED");
		
		try {
			saved = trnPrgRepo.save(trnPrgMapper.createEntity(dto));
			
			TrainingRegistrationSchedule registrationSchedule = saved.getRegistrationSchedule();
			registrationSchedule.setStatus(saved.getStatus());
			trnRegSchRepo.save(registrationSchedule);
			
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving TrainingProgress", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(trnPrgMapper.convertToDto(saved));
	}

	// Find By Id
	// @RequestMapping(value = "/{id}", method = RequestMethod.GET)
	// public Object findOne(@PathVariable("id") Long id) {
	// TrainingProgress trainingProgress = repo.findOne(id);
	// if (null == trainingProgress)
	// return Response.notFound();
	//
	// return Response.found(mapper.convertToDto(trainingProgress));
	// }

}
