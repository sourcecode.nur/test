package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.Curriculum;

public interface CurriculumRepository extends JpaRepository<Curriculum, Long>, QueryDslPredicateExecutor<Curriculum> {

	Curriculum findByCode(String code);

}
