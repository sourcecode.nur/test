package com.prudential.lms.resource.training.service;

import java.io.File;

import com.prudential.lms.shared.exception.LmsException;

public interface TrainingContentService {

	String getBase64Pdf(String sftpPath) throws LmsException;
	
	String sendFtpScorm(File scorm) throws LmsException;

	String sendFtpPDF(File pdf) throws LmsException;

	String sendFtpVideo(File video) throws LmsException;
        
        boolean delete(String sftpPath) throws LmsException;
        
        boolean deleteScorm(String sftpPath) throws LmsException;

}
