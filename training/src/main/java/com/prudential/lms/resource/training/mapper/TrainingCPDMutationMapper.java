package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.TrainingCPDMutationRepository;
import com.prudential.lms.shared.dto.training.TrainingCPDMutationDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingCPDMutationMapper implements EntityToDTOMapper<TrainingCPDMutation, TrainingCPDMutationDTO> {

	@Autowired
	private TrainingCPDMutationRepository repo;

	public TrainingCPDMutation createEntity(TrainingCPDMutationDTO dto) {
		TrainingCPDMutation entity = new TrainingCPDMutation();
//		entity.setTrainingCPDId(dto.getTrainingCPDId());
		entity.setPrevPoint(dto.getPrevPoint());
		entity.setCurrentPoint(dto.getCurrentPoint());
		entity.setDeltaPoint(dto.getDeltaPoint());
//		entity.setMutationOperation(dto.getMutationOperation());

		return entity;
	}

	public TrainingCPDMutation updateEntity(TrainingCPDMutationDTO dto) {
		TrainingCPDMutation entity = repo.findOne(dto.getId());
//		entity.setTrainingCPDId(dto.getTrainingCPDId());
		entity.setPrevPoint(dto.getPrevPoint());
		entity.setCurrentPoint(dto.getCurrentPoint());
		entity.setDeltaPoint(dto.getDeltaPoint());
//		entity.setMutationOperation(dto.getMutationOperation());

		return entity;
	}

	@Override
	public TrainingCPDMutationDTO convertToDto(TrainingCPDMutation entity) {
		TrainingCPDMutationDTO dto = new TrainingCPDMutationDTO();
//		dto.setTrainingCPDId(entity.getTrainingCPDId());
		dto.setPrevPoint(entity.getPrevPoint());
		dto.setCurrentPoint(entity.getCurrentPoint());
		dto.setDeltaPoint(entity.getDeltaPoint());
//		dto.setMutationOperation(entity.getMutationOperation());
		return dto;
	}

	@Override
	public List<TrainingCPDMutationDTO> convertToDto(Iterable<TrainingCPDMutation> entities) {
		List<TrainingCPDMutationDTO> dtoList = new ArrayList<>();
		for (TrainingCPDMutation entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingCPDMutation convertToEntity(TrainingCPDMutationDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingCPDMutation> convertToEntity(Iterable<TrainingCPDMutationDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
