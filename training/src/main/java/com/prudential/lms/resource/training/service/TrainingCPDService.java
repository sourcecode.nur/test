package com.prudential.lms.resource.training.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.prudential.lms.shared.dto.training.TrainingCPDDTO;
import com.prudential.lms.shared.exception.LmsException;

public interface TrainingCPDService {

	List<TrainingCPDDTO> importUpdateCPD(InputStream inputStream) throws IOException, LmsException;
	
	List<TrainingCPDDTO> importParseCPD(InputStream inputStream) throws IOException, LmsException;

}
