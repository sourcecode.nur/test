package com.prudential.lms.resource.training.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_agent_grp")
public class AgentGroup extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1333579546566919199L;
	private static final String SEQ_GEN = "seq_gen_agent_grp";
	private static final String SEQ = "seq_agent_grp";

	private Long id;
	private Set<Agent> agent = new HashSet<>();
	private String group;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;

	}

	@OneToMany(mappedBy = "agentGroup")
	public Set<Agent> getAgent() {
		return agent;
	}

	public void setAgent(Set<Agent> agent) {
		this.agent = agent;
	}

	@Column(name = "group_name")
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

}
