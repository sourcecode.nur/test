package com.prudential.lms.resource.training.service.impl;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.transaction.Transactional;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.file.remote.InputStreamCallback;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.ftp.session.FtpRemoteFileTemplate;
import org.springframework.integration.sftp.session.SftpRemoteFileTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import com.prudential.lms.resource.training.service.TrainingContentService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.exception.LmsException;
import java.util.List;
import org.apache.commons.net.ftp.FTPFile;

@Service
@Transactional
public class TrainingContentServiceImpl implements TrainingContentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingContentServiceImpl.class);

    @Value("${app.upload.base}")
    private String baseUploadPath;

    @Value("${app.upload.temp}")
    private String tempUploadPath;

    @Value("${app.upload.path}")
    private String contentsUploadPath;

    @Value("${app.upload.scorm}")
    private String scormUploadPath;

    @Value("${app.upload.pdf}")
    private String pdfUploadPath;

    @Value("${app.upload.video}")
    private String videoUploadPath;

    @Value("${app.content.ftp.scorm-dir}")
    private String scormFtpPath;

    @Value("${app.content.ftp.pdf-dir}")
    private String pdfFtpPath;

    @Value("${app.content.ftp.video-dir}")
    private String videoFtpPath;

    @Value("${app.content.ftp.sftp}")
    private boolean isSftp;

    @Autowired
    private FtpRemoteFileTemplate ftpTemplate;

    @Autowired
    private SftpRemoteFileTemplate sftpTemplate;

    private String result;

    private InputStreamCallback base64callback = new InputStreamCallback() {

        @Override
        public void doWithInputStream(InputStream inputStream) throws IOException {

            int available = inputStream.available();
            LOGGER.debug("InputStreamPDF.length: " + available);

//			BufferedInputStream reader = new BufferedInputStream(inputStream);
            byte[] bytes = IOUtils.toByteArray(inputStream);
//			reader.read(bytes, 0, available);
//			reader.close();

            result = new String(Base64.encodeBase64(bytes), StandardCharsets.UTF_8);
        }
    };

    @Override
    public String getBase64Pdf(String sftpPath) throws LmsException {

        boolean ok = sftpTemplate.get(sftpPath, base64callback);

        return ok ? getResult() : null;
    }

    @Override
    public String sendFtpVideo(File video) throws LmsException {

        String videoPath = video.getAbsolutePath().replaceAll(tempUploadPath, "");
        String ftpPath = videoFtpPath + videoPath.replaceAll(video.getName(), "");

        String send = sendFtp(video, ftpPath);

        // clean up temp directory
        File uuidDir = video.getParentFile();
        video.delete();
        uuidDir.delete();

        return send;
    }

    @Override
    public String sendFtpPDF(File pdf) throws LmsException {

        String pdfPath = pdf.getAbsolutePath().replaceAll(tempUploadPath, "");
        LOGGER.debug("##### pdfPath: {}", pdfPath);
        String ftpPath = pdfFtpPath + pdfPath.replaceAll(pdf.getName(), "");
        LOGGER.debug("##### ftpPath: {}", ftpPath);

        String send = sendFtp(pdf, ftpPath);

        // clean up temp directory
        File uuidDir = pdf.getParentFile();
        pdf.delete();
        uuidDir.delete();

        return send;
    }

    @Override
    public String sendFtpScorm(File scorm) throws LmsException {

        String scormManifestPath = null;
        boolean isScorm = isScorm(scorm);

        if (isScorm) {

            long fileSize = scorm.length();
            byte[] buffer = new byte[Integer.valueOf(String.valueOf(fileSize))];

            try {

                String uuid = scorm.getAbsolutePath().replaceAll(File.separator + scorm.getName(), "");
                uuid = uuid.replaceAll(tempUploadPath + File.separator, "");

                FileInputStream fis = new FileInputStream(scorm);
                ZipInputStream zis = new ZipInputStream(fis);
                ZipEntry zipEntry = zis.getNextEntry();

                while (null != zipEntry) {
                    String fileName = zipEntry.getName();
                    File newFile = new File(scormUploadPath + File.separator + uuid + File.separator + fileName);
                    LOGGER.debug("unzipping: {}", newFile.getAbsoluteFile());

                    if (zipEntry.isDirectory()) {
                        newFile.mkdirs();
                    } else {

                        if (newFile.getAbsolutePath().endsWith(AppConstants.SCORM_MANIFEST_FILE)) {
                            scormManifestPath = newFile.getAbsolutePath();
                        }

                        FileOutputStream fos = new FileOutputStream(newFile);
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                        fos.close();
                    }

                    zipEntry = zis.getNextEntry();
                }
                zis.closeEntry();
                zis.close();
                fis.close();

                LOGGER.debug("removing temp file {}", scorm.getAbsolutePath());
                File uuidDir = scorm.getParentFile();
                scorm.delete();
                uuidDir.delete();
                LOGGER.debug("temp file {} removed", scorm.getAbsolutePath());

                // send to FTP
                scanScormDirThenSendFtp(scormManifestPath);

            } catch (FileNotFoundException e) {
                LOGGER.error(e.getMessage());
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }

        } else {
            throw new LmsException("Invalid SCORM package");
        }

        return scormManifestPath;

    }

    private void scanScormDirThenSendFtp(String scormManifestPath) {

        LOGGER.debug("scanning scormBaseDir: {}", scormManifestPath);

        String path = scormManifestPath.replaceAll(File.separator + AppConstants.SCORM_MANIFEST_FILE, "");

        try {

            Files.walk(Paths.get(path)).filter(Files::isRegularFile).forEach(this::sendFtpScorm);
            File scormBaseDir = new File(path);
            scormBaseDir.getParentFile().getParentFile().delete();

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    private void sendFtpScorm(Path path) {

        LOGGER.debug("storing: " + path);
        File newFile = new File(path.toString());
        String ftpPath = scormFtpPath
                + path.toString().replaceAll(scormUploadPath, "").replaceAll(newFile.getName(), "");
        String send = sendFtp(newFile, ftpPath);
        LOGGER.debug("stored: " + send);
    }

    private boolean isZip(File file) {

        boolean result = false;
        Tika tika = new Tika();
        FileInputStream fis;
        ZipInputStream zis;
        try {

            String contentType = tika.detect(file);
            LOGGER.debug("ContentType: " + contentType);
            result = contentType.equalsIgnoreCase("application/zip");

            fis = new FileInputStream(file);
            zis = new ZipInputStream(fis);
            ZipEntry zipEntry = zis.getNextEntry();
            LOGGER.debug("ZipEntry: " + zipEntry);
            result = null != zipEntry;

            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return result;

    }

    private boolean isScorm(File file) {

        boolean fileIsScorm = false;
        boolean isZip = isZip(file);

        if (isZip) {

            FileInputStream fis;
            ZipInputStream zis;
            try {

                fis = new FileInputStream(file);
                zis = new ZipInputStream(fis);
                ZipEntry zipEntry = zis.getNextEntry();
                while (null != zipEntry) {
                    String fileName = zipEntry.getName();
                    File newFile = new File(contentsUploadPath + File.separator + fileName);
                    LOGGER.debug("unzipping: " + newFile.getAbsoluteFile());

                    if (!fileIsScorm) {
                        fileIsScorm = fileName.endsWith("imsmanifest.xml");
                    } else {
                        break;
                    }

                    zipEntry = zis.getNextEntry();
                }

                zis.closeEntry();
                zis.close();
                fis.close();
            } catch (FileNotFoundException e) {
                LOGGER.error(e.getMessage());
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }

        }

        return fileIsScorm;
    }

    private String sendFtp(File file, String subDir) {
        String result = "";
        Message<File> fileToSend = MessageBuilder.withPayload(file).build();
        if (isSftp) {
            result = sftpTemplate.send(fileToSend, subDir, FileExistsMode.REPLACE);
        } else {
            result = ftpTemplate.send(fileToSend, subDir, FileExistsMode.REPLACE);
        }
        return result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean delete(String sftpPath) throws LmsException {
        boolean ok = false;
        try {
            if (isSftp) {
                if (sftpTemplate.exists(sftpPath)) {
                    ok = sftpTemplate.remove(sftpPath);
                } else {
                    ok = true;
                }
            } else {
                if (ftpTemplate.exists(sftpPath)) {
                    ok = ftpTemplate.remove(sftpPath);
                } else {
                    ok = true;
                }
            }
        } catch (Exception e) {
            LOGGER.debug("ERROR delete remote file ", e);
        }
        return ok;
    }

    @Override
    public boolean deleteScorm(String scormPath) throws LmsException {
        boolean ok = false;
        String path = scormPath.replaceAll(File.separator + AppConstants.SCORM_MANIFEST_FILE, "");
        try {
            if (isSftp) {
                LsEntry[] files = sftpTemplate.list(path);
                for (LsEntry file : files) {
                    if (file.getAttrs().isDir()) {
                        deleteScorm(path.concat(file.getFilename()));                        
                    }
                    sftpTemplate.remove(path.concat(file.getFilename()));
                }

            } else {
                FTPFile[] files = ftpTemplate.list(path);
                for(FTPFile file:files){
                    if(file.isDirectory()){
                        deleteScorm(path.concat(file.getName())); 
                    }
                    ftpTemplate.remove(path.concat(file.getName()));
                } 
            }
            ok = true;
        } catch (Exception e) {
            LOGGER.debug("ERROR delete remote file ", e);
        }
        return ok;
    }
}
