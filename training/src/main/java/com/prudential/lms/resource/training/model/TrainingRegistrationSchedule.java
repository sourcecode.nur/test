package com.prudential.lms.resource.training.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_registration_x_schedule")
public class TrainingRegistrationSchedule extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8422945058767993087L;
	private static final String SEQ_GEN = "seq_gen_trn_x_reg";
	private static final String SEQ = "seq_trn_x_reg";

	private Long id;
	private TrainingSchedule schedule;
	private TrainingRegistration registration;
	private String registrationNo;
	private Date startDate;
	private TrainingProgressStatus status;
	private Set<TrainingProgress> progresses = new HashSet<>();
	private List<TrainingTestResult> results = new ArrayList<>();

	@PrePersist
	private void prePersist() {
		String agentCode = getRegistration().getAgent().getCode();
		// String trainingCode = getSchedule().getTraining().getCode();
		String batchCode = getSchedule().getBatchCode();
		this.registrationNo = agentCode.concat(batchCode).concat(currentDate())
				.concat(String.valueOf(System.currentTimeMillis()));

		this.startDate = schedule.getStartDate();
	}

	@Transient
	private String currentDate() {
		DateFormat df = new SimpleDateFormat("YYYYMMdd");
		return df.format(new Date());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "registration_id", referencedColumnName = "id", nullable = false)
	public TrainingRegistration getRegistration() {
		return registration;
	}

	public void setRegistration(TrainingRegistration registration) {
		this.registration = registration;
	}

	@Column(unique = true)
	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@OneToMany(mappedBy = "registrationSchedule", cascade = CascadeType.ALL)
	public Set<TrainingProgress> getProgresses() {
		return progresses;
	}

	public void setProgresses(Set<TrainingProgress> progresses) {
		this.progresses = progresses;
	}

	public void addProgress(TrainingProgress... progresseses) {
		for (TrainingProgress p : progresseses) {
			this.progresses.add(p);
		}
	}

	@ManyToOne
	@JoinColumn(name = "schedule_id", referencedColumnName = "id", nullable = false)
	public TrainingSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(TrainingSchedule schedule) {
		this.schedule = schedule;
	}

	@Enumerated(EnumType.STRING)
	public TrainingProgressStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingProgressStatus status) {
		this.status = status;
	}

	@OneToMany(mappedBy = "trainingRegistrationSchedule")
	public List<TrainingTestResult> getResults() {
		return results;
	}

	public void setResults(List<TrainingTestResult> results) {
		this.results = results;
	}

}
