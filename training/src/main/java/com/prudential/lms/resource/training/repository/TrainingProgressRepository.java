package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingProgress;

public interface TrainingProgressRepository extends JpaRepository<TrainingProgress, Long>,
		QueryDslPredicateExecutor<TrainingProgress> {

//	TrainingProgress findByTrainingRegistration(TrainingRegistration trainingRegistration);
//
//	List<TrainingProgress> findByProgressOrder(int progressOrder);
}
