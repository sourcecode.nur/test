package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.ContentLibrary;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingContent;
import com.prudential.lms.resource.training.repository.ContentLibraryRepository;
import com.prudential.lms.resource.training.repository.TrainingContentRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.shared.dto.training.TrainingContentDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingContentMapper implements EntityToDTOMapper<TrainingContent, TrainingContentDTO> {

	@Autowired
	private TrainingContentRepository repo;

	@Autowired
	private TrainingRepository trainingRepo;

	@Autowired
	private ContentLibraryRepository libraryRepo;

	@Autowired
	private ModelMapper mapper;

	public TrainingContent createEntity(TrainingContentDTO dto) {

		TrainingContent entity = new TrainingContent();
		entity.setType(dto.getType());

		Long trainingId = dto.getTrainingId();
		if (null != trainingId) {
			Training training = trainingRepo.findOne(trainingId);
			entity.setTraining(training);
		}

		Long libraryId = dto.getLibraryId();
		if (null != libraryId) {
			ContentLibrary library = libraryRepo.findOne(libraryId);
			entity.setLibrary(library);
		}

		return entity;
	}

	public TrainingContent updateEntity(TrainingContentDTO dto) {

		TrainingContent entity = repo.findOne(dto.getId());
		entity.setType(dto.getType());

		Long trainingId = dto.getTrainingId();
		if (null != trainingId) {
			Training training = trainingRepo.findOne(trainingId);
			entity.setTraining(training);
		}

		Long libraryId = dto.getLibraryId();
		if (null != libraryId) {
			ContentLibrary library = libraryRepo.findOne(libraryId);
			entity.setLibrary(library);
		}

		return entity;
	}

	@Override
	public TrainingContentDTO convertToDto(TrainingContent entity) {
		TrainingContentDTO dto = mapper.map(entity, TrainingContentDTO.class);
		return dto;
	}

	@Override
	public List<TrainingContentDTO> convertToDto(Iterable<TrainingContent> entities) {
		List<TrainingContentDTO> dtoList = new ArrayList<>();
		for (TrainingContent entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingContent convertToEntity(TrainingContentDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingContent> convertToEntity(Iterable<TrainingContentDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
