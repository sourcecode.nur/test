package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCategory;

public interface TrainingRepository extends JpaRepository<Training, Long>, QueryDslPredicateExecutor<Training> {

	Training findByCode(String code);

	List<Training> findByName(String name);

	List<Training> findByDescription(String description);

	List<Training> findByCategory(TrainingCategory category);
	
	@Query("SELECT t FROM Training t WHERE t.cpdPoint >= 1")
	List<Training> findWhereCpdPointWasNotNull();
}
