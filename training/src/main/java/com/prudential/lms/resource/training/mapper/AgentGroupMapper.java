package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.AgentGroup;
import com.prudential.lms.resource.training.repository.AgentGroupRepository;
import com.prudential.lms.shared.dto.training.AgentDTO;
import com.prudential.lms.shared.dto.training.AgentGroupDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class AgentGroupMapper implements EntityToDTOMapper<AgentGroup, AgentGroupDTO> {

    @Autowired
    private AgentGroupRepository repo;

    public AgentGroup createEntity(AgentGroupDTO dto) {
        AgentGroup entity = new AgentGroup();
        entity.setGroup(dto.getGroup());
        return entity;
    }

    public AgentGroup updateEntity(AgentGroupDTO dto) {
        AgentGroup entity = repo.findOne(dto.getId());
        entity.setGroup(dto.getGroup());

        return entity;
    }

    @Override
    public AgentGroupDTO convertToDto(AgentGroup entity) {
        AgentGroupDTO dto = new AgentGroupDTO();
        dto.setId(entity.getId());
        dto.setGroup(entity.getGroup());
        Set<Agent> agents = entity.getAgent();
        for (Agent agent : agents) {
            AgentDTO agentDTO = new AgentDTO();
            agentDTO.setId(agent.getId());
            agentDTO.setCode(agent.getCode());
            agentDTO.setName(agent.getName());
            agentDTO.setStartLicenseDate(agent.getStartLicenseDate());
            agentDTO.setEndLicenseDate(agent.getEndLicenseDate());
            agentDTO.setAgentGroupId(agent.getAgentGroup().getId());
//			agentDTO.setTrainingCpdId(agent.getCpd().getId());
//			Set<TrainingRegistration> registrations = agent.getRegistrations();
//			for (TrainingRegistration reg : registrations) {
//				TrainingRegistrationDTO regDTO = new TrainingRegistrationDTO();
//				regDTO.setId(reg.getId());
//				regDTO.setRegistrationNo(reg.getRegistrationNo());
//				regDTO.setStartDate(reg.getStartDate());
//				regDTO.setAgentId(reg.getAgent().getId());
//
//				agentDTO.getRegistrations().add(regDTO);
//			}
        }
        return dto;
    }

    @Override
    public List<AgentGroupDTO> convertToDto(Iterable<AgentGroup> entities) {
        List<AgentGroupDTO> dtoList = new ArrayList<>();
        for (AgentGroup entity : entities) {
            dtoList.add(convertToDto(entity));
        }
        return dtoList;
    }

    @Override
    public AgentGroup convertToEntity(AgentGroupDTO dto) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<AgentGroup> convertToEntity(Iterable<AgentGroupDTO> dtos) {
        // TODO Auto-generated method stub
        return null;
    }

}
