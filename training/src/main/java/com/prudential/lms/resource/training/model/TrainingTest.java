package com.prudential.lms.resource.training.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_test")
public class TrainingTest extends AuditableEntityBase implements EntityBase<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8944551290835144081L;
	private static final String SEQ_GEN = "seq_gen_trn_test_uuid";
	private static final String SEQ_STRATEGY = "uuid";

	private String id;
	private Training training;
	private TrainingTestType type;
	private int questionCount;
	private int easyCount;
	private int mediumCount;
	private int hardCount;
	private int minimumPoint;
	private int duration;
	private int maxRemedialCount;
	private Long quizId;

	@PrePersist
	private void prePersist() {
		this.questionCount = this.easyCount + this.mediumCount + this.hardCount;
	}
	
	@PreUpdate
	private void preUpdate() {
		this.questionCount = this.easyCount + this.mediumCount + this.hardCount;
	}

	@Id
	@GeneratedValue(generator = SEQ_GEN)
	@GenericGenerator(name = SEQ_GEN, strategy = SEQ_STRATEGY)
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	public TrainingTestType getType() {
		return type;
	}

	public void setType(TrainingTestType type) {
		this.type = type;
	}

	public int getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(int questionCount) {
		this.questionCount = questionCount;
	}

	public int getEasyCount() {
		return easyCount;
	}

	public void setEasyCount(int easyCount) {
		this.easyCount = easyCount;
	}

	public int getMediumCount() {
		return mediumCount;
	}

	public void setMediumCount(int mediumCount) {
		this.mediumCount = mediumCount;
	}

	public int getHardCount() {
		return hardCount;
	}

	public void setHardCount(int hardCount) {
		this.hardCount = hardCount;
	}

	public int getMinimumPoint() {
		return minimumPoint;
	}

	public void setMinimumPoint(int minimumPoint) {
		this.minimumPoint = minimumPoint;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getMaxRemedialCount() {
		return maxRemedialCount;
	}

	public void setMaxRemedialCount(int maxRemedialCount) {
		this.maxRemedialCount = maxRemedialCount;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "training_id")
	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

}
