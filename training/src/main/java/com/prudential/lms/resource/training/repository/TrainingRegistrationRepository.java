package com.prudential.lms.resource.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingRegistration;

public interface TrainingRegistrationRepository
		extends JpaRepository<TrainingRegistration, Long>, QueryDslPredicateExecutor<TrainingRegistration> {

	TrainingRegistration findByAgentId(Long agentId);

	TrainingRegistration findByAgentCode(String agentCode);

	// TrainingRegistration findByRegistrationNo(String registrationNo);
	// List<TrainingRegistration> findByStartDate(Date startDate);
	// List<TrainingRegistration> findByAgent(Agent agent);

}
