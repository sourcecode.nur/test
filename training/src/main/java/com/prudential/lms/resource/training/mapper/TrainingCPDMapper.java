package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.QTrainingCPDMutation;
import com.prudential.lms.resource.training.model.TrainingCPD;
import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDMutationRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.shared.dto.training.TrainingCPDDTO;
import com.prudential.lms.shared.dto.training.TrainingCPDMutationDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class TrainingCPDMapper implements EntityToDTOMapper<TrainingCPD, TrainingCPDDTO> {

	@Autowired
	private TrainingCPDRepository repo;

	@Autowired
	private TrainingCPDMutationRepository trnCpdMtnRepo;

	@Autowired
	private TrainingCPDMutationMapper trnCpdMtnMapper;

	@Autowired
	private AgentRepository agentRepo;

	@Autowired
	private ModelMapper mapper;

	public TrainingCPD createEntity(TrainingCPDDTO dto) {
		TrainingCPD entity = new TrainingCPD();
		entity.setPoint(dto.getPoint());
		entity.setExpiredDate(dto.getExpiredDate());
		entity.setType(dto.getType());

		entity.setTrainingId(dto.getTrainingId());
		entity.setTrainingCode(dto.getTrainingCode());
		entity.setTrainingName(dto.getTrainingName());
		entity.setTrainingDate(dto.getTrainingDate());

		Long agentId = dto.getAgentId();
		if (null != agentId) {
			Agent agent = agentRepo.findOne(agentId);
			entity.setAgent(agent);
		}

		String agentCode = dto.getAgentCode();
		if (null != agentCode) {
			Agent agent = agentRepo.findByCode(agentCode);
			entity.setAgent(agent);
		}

		return entity;
	}

	public TrainingCPD updateEntity(TrainingCPDDTO dto) {
		TrainingCPD entity = repo.findOne(dto.getId());
		entity.setPoint(dto.getPoint());
		entity.setExpiredDate(dto.getExpiredDate());
		entity.setType(dto.getType());

		entity.setTrainingId(dto.getTrainingId());
		entity.setTrainingCode(dto.getTrainingCode());
		entity.setTrainingName(dto.getTrainingName());
		entity.setTrainingDate(dto.getTrainingDate());

		Long agentId = dto.getAgentId();
		if (null != agentId) {
			Agent agent = agentRepo.findOne(agentId);
			entity.setAgent(agent);
		}

		String agentCode = dto.getAgentCode();
		if (null != agentCode) {
			Agent agent = agentRepo.findByCode(agentCode);
			entity.setAgent(agent);
		}

		return entity;
	}

	@Override
	public TrainingCPDDTO convertToDto(TrainingCPD entity) {
		TrainingCPDDTO dto = mapper.map(entity, TrainingCPDDTO.class);
		
		BooleanExpression predicate = QTrainingCPDMutation.trainingCPDMutation.cpd.trainingId.eq(dto.getTrainingId());
		Iterable<TrainingCPDMutation> cpdAAJIList = trnCpdMtnRepo.findAll(predicate);
		List<TrainingCPDMutationDTO> cpdAAJIDtoList = trnCpdMtnMapper.convertToDto(cpdAAJIList);
		dto.setCpdAAJI(cpdAAJIDtoList);
		
		return dto;
	}

	@Override
	public List<TrainingCPDDTO> convertToDto(Iterable<TrainingCPD> entities) {
		List<TrainingCPDDTO> dtoList = new ArrayList<>();
		for (TrainingCPD entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingCPD convertToEntity(TrainingCPDDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingCPD> convertToEntity(Iterable<TrainingCPDDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
