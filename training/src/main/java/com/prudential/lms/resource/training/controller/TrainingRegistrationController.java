package com.prudential.lms.resource.training.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.prudential.lms.resource.training.mapper.TrainingRegistrationMapper;
import com.prudential.lms.resource.training.mapper.TrainingRegistrationScheduleMapper;
import com.prudential.lms.resource.training.mapper.TrainingTestResultMapper;
import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.QTrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.QTrainingTestResult;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.model.TrainingTestQuestion;
import com.prudential.lms.resource.training.model.TrainingTestResult;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingTestResultRepository;
import com.prudential.lms.resource.training.service.TrainingRegistrationService;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.dto.training.TrainingRegistrationDTO;
import com.prudential.lms.shared.dto.training.TrainingRegistrationScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingTestQuestionDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;
import java.util.ArrayList;
import org.modelmapper.ModelMapper;

@RestController
@RequestMapping("/training-registration")
public class TrainingRegistrationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingRegistrationController.class);

    @Autowired
    private TrainingRegistrationRepository repo;

    @Autowired
    private AgentRepository agentRepo;

    @Autowired
    private TrainingScheduleRepository trnSchRepo;

    @Autowired
    private TrainingRegistrationScheduleRepository trnRegSchRepo;

    @Autowired
    private TrainingRegistrationMapper mapper;

    @Autowired
    private TrainingRegistrationScheduleMapper trnRegSchMapper;

    @Autowired
    private TrainingRegistrationService trainingRegistrationService;
    
    @Autowired
    private TrainingTestResultRepository trnTestResRepo;
       
    @Autowired
    private ModelMapper modelMapper;

    // List Paging export to excel all
    @RequestMapping(value = "/list/excel/all", method = RequestMethod.POST)
    public Object listPagingExporttoExcelAll(@RequestBody(required = false) TrainingRegistrationScheduleDTO dto) {

        Iterable<TrainingRegistrationSchedule> trnRegSchPage = null;

        HttpHeaders headers = new HttpHeaders();
        ByteArrayResource resource = null;
        File excelFile = null;

        try {

            BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.id.gt(-1);

            if (null != dto) {

                TrainingScheduleDTO schedule = dto.getSchedule();
                if (null != schedule) {
                    String batchCode = schedule.getBatchCode();
                    if (StringUtils.isNotEmpty(batchCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.batchCode
                                        .likeIgnoreCase("%" + batchCode + "%"));
                    }

                    String trainingName = schedule.getTrainingName();
                    if (StringUtils.isNotEmpty(trainingName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.training.name
                                        .likeIgnoreCase("%" + trainingName + "%"));
                    }

                    Date startDate = schedule.getStartDate();
                    if (null != startDate) {
                        Date startDateTo = new Date(startDate.getTime() + 86399000l);
                        if (null != startDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.startDate
                                            .between(startDate, startDateTo));
                        }
                    }

                    Date endDate = schedule.getEndDate();
                    if (null != endDate) {
                        Date endDateTo = new Date(endDate.getTime() + 86399000l);
                        if (null != endDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.endDate
                                            .between(endDate, endDateTo));
                        }
                    }

                }

                TrainingRegistrationDTO registration = dto.getRegistration();
                if (null != registration) {
                    String agentName = registration.getAgentName();
                    if (StringUtils.isNotEmpty(agentName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.name
                                        .likeIgnoreCase("%" + agentName + "%"));
                    }

                    String agentCode = registration.getAgentCode();
                    if (StringUtils.isNotEmpty(agentCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
                                        .likeIgnoreCase("%" + agentCode + "%"));
                    }

                }

                TrainingProgressStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate
                            .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.status.eq(status));
                }

            }

            trnRegSchPage = trnRegSchRepo.findAll(predicate,
                    new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));

            excelFile = trainingRegistrationService.exportTrainingParticipant(Lists.newArrayList(trnRegSchPage));
            Path path = Paths.get(excelFile.getAbsolutePath());
            resource = new ByteArrayResource(Files.readAllBytes(path));

            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            headers.add("Content-Disposition", "attachment; filename=" + excelFile.getName());
            headers.add("Access-Control-Expose-Headers", "Content-Disposition");

        } catch (Exception e) {
            LOGGER.error("#####ERROR ExportToExcel TrainingRegistrationSchedule", e);
            return Response.internalServerError(e.getMessage());
        }

        return ResponseEntity.ok().headers(headers).contentLength(excelFile.length())
                .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
    }

    // List Paging
    @RequestMapping(value = "/list/all", method = RequestMethod.POST)
    public Object listAll(@RequestBody(required = false) TrainingRegistrationScheduleDTO dto) {

        Response<Object> response = null;
        Iterable<TrainingRegistrationSchedule> trnRegSchPage = null;
        // PageRequest pr = new PageRequest(page, size, Direction.ASC,
        // AppConstants.PAGE_SORT_PROPERTIES);

        try {

            BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.id.gt(-1);

            if (null != dto) {

                TrainingScheduleDTO schedule = dto.getSchedule();
                if (null != schedule) {
                    String batchCode = schedule.getBatchCode();
                    if (StringUtils.isNotEmpty(batchCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.batchCode
                                        .likeIgnoreCase("%" + batchCode + "%"));
                    }

                    String trainingName = schedule.getTrainingName();
                    if (StringUtils.isNotEmpty(trainingName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.training.name
                                        .likeIgnoreCase("%" + trainingName + "%"));
                    }

                    Date startDate = schedule.getStartDate();
                    if (null != startDate) {
                        Date startDateTo = new Date(startDate.getTime() + 86399000l);
                        if (null != startDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.startDate
                                            .between(startDate, startDateTo));
                        }
                    }

                    Date endDate = schedule.getEndDate();
                    if (null != endDate) {
                        Date endDateTo = new Date(endDate.getTime() + 86399000l);
                        if (null != endDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.endDate
                                            .between(endDate, endDateTo));
                        }
                    }

                }

                TrainingRegistrationDTO registration = dto.getRegistration();
                if (null != registration) {
                    String agentName = registration.getAgentName();
                    if (StringUtils.isNotEmpty(agentName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.name
                                        .likeIgnoreCase("%" + agentName + "%"));
                    }

                    String agentCode = registration.getAgentCode();
                    if (StringUtils.isNotEmpty(agentCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
                                        .likeIgnoreCase("%" + agentCode + "%"));
                    }

                }

                TrainingProgressStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate
                            .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.status.eq(status));
                }

            }

            trnRegSchPage = trnRegSchRepo.findAll(predicate,
                    new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));

        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingRegistrationSchedule", e);
            return Response.internalServerError(e.getMessage());
        }

        response = Iterables.size(trnRegSchPage) > 0 ? Response.found(trnRegSchMapper.convertToDto(trnRegSchPage))
                : Response.notFound();

        return response;
    }

    // TraningSchedule Registration by AgentCode
    @RequestMapping(value = "/register/check/{agentCode}/{scheduleId}", method = RequestMethod.POST)
    public Object registerCheckByAgentCode(@PathVariable("agentCode") String agentCode,
            @PathVariable("scheduleId") Long scheduleId) {

        TrainingRegistrationScheduleDTO result = null;

        BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.id
                .eq(scheduleId);

        predicate = predicate.and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
                .equalsIgnoreCase(agentCode));

        Iterable<TrainingRegistrationSchedule> trnRegSch = trnRegSchRepo.findAll(predicate,
                new Sort(Direction.ASC, "id"));
        if (null == trnRegSch) {
            return Response.notFound("Agent not yet registered for TrainingSchedule: " + scheduleId);
        }

        List<TrainingRegistrationSchedule> trnregSchList = Lists.newArrayList(trnRegSch);
        if (trnregSchList.size() > 0) {
            result = trnRegSchMapper.convertToDto(trnregSchList.get(0));
        } else {
            return Response.notFound("Agent not yet registered for TrainingSchedule: " + scheduleId);
        }

        return Response.found(result);

    }

    // TraningSchedule Registration by AgentId
    // @RequestMapping(value = "/register/by-agent-id/{agentId}/{scheduleId}",
    // method = RequestMethod.POST)
    // public Object registerWithAgentId(@PathVariable("agentId") Long agentId,
    // @PathVariable("scheduleId") Long scheduleId) {
    //
    // Agent agent = agentRepo.findOne(agentId);
    // return registerTrainingSchedule(agent, scheduleId);
    //
    // }
    // TraningSchedule Registration by AgentCode
    @RequestMapping(value = "/register/by-agent-code/{agentCode}/{scheduleId}", method = RequestMethod.POST)
    public Object registerWithAgentCode(@PathVariable("agentCode") String agentCode,
            @PathVariable("scheduleId") Long scheduleId) {

        TrainingRegistrationScheduleDTO result = null;

        Agent agent = agentRepo.findByCode(agentCode);
        if (null == agent) {
            return Response.badRequest("Agent not found");
        }

        TrainingSchedule schedule = trnSchRepo.findOne(scheduleId);
        if (null == schedule) {
            return Response.badRequest("Schedule not found");
        }

        try {

            BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.id
                    .eq(scheduleId);
            predicate = predicate.and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
                    .equalsIgnoreCase(agentCode));

            TrainingRegistrationSchedule trnRegSch = trnRegSchRepo.findOne(predicate);
            if (null != trnRegSch) {
                return Response.badRequest("Agent already registered");
            } else {
                trnRegSch = trainingRegistrationService.registerTraining(agent, schedule);
                result = trnRegSchMapper.convertToDto(trnRegSch);
            }

        } catch (LmsException e) {
            LOGGER.error("#####ERROR Registering TrainingSchedule", e);
            return Response.badRequest(e.getMessage());
        }

        return Response.created(result);
    }

    // @Deprecated
    // private Object registerTrainingSchedule(Agent agent, Long scheduleId) {
    //
    // TrainingRegistrationSchedule trnRegSch = null;
    //
    // if (null == agent)
    // return Response.badRequest("Agent not found");
    //
    // TrainingSchedule schedule = trnSchRepo.findOne(scheduleId);
    // if (null == schedule)
    // return Response.badRequest("Schedule not found");
    //
    // try {
    // trnRegSch = trainingRegistrationService.registerTraining(agent,
    // schedule);
    // } catch (LmsException e) {
    // LOGGER.error("#####ERROR Registering TrainingSchedule", e);
    // return Response.badRequest(e.getMessage());
    // }
    //
    // return Response.created(trnRegSchMapper.convertToDto(trnRegSch));
    //
    // }
    // Find List Registered Schedules By AgentCode
    @RequestMapping(value = "/list/by-agent-code/{agentCode}/{page}/{size}", method = RequestMethod.GET)
    public Object findRegisteredSchedules(@PathVariable("agentCode") String agentCode, @PathVariable("page") int page,
            @PathVariable("size") int size) {

        TrainingRegistration trainingRegistration = repo.findByAgentCode(agentCode);
        if (null == trainingRegistration) {
            return Response.notFound("TrainingRegistration with Agent code: " + agentCode + " not found");
        }

        Response<Object> response = null;
        Page<TrainingRegistrationSchedule> trsPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES);

        try {

            BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.id.isNotNull();
            predicate = predicate.and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code.equalsIgnoreCase(agentCode));
            trsPage = trnRegSchRepo.findAll(predicate, pr);

        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingRegistration", e);
            return Response.internalServerError(e.getMessage());
        }

        Page<TrainingRegistrationScheduleDTO> pageResult = new PageImpl<TrainingRegistrationScheduleDTO>(
                trnRegSchMapper.convertToDto(trsPage.getContent()), pr, trsPage.getTotalElements());
        response = trsPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

//		TrainingRegistrationDTO trnRegDto = mapper.convertToDto(trainingRegistration);
        return Response.found(response);
    }

    // Find List Registered Schedules By AgentCode
    @RequestMapping(value = "/list-pruhub/by-agent-code/{agentCode}", method = RequestMethod.GET)
    public Object findRegisteredSchedulesPruhub(@PathVariable("agentCode") String agentCode) {

        TrainingRegistration trainingRegistration = repo.findByAgentCode(agentCode);
        if (null == trainingRegistration) {
            return Response.notFound();
        }

        TrainingRegistrationDTO trnRegDto = mapper.convertToDto(trainingRegistration);

        return Response.found(trnRegDto.getSchedules());
    }

    // Find By Id
    // @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    // public Object findOne(@PathVariable("id") Long id) {
    // TrainingRegistration trainingRegistration = repo.findOne(id);
    // if (null == trainingRegistration)
    // return Response.notFound();
    //
    // return Response.found(mapper.convertToDto(trainingRegistration));
    // }
    // Update
    // @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    // public Object update(@PathVariable("id") Long id, @RequestBody
    // TrainingRegistrationDTO dto) {
    //
    // TrainingRegistration updatedEntity = null;
    // TrainingRegistration trainingRegistration = repo.findOne(id);
    //
    // if (null == trainingRegistration) {
    // return Response.notFound();
    // } else {
    //
    // try {
    // updatedEntity = repo.save(mapper.updateEntity(dto));
    // } catch (Exception e) {
    // LOGGER.error("#####ERROR Updating TrainingRegistration", e);
    // return Response.internalServerError();
    // }
    //
    // }
    //
    // return Response.ok(mapper.convertToDto(updatedEntity));
    // }
    // Find by sample
    // @RequestMapping(value = "/find", method = RequestMethod.GET)
    // public Object findByExample(@RequestBody TrainingRegistrationDTO example)
    // {
    // Response<Object> response = null;
    // List<TrainingRegistration> listTrainingRegistration = null;
    // TrainingRegistration trainingRegistration = null;
    // if (example != null) {
    // if (example.getRegistrationNo() != null) {
    // trainingRegistration =
    // repo.findByRegistrationNo(example.getRegistrationNo());
    // listTrainingRegistration = new ArrayList<>();
    // listTrainingRegistration.add(trainingRegistration);
    // } else if (example.getStartDate() != null) {
    // listTrainingRegistration = repo.findByStartDate(example.getStartDate());
    //
    // } else if (example.getAgentId() != null) {
    // listTrainingRegistration =
    // repo.findByAgent(agentRepo.findOne(example.getAgentId()));
    // }
    // response = listTrainingRegistration.size() > 0
    // ? Response.found(mapper.convertToDto(listTrainingRegistration)) :
    // Response.notFound();
    //
    // return response;
    // } else
    //
    // return Response.notFound();
    // }
    // Create
    // @RequestMapping(value = "", method = RequestMethod.POST)
    // public Object create(@RequestBody TrainingRegistrationDTO dto) {
    // TrainingRegistration saved = null;
    //
    // try {
    // saved = repo.save(mapper.createEntity(dto));
    // } catch (Exception e) {
    // LOGGER.error("#####ERROR Saving TrainingRegistration", e);
    // return Response.internalServerError(e.getMessage());
    // }
    //
    // return Response.created(mapper.convertToDto(saved));
    // }
    // Delete
    // @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    // public Object delete(@PathVariable("id") Long id) {
    // TrainingRegistration trainingRegistration = repo.findOne(id);
    //
    // if (null == trainingRegistration) {
    // return Response.notFound();
    // }
    //
    // try {
    // repo.delete(trainingRegistration);
    // } catch (Exception e) {
    // LOGGER.error("#####ERROR Deleting trainingRegistration", e);
    // return Response.internalServerError();
    // }
    //
    // return Response.ok();
    // }
    // List Paging export to excel
    @RequestMapping(value = "/list/excel/{page}/{size}", method = RequestMethod.POST)
    public Object listPagingExporttoExcel(@PathVariable("page") int page, @PathVariable("size") int size,
            @RequestBody(required = false) TrainingRegistrationScheduleDTO dto) {

        // Response<Object> response = null;
        Page<TrainingRegistrationSchedule> trnRegSchPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.ASC, AppConstants.PAGE_SORT_PROPERTIES);

        HttpHeaders headers = new HttpHeaders();
        ByteArrayResource resource = null;
        File excelFile = null;

        try {

            BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.id.gt(-1);

            if (null != dto) {

                TrainingScheduleDTO schedule = dto.getSchedule();
                if (null != schedule) {
                    String batchCode = schedule.getBatchCode();
                    if (StringUtils.isNotEmpty(batchCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.batchCode
                                        .likeIgnoreCase("%" + batchCode + "%"));
                    }

                    String trainingName = schedule.getTrainingName();
                    if (StringUtils.isNotEmpty(trainingName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.training.name
                                        .likeIgnoreCase("%" + trainingName + "%"));
                    }

                    Date startDate = schedule.getStartDate();
                    if (null != startDate) {
                        Date startDateTo = new Date(startDate.getTime() + 86399000l);
                        if (null != startDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.startDate
                                            .between(startDate, startDateTo));
                        }
                    }

                    Date endDate = schedule.getEndDate();
                    if (null != endDate) {
                        Date endDateTo = new Date(endDate.getTime() + 86399000l);
                        if (null != endDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.endDate
                                            .between(endDate, endDateTo));
                        }
                    }

                }

                TrainingRegistrationDTO registration = dto.getRegistration();
                if (null != registration) {
                    String agentName = registration.getAgentName();
                    if (StringUtils.isNotEmpty(agentName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.name
                                        .likeIgnoreCase("%" + agentName + "%"));
                    }

                    String agentCode = registration.getAgentCode();
                    if (StringUtils.isNotEmpty(agentCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
                                        .likeIgnoreCase("%" + agentCode + "%"));
                    }

                }

                TrainingProgressStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate
                            .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.status.eq(status));
                }

            }

            trnRegSchPage = trnRegSchRepo.findAll(predicate, pr);

            excelFile = trainingRegistrationService.exportTrainingParticipant(trnRegSchPage.getContent());
            Path path = Paths.get(excelFile.getAbsolutePath());
            resource = new ByteArrayResource(Files.readAllBytes(path));

            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            headers.add("Content-Disposition", "attachment; filename=" + excelFile.getName());
            headers.add("Access-Control-Expose-Headers", "Content-Disposition");

        } catch (Exception e) {
            LOGGER.error("#####ERROR ExportToExcel TrainingRegistrationSchedule", e);
            return Response.internalServerError(e.getMessage());
        }

        // Page<TrainingRegistrationScheduleDTO> pageResult = new
        // PageImpl<TrainingRegistrationScheduleDTO>(
        // trnRegSchMapper.convertToDto(trnRegSchPage.getContent()), pr,
        // trnRegSchPage.getTotalElements());
        // response = trnRegSchPage.getTotalElements() > 0 ?
        // Response.found(pageResult) : Response.notFound();
        return ResponseEntity.ok().headers(headers).contentLength(excelFile.length())
                .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
    }

    // List Paging
    @RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
    public Object listPaging(@PathVariable("page") int page, @PathVariable("size") int size,
            @RequestBody(required = false) TrainingRegistrationScheduleDTO dto) {

        Response<Object> response = null;
        Page<TrainingRegistrationSchedule> trnRegSchPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.ASC, AppConstants.PAGE_SORT_PROPERTIES);

        try {

            BooleanExpression predicate = QTrainingRegistrationSchedule.trainingRegistrationSchedule.id.gt(-1);

            if (null != dto) {

                TrainingScheduleDTO schedule = dto.getSchedule();
                if (null != schedule) {
                    String batchCode = schedule.getBatchCode();
                    if (StringUtils.isNotEmpty(batchCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.batchCode
                                        .likeIgnoreCase("%" + batchCode + "%"));
                    }

                    String trainingName = schedule.getTrainingName();
                    if (StringUtils.isNotEmpty(trainingName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.training.name
                                        .likeIgnoreCase("%" + trainingName + "%"));
                    }

                    Date startDate = schedule.getStartDate();
                    if (null != startDate) {
                        Date startDateTo = new Date(startDate.getTime() + 86399000l);
                        if (null != startDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.startDate
                                            .between(startDate, startDateTo));
                        }
                    }

                    Date endDate = schedule.getEndDate();
                    if (null != endDate) {
                        Date endDateTo = new Date(endDate.getTime() + 86399000l);
                        if (null != endDateTo) {
                            predicate = predicate
                                    .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.endDate
                                            .between(endDate, endDateTo));
                        }
                    }

                }

                TrainingRegistrationDTO registration = dto.getRegistration();
                if (null != registration) {
                    String agentName = registration.getAgentName();
                    if (StringUtils.isNotEmpty(agentName)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.name
                                        .likeIgnoreCase("%" + agentName + "%"));
                    }

                    String agentCode = registration.getAgentCode();
                    if (StringUtils.isNotEmpty(agentCode)) {
                        predicate = predicate
                                .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.registration.agent.code
                                        .likeIgnoreCase("%" + agentCode + "%"));
                    }

                }

                TrainingProgressStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate
                            .and(QTrainingRegistrationSchedule.trainingRegistrationSchedule.status.eq(status));
                }

            }

            trnRegSchPage = trnRegSchRepo.findAll(predicate, pr);

        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingRegistrationSchedule", e);
            return Response.internalServerError(e.getMessage());
        }

        Page<TrainingRegistrationScheduleDTO> pageResult = new PageImpl<TrainingRegistrationScheduleDTO>(
                trnRegSchMapper.convertToDto(trnRegSchPage.getContent()), pr, trnRegSchPage.getTotalElements());
        response = trnRegSchPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

        return response;
    }

    
    @RequestMapping(value="/registration-schedule/by-registration-no/{registrationNo}", method = RequestMethod.GET)
    public Object findRegistrationScheduleByRegNo(@PathVariable("registrationNo") String registrationNo){
        TrainingRegistrationSchedule trnRegSch = null;
        try {
            trnRegSch = trnRegSchRepo.findByRegistrationNo(registrationNo);
            if(trnRegSch == null){
                return Response.notFound("Registration Schedule not found!");
            }
            
        } catch (Exception e) { 
            LOGGER.error("#####ERROR findRegistrationScheduleByRegNo {}", e);
            return Response.internalServerError(e.getMessage());
        }
        return Response.found(trnRegSchMapper.convertToDto(trnRegSch));
    }
    
    @RequestMapping(value="/training-test-question/by-registration-no/{registrationNo}", method = RequestMethod.GET)
    public Object getListTrainingTestQuestionByRegNo(@PathVariable("registrationNo") String registrationNo){
        TrainingRegistrationSchedule trnRegSch = null;
        List<TrainingTestQuestionDTO> result = new ArrayList<>();
        try {
            trnRegSch = trnRegSchRepo.findByRegistrationNo(registrationNo);
            if(trnRegSch == null){
                return Response.notFound("Registration Schedule not found!");
            }
            
            BooleanExpression predicate = QTrainingTestResult.trainingTestResult.trainingRegistrationSchedule.id.eq(trnRegSch.getId());
            Iterable<TrainingTestResult> trnTestRes = trnTestResRepo.findAll(predicate);
            if(trnTestRes == null){
                return Response.notFound("Training test result not found!");
            }
            
            TrainingTestResult testResult = trnTestRes.iterator().next(); 
            List<TrainingTestQuestion> questions = testResult.getQuestions();
            TrainingTestQuestionDTO ttqDto;
            for(TrainingTestQuestion qst : questions){
                ttqDto = new TrainingTestQuestionDTO();
                ttqDto.setQuizId(qst.getQuizId());
                ttqDto.setQuestionId(qst.getQuestionId());
                ttqDto.setWeight(qst.getWeight());
                ttqDto.setDifficulty(qst.getDifficulty());
                ttqDto.setQuestionText(qst.getQuestionText());
                ttqDto.setAnswerA(qst.getAnswerA());
                ttqDto.setAnswerB(qst.getAnswerB());
                ttqDto.setAnswerC(qst.getAnswerC());
                ttqDto.setAnswerD(qst.getAnswerD());
                ttqDto.setCorrectAnswer(qst.getCorrectAnswer());
                ttqDto.setUserAnswer(qst.getUserAnswer());
                ttqDto.setCorrect(qst.isCorrect());
                result.add(ttqDto);
            }

        } catch (Exception e) {
            LOGGER.error("#####ERROR getListTrainingTestQuestionByRegNo {}", e);
            return Response.internalServerError(e.getMessage());
        }       
        return Response.found(result);
    }
}
