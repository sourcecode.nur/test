package com.prudential.lms.resource.training.controller;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Iterables;
import com.prudential.lms.resource.training.mapper.TrainingScheduleMapper;
import com.prudential.lms.resource.training.model.QTrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.QTrainingSchedule;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCategory;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.repository.TrainingCategoryRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.repository.TrainingScheduleRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.ContentType;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-schedule")
public class TrainingScheduleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingScheduleController.class);

	@Autowired
	private TrainingScheduleRepository repo;

	@Autowired
	private TrainingRepository trainingRepo;

	@Autowired
	private TrainingCategoryRepository trnCtgrRepo;

	@Autowired
	private TrainingRegistrationScheduleRepository trnRegSchRepo;

	@Autowired
	private TrainingScheduleMapper mapper;

	// List All
	@RequestMapping(value = "/list/all", method = RequestMethod.POST)
	public Object listPagingAll(@RequestBody(required = false) TrainingScheduleDTO dto) {

		Response<Object> response = null;
		Iterable<TrainingSchedule> trnSchPage = null;

		try {

			BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.gt(-1);
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.deleted.isFalse());

			if (null != dto) {

				Boolean published = dto.getPublished();
				if (null != published) {
					if (published.booleanValue()) {
						predicate = predicate.and(QTrainingSchedule.trainingSchedule.published.isTrue());
					} else {
						predicate = predicate.and(QTrainingSchedule.trainingSchedule.published.isFalse());
					}
				}

				String batchCode = dto.getBatchCode();
				if (StringUtils.isNotEmpty(batchCode))
					predicate = predicate.and(QTrainingSchedule.trainingSchedule.batchCode.eq(dto.getBatchCode()));

				boolean draft = dto.isDraft();
				predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(draft));

			}

			trnSchPage = repo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError();
		}

		response = Iterables.size(trnSchPage) > 0 ? Response.found(mapper.convertToDto(trnSchPage))
				: Response.notFound();

		return response;
	}

	@RequestMapping(value = "/list/by-training-id/{trainingId}", method = RequestMethod.GET)
	public Object listByTrainingId(@PathVariable("trainingId") Long trainingId) {

		Training training = trainingRepo.findOne(trainingId);
		if (null == training)
			return Response.ok("Traning not found");

		BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.gt(-1);

		Response<Object> response = null;
		Iterable<TrainingSchedule> listTrainingSchedule = null;

		try {
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.training.eq(training)); 
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(false));
			listTrainingSchedule = repo.findAll(predicate, new Sort(Direction.DESC, "publishStartDate"));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		response = Iterables.size(listTrainingSchedule) > 0 ? Response.found(mapper.convertToDto(listTrainingSchedule))
				: Response.ok("TrainingSchedule not found");

		return response;
	}
	
	@RequestMapping(value = "/list-draft/by-training-id/{trainingId}", method = RequestMethod.GET)
	public Object listDraftByTrainingId(@PathVariable("trainingId") Long trainingId) {

		Training training = trainingRepo.findOne(trainingId);
		if (null == training)
			return Response.ok("Traning not found");

		BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.gt(-1);

		Response<Object> response = null;
		Iterable<TrainingSchedule> listTrainingSchedule = null;

		try {
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.training.eq(training));
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(true));
			listTrainingSchedule = repo.findAll(predicate, new Sort(Direction.DESC, "publishStartDate"));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		response = Iterables.size(listTrainingSchedule) > 0 ? Response.found(mapper.convertToDto(listTrainingSchedule))
				: Response.ok("TrainingSchedule not found");

		return response;
	}

	@RequestMapping(value = "/list/by-category-id/{categoryId}", method = RequestMethod.GET)
	public Object listByCategoryId(@PathVariable("categoryId") Long categoryId) {

		TrainingCategory category = trnCtgrRepo.findOne(categoryId);
		if (null == category)
			return Response.notFound("TraningCategory not found");

		Set<Training> trainings = category.getTrainings();
		if (trainings.size() <= 0)
			return Response.notFound("Traning not found under current TrainingCategory");

		BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.gt(-1);

		Response<Object> response = null;
		Iterable<TrainingSchedule> listTrainingSchedule = null;

		try {
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.training.in(trainings));
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(false));
			listTrainingSchedule = repo.findAll(predicate, new Sort(Direction.DESC, "publishStartDate"));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		response = null != listTrainingSchedule ? Response.found(mapper.convertToDto(listTrainingSchedule))
				: Response.notFound("TrainingSchedule not found");

		return response;
	}
	
	@RequestMapping(value = "/list-draft/by-category-id/{categoryId}", method = RequestMethod.GET)
	public Object listDraftByCategoryId(@PathVariable("categoryId") Long categoryId) {

		TrainingCategory category = trnCtgrRepo.findOne(categoryId);
		if (null == category)
			return Response.notFound("TraningCategory not found");

		Set<Training> trainings = category.getTrainings();
		if (trainings.size() <= 0)
			return Response.notFound("Traning not found under current TrainingCategory");

		BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.gt(-1);

		Response<Object> response = null;
		Iterable<TrainingSchedule> listTrainingSchedule = null;

		try {
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.training.in(trainings));
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(true));
			listTrainingSchedule = repo.findAll(predicate, new Sort(Direction.DESC, "publishStartDate"));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		response = null != listTrainingSchedule ? Response.found(mapper.convertToDto(listTrainingSchedule))
				: Response.notFound("TrainingSchedule not found");

		return response;
	}

	// List Paging
	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
	public Object listPaging(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) TrainingScheduleDTO dto) {

		Response<Object> response = null;
		Page<TrainingSchedule> trnSchPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES);

		try {

			BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.gt(-1);
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.deleted.isFalse());

			if (null != dto) {

				Boolean published = dto.getPublished();
				if (null != published) {
					if (published.booleanValue()) {
						predicate = predicate.and(QTrainingSchedule.trainingSchedule.published.isTrue());
					} else {
						predicate = predicate.and(QTrainingSchedule.trainingSchedule.published.isFalse());
					}
				}

				String batchCode = dto.getBatchCode();
				if (StringUtils.isNotEmpty(batchCode))
					predicate = predicate.and(QTrainingSchedule.trainingSchedule.batchCode.eq(dto.getBatchCode()));
				
				boolean draft = dto.isDraft();
				predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(draft));

			}

			trnSchPage = repo.findAll(predicate, pr);

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError();
		}

		Page<TrainingScheduleDTO> pageResult = new PageImpl<TrainingScheduleDTO>(
				mapper.convertToDto(trnSchPage.getContent()), pr, trnSchPage.getTotalElements());
		response = trnSchPage.getSize() > 0 ? Response.found(pageResult) : Response.notFound();

		return response;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Object list(@RequestBody(required = false) TrainingScheduleDTO dto) {

		Response<Object> response = null;
		Iterable<TrainingSchedule> listTrainingSchedule = null;

		BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.isNotNull();

		if (null != dto) {

			ContentType trainingContentType = dto.getTrainingContentType();
			if (null != trainingContentType)
				predicate = predicate
						.and(QTrainingSchedule.trainingSchedule.training.trainingContentType.eq(trainingContentType));
			
			boolean draft = dto.isDraft();
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(draft));
		}

		try {
			listTrainingSchedule = repo.findAll(predicate);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		response = Iterables.size(listTrainingSchedule) > 0 ? Response.found(mapper.convertToDto(listTrainingSchedule))
				: Response.notFound();

		return response;
	}

	@RequestMapping(value = "/list-cpd-gt-zero", method = RequestMethod.POST)
	public Object listCPDMoreThanZero(@RequestBody(required = false) TrainingScheduleDTO dto) {

		Response<Object> response = null;
		Iterable<TrainingSchedule> listTrainingSchedule = null;

		BooleanExpression predicate = QTrainingSchedule.trainingSchedule.id.isNotNull();
		predicate = predicate.and(QTrainingSchedule.trainingSchedule.training.cpdPoint.gt(0));

		if (null != dto) {

			ContentType trainingContentType = dto.getTrainingContentType();
			if (null != trainingContentType)
				predicate = predicate
						.and(QTrainingSchedule.trainingSchedule.training.trainingContentType.eq(trainingContentType));
			
			boolean draft = dto.isDraft();
			predicate = predicate.and(QTrainingSchedule.trainingSchedule.draft.eq(draft));
		}

		try {
			listTrainingSchedule = repo.findAll(predicate);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		response = Iterables.size(listTrainingSchedule) > 0 ? Response.found(mapper.convertToDto(listTrainingSchedule))
				: Response.notFound();

		return response;
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody TrainingScheduleDTO dto) {
		TrainingSchedule saved = null;

		Long trainingId = dto.getTrainingId();
		if (null == trainingId)
			return Response.badRequest("trainingId is required");
		if (!dto.isDraft() && (null == dto.getStartDate() || null == dto.getEndDate()) )
			return Response.badRequest("startDate & endDate are required");
		if (!dto.isDraft() && (null == dto.getPublishStartDate() || null == dto.getPublishEndDate()))
			return Response.badRequest("publishStartDate & publishEndDate are required");

		Training training = trainingRepo.findOne(trainingId);
		if (null == training)
			return Response.badRequest("Training with id: " + trainingId + " not found");

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		TrainingSchedule ts = repo.findOne(id);
		if (null == ts)
			return Response.notFound();

		return Response.found(mapper.convertToDto(ts));
	}

	// Find by sample
	// @RequestMapping(value = "/find", method = RequestMethod.GET)
	// public Object findByExample(@RequestBody TrainingScheduleDTO example) {
	// Response<Object> response = null;
	// List<TrainingSchedule> listTrainingSchedule = null;
	// TrainingSchedule trainingSchedule = null;
	// if (example != null) {
	// if (example.getBatchCode() != null) {
	// trainingSchedule = repo.findByBatchCode(example.getBatchCode());
	// listTrainingSchedule = new ArrayList<>();
	// listTrainingSchedule.add(trainingSchedule);
	// } else if (example.getStartDate() != null) {
	// listTrainingSchedule = repo.findByStartDate(example.getStartDate());
	// } else if (example.getEndDate() != null) {
	// listTrainingSchedule = repo.findByEndDate(example.getEndDate());
	//
	// } else {
	// listTrainingSchedule =
	// repo.findByTraining(trainingRepo.findOne(example.getTrainingId()));
	// }
	// response = listTrainingSchedule.size() > 0 ?
	// Response.found(mapper.convertToDto(listTrainingSchedule))
	// : Response.notFound();
	//
	// return response;
	// } else
	//
	// return Response.notFound();
	// }

	// Update
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody TrainingScheduleDTO dto) {

		TrainingSchedule updatedEntity = null;
		TrainingSchedule ts = repo.findOne(id);

		if (null == ts) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating TrainingSchedule", e);
				return Response.internalServerError(e.getMessage());
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		TrainingSchedule ts = repo.findOne(id);

		if (null == ts) {
			return Response.notFound();
		}

		Iterable<TrainingRegistrationSchedule> registrants = trnRegSchRepo
				.findAll(QTrainingRegistrationSchedule.trainingRegistrationSchedule.schedule.id.eq(ts.getId()));

		if (0 < Iterables.size(registrants))
			return Response.badRequest("Cannot delete schedule which have registered attendance(s)");

		try {
			repo.delete(ts);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting TrainingSchedule", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

	// Soft Delete
	@RequestMapping(value = "/softdelete/{del}/{id}", method = RequestMethod.PATCH)
	public Object softDelete(@PathVariable("del") int del, @PathVariable("id") Long id) {

		if (del != 0 || del != 1)
			return Response.badRequest("Only 0 & 1 are allowed for soft delete flag");

		boolean deleted = del == 0 ? true : false;

		TrainingSchedule ts = repo.findOne(id);
		if (null == ts)
			return Response.notFound();

		try {
			processSoftDelete(ts, deleted);
		} catch (Exception e) {
			return Response.internalServerError();
		}

		return Response.ok();
	}

	private void processSoftDelete(TrainingSchedule trainingSchedule, boolean deleted) {
		try {
			trainingSchedule.setDeleted(deleted);
			repo.save(trainingSchedule);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Soft-Deleting TrainingCategory", e);
		}
	}
}
