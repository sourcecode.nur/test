package com.prudential.lms.resource.training.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;

public interface TrainingTestService {

	List<TrainingTestResultDTO> importTrainingResult(InputStream inputStream) throws IOException, LmsException;
	
	List<TrainingTestResultDTO> importParseTrainingResult(InputStream inputStream) throws IOException, LmsException;

}
