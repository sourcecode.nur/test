package com.prudential.lms.resource.training.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.prudential.lms.shared.constant.TrainingCPDType;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_cpd")
public class TrainingCPD extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 536603702743700096L;
	private static final String SEQ_GEN = "seq_gen_trn_cpd";
	private static final String SEQ = "seq_trn_cpd";

	private Long id;
	private int point;
	private Agent agent;
	private Date expiredDate;
	private TrainingCPDType type = TrainingCPDType.LMS;

	// Training Information
	private Long trainingId;
	private String trainingCode;
	private String trainingName;
	private Date trainingDate;
	
	private List<TrainingCPDMutation> mutations = new ArrayList<TrainingCPDMutation>();
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "agent_id")
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public String getTrainingCode() {
		return trainingCode;
	}

	public void setTrainingCode(String trainingCode) {
		this.trainingCode = trainingCode;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public Date getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(Date trainingDate) {
		this.trainingDate = trainingDate;
	}

	@Enumerated(EnumType.STRING)
	public TrainingCPDType getType() {
		return type;
	}

	public void setType(TrainingCPDType type) {
		this.type = type;
	}

	@OneToMany(mappedBy = "cpd")
	public List<TrainingCPDMutation> getMutations() {
		return mutations;
	}
	
	public void setMutations(List<TrainingCPDMutation> mutations) {
		this.mutations = mutations;
	}
	
	public void addMutations(TrainingCPDMutation... mutations){
		for (TrainingCPDMutation trainingCPDMutation : mutations) {
			this.mutations.add(trainingCPDMutation);
		}
	}
	

}
