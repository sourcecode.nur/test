package com.prudential.lms.resource.training.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Iterables;
import com.prudential.lms.resource.training.mapper.TrainingTestMapper;
import com.prudential.lms.resource.training.mapper.TrainingTestResultMapper;
import com.prudential.lms.resource.training.model.QTrainingTest;
import com.prudential.lms.resource.training.model.QTrainingTestResult;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingTest;
import com.prudential.lms.resource.training.model.TrainingTestResult;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingTestRepository;
import com.prudential.lms.resource.training.repository.TrainingTestResultRepository;
import com.prudential.lms.resource.training.service.TrainingTestService;
import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.dto.training.TrainingTestQuestionDTO;
import com.prudential.lms.shared.dto.training.TrainingTestResultDTO;
import com.prudential.lms.shared.exception.LmsException;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-test")
public class TrainingTestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingTestController.class);

    @Autowired
    private TrainingTestRepository repo;

    @Autowired
    private TrainingTestMapper mapper;

    @Autowired
    private TrainingRegistrationScheduleRepository trnRegSchRepo;

    @Autowired
    private TrainingTestResultMapper trnTestResMapper;

    @Autowired
    private TrainingTestResultRepository trnTestResRepo;

    @Autowired
    private TrainingTestService trnTestService;

    @Value("${app.download.training-result-template}")
    private String trainingResultTemplate;

    // Flag Life Asia
    @RequestMapping(value = "/flag-life-asia/{trainingResultId}/{flagTimeStamp}", method = RequestMethod.POST)
    public Object flagLifeAsia(@PathVariable("trainingResultId") String trainingResultId,
            @PathVariable("flagTimeStamp") Long flagTimeStamp) {

        TrainingTestResultDTO result = null;
        TrainingTestResult trnTestRes = trnTestResRepo.findOne(trainingResultId);
        if (null == trnTestRes) {
            return Response.notFound("TrainingTestResult with id: " + trainingResultId + " not found");
        }

        Date submitedtoLASDate = new Date(flagTimeStamp);
        trnTestRes.setSubmitedToLAS(submitedtoLASDate);

        try {
            result = trnTestResMapper.convertToDto(trnTestResRepo.saveAndFlush(trnTestRes));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);
    }

    // List Paging
    @RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
    public Object list(@PathVariable("page") int page, @PathVariable("size") int size,
            @RequestBody(required = false) TrainingTestResultDTO dto) {

        Response<Object> response = null;
        Page<TrainingTestResult> tcPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.DESC, "updatedDate");

        try {

            BooleanExpression predicate = QTrainingTestResult.trainingTestResult.isNotNull();

            tcPage = trnTestResRepo.findAll(predicate, pr);

        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingTestResult", e);
            return Response.internalServerError(e.getMessage());
        }

        Page<TrainingTestResultDTO> pageResult = new PageImpl<TrainingTestResultDTO>(
                trnTestResMapper.convertToDto(tcPage.getContent()), pr, tcPage.getTotalElements());
        response = tcPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

        return response;
    }

    // download training result template
    @RequestMapping(value = "/find-by-quiz/{quizId}", method = RequestMethod.GET)
    public Object findByQuizId(@PathVariable("quizId") Long quizId) {

        Response<Object> result = null;

        BooleanExpression predicate = QTrainingTest.trainingTest.quizId.eq(quizId);
        Iterable<TrainingTest> trnTests = repo.findAll(predicate);

        int size = Iterables.size(trnTests);
        result = 0 < size ? Response.found(mapper.convertToDto(trnTests)) : Response.notFound();

        return result;
    }

    // download training result template
    @RequestMapping(value = "/download/training-result-template", method = RequestMethod.GET)
    public Object downloadTrainingResultTemplate() {

        HttpHeaders headers = new HttpHeaders();
        ByteArrayResource resource = null;
        File excelFile = null;

        try {

            excelFile = new File(trainingResultTemplate);
            Path path = Paths.get(trainingResultTemplate);
            resource = new ByteArrayResource(Files.readAllBytes(path));

            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            headers.add("Content-Disposition", "attachment; filename=" + excelFile.getName());
            headers.add("Access-Control-Expose-Headers", "Content-Disposition");

        } catch (LmsException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return ResponseEntity.ok().headers(headers).contentLength(excelFile.length())
                .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

    }

    // upload update quiz
    @RequestMapping(value = "/upload/training-result", method = RequestMethod.POST)
    public Object parseQuiz(@RequestParam("content") MultipartFile content) {

        List<TrainingTestResultDTO> result = null;

        try {
            result = trnTestService.importTrainingResult(content.getInputStream());
        } catch (LmsException | IOException e) {
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);

    }

    // upload parse training result
    @RequestMapping(value = "/parse", method = RequestMethod.POST)
    public Object parseTrainingResult(@RequestParam("content") MultipartFile content) {

        List<TrainingTestResultDTO> result = null;

        try {
            result = trnTestService.importParseTrainingResult(content.getInputStream());
        } catch (LmsException | IOException e) {
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok(result);

    }

    @RequestMapping(value = "/add-result", method = RequestMethod.POST)
    public Object addResultByRegistrationNo(@RequestBody TrainingTestResultDTO dto) {

        TrainingTestResultDTO result = null;

        String registrationNo = dto.getRegistrationNo();

        TrainingRegistrationSchedule trnRegSch = trnRegSchRepo.findByRegistrationNo(registrationNo);
        if (null == trnRegSch) {
            return Response.notFound("TrainingRegistrationSchedule not found for registrationNo: " + registrationNo);
        }

        dto.setTrainingRegistrationScheduleId(trnRegSch.getId());
        dto.setRegistrationNo(registrationNo);

        List<TrainingTest> trainingTests = trnRegSch.getSchedule().getTraining().getTrainingTests();
        TrainingTest currentTest = getTest(trainingTests, dto.getTestType());
        if (null == currentTest) {
            return Response.badRequest("There was no " + dto.getTestType().toString() + " for registrationNo: " + registrationNo);
        }

        dto.setMinPoint(BigDecimal.valueOf(currentTest.getMinimumPoint()));
        dto.setPass(dto.getPoint().compareTo(dto.getMinPoint()) >= 0);

        Iterable<TrainingTestResult> passes = trnTestResRepo.findAll(QTrainingTestResult.trainingTestResult.pass.isTrue()
                .and(QTrainingTestResult.trainingTestResult.trainingRegistrationSchedule.registrationNo.equalsIgnoreCase(registrationNo))
                .and(QTrainingTestResult.trainingTestResult.testType.eq(TrainingTestType.POST)));
        if (0 < Iterables.size(passes)) {
            return Response.badRequest("Registrant No: " + registrationNo + " already pass the test");
        }

        try {
            TrainingTestResult trnTestRes = trnTestResRepo.save(trnTestResMapper.createEntity(dto));
            result = trnTestResMapper.convertToDto(trnTestRes);
            List<TrainingTestQuestionDTO> questions = result.getQuestions();
            for (TrainingTestQuestionDTO qdto : questions) {
                qdto = null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.created(result);

    }
    
    @RequestMapping(value = "/get-training-test-result/by-register-schedule-id/{id}", method = RequestMethod.GET)
    public Object getTrainingTestResultByRegisterScheduleId(@PathVariable("id") Long id){
        TrainingTestResultDTO result = null;
        try {
            BooleanExpression predicate = QTrainingTestResult.trainingTestResult.trainingRegistrationSchedule.id.eq(id);
            Iterable<TrainingTestResult> trnTestRes = trnTestResRepo.findAll(predicate);
            if(trnTestRes == null){
                return Response.notFound("Data not found!");
            }
            result = trnTestResMapper.convertToDto(trnTestRes.iterator().next());
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return Response.internalServerError(e.getMessage());
        }
        return Response.ok(result);
    }

    private TrainingTest getTest(List<TrainingTest> tests, TrainingTestType type) {

        TrainingTest result = null;
        for (TrainingTest trainingTest : tests) {
            if (trainingTest.getType().equals(type)) {
                result = trainingTest;
                break;
            }
        }

        return result;
    }

}
