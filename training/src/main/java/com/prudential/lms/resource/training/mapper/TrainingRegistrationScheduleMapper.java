package com.prudential.lms.resource.training.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.prudential.lms.resource.training.model.QTrainingTestResult;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.model.TrainingTest;
import com.prudential.lms.resource.training.model.TrainingTestResult;
import com.prudential.lms.resource.training.repository.TrainingTestResultRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.dto.training.TrainingRegistrationDTO;
import com.prudential.lms.shared.dto.training.TrainingRegistrationScheduleDTO;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class TrainingRegistrationScheduleMapper
		implements EntityToDTOMapper<TrainingRegistrationSchedule, TrainingRegistrationScheduleDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private TrainingRegistrationMapper trnRegMapper;

	@Autowired
	private TrainingScheduleMapper trnSchMapper;
	
	@Autowired
	private TrainingTestResultRepository trnTestResRepo;
	
	@Autowired
	private TrainingTestMapper trnTestMapper;

	@Autowired
	private TrainingContentMapper trnCtnMapper;

	@Override
	public TrainingRegistrationSchedule createEntity(TrainingRegistrationScheduleDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrainingRegistrationSchedule updateEntity(TrainingRegistrationScheduleDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrainingRegistrationScheduleDTO convertToDto(TrainingRegistrationSchedule entity) {

		TrainingRegistrationScheduleDTO dto = new TrainingRegistrationScheduleDTO();
		dto.setId(entity.getId());
		dto.setRegistrationNo(entity.getRegistrationNo());
		dto.setStartDate(entity.getStartDate());
		dto.setStatus(entity.getStatus());
		
		BooleanExpression predicate = QTrainingTestResult.trainingTestResult.trainingRegistrationSchedule.id.eq(entity.getId());
		predicate = predicate.and(QTrainingTestResult.trainingTestResult.testType.eq(TrainingTestType.POST));
		
		Iterable<TrainingTestResult> testResult = trnTestResRepo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));
		if(0 < Iterables.size(testResult)){
			TrainingTestResult trnTestRes = testResult.iterator().next();
			if(null != trnTestRes){
				BigDecimal point = trnTestRes.getPoint();
				if(null != point) dto.setPoint(point.intValue());
				
				dto.setSubmitedToLAS(trnTestRes.getSubmitedToLAS());
				dto.setPass(trnTestRes.isPass());
			}
		}

		TrainingSchedule schedule = entity.getSchedule();
		if (null != schedule){
			TrainingScheduleDTO trnSchDto = new TrainingScheduleDTO();
			trnSchDto.setId(schedule.getId());
			trnSchDto.setBatchCode(schedule.getBatchCode());
			trnSchDto.setPublishStartDate(schedule.getPublishStartDate());
			trnSchDto.setPublishEndDate(schedule.getPublishEndDate());
			trnSchDto.setStartDate(schedule.getStartDate());
			trnSchDto.setEndDate(schedule.getEndDate());
			trnSchDto.setQuota(schedule.getQuota());
			trnSchDto.setRegistered(schedule.getRegistered());
			
			Training training = schedule.getTraining();
			if(null != training){
				trnSchDto.setTrainingId(training.getId());
				trnSchDto.setTrainingName(training.getName());
				trnSchDto.setCpdPoint(training.getCpdPoint());
				trnSchDto.setTrainingCategoryName(training.getCategory().getName());
				trnSchDto.setTrainingTests(trnTestMapper.convertToDto(training.getTrainingTests()));
				trnSchDto.setContents(trnCtnMapper.convertToDto(training.getContents()));
			}
			
			dto.setSchedule(trnSchDto);
		}

		TrainingRegistration registration = entity.getRegistration();
		if (null != registration){
			TrainingRegistrationDTO trnRegDto = new TrainingRegistrationDTO();
			trnRegDto.setId(registration.getId());
			trnRegDto.setAgentId(registration.getAgent().getId());
			trnRegDto.setAgentCode(registration.getAgent().getCode());
			trnRegDto.setAgentName(registration.getAgent().getName());
			dto.setRegistration(trnRegDto);
		}
			
		// Auditable
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setUpdatedBy(entity.getUpdatedBy());
		dto.setUpdatedDate(entity.getUpdatedDate());
		dto.setVersion(entity.getVersion());

		return dto;
	}

	@Override
	public List<TrainingRegistrationScheduleDTO> convertToDto(Iterable<TrainingRegistrationSchedule> entities) {
		List<TrainingRegistrationScheduleDTO> dtoList = new ArrayList<>();
		for (TrainingRegistrationSchedule entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingRegistrationSchedule convertToEntity(TrainingRegistrationScheduleDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingRegistrationSchedule> convertToEntity(Iterable<TrainingRegistrationScheduleDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
