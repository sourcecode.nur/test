package com.prudential.lms.resource.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingTestResult;

public interface TrainingTestResultRepository
		extends JpaRepository<TrainingTestResult, String>, QueryDslPredicateExecutor<TrainingTestResult> {

}
