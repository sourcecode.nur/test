package com.prudential.lms.resource.training.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.TrainingCPDMutationMapper;
import com.prudential.lms.resource.training.model.QTrainingCPDMutation;
import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.TrainingCPDMutationRepository;
import com.prudential.lms.shared.dto.training.TrainingCPDMutationDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-cpd-mutation")
public class TrainingCPDMutationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCPDMutationController.class);

	@Autowired
	private TrainingCPDMutationRepository repo;

	@Autowired
	private TrainingCPDMutationMapper mapper;

	// List Paging
	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
	public Object list(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) TrainingCPDMutationDTO dto) {

		Response<Object> response = null;
		Page<TrainingCPDMutation> trainingCPDMutationPage = null;

		try {
			PageRequest pr = new PageRequest(page, size, Direction.ASC, "sortOrder");

			BooleanExpression prevPoint = QTrainingCPDMutation.trainingCPDMutation.prevPoint.eq(dto.getPrevPoint());
			BooleanExpression currPoint = prevPoint.and(QTrainingCPDMutation.trainingCPDMutation.currentPoint.eq(dto.getCurrentPoint()));
			BooleanExpression deltaPoint = currPoint.and(QTrainingCPDMutation.trainingCPDMutation.deltaPoint.eq(dto.getDeltaPoint()));
//			BooleanExpression trainingCPDId = deltaPoint.and(QTrainingCPDMutation.trainingCPDMutation.trainingCPDId.eq(dto.getTrainingCPDId()));
//			BooleanExpression mutationOperation = trainingCPDId.and(QTrainingCPDMutation.trainingCPDMutation.mutationOperation.eq(dto.getMutationOperation()));

			trainingCPDMutationPage = repo.findAll(deltaPoint, pr);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingCPD", e);
			return Response.internalServerError();
		}

		response = trainingCPDMutationPage.getSize() > 0 ? Response.found(mapper.convertToDto(trainingCPDMutationPage.getContent()))
				: Response.notFound();

		return response;
	}

	// Create
	@RequestMapping(value = { "/", "" }, method = RequestMethod.POST)
	public Object create(@RequestBody TrainingCPDMutationDTO dto) {
		TrainingCPDMutation saved = null;

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving TrainingCPD", e);
			return Response.internalServerError();
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		TrainingCPDMutation trainingCPD = repo.findOne(id);
		if (null == trainingCPD)
			return Response.notFound();

		return Response.found(mapper.convertToDto(trainingCPD));
	}

	// Update
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody TrainingCPDMutationDTO dto) {

		TrainingCPDMutation updatedEntity = null;
		TrainingCPDMutation trainingCPDMutation = repo.findOne(id);

		if (null == trainingCPDMutation) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating TrainingCPD", e);
				return Response.internalServerError();
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Find by sample
//	@RequestMapping(value = "/find", method = RequestMethod.GET)
//	public Object findByExample(@RequestBody TrainingCPDMutationDTO example) {
//		Response<Object> response = null;
//		List<TrainingCPDMutation> listTrainingCPDMutation = null;
//		TrainingCPDMutation cpdMutation = null;
//		if (example != null) {
//			if (example.getPrevPoint() > 0) {
//				listTrainingCPDMutation = repo.findByPrevPoint(example.getPrevPoint());
//			}else if(example.getCurrentPoint() > 0){
//				listTrainingCPDMutation = repo.findByCurrentPoint(example.getCurrentPoint());
//			}else if(example.getDeltaPoint() > 0){
//				listTrainingCPDMutation = repo.findByDeltaPoint(example.getDeltaPoint());
//			}else if(example.getCurrentPoint() > 0){
//				listTrainingCPDMutation = repo.findByCurrentPoint(example.getCurrentPoint());
//			}else if(example.getTrainingCPDId()!= null){
//				cpdMutation = repo.findByTrainingCPDId(example.getTrainingCPDId());
//				listTrainingCPDMutation = new ArrayList<>();
//				listTrainingCPDMutation.add(cpdMutation);
//			}else if(example.getMutationOperation() != null){
//				listTrainingCPDMutation = repo.findByMutationOperation(example.getMutationOperation());
//			}
//			response = listTrainingCPDMutation.size() > 0 ? Response.found(mapper.convertToDto(listTrainingCPDMutation))
//					: Response.notFound();
//
//			return response;
//		} else
//
//			return Response.notFound();
//	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		TrainingCPDMutation trainingCPDMutation = repo.findOne(id);

		if (null == trainingCPDMutation) {
			return Response.notFound();
		}

		try {
			repo.delete(trainingCPDMutation);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting TrainingCPD", e);
			return Response.internalServerError();
		}

		return Response.ok();
	}
}
