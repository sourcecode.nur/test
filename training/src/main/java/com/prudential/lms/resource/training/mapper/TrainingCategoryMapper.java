package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.QTrainingCategory;
import com.prudential.lms.resource.training.model.TrainingCategory;
import com.prudential.lms.resource.training.repository.TrainingCategoryRepository;
import com.prudential.lms.shared.constant.TrainingCategoryStatus;
import com.prudential.lms.shared.dto.training.TrainingCategoryDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingCategoryMapper implements EntityToDTOMapper<TrainingCategory, TrainingCategoryDTO> {

	@Autowired
	private TrainingCategoryRepository repo;
	
	@Autowired
	private ModelMapper mapper;
	
	@Override
	public TrainingCategory createEntity(TrainingCategoryDTO dto){
		TrainingCategory entity = new TrainingCategory();
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setStatus(dto.getStatus());
		entity.setFileName(dto.getFileName());
		entity.setThumbnail(dto.getThumbnail());
		entity.setDraft(dto.isDraft());
		
		Long parentId = dto.getParentId();
		if(null != parentId){
			TrainingCategory parent = repo.findOne(parentId);
			if(null != parent){
				entity.setParent(parent);
			}
		}
		
		return entity;
	}
	
	@Override
	public TrainingCategory updateEntity(TrainingCategoryDTO dto){
		TrainingCategory entity = repo.findOne(dto.getId());
		
		//entity.setCode(dto.getCode());
		
		String name = dto.getName();
		if(StringUtils.isNotEmpty(name))
			entity.setName(name);
		
		String description = dto.getDescription();
		if(null != description)
			entity.setDescription(description);
		
		TrainingCategoryStatus status = dto.getStatus();
		if(null != status)
			entity.setStatus(status);
		
		
		String thumbnail = dto.getThumbnail();
		if(StringUtils.isNotEmpty(thumbnail)) {
			entity.setThumbnail(thumbnail);
			entity.setFileName(dto.getFileName());
		}
			
		
		if(dto.isDraft()) {
			entity.setThumbnail(thumbnail); 
			entity.setFileName(dto.getFileName());
		}
		
		Long parentId = dto.getParentId();
		if(null != parentId){
			TrainingCategory parent = repo.findOne(parentId);
			if(null != parent){
				entity.setParent(parent);
			}
		}
		
		boolean draft = dto.isDraft(); 
		entity.setDraft(draft);
		
		return entity;
	}

	@Override
	public TrainingCategoryDTO convertToDto(TrainingCategory entity) {
		
		TrainingCategoryDTO dto = new TrainingCategoryDTO();
		dto.setId(entity.getId());
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setStatus(entity.getStatus());
		dto.setFileName(entity.getFileName());
		dto.setThumbnail(entity.getThumbnail());
		dto.setDeleted(entity.isDeleted());
		dto.setTrainingCount(entity.getTrainingCount());
		dto.setActive(entity.getTrainings().size() > 0);
		dto.setDraft(entity.isDraft());
		
		TrainingCategory parent = entity.getParent();
		if(null != parent){
			dto.setParentId(parent.getId());
			dto.setParentCode(parent.getCode());
			dto.setParentName(parent.getName());
			dto.setParentDescription(parent.getDescription());
		}
		
//		Set<TrainingCategory> children = entity.getChildren();
//		for (TrainingCategory child : children) {
//			TrainingCategoryDTO childDTO = new TrainingCategoryDTO();
//			childDTO.setId(child.getId());
//			childDTO.setCode(child.getCode());
//			childDTO.setName(child.getName());
//			childDTO.setDescription(child.getDescription());
//			dto.getChildren().add(childDTO);
//		}
		
		addChildren(dto, entity.getChildren());
		
		// Auditable
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setUpdatedBy(entity.getUpdatedBy());
		dto.setUpdatedDate(entity.getUpdatedDate());
		dto.setVersion(entity.getVersion());
		
		return dto;
	}
	
	private void addChildren(TrainingCategoryDTO dto, Set<TrainingCategory> children){
		for (TrainingCategory child : children) {
			TrainingCategoryDTO childDTO = new TrainingCategoryDTO();
			childDTO.setId(child.getId());
			childDTO.setCode(child.getCode());
			childDTO.setName(child.getName());
			childDTO.setDescription(child.getDescription());
			childDTO.setTrainingCount(child.getTrainingCount());
			childDTO.setDeleted(child.isDeleted());
			childDTO.setFileName(child.getFileName());
			childDTO.setThumbnail(child.getThumbnail());
			childDTO.setDraft(child.isDraft());
			
			// Auditable
			childDTO.setCreatedBy(child.getCreatedBy());
			childDTO.setCreatedDate(child.getCreatedDate());
			childDTO.setUpdatedBy(child.getUpdatedBy());
			childDTO.setUpdatedDate(child.getUpdatedDate());
			childDTO.setVersion(child.getVersion());
			
			if(child.hasChildren()){
				addChildren(childDTO, child.getChildren());
			}
			dto.getChildren().add(childDTO);
		}
	}

	@Override
	public List<TrainingCategoryDTO> convertToDto(Iterable<TrainingCategory> entities) {
		List<TrainingCategoryDTO> dtoList = new ArrayList<>();
		for (TrainingCategory entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingCategory convertToEntity(TrainingCategoryDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingCategory> convertToEntity(Iterable<TrainingCategoryDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
