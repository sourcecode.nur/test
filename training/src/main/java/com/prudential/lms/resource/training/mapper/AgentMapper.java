package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.TrainingCPD;
import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.AgentGroupRepository;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.shared.dto.training.AgentDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class AgentMapper implements EntityToDTOMapper<Agent, AgentDTO> {

	@Autowired
	private AgentRepository repo;

	@Autowired
	private AgentGroupRepository agentGroupRepo;

	@Autowired
	private TrainingCPDRepository cpdRepo;

	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private TrainingCPDMapper trainingCPDMapper;

	@Autowired
	private TrainingCategoryMapper trainingCategoryMapper;

	@Autowired
	private TrainingRegistrationMapper trainingRegistrationMapper;
	
	@PostConstruct
	private void postConstruct(){
		
		PropertyMap<Agent, AgentDTO> typeMap = new PropertyMap<Agent, AgentDTO>() {
			
			@Override
			protected void configure() {
				skip().setCpds(null);
				skip().setRegistration(null);
			}
		};
		
		mapper.addMappings(typeMap);
	}

	public Agent createEntity(AgentDTO dto) {

		Agent entity = new Agent();
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setStartLicenseDate(dto.getStartLicenseDate());
		entity.setEndLicenseDate(dto.getEndLicenseDate());
		entity.setGroupLeader(dto.isGroupLeader());
		entity.setBirthDate(dto.getBirthDate());
		entity.setAge(dto.getAge());
		entity.setCasePoint(dto.getCasePoint());
		entity.setApiPoint(dto.getApiPoint());
		entity.setUnderReview(dto.isUnderReview());
		entity.setLevelType(dto.getLevelType());
		entity.setOfficeCode(dto.getOfficeCode());
		entity.setOfficeName(dto.getOfficeName());
		entity.setRadd(dto.getRadd());
		entity.setAds(dto.getAds());
		entity.setLeaderCode(dto.getLeaderCode());
		entity.setLeaderName(dto.getLeaderName());

		Long agentGroupId = dto.getAgentGroupId();
		if (null != agentGroupId) {
			entity.setAgentGroup(agentGroupRepo.findOne(agentGroupId));
		}

		// Long cpdId = dto.getTrainingCpdId();
		// if (null != cpdId) {
		// entity.setCpd(cpdRepo.findOne(cpdId));
		// }

		return entity;
	}

	public Agent updateEntity(AgentDTO dto) {
		Agent entity = repo.findByCode(dto.getCode());
//		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setStartLicenseDate(dto.getStartLicenseDate());
		entity.setEndLicenseDate(dto.getEndLicenseDate());
		entity.setGroupLeader(dto.isGroupLeader());
		entity.setBirthDate(dto.getBirthDate());
		entity.setAge(dto.getAge());
		entity.setCasePoint(dto.getCasePoint());
		entity.setApiPoint(dto.getApiPoint());
		entity.setUnderReview(dto.isUnderReview());
		entity.setLevelType(dto.getLevelType());
		entity.setOfficeCode(dto.getOfficeCode());
		entity.setOfficeName(dto.getOfficeName());
		entity.setRadd(dto.getRadd());
		entity.setAds(dto.getAds());
		entity.setLeaderCode(dto.getLeaderCode());
		entity.setLeaderName(dto.getLeaderName());

		// Long agentGroupId = dto.getAgentGroupId();
		// if (null != agentGroupId) {
		// entity.setAgentGroup(agentGroupRepo.findOne(agentGroupId));
		// }
		// Long cpdId = dto.getTrainingCpdId();
		// if (null != cpdId) {
		// entity.setCpd(cpdRepo.findOne(cpdId));
		// }
		return entity;
	}

	@Override
	public AgentDTO convertToDto(Agent entity) {
		AgentDTO dto = mapper.map(entity, AgentDTO.class);
		
		int cpdRegister = 0;
		int cpdAAJI = 0;
		List<TrainingCPD> cpds = entity.getCpds();
		for (TrainingCPD cpd : cpds) {
			cpdRegister += cpd.getPoint();
			for(TrainingCPDMutation mutation : cpd.getMutations()){
				cpdAAJI += mutation.getCurrentPoint();
			}
		}

		dto.setCpdRegister(cpdRegister);
		dto.setCpdAAJI(cpdAAJI);
		
		// AgentGroup agentGroup = entity.getAgentGroup();
		// if (null != agentGroup) {
		// dto.setAgentGroupId(agentGroup.getId());
		// }
		// TrainingCPD cpd = entity.getCpd();
		// if (null != cpd) {
		// dto.setTrainingCpdId(cpd.getId());
		// }
		// Set<TrainingRegistration> registrations = entity.getRegistrations();
		// for (TrainingRegistration reg : registrations) {
		// TrainingRegistrationDTO regDTO = new TrainingRegistrationDTO();
		// regDTO.setId(reg.getId());
		// regDTO.setRegistrationNo(reg.getRegistrationNo());
		// regDTO.setStartDate(reg.getStartDate());
		// regDTO.setAgentId(reg.getAgent().getId());
		// Set<TrainingRegistrationProgress> progress = reg.getProgress();
		// for (TrainingRegistrationProgress regProgress : progress) {
		// TrainingRegistrationProgressDTO progressDTO = new
		// TrainingRegistrationProgressDTO();
		// progressDTO.setId(regProgress.getId());
		// progressDTO.setTrainingRegistrationId(regProgress.getTrainingRegistration().getId());
		// progressDTO.setProgressOrder(regProgress.getProgressOrder());
		//
		// regDTO.getProgress().add(progressDTO);
		// }
		//
		// dto.getRegistrations().add(regDTO);
		// }

		return dto;
	}

	@Override
	public List<AgentDTO> convertToDto(Iterable<Agent> entities) {
		List<AgentDTO> dtoList = new ArrayList<>();
		for (Agent entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public Agent convertToEntity(AgentDTO dto) {

		Agent entity = mapper.map(dto, Agent.class);

		// Agent entity = null;
		//
		// if(dto.getId() != null) {
		// entity = repo.getOne(dto.getId());
		// } else {
		// entity = new Agent();
		// }
		//
		// AgentGroup agentGroup = null;
		// if(dto.getAgentGroupId() != null) {
		// agentGroup = agentGroupRepo.getOne(dto.getAgentGroupId());
		// }
		//
		// entity.setAgentGroup(agentGroup);
		// entity.setCode(dto.getCode());
		//
		// TrainingCPD trainingCPD = null;
		// if(dto.getTrainingCpdId() != null) {
		// trainingCPD = cpdRepo.getOne(dto.getTrainingCpdId());
		// }
		//
		// entity.setCpd(trainingCPD);
		//
		// entity.setEndLicenseDate(dto.getEndLicenseDate());
		// entity.setGroupLeader(dto.isGroupLeader());
		// entity.setName(dto.getName());
		//
		// if(dto.getRegistrations() != null &&
		// !dto.getRegistrations().isEmpty()) {
		//
		// }
		// entity.setRegistrations(registrations);
		// // TODO Auto-generated method stub
		return entity;
	}

	@Override
	public List<Agent> convertToEntity(Iterable<AgentDTO> dtos) {
		List<Agent> entityList = new ArrayList<Agent>();

		for (AgentDTO dto : dtos) {
			entityList.add(convertToEntity(dto));
		}
		return entityList;
	}

}
