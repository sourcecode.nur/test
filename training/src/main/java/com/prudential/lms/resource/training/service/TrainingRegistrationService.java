package com.prudential.lms.resource.training.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.shared.exception.LmsException;

public interface TrainingRegistrationService {

	TrainingRegistrationSchedule registerTraining(Agent agent, TrainingSchedule schedule) throws LmsException;

	File exportTrainingParticipant(List<TrainingRegistrationSchedule> trnRegSchList) throws IOException, LmsException;

}
