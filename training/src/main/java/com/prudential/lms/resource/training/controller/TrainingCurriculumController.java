package com.prudential.lms.resource.training.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.TrainingCurriculumMapper;
import com.prudential.lms.resource.training.model.TrainingCurriculum;
import com.prudential.lms.resource.training.repository.CurriculumRepository;
import com.prudential.lms.resource.training.repository.TrainingCurriculumRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.shared.dto.training.TrainingCurriculumDTO;
import com.prudential.lms.shared.message.Response;

//@RestController
//@RequestMapping("/training-curriculum")
@Deprecated
public class TrainingCurriculumController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCurriculumController.class);

	@Autowired
	private TrainingCurriculumRepository repo;

	@Autowired
	private CurriculumRepository curriculumRepo;

	@Autowired
	private TrainingRepository trainingRepo;

	@Autowired
	private TrainingCurriculumMapper mapper;

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Object list(@RequestBody(required = false) TrainingCurriculumDTO dto) {

		Response<Object> response = null;
		List<TrainingCurriculum> listQuestion = null;

		try {

			listQuestion = repo.findAll();
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing Answer", e);
			return Response.internalServerError();
		}

		response = listQuestion.size() > 0 ? Response.found(mapper.convertToDto(listQuestion)) : Response.notFound();

		return response;
	}

	// Create
	@RequestMapping(value = { "/", "" }, method = RequestMethod.POST)
	public Object create(@RequestBody TrainingCurriculumDTO dto) {
		TrainingCurriculum saved = null;

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving Answer", e);
			return Response.internalServerError();
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		TrainingCurriculum question = repo.findOne(id);
		if (null == question)
			return Response.notFound();

		return Response.found(mapper.convertToDto(question));
	}

	// Find by sample
//	@RequestMapping(value = "/find", method = RequestMethod.GET)
//	public Object findByExample(@RequestBody TrainingCurriculumDTO example) {
//		Response<Object> response = null;
//		List<TrainingCurriculum> listTrainingCurriculum = null;
//		if (example != null) {
//			if (example.getCurriculumId() != null) {
//				listTrainingCurriculum = repo.findByCurriculum(curriculumRepo.findOne(example.getCurriculumId()));
//			} else if (example.getTrainingId() != null) {
//				listTrainingCurriculum = repo.findByTraining(trainingRepo.findOne(example.getTrainingId()));
//			}
//			response = listTrainingCurriculum.size() > 0 ? Response.found(mapper.convertToDto(listTrainingCurriculum))
//					: Response.notFound();
//
//			return response;
//		} else
//
//			return Response.notFound();
//	}

	// Update
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody TrainingCurriculumDTO dto) {

		TrainingCurriculum updatedEntity = null;
		TrainingCurriculum trainingCurriculum = repo.findOne(id);

		if (null == trainingCurriculum) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating Answer", e);
				return Response.internalServerError();
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		TrainingCurriculum trainingCurriculum = repo.findOne(id);

		if (null == trainingCurriculum) {
			return Response.notFound();
		}

		try {
			repo.delete(trainingCurriculum);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting Answer", e);
			return Response.internalServerError();
		}

		return Response.ok();
	}
}
