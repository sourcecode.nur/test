package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.TrainingProgress;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.repository.TrainingProgressRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.shared.dto.training.TrainingProgressDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingProgressMapper
		implements EntityToDTOMapper<TrainingProgress, TrainingProgressDTO> {

	@Autowired
	private TrainingProgressRepository repo;
	
	@Autowired
	private TrainingRegistrationScheduleRepository trnRegSchRepo;
	
	@Autowired
	private ModelMapper mapper;

	public TrainingProgress createEntity(TrainingProgressDTO dto) {
		
		TrainingProgress entity = new TrainingProgress();
		entity.setStatus(dto.getStatus());
		entity.setSubProgressOrder(dto.getSubProgressOrder());

		Long trainingRegistrationId = dto.getTrainingRegistrationId();
		if (null != trainingRegistrationId) {
			entity.setRegistrationSchedule(trnRegSchRepo.findOne(trainingRegistrationId));
		}

		return entity;
	}

	public TrainingProgress updateEntity(TrainingProgressDTO dto) {
		TrainingProgress entity = repo.findOne(dto.getId());

		Long trainingRegistrationId = dto.getTrainingRegistrationId();
		if (null != trainingRegistrationId) {
			entity.setRegistrationSchedule(trnRegSchRepo.findOne(trainingRegistrationId));
		}
		
		return entity;
	}

	@Override
	public TrainingProgressDTO convertToDto(TrainingProgress entity) {
		TrainingProgressDTO dto = mapper.map(entity, TrainingProgressDTO.class);

		TrainingRegistrationSchedule trnRegSch = entity.getRegistrationSchedule();
		if (null != trnRegSch) {
			dto.setTrainingRegistrationId(trnRegSch.getId());
			dto.setTrainingRegistrationNo(trnRegSch.getRegistrationNo());
		}

		return dto;
	}

	@Override
	public List<TrainingProgressDTO> convertToDto(Iterable<TrainingProgress> entities) {
		List<TrainingProgressDTO> dtoList = new ArrayList<>();
		for (TrainingProgress entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingProgress convertToEntity(TrainingProgressDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingProgress> convertToEntity(Iterable<TrainingProgressDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
