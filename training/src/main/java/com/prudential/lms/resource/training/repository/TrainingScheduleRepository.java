package com.prudential.lms.resource.training.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingSchedule;

public interface TrainingScheduleRepository
		extends JpaRepository<TrainingSchedule, Long>, QueryDslPredicateExecutor<TrainingSchedule> {

	TrainingSchedule findByBatchCode(String batchCode);

	List<TrainingSchedule> findByStartDate(Date startDate);

	List<TrainingSchedule> findByEndDate(Date endDate);

	List<TrainingSchedule> findByTraining(Training findOne);
}
