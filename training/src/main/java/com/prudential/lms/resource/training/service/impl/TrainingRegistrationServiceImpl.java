package com.prudential.lms.resource.training.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.AgentTypeRequirement;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.repository.TrainingRegistrationRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationScheduleRepository;
import com.prudential.lms.resource.training.repository.TrainingScheduleRepository;
import com.prudential.lms.resource.training.service.TrainingProgressService;
import com.prudential.lms.resource.training.service.TrainingRegistrationService;
import com.prudential.lms.shared.constant.TrainingProgressStatus;
import com.prudential.lms.shared.exception.LmsException;

@Service
@Transactional
public class TrainingRegistrationServiceImpl implements TrainingRegistrationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingRegistrationServiceImpl.class);

	@Autowired
	private TrainingRegistrationRepository trnRegRepo;

	@Autowired
	private TrainingScheduleRepository trnSchRepo;

	@Autowired
	private TrainingRegistrationScheduleRepository trnRegSchRepo;

	@Autowired
	private TrainingProgressService progressService;
	
	@Value("${app.upload.temp}")
	private String tempUploadPath;

	@Transactional
	@Override
	public TrainingRegistrationSchedule registerTraining(Agent agent, TrainingSchedule schedule) throws LmsException {

		Long agentId = agent.getId();
		TrainingRegistration registration = trnRegRepo.findByAgentId(agentId);
		if (null == registration)
			throw new LmsException("TrainingRegistration with Agent " + agentId + " not found");

		Training training = schedule.getTraining();

		// validation

		// age
		if (agent.getAge() < training.getMinAgentAge())
			throw new LmsException("TrainingRegistration failed: Agent age not capable");

		// level
		boolean agentLevelAllowed = false;
		List<AgentTypeRequirement> agentTypeReqs = training.getAgentTypeReqs();
		for (AgentTypeRequirement atr : agentTypeReqs) {
			if (StringUtils.equalsIgnoreCase(atr.getAgentType(), agent.getLevelType())) {
				agentLevelAllowed = true;
				break;
			}
		}
		if (!agentLevelAllowed)
			throw new LmsException("TrainingRegistration failed: Agent level not capable");

		// case point
//		if (agent.getCasePoint() < training.getMinCasePoint())
//			throw new LmsException("TrainingRegistration failed: Agent Minimum Case Point not capable");

		// API Point
//		if (agent.getApiPoint().compareTo(training.getMinAPIPoint()) < 0)
//			throw new LmsException("TrainingRegistration failed: Agent Minimum API Point not capable");

		// Agent Under Review
//		boolean isUnderReview = agent.isUnderReview() && training.isAgentUnderReview();
//		if (!isUnderReview)
//			throw new LmsException("TrainingRegistration failed: Agent under Review Status not capable");

		// TODO: training registration schedule

		TrainingRegistrationSchedule regSch = new TrainingRegistrationSchedule();
		regSch.setSchedule(schedule);
		regSch.setRegistration(registration);
		regSch.setStatus(TrainingProgressStatus.REGISTERED);
		TrainingRegistrationSchedule savedRegSch = trnRegSchRepo.save(regSch);
		progressService.register(savedRegSch);

		// add people to register
		schedule.register(1);
		trnSchRepo.save(schedule);

		LOGGER.debug("Registration Success for registrationNo: " + savedRegSch.getRegistrationNo());
		return savedRegSch;
	}

	@Override
	public File exportTrainingParticipant(List<TrainingRegistrationSchedule> trnRegSchList)
			throws IOException, LmsException {

		FileOutputStream excelOutputStream = null;
		
		if (null == trnRegSchList)
			throw new LmsException("Unable export null TrainingRegistrationSchedule List");

		String fileName = "Training_Participant";
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet(fileName);

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		String[] headers = { "Batch Code", "Training Name", "Agent Number", "Agent Name", "Start Date", "End Date",
				"Activity Status", "Life Asia System Status" };
		int cellCount = headers.length;
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < cellCount; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(headers[i]);
		}

		int rowIdx = 1;
		for (TrainingRegistrationSchedule t : trnRegSchList) {
			Row row = sheet.createRow(rowIdx);
			for (int i = 0; i < cellCount; i++) {

				// batch code
				Cell cell = row.createCell(i++);
				cell.setCellValue(t.getSchedule().getBatchCode());
				
				// training name
				Cell cellTrnName = row.createCell(i++);
				cellTrnName.setCellValue(t.getSchedule().getTraining().getName());
				
				// agent number
				Cell cellAgentNum = row.createCell(i++);
				cellAgentNum.setCellValue(t.getRegistration().getAgent().getCode());
				
				// agent Name
				Cell cellAgentName = row.createCell(i++);
				cellAgentName.setCellValue(t.getRegistration().getAgent().getName());
				
				// start date
				Cell cellStartDate = row.createCell(i++);
				Date startDate = t.getSchedule().getStartDate();
				cellStartDate.setCellValue(null != startDate ? df.format(startDate) : "");
				
				// end date
				Cell cellEndDate = row.createCell(i++);
				Date endDate = t.getSchedule().getEndDate();
				cellEndDate.setCellValue(null != endDate ? df.format(endDate) : "");
				
				// status
				Cell cellStatus = row.createCell(i++);
				TrainingProgressStatus status = t.getStatus();
				cellStatus.setCellValue(String.valueOf(status));

			}

			rowIdx++;
		}

		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		String uniqDir = tempUploadPath.concat(File.separator).concat(uuid);
		new File(uniqDir).mkdir();
		File result = new File(uniqDir.concat(File.separator).concat(fileName).concat(".xlsx"));
		excelOutputStream = new FileOutputStream(result);
		
		if(null != excelOutputStream){
			workbook.write(excelOutputStream);
			workbook.close();
			excelOutputStream.close();
		}
		
		return result;
	}

}
