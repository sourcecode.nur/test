package com.prudential.lms.resource.training.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Order(Integer.MIN_VALUE)
public class AppAuditorAware extends OncePerRequestFilter implements AuditorAware<String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppAuditorAware.class);
	
	private String defaultAuditor;

	@Override
	public String getCurrentAuditor() {
		return getDefaultAuditor();
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String requestURI = request.getRequestURI();
		String requestURL = request.getRequestURL().toString();
		LOGGER.debug("uri : {} - url: {}", requestURI, requestURL);
		
		String authUsername = request.getHeader("Username");
		LOGGER.debug("authUsername : {}", authUsername);
		
		if (null != authUsername) {
			if (StringUtils.isNotEmpty(authUsername)) {
				setDefaultAuditor(authUsername);
			} else {
				setDefaultAuditor("SYSTEM");
			}
		}

		filterChain.doFilter(request, response);
	}

	public String getDefaultAuditor() {
		return defaultAuditor;
	}

	public void setDefaultAuditor(String defaultAuditor) {
		this.defaultAuditor = defaultAuditor;
	}

}
