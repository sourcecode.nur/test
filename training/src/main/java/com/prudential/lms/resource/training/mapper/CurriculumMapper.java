package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Curriculum;
import com.prudential.lms.resource.training.model.TrainingCurriculum;
import com.prudential.lms.resource.training.repository.CurriculumRepository;
import com.prudential.lms.shared.dto.training.CurriculumDTO;
import com.prudential.lms.shared.dto.training.TrainingCurriculumDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class CurriculumMapper implements EntityToDTOMapper<Curriculum, CurriculumDTO> {

	@Autowired
	private CurriculumRepository repo;

	@Autowired
	private TrainingCurriculumMapper trnCurMapper;

	@Autowired
	private ModelMapper mapper;

	public Curriculum createEntity(CurriculumDTO dto) {
		Curriculum entity = new Curriculum();
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setFileName(dto.getFileName());
		entity.setThumbnail(dto.getThumbnail());
		entity.setDraft(dto.isDraft());

		return entity;
	}

	public Curriculum updateEntity(CurriculumDTO dto) {
		Curriculum entity = repo.findOne(dto.getId());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setThumbnail(dto.getThumbnail());
		entity.setFileName(dto.getFileName());
		entity.setDraft(dto.isDraft());

		return entity;
	}

	@Override
	public CurriculumDTO convertToDto(Curriculum entity) {
		CurriculumDTO dto = new CurriculumDTO();
		dto.setId(entity.getId());
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setThumbnail(entity.getThumbnail());
		dto.setFileName(entity.getFileName());
		dto.setUpdatedBy(entity.getUpdatedBy());
		dto.setUpdatedDate(entity.getUpdatedDate());
		dto.setDraft(entity.isDraft());

		Set<TrainingCurriculum> trainings = entity.getTrainings();
		for (TrainingCurriculum trnCur : trainings) {
			TrainingCurriculumDTO trnCurDto = trnCurMapper.convertToDto(trnCur);
			dto.getTrainings().add(trnCurDto);
		}

		return dto;
	}

	@Override
	public Curriculum convertToEntity(CurriculumDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CurriculumDTO> convertToDto(Iterable<Curriculum> entities) {
		List<CurriculumDTO> dtoList = new ArrayList<>();
		for (Curriculum entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public List<Curriculum> convertToEntity(Iterable<CurriculumDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
