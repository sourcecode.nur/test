package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.repository.TrainingScheduleRepository;
import com.prudential.lms.shared.constant.TrainingScheduleStatus;
import com.prudential.lms.shared.dto.training.TrainingScheduleDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingScheduleMapper implements EntityToDTOMapper<TrainingSchedule, TrainingScheduleDTO> {

	@Autowired
	private TrainingScheduleRepository repo;

	@Autowired
	private TrainingRepository trainingRepo;
	
	@Autowired
	private TrainingContentMapper trnCtnMapper;

	@Autowired
	private ModelMapper mapper;

	public TrainingSchedule createEntity(TrainingScheduleDTO dto) {
		TrainingSchedule entity = new TrainingSchedule();
		entity.setBatchCode(dto.getBatchCode());
		entity.setPublishStartDate(dto.getPublishStartDate());
		entity.setPublishEndDate(dto.getPublishEndDate());
		entity.setStartDate(dto.getStartDate());
		entity.setEndDate(dto.getEndDate());
		entity.setQuota(dto.getQuota());
		entity.setPublished(dto.getPublished());
		entity.setStatus(null == dto.getStatus() ? TrainingScheduleStatus.REQUEST : dto.getStatus());
		entity.setDraft(dto.isDraft());

		Long trainingId = dto.getTrainingId();
		if (null != trainingId) {
			entity.setTraining((trainingRepo.findOne(trainingId)));
		}

		return entity;
	}

	public TrainingSchedule updateEntity(TrainingScheduleDTO dto) {
		TrainingSchedule entity = repo.findOne(dto.getId());
		entity.setBatchCode(dto.getBatchCode());
		entity.setPublishStartDate(dto.getPublishStartDate());
		entity.setPublishEndDate(dto.getPublishEndDate());
		entity.setStartDate(dto.getStartDate());
		entity.setEndDate(dto.getEndDate());
		entity.setQuota(dto.getQuota());
		entity.setPublished(dto.getPublished());
		entity.setStatus(null == dto.getStatus() ? TrainingScheduleStatus.REQUEST : dto.getStatus());
		entity.setDraft(dto.isDraft());
		
		Long trainingId = dto.getTrainingId();
		if (null != trainingId) {
			entity.setTraining((trainingRepo.findOne(trainingId)));
		}

		return entity;
	}

	@Override
	public TrainingScheduleDTO convertToDto(TrainingSchedule entity) {
		TrainingScheduleDTO dto = mapper.map(entity, TrainingScheduleDTO.class);

		Training training = entity.getTraining();
		if (null != training) {
//			dto.setTrainingId(training.getId());
//			dto.setTrainingName(training.getName());
//			dto.setTrainingType(training.getTrainingType());
//			dto.setTrainingCategoryName(training.getCategory().getName());
//			dto.setTrainingContentType(training.getTrainingContentType());
//			dto.setTrainingDescription(training.getDescription());
			dto.setCpdPoint(training.getCpdPoint());
			
			dto.setContents(trnCtnMapper.convertToDto(training.getContents()));
		}

		return dto;
	}

	@Override
	public List<TrainingScheduleDTO> convertToDto(Iterable<TrainingSchedule> entities) {
		List<TrainingScheduleDTO> dtoList = new ArrayList<>();
		for (TrainingSchedule entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingSchedule convertToEntity(TrainingScheduleDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingSchedule> convertToEntity(Iterable<TrainingScheduleDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
