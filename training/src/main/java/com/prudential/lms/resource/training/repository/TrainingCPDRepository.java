package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingCPD;

public interface TrainingCPDRepository
		extends JpaRepository<TrainingCPD, Long>, QueryDslPredicateExecutor<TrainingCPD> {

	TrainingCPD findByAgentId(Long agentId);
	
	List<TrainingCPD> findByAgentCode(String agentCode);

}
