package com.prudential.lms.resource.training.model;

import javax.persistence.Embeddable;

@Embeddable
public class PreRequiredTraining {

	private Long trnId;
	private String trnName;
	private String trnDesc;

	public Long getTrnId() {
		return trnId;
	}

	public void setTrnId(Long trnId) {
		this.trnId = trnId;
	}

	public String getTrnName() {
		return trnName;
	}

	public void setTrnName(String trnName) {
		this.trnName = trnName;
	}

	public String getTrnDesc() {
		return trnDesc;
	}

	public void setTrnDesc(String trnDesc) {
		this.trnDesc = trnDesc;
	}

}
