package com.prudential.lms.resource.training.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.TrainingCategoryMapper;
import com.prudential.lms.resource.training.mapper.TrainingMapper;
import com.prudential.lms.resource.training.model.ContentLibrary;
import com.prudential.lms.resource.training.model.QTrainingCategory;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCategory;
import com.prudential.lms.resource.training.model.TrainingContent;
import com.prudential.lms.resource.training.repository.TrainingCategoryRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.constant.TrainingCategoryStatus;
import com.prudential.lms.shared.dto.training.TrainingCategoryDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-category")
public class TrainingCategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCategoryController.class);

    @Autowired
    private TrainingCategoryRepository repo;

    @Autowired
    private TrainingCategoryMapper mapper;

    @Autowired
    private TrainingMapper trainingMapper;

    // List All for pruhub without thumbnail
    @RequestMapping(value = "/list/pruhub/all", method = RequestMethod.POST)
    public Object listPruhubAll(@RequestBody(required = false) TrainingCategoryDTO dto) {

        Response<Object> response = null;
        Iterable<TrainingCategory> tcIt = null;

        try {

            BooleanExpression predicate = QTrainingCategory.trainingCategory.id.gt(-1);
            // isNotDeleted
            predicate = predicate.and(QTrainingCategory.trainingCategory.deleted.isFalse());
            // isRoot
            predicate = predicate.and(QTrainingCategory.trainingCategory.parent.isNull());

            if (null != dto) {

                TrainingCategoryStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.status.eq(status));
                }

                String code = dto.getCode();
                if (StringUtils.isNotEmpty(code)) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.code.likeIgnoreCase("%" + code + "%"));
                }

                String name = dto.getName();
                if (StringUtils.isNotEmpty(name)) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.name.likeIgnoreCase("%" + name + "%"));
                }

                String description = dto.getDescription();
                if (StringUtils.isNotEmpty(description)) {
                    predicate = predicate.and(
                            QTrainingCategory.trainingCategory.description.likeIgnoreCase("%" + description + "%"));
                }

                String fileName = dto.getFileName();
                if (StringUtils.isNotEmpty(fileName)) {
                    predicate = predicate.and(
                            QTrainingCategory.trainingCategory.fileName.likeIgnoreCase("%" + fileName + "%"));
                }

                boolean draft = dto.isDraft();
                predicate = predicate.and(QTrainingCategory.trainingCategory.draft.eq(draft));

            }

            tcIt = repo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));
        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingCategory", e);
            return Response.internalServerError(e.getMessage());
        }

        List<TrainingCategoryDTO> resultList = new ArrayList<TrainingCategoryDTO>();
        for (TrainingCategory category : tcIt) {
            TrainingCategoryDTO ctgrDto = mapper.convertToDto(category);
            int videoCount = 0;
            Set<Training> trainings = category.getTrainings();
            for (Training training : trainings) {
                List<TrainingContent> contents = training.getContents();
                for (TrainingContent trainingContent : contents) {
                    ContentLibrary library = trainingContent.getLibrary();
                    if (library.getMimeType().equalsIgnoreCase("video/mp4")) {
                        videoCount++;
                    }
                }
            }
            ctgrDto.setVideoCount(videoCount);
            ctgrDto.setThumbnail("");
            resultList.add(ctgrDto);
            videoCount = 0;
        }

        int resultSize = resultList.size();
        response = resultSize > 0 ? Response.found(resultList) : Response.notFound();

        return response;
    }

    // List Paging for pruhub without thumbnail
    @RequestMapping(value = "/list/pruhub/{page}/{size}", method = RequestMethod.POST)
    public Object listPruhub(@PathVariable("page") int page, @PathVariable("size") int size,
            @RequestBody(required = false) TrainingCategoryDTO dto) {

        Response<Object> response = null;
        Page<TrainingCategory> tcPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.DESC, "updatedDate", "sortOrder");

        try {

            BooleanExpression predicate = QTrainingCategory.trainingCategory.id.gt(-1);
            // isNotDeleted
            predicate = predicate.and(QTrainingCategory.trainingCategory.deleted.isFalse());
            // isRoot
            predicate = predicate.and(QTrainingCategory.trainingCategory.parent.isNull());

            if (null != dto) {

                TrainingCategoryStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.status.eq(status));
                }

                String code = dto.getCode();
                if (StringUtils.isNotEmpty(code)) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.code.likeIgnoreCase("%" + code + "%"));
                }

                String name = dto.getName();
                if (StringUtils.isNotEmpty(name)) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.name.likeIgnoreCase("%" + name + "%"));
                }

                String description = dto.getDescription();
                if (StringUtils.isNotEmpty(description)) {
                    predicate = predicate.and(
                            QTrainingCategory.trainingCategory.description.likeIgnoreCase("%" + description + "%"));
                }

                String fileName = dto.getFileName();
                if (StringUtils.isNotEmpty(fileName)) {
                    predicate = predicate.and(
                            QTrainingCategory.trainingCategory.fileName.likeIgnoreCase("%" + fileName + "%"));
                }

                boolean draft = dto.isDraft();
                predicate = predicate.and(QTrainingCategory.trainingCategory.draft.eq(draft));

            }

            tcPage = repo.findAll(predicate, pr);
        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingCategory", e);
            return Response.internalServerError(e.getMessage());
        }

        List<TrainingCategoryDTO> resultList = mapper.convertToDto(tcPage.getContent());
        for (TrainingCategoryDTO trainingCategoryDTO : resultList) {
            trainingCategoryDTO.setThumbnail("");
        }

        Page<TrainingCategoryDTO> pageResult = new PageImpl<TrainingCategoryDTO>(resultList, pr,
                tcPage.getTotalElements());
        response = tcPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

        return response;
    }

    // List Paging
    @RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
    public Object list(@PathVariable("page") int page, @PathVariable("size") int size,
            @RequestBody(required = false) TrainingCategoryDTO dto) {

        Response<Object> response = null;
        Page<TrainingCategory> tcPage = null;
        PageRequest pr = new PageRequest(page, size, Direction.DESC, "updatedDate", "sortOrder");

        try {

            BooleanExpression predicate = QTrainingCategory.trainingCategory.id.gt(-1);
            // isNotDeleted
            predicate = predicate.and(QTrainingCategory.trainingCategory.deleted.isFalse());
            // isRoot
            predicate = predicate.and(QTrainingCategory.trainingCategory.parent.isNull());

            if (null != dto) {

                TrainingCategoryStatus status = dto.getStatus();
                if (null != status) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.status.eq(status));
                }

                String code = dto.getCode();
                if (StringUtils.isNotEmpty(code)) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.code.likeIgnoreCase("%" + code + "%"));
                }

                String name = dto.getName();
                if (StringUtils.isNotEmpty(name)) {
                    predicate = predicate.and(QTrainingCategory.trainingCategory.name.likeIgnoreCase("%" + name + "%"));
                }

                String description = dto.getDescription();
                if (StringUtils.isNotEmpty(description)) {
                    predicate = predicate.and(
                            QTrainingCategory.trainingCategory.description.likeIgnoreCase("%" + description + "%"));
                }

                String fileName = dto.getFileName();
                if (StringUtils.isNotEmpty(fileName)) {
                    predicate = predicate.and(
                            QTrainingCategory.trainingCategory.fileName.likeIgnoreCase("%" + fileName + "%"));
                }

                boolean draft = dto.isDraft();
                predicate = predicate.and(QTrainingCategory.trainingCategory.draft.eq(draft));

            }

            tcPage = repo.findAll(predicate, pr);
        } catch (Exception e) {
            LOGGER.error("#####ERROR Paging TrainingCategory", e);
            return Response.internalServerError(e.getMessage());
        }

        List<TrainingCategoryDTO> resultList = new ArrayList<TrainingCategoryDTO>();
        List<TrainingCategory> content = tcPage.getContent();
        for (TrainingCategory category : content) {
            TrainingCategoryDTO ctgrDto = mapper.convertToDto(category);
            int videoCount = 0;
            Set<Training> trainings = category.getTrainings();
            for (Training training : trainings) {
                List<TrainingContent> contents = training.getContents();
                for (TrainingContent trainingContent : contents) {
                    ContentLibrary library = trainingContent.getLibrary();
                    if (library.getMimeType().equalsIgnoreCase("video/mp4")) {
                        videoCount++;
                    }
                }
            }
            ctgrDto.setVideoCount(videoCount);
            resultList.add(ctgrDto);
            videoCount = 0;
        }

        Page<TrainingCategoryDTO> pageResult = new PageImpl<TrainingCategoryDTO>(resultList, pr,
                tcPage.getTotalElements());
        response = tcPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

        return response;
    }

    // Create
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Object create(@RequestBody TrainingCategoryDTO dto) {
        TrainingCategory saved = null;

        try {
            saved = repo.save(mapper.createEntity(dto));
        } catch (Exception e) {
            LOGGER.error("#####ERROR Saving TrainingCategory", e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.created(mapper.convertToDto(saved));
    }

    // Find Training By Category Id
    @RequestMapping(value = "/training/{categoryId}", method = RequestMethod.GET)
    public Object findTrainingByCategory(@PathVariable("categoryId") Long categoryId) {

        TrainingCategory tc = repo.findOne(categoryId);
        if (null == tc) {
            return Response.notFound();
        }

        Set<Training> trainings = tc.getTrainings();
        if (trainings.size() == 0) {
            return Response.notFound();
        }

        return Response.found(trainingMapper.convertToDto(trainings));
    }

    // Find By Id
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Object findOne(@PathVariable("id") Long id) {
        TrainingCategory tc = repo.findOne(id);
        if (null == tc) {
            return Response.notFound();
        }

        return Response.found(mapper.convertToDto(tc));
    }

    // Update
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Object update(@PathVariable("id") Long id, @RequestBody TrainingCategoryDTO dto) {

        TrainingCategory updatedEntity = null;
        TrainingCategory tc = repo.findOne(id);

        if (null == tc) {
            return Response.notFound("TrainingCategory with id: " + id + " not found");
        } else {

            try {
                dto.setId(id);
                updatedEntity = repo.save(mapper.updateEntity(dto));
            } catch (Exception e) {
                LOGGER.error("#####ERROR Updating TrainingCategory", e);
                return Response.internalServerError(e.getMessage());
            }

        }

        return Response.ok(mapper.convertToDto(updatedEntity));
    }

    // Find by example
    // @RequestMapping(value = "/find", method = RequestMethod.POST)
//	public Object findByExample(@RequestBody TrainingCategoryDTO example) {
//
//		Response<Object> response = null;
//		List<TrainingCategory> listTrainingCategory = null;
//
//		if (null != example) {
//			if (example.getCode() != null) {
//				listTrainingCategory = repo.findByCode(example.getCode());
//			} else if (example.getName() != null) {
//				listTrainingCategory = repo.findByName(example.getName());
//			} else if (example.getDescription() != null) {
//				listTrainingCategory = repo.findByDescription(example.getDescription());
//			} else if (example.getParentId() != null) {
//				listTrainingCategory = repo.findByParent(repo.findOne(example.getParentId()));
//			} else if (example.getStatus() != null) {
//				listTrainingCategory = repo.findByStatus(example.getStatus());
//			}
//			response = listTrainingCategory.size() > 0 ? Response.found(mapper.convertToDto(listTrainingCategory))
//					: Response.notFound();
//
//			return response;
//		} else
//
//			return Response.notFound();
//	}
    // Delete
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Object delete(@PathVariable("id") Long id) {
        TrainingCategory tc = repo.findOne(id);

        if (null == tc) {
            return Response.notFound("TrainingCategory with id: " + id + " not found");
        } else {
            if (tc.hasChildren()) {
                return Response.badRequest("Cannot Delete TrainingCategory which has sub Category");
            } else if (tc.hasTraining()) {
                return Response.badRequest("Cannot Delete TrainingCategory which has Training(s)");
            }
        }

        try {
            repo.delete(tc);
        } catch (Exception e) {
            LOGGER.error("#####ERROR Deleting TrainingCategory", e);
            return Response.internalServerError(e.getMessage());
        }

        return Response.ok();
    }

    // Soft Delete
    // @RequestMapping(value = "/softdelete/{del}/{id}", method =
    // RequestMethod.PATCH)
    public Object softDelete(@PathVariable("del") int del, @PathVariable("id") Long id) {

        if (del != 0 || del != 1) {
            return Response.badRequest("Only 0 & 1 are allowed for soft delete flag");
        }

        boolean deleted = del == 0 ? true : false;

        TrainingCategory tc = repo.findOne(id);
        if (null == tc) {
            return Response.notFound();
        }

        try {
            processSoftDelete(tc, deleted);
        } catch (Exception e) {
            return Response.internalServerError();
        }

        return Response.ok();
    }

    private void processSoftDelete(TrainingCategory trainingCategory, boolean deleted) {
        try {

            trainingCategory.setDeleted(deleted);
            repo.save(trainingCategory);
            if (trainingCategory.hasChildren()) {
                Set<TrainingCategory> children = trainingCategory.getChildren();
                for (TrainingCategory child : children) {
                    processSoftDelete(child, deleted);
                }
            }

        } catch (Exception e) {
            LOGGER.error("#####ERROR Soft-Deleting TrainingCategory", e);
        }

    }

}
