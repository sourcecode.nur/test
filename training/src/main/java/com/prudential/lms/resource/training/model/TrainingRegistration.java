package com.prudential.lms.resource.training.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_registration")
public class TrainingRegistration extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4260206193114985011L;
	private static final String SEQ_GEN = "seq_gen_training_registration";
	private static final String SEQ = "seq_training_registration";

	private Long id;
	private Agent agent;
	private List<TrainingRegistrationSchedule> schedules = new ArrayList<>();

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "agent_id", nullable = false)
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	@OneToMany(mappedBy = "registration")
	public List<TrainingRegistrationSchedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<TrainingRegistrationSchedule> schedules) {
		this.schedules = schedules;
	}

	public void addSchedules(TrainingRegistrationSchedule... schedules) {
		for (TrainingRegistrationSchedule schedule : schedules) {
			this.schedules.add(schedule);
		}
	}

}
