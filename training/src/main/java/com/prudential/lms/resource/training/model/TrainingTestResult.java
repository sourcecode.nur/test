package com.prudential.lms.resource.training.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.prudential.lms.shared.constant.TrainingTestType;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_test_result")
public class TrainingTestResult extends AuditableEntityBase implements EntityBase<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2926772960188003021L;
	private static final String SEQ_GEN = "seq_gen_trn_test_result_uuid";
	private static final String SEQ_STRATEGY = "uuid";

	private String id;
	private BigDecimal point;
	private BigDecimal minPoint;
	private int attempt;
	private int finishDuration;
	private Date finishDate;
	private TrainingTestType testType;
	private boolean pass;
	private Date submitedToLAS;
	private TrainingRegistrationSchedule trainingRegistrationSchedule;

	private List<TrainingTestQuestion> questions = new ArrayList<>();

	@Id
	@GeneratedValue(generator = SEQ_GEN)
	@GenericGenerator(name = SEQ_GEN, strategy = SEQ_STRATEGY)
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getPoint() {
		return point;
	}

	public void setPoint(BigDecimal point) {
		this.point = point;
	}

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}

	public int getFinishDuration() {
		return finishDuration;
	}

	public void setFinishDuration(int finishDuration) {
		this.finishDuration = finishDuration;
	}

	@Type(type = "yes_no")
	public boolean isPass() {
		return pass;
	}

	public void setPass(boolean pass) {
		this.pass = pass;
	}

	@Column(name = "submitted_to_las")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSubmitedToLAS() {
		return submitedToLAS;
	}

	public void setSubmitedToLAS(Date submitedToLAS) {
		this.submitedToLAS = submitedToLAS;
	}

	@ManyToOne
	@JoinColumn(name = "trn_reg_sch_id", referencedColumnName = "id")
	public TrainingRegistrationSchedule getTrainingRegistrationSchedule() {
		return trainingRegistrationSchedule;
	}

	public void setTrainingRegistrationSchedule(TrainingRegistrationSchedule trainingRegistrationSchedule) {
		this.trainingRegistrationSchedule = trainingRegistrationSchedule;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	@Enumerated(EnumType.STRING)
	public TrainingTestType getTestType() {
		return testType;
	}

	public void setTestType(TrainingTestType testType) {
		this.testType = testType;
	}

	@ElementCollection
	@CollectionTable(name = "lms_trn_test_question", joinColumns = { @JoinColumn(name = "trn_test_res_id") })
	@OrderColumn(name = "trn_test_res_index")
	public List<TrainingTestQuestion> getQuestions() {
		return questions;
	}

	public void setQuestions(List<TrainingTestQuestion> questions) {
		this.questions = questions;
	}
	
	@Transient
	public void addQuestions(TrainingTestQuestion... questions) {
		for (TrainingTestQuestion question : questions) {
			this.questions.add(question);
		}
	}

	public BigDecimal getMinPoint() {
		return minPoint;
	}
	

	public void setMinPoint(BigDecimal minPoint) {
		this.minPoint = minPoint;
	}
	

}
