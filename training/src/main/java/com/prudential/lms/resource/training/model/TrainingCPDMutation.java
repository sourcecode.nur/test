package com.prudential.lms.resource.training.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.prudential.lms.shared.constant.CPDMutationOperation;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_training_cpd_mtn")
public class TrainingCPDMutation extends AuditableEntityBase implements EntityBase<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4396182080264643604L;
	private static final String SEQ_GEN = "seq_gen_trn_cpd_mtn_uuid";
	private static final String SEQ_STRATEGY = "uuid";

	private String id;
	private TrainingCPD cpd;
	private int prevPoint;
	private int deltaPoint;
	private int currentPoint;
	private CPDMutationOperation mutationOperation;

	@Id
	@GeneratedValue(generator = SEQ_GEN)
	@GenericGenerator(name = SEQ_GEN, strategy = SEQ_STRATEGY)
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public int getPrevPoint() {
		return prevPoint;
	}

	public void setPrevPoint(int prevPoint) {
		this.prevPoint = prevPoint;
	}

	public int getDeltaPoint() {
		return deltaPoint;
	}

	public void setDeltaPoint(int deltaPoint) {
		this.deltaPoint = deltaPoint;
	}

	public int getCurrentPoint() {
		return currentPoint;
	}

	public void setCurrentPoint(int currentPoint) {
		this.currentPoint = currentPoint;
	}

	@Column(name = "mutation_operation")
	@Enumerated(EnumType.STRING)
	public CPDMutationOperation getMutationOperation() {
		return mutationOperation;
	}

	public void setMutationOperation(CPDMutationOperation mutationOperation) {
		this.mutationOperation = mutationOperation;
	}

	@ManyToOne
	@JoinColumn(name = "training_cpd_id", referencedColumnName = "id")
	public TrainingCPD getCpd() {
		return cpd;
	}

	public void setCpd(TrainingCPD cpd) {
		this.cpd = cpd;
	}

}
