package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationRepository;
import com.prudential.lms.shared.dto.training.TrainingRegistrationDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingRegistrationMapper implements EntityToDTOMapper<TrainingRegistration, TrainingRegistrationDTO> {

	@Autowired
	private TrainingRegistrationRepository repo;

	@Autowired
	private AgentRepository agentRepo;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private TrainingRegistrationScheduleMapper trnRegSchMapper;

	@Override
	public TrainingRegistration createEntity(TrainingRegistrationDTO dto) {

		TrainingRegistration entity = new TrainingRegistration();

		Long agentId = dto.getAgentId();
		if (null != agentId) {
			entity.setAgent(agentRepo.findOne(agentId));
		}

		return entity;
	}

	public TrainingRegistration updateEntity(TrainingRegistrationDTO dto) {
		TrainingRegistration entity = repo.findOne(dto.getId());

		Long agentId = dto.getAgentId();
		if (null != agentId) {
			entity.setAgent(agentRepo.findOne(agentId));
		}

		return entity;
	}

	@Override
	public TrainingRegistrationDTO convertToDto(TrainingRegistration entity) {

		TrainingRegistrationDTO dto = mapper.map(entity, TrainingRegistrationDTO.class);

		Agent agent = entity.getAgent();
		if (null != agent) {
			dto.setAgentId(agent.getId());
			dto.setAgentName(agent.getName());
		}

		List<TrainingRegistrationSchedule> schedules = entity.getSchedules();
		if (null != schedules) {
			dto.setSchedules(trnRegSchMapper.convertToDto(schedules));
		}

		return dto;
	}

	@Override
	public List<TrainingRegistrationDTO> convertToDto(Iterable<TrainingRegistration> entities) {
		List<TrainingRegistrationDTO> dtoList = new ArrayList<>();
		for (TrainingRegistration entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingRegistration convertToEntity(TrainingRegistrationDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingRegistration> convertToEntity(Iterable<TrainingRegistrationDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
