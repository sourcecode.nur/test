package com.prudential.lms.resource.training.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;

@Entity
@Table(name = "lms_content_library")
public class ContentLibrary extends AuditableEntityBase implements EntityBase<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8230083508349075866L;
	private static final String SEQ_GEN = "seq_gen_ctn_lib";
	private static final String SEQ = "seq_ctn_lib";

	private Long id;
	private String title;
	private String description;
	private String mimeType;
	private String path;
	private String baseUri;
	private String uri;
	private String thumbnail;
	private String fileName;
	private List<TrainingContent> trainingContents = new ArrayList<>();

//	@PrePersist
//	private void prePersist() {
//		this.uri = this.baseUri.concat(this.path);
//	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@OneToMany(mappedBy = "library")
	public List<TrainingContent> getTrainingContents() {
		return trainingContents;
	}

	public void setTrainingContents(List<TrainingContent> trainingContents) {
		this.trainingContents = trainingContents;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getBaseUri() {
		return baseUri;
	}

	public void setBaseUri(String baseUri) {
		this.baseUri = baseUri;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getFileName() {
		return fileName;
	}
	

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	

}
