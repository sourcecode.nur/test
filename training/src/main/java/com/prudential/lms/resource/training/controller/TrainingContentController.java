package com.prudential.lms.resource.training.controller;

import org.apache.poi.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.TrainingContentMapper;
import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.QTrainingContent;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingContent;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingContentRepository;
import com.prudential.lms.shared.constant.TrainingContentType;
import com.prudential.lms.shared.dto.training.TrainingContentDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/training-content")
public class TrainingContentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingContentController.class);

	@Autowired
	private TrainingContentRepository repo;
	
	@Autowired
	private AgentRepository agentRepo;

	@Autowired
	private TrainingContentMapper mapper;

	// List Paging
	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
	public Object list(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) TrainingContentDTO dto) {

		Response<Object> response = null;
		Page<TrainingContent> tcPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.ASC, "training", "createdDate");
		try {

			BooleanExpression predicate = QTrainingContent.trainingContent.id.gt(0);

			if (null != dto) {

				Long trainingId = dto.getTrainingId();
				if (null != trainingId)
					predicate = predicate.and(QTrainingContent.trainingContent.training.id.eq(trainingId));

				Long libraryId = dto.getLibraryId();
				if (null != libraryId)
					predicate = predicate.and(QTrainingContent.trainingContent.library.id.eq(libraryId));

			}

			tcPage = repo.findAll(predicate, pr);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing TrainingContent", e);
			return Response.internalServerError(e.getMessage());
		}

		Page<TrainingContentDTO> pageResult = new PageImpl<TrainingContentDTO>(mapper.convertToDto(tcPage.getContent()),
				pr, tcPage.getTotalElements());
		response = tcPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

		return response;
	}
	
	
	@RequestMapping(value = "/list-by-agentcode/{agentCode}/{page}/{size}", method = RequestMethod.GET)
	public Object listByAgentCode(@PathVariable("agentCode") String agentCode, @PathVariable("page") int page, @PathVariable("size") int size) {
		Response<Object> response = null;
		Page<TrainingContent> tcPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.ASC, "training", "createdDate");
		try {
			if(agentCode == null || agentCode == "") {
				return Response.notFound("AgentCode is required!");
			}
			
			Agent agent = agentRepo.findByCode(agentCode);
			if(agent == null) {
				return Response.notFound("Agent not found!");
			}
			
			LOGGER.debug("Agent age: "+agent.getAge()+", Agent Api Point: "+agent.getApiPoint()+", Agent case Point: "+agent.getCasePoint());
			BooleanExpression predicate = QTrainingContent.trainingContent.id.gt(0);
			
			predicate = predicate.and(QTrainingContent.trainingContent.type.eq(TrainingContentType.SUPPORT));
			
			predicate = predicate.and(QTrainingContent.trainingContent.library.mimeType.eq("video/mp4"));
			
			predicate = predicate.and(QTrainingContent.trainingContent.training.minAgentAge.loe(agent.getAge()));
			
			predicate = predicate.and(QTrainingContent.trainingContent.training.minAPIPoint.loe(agent.getApiPoint()));			
			
			predicate = predicate.and(QTrainingContent.trainingContent.training.minCasePoint.loe(agent.getCasePoint()));		
			
			predicate = predicate.and(QTrainingContent.trainingContent.training.agentTypeReqs.any().agentType.contains(agent.getLevelType()));		
			
			predicate = predicate.and(QTrainingContent.trainingContent.training.schedules.any().published.eq(true));
			
			tcPage = repo.findAll(predicate, pr);			
			
		}catch(Exception e) {
			LOGGER.error("#####ERROR Listing TrainingContent", e);
			return Response.internalServerError(e.getMessage());
		}
		
		Page<TrainingContentDTO> pageResult = new PageImpl<TrainingContentDTO>(mapper.convertToDto(tcPage.getContent()), pr, tcPage.getTotalElements());
		response = tcPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();
		
		return response;
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody TrainingContentDTO dto) {
		TrainingContent saved = null;

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving TrainingContent", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		TrainingContent tc = repo.findOne(id);
		if (null == tc)
			return Response.notFound();

		return Response.found(mapper.convertToDto(tc));
	}

	// Update
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody TrainingContentDTO dto) {

		TrainingContent updatedEntity = null;
		TrainingContent tc = repo.findOne(id);

		if (null == tc) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating TrainingContent", e);
				return Response.internalServerError(e.getMessage());
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		TrainingContent tc = repo.findOne(id);

		if (null == tc) {
			return Response.notFound();

		}
		try {
			repo.delete(tc);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting TrainingContent", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

}
