package com.prudential.lms.resource.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;

public interface TrainingRegistrationScheduleRepository extends JpaRepository<TrainingRegistrationSchedule, Long>,
		QueryDslPredicateExecutor<TrainingRegistrationSchedule> {

	TrainingRegistrationSchedule findByRegistrationNo(String registrationNo);

}
