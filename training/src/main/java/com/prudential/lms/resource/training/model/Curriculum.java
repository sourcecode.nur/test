package com.prudential.lms.resource.training.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;
import com.prudential.lms.shared.model.SoftDelete;

@Entity
@Table(name = "lms_curriculum")
public class Curriculum extends AuditableEntityBase implements EntityBase<Long>, SoftDelete {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7141018293613513931L;
	private static final String SEQ_GEN = "seq_gen_curriculum";
	private static final String SEQ = "seq_curriculum";

	private Long id;
	private String code;
	private String name;
	private String description;

	private Set<TrainingCurriculum> trainings = new HashSet<>();
	private boolean deleted;
	private int sortOrder;
	private int curriculumCount;
	private int depth = 1;
	private boolean draft;
	private String fileName;
	private String thumbnail;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(length = 32, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "curriculum")
	public Set<TrainingCurriculum> getTrainings() {
		return trainings;
	}

	public void setTrainings(Set<TrainingCurriculum> trainings) {
		this.trainings = trainings;
	}

	public void addTraining(TrainingCurriculum trainingCurriculum) {
		this.trainings.add(trainingCurriculum);
	}

	@Transient
	public boolean hasTrainingCurriculum() {
		return getTrainings().size() > 0;
	}

	@Transient
	public boolean isRoot() {
		return null == getTrainings();
	}

	@Transient
	public boolean isNode() {
		return null != getTrainings() && hasTrainingCurriculum();
	}

	@Transient
	public boolean isLeaf() {
		return !isRoot() && !hasTrainingCurriculum();
	}

	@Type(type = "yes_no")
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Column(name = "sort_order")
	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	@Column(name = "curriculum_count")
	public int getCurriculumCount() {
		return curriculumCount;
	}

	public void setCurriculumCount(int curriculumCount) {
		this.curriculumCount = curriculumCount;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	@Type(type = "yes_no")
	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}
	
	
}
