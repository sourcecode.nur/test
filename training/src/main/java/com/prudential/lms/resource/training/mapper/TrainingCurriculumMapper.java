package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Curriculum;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCurriculum;
import com.prudential.lms.resource.training.repository.CurriculumRepository;
import com.prudential.lms.resource.training.repository.TrainingCurriculumRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.shared.dto.training.TrainingCurriculumDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingCurriculumMapper implements EntityToDTOMapper<TrainingCurriculum, TrainingCurriculumDTO> {

	@Autowired
	private TrainingCurriculumRepository repo;

	@Autowired
	private CurriculumRepository curriculumRepo;

	@Autowired
	private TrainingRepository trainingRepo;

	public TrainingCurriculum createEntity(TrainingCurriculumDTO dto) {
		TrainingCurriculum entity = new TrainingCurriculum();
		Curriculum curriculum = curriculumRepo.findOne(dto.getCurriculumId());
		Training training = trainingRepo.findOne(dto.getTrainingId());

		entity.setTraining(training);
		entity.setCurriculum(curriculum);

		return entity;
	}

	public TrainingCurriculum updateEntity(TrainingCurriculumDTO dto) {
		TrainingCurriculum entity = repo.findOne(dto.getId());
		Curriculum updateCurriculum = curriculumRepo.findOne(dto.getCurriculumId());
		Training updateTraining = trainingRepo.findOne(dto.getTrainingId());
		entity.setTraining(updateTraining);
		entity.setCurriculum(updateCurriculum);

		return entity;
	}

	@Override
	public TrainingCurriculumDTO convertToDto(TrainingCurriculum entity) {
		TrainingCurriculumDTO dto = new TrainingCurriculumDTO();
		dto.setId(entity.getId());
		
		dto.setCurriculumId(entity.getCurriculum().getId());
		dto.setCurriculumCode(entity.getCurriculum().getCode());
		dto.setCurriculumName(entity.getCurriculum().getName());
		dto.setCurriculumDescription(entity.getCurriculum().getDescription());

		dto.setTrainingId(entity.getTraining().getId());
		dto.setTrainingCode(entity.getTraining().getCode());
		dto.setTrainingName(entity.getTraining().getName());
		dto.setTrainingDescription(entity.getTraining().getDescription());

		return dto;
	}

	@Override
	public List<TrainingCurriculumDTO> convertToDto(Iterable<TrainingCurriculum> entities) {
		List<TrainingCurriculumDTO> dtoList = new ArrayList<>();
		for (TrainingCurriculum entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingCurriculum convertToEntity(TrainingCurriculumDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingCurriculum> convertToEntity(Iterable<TrainingCurriculumDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
