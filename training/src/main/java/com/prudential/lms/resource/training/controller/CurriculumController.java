package com.prudential.lms.resource.training.controller;

import java.util.List;
import java.util.Set;

import static com.prudential.lms.shared.constant.ErrMsg.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.prudential.lms.resource.training.mapper.CurriculumMapper;
import com.prudential.lms.resource.training.mapper.TrainingCurriculumMapper;
import com.prudential.lms.resource.training.model.Curriculum;
import com.prudential.lms.resource.training.model.QCurriculum;
import com.prudential.lms.resource.training.model.QTrainingCurriculum;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCurriculum;
import com.prudential.lms.resource.training.repository.CurriculumRepository;
import com.prudential.lms.resource.training.repository.TrainingCurriculumRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.training.CurriculumDTO;
import com.prudential.lms.shared.dto.training.TrainingCurriculumDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/curriculum")
public class CurriculumController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CurriculumController.class);

	@Autowired
	private CurriculumRepository repo;

	@Autowired
	private TrainingCurriculumRepository trnCurRepo;

	@Autowired
	private TrainingCurriculumMapper trnCurMapper;

	@Autowired
	private TrainingRepository trnRepo;

	@Autowired
	private CurriculumMapper mapper;

	// List Paging pruhub without thumbnail
	@RequestMapping(value = "/list/pruhub/all", method = RequestMethod.POST)
	public Object listPruhubAll(@RequestBody(required = false) CurriculumDTO dto) {

		Response<Object> response = null;
		Iterable<Curriculum> curriculumPage = null;

		try {

			BooleanExpression predicate = QCurriculum.curriculum.deleted.isFalse();

			if (null != dto) {

				String name = dto.getName();
				if (StringUtils.isNotEmpty(name))
					predicate = predicate.and(QCurriculum.curriculum.name.likeIgnoreCase("%" + name + "%"));

				String description = dto.getDescription();
				if (StringUtils.isNotEmpty(description))
					predicate = predicate
							.and(QCurriculum.curriculum.description.likeIgnoreCase("%" + description + "%"));

				boolean draft = dto.isDraft();
				predicate = predicate.and(QCurriculum.curriculum.draft.eq(draft));
				

			}

			curriculumPage = repo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));

		} catch (Exception e) {
			LOGGER.error("#####ERROR List Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		List<CurriculumDTO> resultList = mapper.convertToDto(curriculumPage);
		for (CurriculumDTO curriculumDTO : resultList) {
			curriculumDTO.setThumbnail("");
		}

		response = resultList.size() > 0 ? Response.found(resultList) : Response.notFound();

		return response;
	}

	// remove training
	@RequestMapping(value = "/remove-training/{curiculumId}/{trainingId}", method = RequestMethod.POST)
	public Object removeTrainingByCuriculumId(@PathVariable("curiculumId") Long curiculumId,
			@PathVariable("trainingId") Long trainingId) {

		try {

			BooleanExpression predicate = QTrainingCurriculum.trainingCurriculum.id.gt(-1);
			predicate = predicate.and(QTrainingCurriculum.trainingCurriculum.curriculum.id.eq(curiculumId));
			predicate = predicate.and(QTrainingCurriculum.trainingCurriculum.training.id.eq(trainingId));

			TrainingCurriculum result = trnCurRepo.findOne(predicate);
			if (null == result)
				return Response.notFound("TrainingCuriculum not found");

			trnCurRepo.delete(result);

		} catch (Exception e) {
			LOGGER.error("#####ERROR Remove Training from Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

	// add training
	@RequestMapping(value = "/add-training/{curiculumId}/{trainingId}", method = RequestMethod.POST)
	public Object addTrainingByCuriculumId(@PathVariable("curiculumId") Long curiculumId,
			@PathVariable("trainingId") Long trainingId) {

		TrainingCurriculum result = null;

		Curriculum curiculum = repo.findOne(curiculumId);
		if (null == curiculum)
			return Response.notFound("Curiculum with id: " + curiculumId + " not found");

		Training training = trnRepo.findOne(trainingId);
		if (null == training)
			return Response.notFound("Training with id: " + trainingId + " not found");

		BooleanExpression predicate = QTrainingCurriculum.trainingCurriculum.id.eq(curiculumId);
		predicate = predicate.and(QTrainingCurriculum.trainingCurriculum.training.id.eq(trainingId));
		TrainingCurriculum currentExist = trnCurRepo.findOne(predicate);
		if (null != currentExist)
			return Response.badRequest("Training with id: " + trainingId + " already added to TrainingCurriculum");

		try {

			TrainingCurriculumDTO trnCurDto = new TrainingCurriculumDTO();
			trnCurDto.setCurriculumId(curiculumId);
			trnCurDto.setTrainingId(trainingId);

			result = trnCurRepo.save(trnCurMapper.createEntity(trnCurDto));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Adding Training to Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(trnCurMapper.convertToDto(result));
	}

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody CurriculumDTO dto) {
		Curriculum saved = null;

		String code = dto.getCode();
		if (code.isEmpty() || code.equals(null)) {
			return Response.badRequest("Curriculum code is required!");
		}
		
		Curriculum curriculum = repo.findByCode(code);
		if (null != curriculum)
			return Response.badRequest("Curriculum code: " + code + " already exist");

		try {
			saved = repo.save(mapper.createEntity(dto));
		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// List Paging pruhub without thumbnail
	@RequestMapping(value = "/list/pruhub/{page}/{size}", method = RequestMethod.POST)
	public Object listPruhub(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) CurriculumDTO dto) {

		Response<Object> response = null;
		Page<Curriculum> curriculumPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES);

		try {

			BooleanExpression predicate = QCurriculum.curriculum.deleted.isFalse();

			if (null != dto) {

				String name = dto.getName();
				if (StringUtils.isNotEmpty(name))
					predicate = predicate.and(QCurriculum.curriculum.name.likeIgnoreCase("%" + name + "%"));

				String description = dto.getDescription();
				if (StringUtils.isNotEmpty(description))
					predicate = predicate
							.and(QCurriculum.curriculum.description.likeIgnoreCase("%" + description + "%"));
				
				boolean draft = dto.isDraft();
				predicate = predicate.and(QCurriculum.curriculum.draft.eq(draft));

			}

			curriculumPage = repo.findAll(predicate, pr);

		} catch (Exception e) {
			LOGGER.error("#####ERROR Paging Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		List<CurriculumDTO> resultList = mapper.convertToDto(curriculumPage.getContent());
		for (CurriculumDTO curriculumDTO : resultList) {
			curriculumDTO.setThumbnail("");
		}

		Page<CurriculumDTO> pageResult = new PageImpl<CurriculumDTO>(resultList, pr, curriculumPage.getTotalElements());
		response = curriculumPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

		return response;
	}

	// List Paging
	@RequestMapping(value = "/list/{page}/{size}", method = RequestMethod.POST)
	public Object list(@PathVariable("page") int page, @PathVariable("size") int size,
			@RequestBody(required = false) CurriculumDTO dto) {

		Response<Object> response = null;
		Page<Curriculum> curriculumPage = null;
		PageRequest pr = new PageRequest(page, size, Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES);

		try {

			BooleanExpression predicate = QCurriculum.curriculum.deleted.isFalse();

			if (null != dto) {

				String name = dto.getName();
				if (StringUtils.isNotEmpty(name))
					predicate = predicate.and(QCurriculum.curriculum.name.likeIgnoreCase("%" + name + "%"));

				String description = dto.getDescription();
				if (StringUtils.isNotEmpty(description))
					predicate = predicate
							.and(QCurriculum.curriculum.description.likeIgnoreCase("%" + description + "%"));

				boolean draft = dto.isDraft();
				predicate = predicate.and(QCurriculum.curriculum.draft.eq(draft));

			}

			curriculumPage = repo.findAll(predicate, pr);

		} catch (Exception e) {
			LOGGER.error("#####ERROR Paging Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		Page<CurriculumDTO> pageResult = new PageImpl<CurriculumDTO>(mapper.convertToDto(curriculumPage.getContent()),
				pr, curriculumPage.getTotalElements());
		response = curriculumPage.getTotalElements() > 0 ? Response.found(pageResult) : Response.notFound();

		return response;
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findOne(@PathVariable("id") Long id) {
		Curriculum tc = repo.findOne(id);
		if (null == tc)
			return Response.notFound();

		return Response.found(mapper.convertToDto(tc));
	}

	// Find by sample
	// @RequestMapping(value = "/find", method = RequestMethod.GET)
	// public Object findByExample(@RequestBody CurriculumDTO example) {
	// Response<Object> response = null;
	// List<Curriculum> listCurriculum = null;
	// if (example != null) {
	// if (example.getCode() != null) {
	// listCurriculum = repo.findByCode(example.getCode());
	// } else if (example.getName() != null) {
	// listCurriculum = repo.findByName(example.getName());
	// } else if (example.getDescription() != null) {
	// listCurriculum = repo.findByDescription(example.getDescription());
	// }
	// response = listCurriculum.size() > 0 ?
	// Response.found(mapper.convertToDto(listCurriculum))
	// : Response.notFound();
	//
	// return response;
	// } else
	//
	// return Response.notFound();
	// }

	// Update
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
	public Object update(@PathVariable("id") Long id, @RequestBody CurriculumDTO dto) {

		Curriculum updatedEntity = null;
		Curriculum curriculum = repo.findOne(id);

		if (null == curriculum) {
			return Response.notFound();
		} else {

			try {
				updatedEntity = repo.save(mapper.updateEntity(dto));
			} catch (Exception e) {
				LOGGER.error("#####ERROR Updating Curriculum", e);
				return Response.internalServerError();
			}

		}

		return Response.ok(mapper.convertToDto(updatedEntity));
	}

	// Delete
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Object delete(@PathVariable("id") Long id) {
		Curriculum curriculum = repo.findOne(id);

		if (null == curriculum) {
			return Response.notFound("TrainingCuriculum with id: " + id + " not found");
		} else {
			if (curriculum.hasTrainingCurriculum()) {
				return Response.customError(CURRICULUM_DELETE_HAS_TRAINING_CODE, CURRICULUM_DELETE_HAS_TRAINING_MSG);
			}
		}

		try {
			trnCurRepo.delete(curriculum.getTrainings());
			repo.delete(curriculum);
		} catch (Exception e) {
			LOGGER.error("#####ERROR Deleting Curriculum", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.ok();
	}

	// Soft Delete
	@RequestMapping(value = "/softdelete/{del}/{id}", method = RequestMethod.PATCH)
	public Object softDelete(@PathVariable("del") int del, @PathVariable("id") Long id) {

		if (del != 0 || del != 1)
			return Response.badRequest("Only 0 & 1 are allowed for soft delete flag");

		boolean deleted = del == 0 ? true : false;

		Curriculum curriculum = repo.findOne(id);
		if (null == curriculum)
			return Response.notFound();

		try {
			processSoftDelete(curriculum, deleted);
		} catch (Exception e) {
			return Response.internalServerError();
		}

		return Response.ok();
	}

	private void processSoftDelete(Curriculum curriculum, boolean deleted) {
		try {

			curriculum.setDeleted(deleted);
			repo.save(curriculum);
			if (curriculum.hasTrainingCurriculum()) {
				Set<TrainingCurriculum> children = curriculum.getTrainings();
				for (TrainingCurriculum child : children) {
					processSoftDelete(child.getCurriculum(), deleted);
				}
			}

		} catch (Exception e) {
			LOGGER.error("#####ERROR Soft-Deleting TrainingCategory", e);
		}

	}
}
