package com.prudential.lms.resource.training.service;

import com.prudential.lms.resource.training.model.TrainingRegistrationSchedule;

public interface TrainingProgressService {

	void register(TrainingRegistrationSchedule regSch);
	
	void begin(TrainingRegistrationSchedule regSch);
	
	void pretestStart(TrainingRegistrationSchedule regSch);
	
	void pretestFinish(TrainingRegistrationSchedule regSch);
	
	void trainingStart(TrainingRegistrationSchedule regSch);
	
	void trainingFinish(TrainingRegistrationSchedule regSch);
	
	void posttestStart(TrainingRegistrationSchedule regSch);
	
	void posttestFinish(TrainingRegistrationSchedule regSch);
	
	void end(TrainingRegistrationSchedule regSch);
	
}
