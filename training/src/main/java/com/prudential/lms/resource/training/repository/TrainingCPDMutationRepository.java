package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.shared.constant.CPDMutationOperation;

public interface TrainingCPDMutationRepository
		extends JpaRepository<TrainingCPDMutation, Long>, QueryDslPredicateExecutor<TrainingCPDMutation> {

//	TrainingCPDMutation findByTrainingCPDId(Long trainingCPDId);

	List<TrainingCPDMutation> findByPrevPoint(int prevPoint);

	List<TrainingCPDMutation> findByCurrentPoint(int currentPoint);

	List<TrainingCPDMutation> findByDeltaPoint(int deltaPoint);

	List<TrainingCPDMutation> findByMutationOperation(CPDMutationOperation mutationOperation);
}
