package com.prudential.lms.resource.training.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prudential.lms.resource.training.model.TrainingCPD;
import com.prudential.lms.resource.training.model.TrainingCPDMutation;
import com.prudential.lms.resource.training.repository.TrainingCPDMutationRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.resource.training.service.TrainingCPDMutationService;
import com.prudential.lms.shared.constant.CPDMutationOperation;

@Service
@Transactional
public class TrainingCPDMutationServiceImpl implements TrainingCPDMutationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrainingCPDMutationServiceImpl.class);
	
	@Autowired
	private TrainingCPDRepository trainingCPDRepo;
	
	@Autowired
	private TrainingCPDMutationRepository trainingCPDMutationRepo;
	
	@Override
	public TrainingCPDMutation plusCPD(Long trainingCPDId, int point) throws Exception{
		
		LOGGER.debug("Adding CPD ID: "+ trainingCPDId); 
		TrainingCPDMutation result = null;
		TrainingCPD cpd = trainingCPDRepo.findOne(trainingCPDId);
		
		if(null == cpd)
			throw new Exception("Training CPD ID not found");
		
		int prevPoint = cpd.getPoint();
		int currentPoint = prevPoint + point;
		TrainingCPDMutation mutation = new TrainingCPDMutation();
//		mutation.setTrainingCPDId(trainingCPDId);
		mutation.setCurrentPoint(currentPoint);
		mutation.setPrevPoint(prevPoint);
		mutation.setDeltaPoint(point);
		mutation.setMutationOperation(CPDMutationOperation.PLUS);
		
		try {
			result = trainingCPDMutationRepo.save(mutation);
			
			cpd.setPoint(result.getCurrentPoint());
			trainingCPDRepo.save(cpd);
		} catch (Exception e) {
			LOGGER.error("Fail adding CPD point", e);
		}
		
		return result;
	}

	@Override
	public TrainingCPDMutation minusCPD(Long trainingCPDId, int point) throws Exception{
		
		LOGGER.debug("Subtracting CPD ID: "+ trainingCPDId);
		TrainingCPDMutation result = null;
		TrainingCPD cpd = trainingCPDRepo.findOne(trainingCPDId);
		if(null == cpd)
			throw new Exception("Training CPD ID not found");
		
		int prevPoint = cpd.getPoint();
		int currentPoint = prevPoint - point;
		TrainingCPDMutation mutation = new TrainingCPDMutation();
//		mutation.setTrainingCPDId(trainingCPDId);
		mutation.setCurrentPoint(currentPoint);
		mutation.setPrevPoint(prevPoint);
		mutation.setDeltaPoint(point);
		mutation.setMutationOperation(CPDMutationOperation.MINUS);
		
		try {
			result = trainingCPDMutationRepo.save(mutation);
			
			cpd.setPoint(result.getCurrentPoint());
			trainingCPDRepo.save(cpd);
		} catch (Exception e) {
			LOGGER.error("Fail subtracting CPD point", e);
		}
		
		return result;
	}

}
