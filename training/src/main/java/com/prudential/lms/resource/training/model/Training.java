package com.prudential.lms.resource.training.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.prudential.lms.shared.constant.ContentType;
import com.prudential.lms.shared.constant.TrainingStatus;
import com.prudential.lms.shared.constant.TrainingType;
import com.prudential.lms.shared.model.AuditableEntityBase;
import com.prudential.lms.shared.model.EntityBase;
import com.prudential.lms.shared.model.SoftDelete;

@Entity
@Table(name = "lms_training")
public class Training extends AuditableEntityBase implements EntityBase<Long>, SoftDelete {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8117571720630533568L;
	private static final String SEQ_GEN = "seq_gen_training";
	private static final String SEQ = "seq_training";

	private Long id;
	private String code;
	private String name;
	private String description;
	private TrainingCategory category;
	private List<TrainingCurriculum> curriculums = new ArrayList<>();
	private boolean deleted;
	private boolean refreshment;
	private TrainingType trainingType;
	private TrainingStatus status;
	private String fileName;
	private String thumbnail;
	private int cpdPoint;
	private ContentType trainingContentType;
	private boolean draft;
	private boolean preEqualPost;

	// Training PRE & POST Test
	private List<TrainingTest> trainingTests = new ArrayList<>();

	// Training Requirements
	private int minAgentAge;
	private int minCasePoint;
	private BigDecimal minAPIPoint;
	private boolean agentUnderReview;
	private List<AgentTypeRequirement> agentTypeReqs = new ArrayList<>();
	private List<PreRequiredTraining> preReqTrainings = new ArrayList<>();

	// Training Contents
	private List<TrainingContent> contents = new ArrayList<>();
	
	//Training Schedule
	private List<TrainingSchedule> schedules = new ArrayList<>();

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(final Long id) {
		this.id = id;
	}

	@Column(length = 32, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "training_ctgr_id")
	public TrainingCategory getCategory() {
		return category;
	}

	public void setCategory(TrainingCategory category) {
		this.category = category;
	}

	@Type(type = "yes_no")
	@Override
	public boolean isDeleted() {
		return deleted;
	}

	@Override
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@OneToMany(mappedBy = "training")
	public List<TrainingCurriculum> getCurriculums() {
		return curriculums;
	}

	public void setCurriculums(List<TrainingCurriculum> curriculums) {
		this.curriculums = curriculums;
	}

	@Type(type = "yes_no")
	public boolean isRefreshment() {
		return refreshment;
	}

	public void setRefreshment(boolean refreshment) {
		this.refreshment = refreshment;
	}

	@Enumerated(EnumType.STRING)
	public TrainingType getTrainingType() {
		return trainingType;
	}

	public void setTrainingType(TrainingType trainingType) {
		this.trainingType = trainingType;
	}

	@Enumerated(EnumType.STRING)
	public TrainingStatus getStatus() {
		return status;
	}

	public void setStatus(TrainingStatus status) {
		this.status = status;
	}

	@OneToMany(mappedBy = "training")
	public List<TrainingContent> getContents() {
		return contents;
	}

	public void setContents(List<TrainingContent> contents) {
		this.contents = contents;
	}

	@Transient
	public void addContents(TrainingContent... contents) {
		for (TrainingContent content : contents) {
			this.contents.add(content);
		}
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public int getCpdPoint() {
		return cpdPoint;
	}

	public void setCpdPoint(int cpdPoint) {
		this.cpdPoint = cpdPoint;
	}

	public int getMinAgentAge() {
		return minAgentAge;
	}

	public void setMinAgentAge(int minAgentAge) {
		this.minAgentAge = minAgentAge;
	}

	public int getMinCasePoint() {
		return minCasePoint;
	}

	public void setMinCasePoint(int minCasePoint) {
		this.minCasePoint = minCasePoint;
	}

	@Column(name = "min_api_point")
	public BigDecimal getMinAPIPoint() {
		return minAPIPoint;
	}

	public void setMinAPIPoint(BigDecimal minAPIPoint) {
		this.minAPIPoint = minAPIPoint;
	}

	@Type(type = "yes_no")
	public boolean isAgentUnderReview() {
		return agentUnderReview;
	}

	public void setAgentUnderReview(boolean agentUnderReview) {
		this.agentUnderReview = agentUnderReview;
	}

	@ElementCollection
	@CollectionTable(name = "lms_trn_agent_type_req", joinColumns = { @JoinColumn(name = "training_id") })
	@OrderColumn(name = "req_id")
	public List<AgentTypeRequirement> getAgentTypeReqs() {
		return agentTypeReqs;
	}

	public void setAgentTypeReqs(List<AgentTypeRequirement> agentTypeReqs) {
		this.agentTypeReqs = agentTypeReqs;
	}

	@Transient
	public void addAgentTypeReqs(AgentTypeRequirement... agentTypeRequirements) {
		for (AgentTypeRequirement agentTypeRequirement : agentTypeRequirements) {
			getAgentTypeReqs().add(agentTypeRequirement);
		}
	}

	@ElementCollection
	@CollectionTable(name = "lms_trn_pre_req", joinColumns = { @JoinColumn(name = "training_id") })
	@OrderColumn(name = "pre_req_id")
	public List<PreRequiredTraining> getPreReqTrainings() {
		return preReqTrainings;
	}

	public void setPreReqTrainings(List<PreRequiredTraining> preReqTrainings) {
		this.preReqTrainings = preReqTrainings;
	}

	@Transient
	public void addPreReqTrainings(PreRequiredTraining... preRequiredTrainings) {
		for (PreRequiredTraining preRequiredTraining : preRequiredTrainings) {
			getPreReqTrainings().add(preRequiredTraining);
		}
	}

	@Enumerated(EnumType.STRING)
	public ContentType getTrainingContentType() {
		return trainingContentType;
	}

	public void setTrainingContentType(ContentType trainingContentType) {
		this.trainingContentType = trainingContentType;
	}

	@OneToMany(mappedBy = "training")
	public List<TrainingTest> getTrainingTests() {
		return trainingTests;
	}

	public void setTrainingTests(List<TrainingTest> trainingTests) {
		this.trainingTests = trainingTests;
	}

	@Type(type = "yes_no")
	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}

	@Type(type = "yes_no")
	public boolean isPreEqualPost() {
		return preEqualPost;
	}

	public void setPreEqualPost(boolean preEqualPost) {
		this.preEqualPost = preEqualPost;
	}

	@OneToMany(mappedBy = "training")
	public List<TrainingSchedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<TrainingSchedule> schedules) {
		this.schedules = schedules;
	} 
	
	

}
