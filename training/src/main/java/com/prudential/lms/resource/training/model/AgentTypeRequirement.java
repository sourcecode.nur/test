package com.prudential.lms.resource.training.model;

import javax.persistence.Embeddable;

@Embeddable
public class AgentTypeRequirement {
	
	private String agentType;

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

}
