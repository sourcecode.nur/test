package com.prudential.lms.resource.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.TrainingCategory;
import com.prudential.lms.shared.constant.TrainingCategoryStatus;

public interface TrainingCategoryRepository
		extends JpaRepository<TrainingCategory, Long>, QueryDslPredicateExecutor<TrainingCategory> {

	List<TrainingCategory> findByCode(String code);

	List<TrainingCategory> findByName(String name);

	List<TrainingCategory> findByDescription(String description);

	List<TrainingCategory> findByParent(TrainingCategory parent);

	List<TrainingCategory> findByStatus(TrainingCategoryStatus status);

}
