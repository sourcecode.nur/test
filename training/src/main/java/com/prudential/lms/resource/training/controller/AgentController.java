package com.prudential.lms.resource.training.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Iterables;
import com.prudential.lms.resource.training.mapper.AgentMapper;
import com.prudential.lms.resource.training.model.Agent;
import com.prudential.lms.resource.training.model.QAgent;
import com.prudential.lms.resource.training.model.TrainingRegistration;
import com.prudential.lms.resource.training.repository.AgentRepository;
import com.prudential.lms.resource.training.repository.TrainingCPDRepository;
import com.prudential.lms.resource.training.repository.TrainingRegistrationRepository;
import com.prudential.lms.shared.constant.AppConstants;
import com.prudential.lms.shared.dto.training.AgentDTO;
import com.prudential.lms.shared.message.Response;
import com.querydsl.core.types.dsl.BooleanExpression;

@RestController
@RequestMapping("/agent")
public class AgentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgentController.class);

	@Autowired
	private AgentRepository repo;

	@Autowired
	private TrainingCPDRepository trainingCPDRepo;

	@Autowired
	private TrainingRegistrationRepository trainingRegRepo;

	@Autowired
	private AgentMapper mapper;

	// Create
	@RequestMapping(value = "", method = RequestMethod.POST)
	public Object create(@RequestBody AgentDTO dto) {
		Agent saved = null;

		String code = dto.getCode();
		if (StringUtils.isEmpty(code))
			Response.badRequest("Agent Code not provided");

		try {

			Agent currentAgent = repo.findByCode(code);
			if (null != currentAgent) {

				saved = repo.save(mapper.updateEntity(dto));
				return Response.ok(mapper.convertToDto(saved));

			} else {

				saved = repo.save(mapper.createEntity(dto));

				if (null != saved) {
					LOGGER.debug("Creating TrainingRegistration for Agent " + saved.getCode());
					TrainingRegistration treg = new TrainingRegistration();
					treg.setAgent(saved);
					trainingRegRepo.save(treg);
					LOGGER.debug("TrainingRegistration created for Agent " + saved.getCode());
				}

			}

		} catch (Exception e) {
			LOGGER.error("#####ERROR Saving Agent", e);
			return Response.internalServerError(e.getMessage());
		}

		return Response.created(mapper.convertToDto(saved));
	}

	// Find By Id
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Object findAgentById(@PathVariable("id") Long id) {
		Agent agent = repo.findOne(id);
		if (null == agent)
			return Response.notFound("Agent with id: " + id + " not found");

		return Response.found(mapper.convertToDto(agent));
	}

	// Find By Code
	@RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
	public Object findAgentByCode(@PathVariable("code") String code) {
		Agent agent = repo.findByCode(code);
		if (null == agent)
			return Response.notFound("Agent with code: " + code + " not found");

		return Response.found(mapper.convertToDto(agent));
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Object list(@RequestBody(required = false) AgentDTO dto) {

		Response<Object> response = null;
		Iterable<Agent> listAgents = null;

		try {
			BooleanExpression predicate = QAgent.agent.id.gt(-1);

			if (null != dto) {

				String code = dto.getCode();
				if (StringUtils.isNotEmpty(code))
					predicate = predicate.and(QAgent.agent.code.likeIgnoreCase("%" + code + "%"));

				String name = dto.getName();
				if (StringUtils.isNotEmpty(name))
					predicate = predicate.and(QAgent.agent.name.likeIgnoreCase("%" + name + "%"));

				String levelType = dto.getLevelType();
				if (StringUtils.isNotEmpty(levelType))
					predicate = predicate.and(QAgent.agent.levelType.likeIgnoreCase("%" + levelType + "%"));

				String officeCode = dto.getOfficeCode();
				if (StringUtils.isNotEmpty(officeCode))
					predicate = predicate.and(QAgent.agent.officeCode.likeIgnoreCase("%" + officeCode + "%"));

				String officeName = dto.getOfficeName();
				if (StringUtils.isNotEmpty(officeName))
					predicate = predicate.and(QAgent.agent.officeName.likeIgnoreCase("%" + officeName + "%"));

				String radd = dto.getRadd();
				if (StringUtils.isNotEmpty(radd))
					predicate = predicate.and(QAgent.agent.radd.likeIgnoreCase("%" + radd + "%"));

				String ads = dto.getAds();
				if (StringUtils.isNotEmpty(ads))
					predicate = predicate.and(QAgent.agent.ads.likeIgnoreCase("%" + ads + "%"));

			}

			listAgents = repo.findAll(predicate, new Sort(Direction.DESC, AppConstants.PAGE_SORT_PROPERTIES));

		} catch (Exception e) {
			LOGGER.error("#####ERROR Listing Agent", e);
			return Response.internalServerError(e.getMessage());
		}

		response = Iterables.size(listAgents) > 0 ? Response.found(mapper.convertToDto(listAgents))
				: Response.notFound();

		return response;
	}

}
