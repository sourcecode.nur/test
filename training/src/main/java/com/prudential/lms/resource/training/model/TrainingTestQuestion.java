package com.prudential.lms.resource.training.model;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.prudential.lms.shared.constant.CorrectAnswer;
import com.prudential.lms.shared.constant.QuestionDifficulty;

@Embeddable
public class TrainingTestQuestion {

	private Long quizId;
	private Long questionId;
	private int weight;
	private QuestionDifficulty difficulty;
	private String questionText;
	private String answerA;
	private String answerB;
	private String answerC;
	private String answerD;
	private CorrectAnswer correctAnswer;
	private CorrectAnswer userAnswer;
	private boolean correct;

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

	public Long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Long questionId) {
		this.questionId = questionId;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Enumerated(EnumType.STRING)
	public QuestionDifficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(QuestionDifficulty difficulty) {
		this.difficulty = difficulty;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getAnswerA() {
		return answerA;
	}

	public void setAnswerA(String answerA) {
		this.answerA = answerA;
	}

	public String getAnswerB() {
		return answerB;
	}

	public void setAnswerB(String answerB) {
		this.answerB = answerB;
	}

	public String getAnswerC() {
		return answerC;
	}

	public void setAnswerC(String answerC) {
		this.answerC = answerC;
	}

	public String getAnswerD() {
		return answerD;
	}

	public void setAnswerD(String answerD) {
		this.answerD = answerD;
	}

	@Enumerated(EnumType.STRING)
	public CorrectAnswer getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(CorrectAnswer correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	@Enumerated(EnumType.STRING)
	public CorrectAnswer getUserAnswer() {
		return userAnswer;
	}

	public void setUserAnswer(CorrectAnswer userAnswer) {
		this.userAnswer = userAnswer;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}

}
