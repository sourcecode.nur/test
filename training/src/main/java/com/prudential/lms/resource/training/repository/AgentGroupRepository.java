package com.prudential.lms.resource.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.prudential.lms.resource.training.model.AgentGroup;

public interface AgentGroupRepository extends JpaRepository<AgentGroup, Long>, QueryDslPredicateExecutor<AgentGroup>{

	AgentGroup findByGroup (String group);
}
