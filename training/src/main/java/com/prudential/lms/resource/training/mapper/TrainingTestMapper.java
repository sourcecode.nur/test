package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingTest;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.repository.TrainingTestRepository;
import com.prudential.lms.shared.dto.training.TrainingTestDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingTestMapper implements EntityToDTOMapper<TrainingTest, TrainingTestDTO> {
	
	
	@Autowired
	private TrainingTestRepository repo;
	
	@Autowired
	private TrainingRepository trnRepo;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public TrainingTest createEntity(TrainingTestDTO dto) {
		
		TrainingTest entity = new TrainingTest();
		entity.setType(dto.getType());
		entity.setQuestionCount(dto.getQuestionCount());
		entity.setEasyCount(dto.getEasyCount());
		entity.setMediumCount(dto.getMediumCount());
		entity.setHardCount(dto.getHardCount());
		entity.setMinimumPoint(dto.getMinimumPoint());
		entity.setDuration(dto.getDuration());
		entity.setMaxRemedialCount(dto.getMaxRemedialCount());
		entity.setQuizId(dto.getQuizId());
		
		Long trainingId = dto.getTrainingId();
		if(null != trainingId) {
			Training training = trnRepo.findOne(trainingId);
			entity.setTraining(training);
		}
		
		return entity;
	}

	@Override
	public TrainingTest updateEntity(TrainingTestDTO dto) {
		
		TrainingTest entity = repo.findOne(dto.getId());
		entity.setType(dto.getType());
		entity.setQuestionCount(dto.getQuestionCount());
		entity.setEasyCount(dto.getEasyCount());
		entity.setMediumCount(dto.getMediumCount());
		entity.setHardCount(dto.getHardCount());
		entity.setMinimumPoint(dto.getMinimumPoint());
		entity.setDuration(dto.getDuration());
		entity.setMaxRemedialCount(dto.getMaxRemedialCount());
		entity.setQuizId(dto.getQuizId());
		
		Long trainingId = dto.getTrainingId();
		if(null != trainingId) {
			Training training = trnRepo.findOne(trainingId);
			entity.setTraining(training);
		}
		
		return entity;
	}

	@Override
	public TrainingTestDTO convertToDto(TrainingTest entity) {
		
		TrainingTestDTO dto = mapper.map(entity, TrainingTestDTO.class);
		
		Training training = entity.getTraining();
		if(null != training)
			dto.setTrainingId(training.getId());
		
		return dto;
	}

	@Override
	public List<TrainingTestDTO> convertToDto(Iterable<TrainingTest> entities) {
		List<TrainingTestDTO> dtoList = new ArrayList<>();
		for (TrainingTest entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public TrainingTest convertToEntity(TrainingTestDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrainingTest> convertToEntity(Iterable<TrainingTestDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
