package com.prudential.lms.resource.training.mapper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.prudential.lms.resource.training.model.AgentTypeRequirement;
import com.prudential.lms.resource.training.model.PreRequiredTraining;
import com.prudential.lms.resource.training.model.QTrainingSchedule;
import com.prudential.lms.resource.training.model.Training;
import com.prudential.lms.resource.training.model.TrainingCategory;
import com.prudential.lms.resource.training.model.TrainingContent;
import com.prudential.lms.resource.training.model.TrainingSchedule;
import com.prudential.lms.resource.training.model.TrainingTest;
import com.prudential.lms.resource.training.repository.TrainingCategoryRepository;
import com.prudential.lms.resource.training.repository.TrainingRepository;
import com.prudential.lms.resource.training.repository.TrainingScheduleRepository;
import com.prudential.lms.shared.constant.TrainingStatus;
import com.prudential.lms.shared.dto.training.AgentTypeRequirementDTO;
import com.prudential.lms.shared.dto.training.PreRequiredTrainingDTO;
import com.prudential.lms.shared.dto.training.TrainingDTO;
import com.prudential.lms.shared.mapper.EntityToDTOMapper;

@Component
public class TrainingMapper implements EntityToDTOMapper<Training, TrainingDTO> {

	@Autowired
	private TrainingRepository repo;

	@Autowired
	private TrainingCategoryRepository trainingCategoryRepo;

	@Autowired
	private TrainingScheduleRepository trnSchRepo;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private TrainingTestMapper trnTestMapper;

	@Autowired
	private TrainingContentMapper trnCtnMapper;

	public Training createEntity(TrainingDTO dto) {
		Training entity = new Training();
				
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		
		if(null != dto.getTrainingCategoryId()) {
			TrainingCategory trainingCategory = trainingCategoryRepo.findOne(dto.getTrainingCategoryId());
			entity.setCategory(trainingCategory);
		}		
		entity.setRefreshment(dto.isRefreshment());
		entity.setTrainingType(dto.getTrainingType());
		entity.setStatus(null != dto.getStatus() ? dto.getStatus() : TrainingStatus.REQUEST);
		entity.setFileName(dto.getFileName());
		entity.setThumbnail(dto.getThumbnail());
		entity.setCpdPoint(dto.getCpdPoint());
		entity.setTrainingContentType(dto.getTrainingContentType());
		entity.setDraft(dto.isDraft());
		entity.setPreEqualPost(dto.isPreEqualPost());

		// Training Requirements
		entity.setMinAgentAge(dto.getMinAgentAge());
		entity.setMinCasePoint(dto.getMinCasePoint());
		entity.setMinAPIPoint(dto.getMinAPIPoint());
		entity.setAgentUnderReview(dto.isAgentUnderReview());
		List<AgentTypeRequirementDTO> agentTypeReqs = dto.getAgentTypeReqs();
		for (AgentTypeRequirementDTO atrDTO : agentTypeReqs) {
			entity.addAgentTypeReqs(mapper.map(atrDTO, new AgentTypeRequirement().getClass()));
		}
		List<PreRequiredTrainingDTO> preReqTrainings = dto.getPreReqTrainings();
		for (PreRequiredTrainingDTO prDto : preReqTrainings) {
			entity.addPreReqTrainings(mapper.map(prDto, new PreRequiredTraining().getClass()));
		}

		return entity;
	}

	public Training updateEntity(TrainingDTO dto) {
		Training entity = repo.findOne(dto.getId()); 
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		if(null != dto.getTrainingCategoryId()) {
			TrainingCategory trainingCategory = trainingCategoryRepo.findOne(dto.getTrainingCategoryId());
			entity.setCategory(trainingCategory);
		}
		entity.setRefreshment(dto.isRefreshment());
		entity.setTrainingType(dto.getTrainingType());
		entity.setStatus(null != dto.getStatus() ? dto.getStatus() : TrainingStatus.REQUEST);
		entity.setFileName(dto.getFileName());
		entity.setThumbnail(dto.getThumbnail());
		entity.setCpdPoint(dto.getCpdPoint());
		entity.setTrainingContentType(dto.getTrainingContentType());
		entity.setDraft(dto.isDraft());
		entity.setPreEqualPost(dto.isPreEqualPost());

		// Training Requirements
		entity.setMinAgentAge(dto.getMinAgentAge());
		entity.setMinCasePoint(dto.getMinCasePoint());
		entity.setMinAPIPoint(dto.getMinAPIPoint());
		entity.setAgentUnderReview(dto.isAgentUnderReview());

		List<AgentTypeRequirementDTO> agentTypeReqs = dto.getAgentTypeReqs();
		if(agentTypeReqs == null) {
			agentTypeReqs = new ArrayList<>();
		}
		
		//if (agentTypeReqs.size() > 0) {
			entity.getAgentTypeReqs().clear();
			for (AgentTypeRequirementDTO atrDTO : agentTypeReqs) {
				entity.addAgentTypeReqs(mapper.map(atrDTO, new AgentTypeRequirement().getClass()));
			}
		//}

		List<PreRequiredTrainingDTO> preReqTrainings = dto.getPreReqTrainings();
		if(preReqTrainings == null) {
			preReqTrainings = new ArrayList<>();
		}
		//if (preReqTrainings.size() > 0) {
			entity.getPreReqTrainings().clear();
			for (PreRequiredTrainingDTO atrDTO : preReqTrainings) {
				entity.addPreReqTrainings(mapper.map(atrDTO, new PreRequiredTraining().getClass()));
			}
		//}

		return entity;
	}

	@Override
	public TrainingDTO convertToDto(Training entity) {
		TrainingDTO dto = new TrainingDTO();
		dto.setId(entity.getId());
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		dto.setDescription(entity.getDescription());
		dto.setRefreshment(entity.isRefreshment());
		dto.setTrainingType(entity.getTrainingType());
		dto.setStatus(entity.getStatus());
		dto.setFileName(entity.getFileName());
		dto.setThumbnail(entity.getThumbnail());
		dto.setCpdPoint(entity.getCpdPoint());
		dto.setTrainingContentType(entity.getTrainingContentType());
		dto.setDraft(entity.isDraft());
		dto.setPreEqualPost(entity.isPreEqualPost());

		// Training Requirements
		dto.setMinAgentAge(entity.getMinAgentAge());
		dto.setMinCasePoint(entity.getMinCasePoint());
		dto.setMinAPIPoint(entity.getMinAPIPoint());
		dto.setAgentUnderReview(entity.isAgentUnderReview());

		List<AgentTypeRequirement> agentTypeReqs = entity.getAgentTypeReqs();
		List<AgentTypeRequirementDTO> agTyResDTOList = new ArrayList<AgentTypeRequirementDTO>();
		for (AgentTypeRequirement atr : agentTypeReqs) {
			agTyResDTOList.add(mapper.map(atr, AgentTypeRequirementDTO.class));
		}
		dto.setAgentTypeReqs(agTyResDTOList);

		List<PreRequiredTraining> preReqTrainings = entity.getPreReqTrainings();
		List<PreRequiredTrainingDTO> preReqTrnDTOList = new ArrayList<PreRequiredTrainingDTO>();
		for (PreRequiredTraining prt : preReqTrainings) {
			preReqTrnDTOList.add(mapper.map(prt, PreRequiredTrainingDTO.class));
		}
		dto.setPreReqTrainings(preReqTrnDTOList);

		TrainingCategory category = entity.getCategory();
		if (null != category) {
			dto.setTrainingCategoryId(category.getId());
			dto.setTrainingCategoryCode(category.getCode());
			dto.setTrainingCategoryName(category.getName());
			dto.setTraingCategoryDescription(category.getDescription());
		}

		List<TrainingTest> trainingTests = entity.getTrainingTests();
		if (null != trainingTests)
			dto.setTrainingTests(trnTestMapper.convertToDto(trainingTests));

		List<TrainingContent> contents = entity.getContents();
		if (null != contents) {
			dto.setContents(trnCtnMapper.convertToDto(contents));
		}

		Iterable<TrainingSchedule> schedules = trnSchRepo.findAll(QTrainingSchedule.trainingSchedule.training.id.eq(entity.getId()));
		dto.setActive(Iterables.size(schedules) > 0);

		// Auditable
		dto.setCreatedBy(entity.getCreatedBy());
		dto.setCreatedDate(entity.getCreatedDate());
		dto.setUpdatedBy(entity.getUpdatedBy());
		dto.setUpdatedDate(entity.getUpdatedDate());
		dto.setVersion(entity.getVersion());

		return dto;
	}

	@Override
	public List<TrainingDTO> convertToDto(Iterable<Training> entities) {
		List<TrainingDTO> dtoList = new ArrayList<>();
		for (Training entity : entities) {
			dtoList.add(convertToDto(entity));
		}
		return dtoList;
	}

	@Override
	public Training convertToEntity(TrainingDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Training> convertToEntity(Iterable<TrainingDTO> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
